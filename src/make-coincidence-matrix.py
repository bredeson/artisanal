#!/usr/bin/env python

import os
import sys
import gzip
import numpy
import getopt
import intervals
import collections

from fileutil import zopen

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Generate gene-LG coincidence table'

out = sys.stdout
err = sys.stderr

# 1. Read BEDPE files of individual genes or collinear gene runs
#
# 2. Use BEDPE to query (cols 1-3) chromosome names of genes (cols 4-6) in target species
#
# 3. Build matrix

num = len

_eol = '\n'
_tab = '\t'
_null = '.'
_empty = ''
_comment = '#'


class BED(intervals.BaseInterval):
    def __init__(self, chr=_null, beg=-1, end=-1, id=_null, score=0, strand=_null):
        intervals.BaseInterval.__init__(self, chr, beg, end)
        self.id = id
        self.score = score
        self.strand = strand

    @property
    def strand(self):
        return self._strand

    @strand.setter
    def strand(self, strand):
        self._strand = intervals.Strand(strand)
        
    @property
    def mid(self):
        return float(self.beg + self.end) / 2.0

    @property
    def chr(self):
        return self.name

    @chr.setter
    def chr(self, name):
        self.name = name


class BEDPE(object):
    def __init__(self, trg, qry, name=_null, score=0.0, index=None, num_members=0):
        self.trg = trg
        self.qry = qry
        self.name = name
        self.score = score
        self.index = index
        self.num_members = num_members

    @property
    def strand(self):
        return intervals.Strand(self.trg.strand.int * self.qry.strand.int)
        
    def __str__(self):
        return "%s\t%d\t%d\t%s\t%d\t%d\t%s\t%.04f\t+\t%s" % (
            self.trg.chr,
            self.trg.beg,
            self.trg.end,
            self.qry.chr,
            self.qry.beg,
            self.qry.end,
            self.name,
            self.score,
            self.strand
        )

    def __eq__(self, other):
        return \
            self.trg == other.trg and \
            self.qry == other.qry and \
            self.name == other.name and \
            self.score == other.score

    __repr__ = __str__

    

def sign(value):
    nvalue = float(value if value else 0)
    dvalue = float(value if value else 1)
    return int(nvalue / abs(dvalue))


def _record_qry_pos(record):
    return (record.qry.chr, record.qry.beg, record.qry.end)


def _record_trg_pos(record):
    return (record.trg.chr, record.trg.beg, record.trg.end)


def _record_trg_idx(record):
    return (record.trg, record.index[0])


def _record_qry_idx(record):
    return (record.qry, record.index[1])

    
def _record_index_trg_pos(record):
    return (record.index, record.trg.chr, record.trg.beg, record.trg.end)


def _cluster_infer_strand(cluster, key=lambda x: (x, -1)):
    record0, idx0 = key(cluster[0])
    recordN, idxN = key(cluster[-1])
    if num(cluster) > 1:
        strand = sign(idxN - idx0)
        if not strand:
            strand = sign(recordN.end - record0.beg)
        return strand            
    else:
        return record0.strand.int
    

def _cluster_num_members(cluster):
    return max(num(cluster), cluster[0].num_members)

    
def cluster_classified_records(records):
    prev = None
    clusters = []
    for curr in sorted(records, key=_record_index_trg_pos):
        if prev is None:
            clusters.append([curr])
        elif curr.index[0] == prev.index[0]:
            clusters[-1].append(curr)
        else:
            clusters.append([curr])
        
        prev = curr

    return clusters



def collapse_clusters(clusters):
    collapsed_clusters = []
    for cluster in clusters:
        cluster.sort(key=_record_trg_pos)
        trg_strand  = _cluster_infer_strand(cluster, key=_record_trg_idx)
        qry_strand  = _cluster_infer_strand(cluster, key=_record_qry_idx)
        num_members = _cluster_num_members(cluster)
        
        if trg_strand < 0 and qry_strand < 0:
            trg_strand = +1
            qry_strand = +1
            
        collapsed_clusters.append(
            [
                BEDPE(
                    BED(cluster[0].trg.chr,
                        min(map(lambda c: c.trg.beg, cluster)),
                        max(map(lambda c: c.trg.end, cluster)),
                        trg_strand
                    ),
                    BED(cluster[0].qry.chr,
                        min(map(lambda c: c.qry.beg, cluster)),
                        max(map(lambda c: c.qry.end, cluster)),
                        qry_strand
                    ),
                    name=_null,
                    score=sum(map(lambda c: c.score, cluster)) / num_members,
                    num_members=num_members
                )
            ]
        )
    return collapsed_clusters


def read_bedpe(bedpe_filename, collapse=False):
    bedpe_file = open(bedpe_filename)

    clusters = []
    for line in bedpe_file:
        line = line.strip()

        if line is _empty or \
           line.startswith(_comment):
            continue

        field = line.split(_tab)
        
        if num(field) != 10 and \
           num(field) != 12:
            err.write("ERROR: BEDPE: Too few fields (=%d), 10 or 12 expected\n" % num(field))
            sys.exit(1)

        try:
            for i in range(2):
                chr = 3*i+0
                beg = 3*i+1
                end = 3*i+2
                field[beg] = int(field[beg])
                field[end] = int(field[end])
                assert field[beg] <= field[end]
                
        except IndexError:
            err.write("ERROR: BEDPE: Too few fields (=%d)\n" % num(field))
            sys.exit(1)
        except:
            err.write("ERROR: BEDPE: Invalid record: '%s'\n" % line)
            sys.exit(1)

        member = BEDPE(
            BED(field[0],
                field[1],
                field[2],
                field[8]
            ),
            BED(field[3],
                field[4],
                field[5],
                field[9]
            )
        )
        if num(field) == 12:
            member.index = [int(field[10]), int(field[10])]
            member.num_members = int(field[11])
            collapse = True
        
        if member.trg.chr is _null or member.trg.beg < 0 or member.trg.end < 0 or \
           member.qry.chr is _null or member.qry.beg < 0 or member.qry.end < 0:           
            continue

        clusters.append(member)

    if collapse:
        clusters = cluster_classified_records(clusters)
        clusters = map(lambda c: c[0], collapse_clusters(clusters))

    return clusters


def read_bed(bed_filename):
    bed_file = open(bed_filename)

    bed = collections.OrderedDict()
    for line in bed_file:
        line = line.strip()

        if line is _empty or \
           line.startswith(_comment):
            continue

        field = line.split(_tab)

        try:
            field[1] = int(field[1])
            field[2] = int(field[2])
            field[3] = str(field[3])
        except IndexError:
            err.write("ERROR: BED: Too few fields (=%d)\n" % num(field))
            sys.exit(1)
        except:
            err.write("ERROR: BED: Invalid record: '%s'\n" % line)
            sys.exit(1)
            
        record = BED(
            field[0],
            field[1],
            field[2],
            field[3]
        )
        if num(field) > 4:
            record.score = float(field[4])
        if num(field) > 5:
            record.strand = field[5]
        
        if record.chr not in bed:
            bed[record.chr] = intervals.IntervalList()

        bed[record.chr].add(record)

    return bed


def usage(message=None, exitcode=1, stream=sys.stderr):
    message = _empty if message is None else 'ERROR: %s\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] \\\n" % __program__)
    stream.write("             <input.config> <qry-id> <qry.loc.bed> <out-prefix>\n")
    stream.write("\n")
    stream.write("Options:\n")
    #------------|----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    #            0        10        20        30        40        50        60        70        80
    stream.write("  -c,--collapse       If pre-clustered with `cluster-collinear-bedpe`,\n")
    stream.write("                      collapse clusters to their maximal genomic ranges\n")
    stream.write("                      before building the coincidence; the mean score is\n")
    stream.write("                      reported for each collapsed cluster\n")
    stream.write("  -Q,--include-query  Record query synteny from input qry.loc.bed file\n")
    stream.write("  -h,--help           This help message\n")
    stream.write("\n")
    stream.write("Notes:\n")
    stream.write("\n")
    stream.write("  1. input.config has format: `<trg-id>\\t<trg.bedpe>\\t<trg.loc.bed>\\n`\n")
    stream.write("     where trg-id is a short dataset-specific (e.g., species) ID. Each\n")
    stream.write("     trg.bedpe must contain the query coordinate info in columns 1-3 and\n")
    stream.write("     target coordinates in columns 4-6.\n")
    stream.write("     positions.\n")
    stream.write("\n")
    stream.write("  2. qry-id is a short dataset-specific (e.g., species) ID.\n")
    stream.write("\n")
    stream.write("  3. qry.loc.bed and trg.loc.bed are a BED-formatted files of gene locus\n")
    stream.write("     position information\n")
    stream.write("\n")
    stream.write("  4. Synteny among query genes is not recorded in the output coincidence\n")
    stream.write("     by default, but may be recorded with the `--include-query` flag or by\n")
    stream.write("     including the query in the input.config file.\n")
    stream.write
    stream.write("\n")
    stream.write("%s\n" % message)
    sys.exit(exitcode)
    #------------|----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    #            0        10        20        30        40        50        60        70        80
    
def main(argv):
    long_opts = (
        'collapse',
        'include-query',
        'help'
    )
    short_opts = 'chQ'
    
    try:
        options, arguments = getopt.getopt(argv, short_opts, long_opts)
    except getopt.GetoptError as error:
        usage(error)

    collapse = False
    include_query = False
    for flag, value in options:
        if   flag in ('-h','--help'):
            usage(exitcode=0)
        elif flag in ('-c','--collapse'):
            collapse = True
        elif flag in ('-Q','--include-query'):
            include_query = True
        
    if len(arguments) != 4:
        usage("Unexpected number of arguments")

    config_file_name  = arguments[0]
    qry_species_name  = arguments[1]
    qry_bed_file_name = arguments[2]
    out_prefix        = arguments[3]
    
    config_file = zopen(config_file_name)
    qry_bed     = read_bed(qry_bed_file_name)

    out_matrix = None
    qry_chr_count = 0
    row_count = 0
    row_names = []
    loc_count = 0
    qry_index = collections.OrderedDict()
    loc_index = collections.OrderedDict()
    for qry_chr in qry_bed:
        qry_index[qry_chr] = qry_chr_count
        qry_chr_count += 1
        for qry_locus in qry_bed[qry_chr]:
            loc_index[qry_locus.id] = loc_count
            loc_count += 1

    if include_query:
        out_matrix = numpy.zeros((qry_chr_count, loc_count), dtype='int')
        for qry_chr in qry_bed:
            row_names.append([qry_species_name, qry_chr, row_count])
            row_count += 1
            for qry_locus in qry_bed[qry_chr]:
                out_matrix[qry_index[qry_chr], loc_index[qry_locus.id]] = 1

    for line in config_file:
        line = line.strip()

        if line is _empty or \
           line.startswith(_comment):
            continue

        fields = line.split(_tab)

        trg_species_name = fields[0]
        trg_bedpe = read_bedpe(fields[1], collapse)
        trg_bed   = read_bed(fields[2])

        # if trg_species_name == qry_species_name:
        #     continue
        
        trg_chrs  = 0
        trg_index = collections.OrderedDict()
        for trg_chr in trg_bed:
            row_names.append([trg_species_name, trg_chr, row_count])
            row_count += 1
            trg_index[trg_chr] = trg_chrs
            trg_chrs += 1
        
        trg_matrix = numpy.zeros((trg_chrs, loc_count), dtype='int')

        for record in trg_bedpe:
            qry_chr = record.trg.chr
            trg_chr = record.qry.chr
            if qry_chr not in qry_bed:
                continue
            if trg_chr not in trg_bed:
                continue
            
            (qry_beg, qry_end) = qry_bed[qry_chr].overlap_range(record.trg)

            if qry_beg < 0 or qry_end < 0:
                continue

            for qry_i in range(qry_beg, qry_end):
                qry_locus = qry_bed[qry_chr][qry_i]
                trg_matrix[trg_index[trg_chr], loc_index[qry_locus.id]] = 1

        if out_matrix is None:
            out_matrix = trg_matrix
        else:
            out_matrix = numpy.vstack((out_matrix, trg_matrix))


    out_col_lab  = zopen("%s.col.labels.txt" % out_prefix, 'w')
    out_row_lab  = zopen("%s.row.labels.txt" % out_prefix, 'w')
    out_mat_file = zopen("%s.matrix.gz" % out_prefix, 'w')
    for col in loc_index.items():
        out_col_lab.write(_tab.join(map(str, col)) + _eol)

    for row in row_names:
        out_row_lab.write(_tab.join(map(str, row)) + _eol)

    if out_matrix is not None:
        numpy.savetxt(
            out_mat_file,
            out_matrix,
            fmt='%d',
            delimiter=_tab,
            newline=_eol,
            header=(_tab.join(loc_index.keys())+_eol)
        )

    out_col_lab.close()
    out_row_lab.close()
    out_mat_file.close()
    
main(sys.argv[1:])
