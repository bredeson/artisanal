
import os
import re
import sys
import getopt
import intervals
import collections

from fileutil import zopen, zstrip

__authors__ = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Cluster orthologs into collinear blocks'

_tab = '\t'
_null = '.'
_empty = ''
_colon = ':'
_comma = ','
_comment = '#'
_minuint = -1 * (sys.maxsize - 1)
_maxuint = +1 * (sys.maxsize + 0)


err = sys.stderr

num = len
digits = re.compile('(\d+)')

format_bedpe = "{record:s}\t{cluster:d}\t{members:d}\n".format
format_paf = "{record:s}\txc:i:{cluster:d}\txm:i:{members:d}\n".format

AUX_TYPE = {
    'A': str,
    'i': int,
    'f': float,
    'Z': str,
    'H': str,
    'B': str
}

PAF_SUFFIX = ('.paf',)
SAM_SUFFIX = ('.bam','.sam','.cram')
BED_SUFFIX = ('.bedpe',)


Edge = collections.namedtuple('Edge', ('prev','next'))


def _cluster_average_score(members, num_members):
    return sum(map(
        lambda m: float(m.score or 0.0) * m.num_members,
        members
    )) / float(num_members)


class Node(object):
    def __init__(self, node=None, weight=_maxuint):
        self.node = node
        self.weight = weight
        

class Interval(intervals.BaseInterval):
    def __init__(self, chr=_null, beg=-1, end=-1, length=-1, strand=_null):
        intervals.BaseInterval.__init__(self, chr, beg, end)
        self.length = length
        self.strand = strand

    @property
    def strand(self):
        return self._strand

    @strand.setter
    def strand(self, strand):
        self._strand = intervals.Strand(strand)
        
    @property
    def mid(self):
        return float(self.beg + self.end) / 2.0

    @property
    def chr(self):
        return self.name

    @chr.setter
    def chr(self, name):
        self.name = name

        
class IntervalPair(object):
    def __init__(self, trg, qry, score=None, index=None, members=None):
        self.trg = trg
        self.qry = qry
        self.score = score
        self.index = index
        self.members = members
        self.num_members = None

    def __eq__(self, other):
        return \
            self.trg == other.trg and \
            self.qry == other.qry and \
            self.score == other.score

    def __lt__(self, other):
        return ((self.trg <  other.trg) or
                (self.trg == other.trg and self.qry < other.qry))
    
    def __hash__(self):
        return id(self)

    @property
    def score(self):
        if self._score is None:
            if self.members is None:
                return 0.0
            else:
                return _cluster_average_score(self.members, self.num_members)
        else:
            return self._score

    @score.setter
    def score(self, score):
        self._score = score
        
    @property
    def strand(self):
        return intervals.Strand(self.trg.strand.int * self.qry.strand.int)

    @property
    def num_members(self):
        if self._num_members is None:
            if self.members is None:
                return 1
            else:
                return num(self.members)
        else:
            return self._num_members

    @num_members.setter
    def num_members(self, num_members):
        self._num_members = num_members
    

        

class BEDPE(IntervalPair):
    def __init__(self, trg, qry, name=_null, score=None, index=None, members=None):
        IntervalPair.__init__(self, trg, qry, score, index, members)
        self.name = name
    
    def __hash__(self):
        return id(self)
        
    def __str__(self):
        return "%s\t%d\t%d\t%s\t%d\t%d\t%s\t%.04f\t%s\t%s" % (
            str(self.trg.chr),
            self.trg.beg,
            self.trg.end,
            str(self.qry.chr),
            self.qry.beg,
            self.qry.end,
            str(self.name),
            self.score,
            self.trg.strand.str,
            self.qry.strand.str
        )

    __repr__ = __str__



class AuxTag(object):
    def __init__(self, name=None, value=None, type=None):
        self.name = name
        self.value = value
        self.type = type

    def __str__(self):
        return _colon.join((self.name, self.type, str(self.value)))
    
        

class PAF(IntervalPair):
    def __init__(self, qry, trg, matches=0, length=-1, mapqual=255, auxtags=None, index=None, members=None):
        IntervalPair.__init__(self, trg, qry, 0, index, members)
        self.score = float(mapqual * matches) / 60.0
        self.matches = matches
        self.length = length
        self.mapqual = mapqual if mapqual != 255 else 0
        self.auxtags = auxtags
        self.flags = int(mapqual == 255)
        
    def __hash__(self):
        return id(self)
        
    def get_auxtag(self, tag):
        if tag in self.auxtags:
            return self.auxtags[tag].value
        return None

    def set_auxtag(self, tag, val=None, typ=None):
        if val is None:
            tag, typ, val = tag.split(_colon)
        if self.auxtags is None:
            self.auxtags = collections.OrderedDict()
        self.auxtags[tag] = AuxTag(tag, val, typ)
    
    def str_auxtags(self):
        if self.auxtags is None or \
           num(self.auxtags) < 1:
            return _empty
        return _tab.join((str(self.auxtags[tag]) for tag in self.auxtags))
        
    def __str__(self):
        return "%s\t%d\t%d\t%d\t%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d%s" % (
            self.qry.chr,
            self.qry.length,
            self.qry.beg,
            self.qry.end,
            self.strand.str,
            self.trg.chr,
            self.trg.length,
            self.trg.beg,
            self.trg.end,
            self.matches,
            self.length,
            255 if self.flags & 0x1 else self.mapqual,
            self.str_auxtags()
        )

    __repr__ = __str__



class Queue(collections.deque):
    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)
        self.index = -1
        self.length = 0

        
class Graph(object):
    def __init__(self):
        self.adj = dict()
        self.num_nodes = 0
        self.num_edges = 0
        self._visited = set()
        self._sorted = False

    @property
    def nodes(self):
        return self.adj.keys()
        
    def add_edge(self, node_u, node_v, weight=_maxuint):
        if node_u not in self.adj:
            self.adj[node_u] = list()
            self.num_nodes += 1
        if node_v not in self.adj:
            self.adj[node_v] = list()
            self.num_nodes += 1
        self.adj[node_u].append(Node(node_v, weight))
        self.adj[node_v].append(Node(node_u, weight))
        self.num_edges += 1
        self._sorted = False
        
    def __str__(self):
        return "%s(nodes=%s)" % (
            self.__class__.__name__,
            str(sorted(self.adj))
        )

    def doubly_linked_list(self):
        dlist = {}
        self.sort()
        for node in self.adj:
            dlist[node] = Edge(Node(), Node())
            
        for node_u in self.adj:
            for edge in self.adj[node_u]:
                node_v = edge.node
                if node_u < node_v:
                    if edge.weight < dlist[node_u].next.weight:
                        dlist[node_u].next.node = node_v
                        dlist[node_u].next.weight = edge.weight
                    if edge.weight < dlist[node_v].prev.weight:
                        dlist[node_v].prev.node = node_u
                        dlist[node_v].prev.weight = edge.weight
                elif node_v < node_u:
                    if edge.weight < dlist[node_v].next.weight:
                        dlist[node_v].next.node = node_u
                        dlist[node_v].next.weight = edge.weight
                    if edge.weight < dlist[node_u].prev.weight:
                        dlist[node_u].prev.node = node_v
                        dlist[node_u].prev.weight = edge.weight
        return dlist

    
    def find(self, parent, node):
        while parent[node] != node:
            node = parent[node]
        return node

    
    def union(self, parent, rank, node_u, node_v):
        root_u = self.find(parent, node_u)
        root_v = self.find(parent, node_v)

        # Attach smaller rank tree under root of
        # high rank tree (Union by Rank)
        if root_u == root_v:
            return
    
        elif rank[root_u] == rank[root_v]:
            parent[root_v] = root_u
            rank[root_u] += 1
            
        elif rank[root_u] < rank[root_v]:
            parent[root_u] = root_v

        elif rank[root_u] > rank[root_v]:
            parent[root_v] = root_u

        else:
            raise AssertionError('Impossible condition')
        
    def sort(self, reverse=False):
        for node in self.adj:
            self.adj[node].sort(key=lambda n:n.weight, reverse=reverse)
        self._sorted = True
        

    def _dft_iterative(self, ancestor):
        ancestors = collections.deque()
        ancestors.append(ancestor)
        descendants = list()
        while ancestors:
            ancestor = ancestors.popleft()
            if ancestor in self._visited:
                continue
            descendants.append(ancestor)
            self._visited.add(ancestor)
            
            if ancestor in self.adj:
                for descendant in reversed(self.adj[ancestor]):
                    if descendant in self._visited:
                        continue
                    ancestors.appendleft(descendant.node)

        return descendants
    
                
    def depth_first_traversal(self, maximize=False):
        clusters = list()
        minimize = not maximize
        self._visited = set()
        self.sort(reverse=minimize)
        for node in sorted(self.adj, key=lambda k: self.adj[k][0].weight, reverse=not maximize):
            if node not in self._visited:
                clusters.append(self._dft_iterative(node))
        self._visited = set()
        return clusters

    
    def kruskal_mst(self, max_dist=_maxuint):
        # Step 1: Sort all the edges in ascending order by their
        #         weights. If we are not allowed to change the
        #         given graph, we can create a copy of graph
        
        mstgraph = self.__class__()  # This will store the resultant MST
        parent = dict()
        rank = dict()
        
        # Create V subsets with single elements
        edges = list()
        for node in self.adj:
            parent[node] = node
            rank[node] = 0
            for edge in self.adj[node]:
                edges.append((edge.weight, node, edge.node))

        edges.sort()
            
        edge_i = 0
        edge_j = 0
        while edge_j < self.num_edges:
            # Step 2: Pick the smallest edge and increment
            #         the index for next iteration
            if edge_i == self.num_edges:
                break

            dist_i, node_u, node_v = edges[edge_i]  # edge nodes
            edge_i += 1

            if (node_u, node_v) in self._visited:
                continue

            root_u = self.find(parent, node_u)
            root_v = self.find(parent, node_v)
            
            # If including this edge does't cause a cycle,
            # include it in result and increment the index
            # of result for next edge
            if root_u != root_v and dist_i <= max_dist:
                self.union(parent, rank, root_u, root_v)
                mstgraph.add_edge(node_u, node_v, dist_i)
                edge_j += 1
            # Else discard the edge
            self._visited.add((node_u, node_v))
            self._visited.add((node_v, node_u))

        self._visited = set()
            
        return mstgraph
    
    
    
class EGraph: 
    def __init__(self):
        self.edges = dict()
        self._nodes = None
        self._visited = set()
        
    def __str__(self):
        return str(self.nodes)

    def __repr__(self):
        return str(self.nodes)

    @property
    def nodes(self):
        # lazy-load the nodes list:
        if self._nodes is None:
            nodes = set()
            for node_u, node_v in self.edges:
                nodes.add(node_u)
                nodes.add(node_v)
            self._nodes = sorted(nodes)
        return self._nodes
    
    # function to add an edge to graph
    def add_edge(self, node_u, node_v, weight=_maxuint):
        self.edges[(node_u, node_v)] = weight
        self.edges[(node_v, node_u)] = weight
        self._nodes = None
        # if node_u not in self.nodes:
        #     self.nodes[node_u] = set()
        # if node_v not in self.nodes:
        #     self.nodes[node_v] = set()
        # self.nodes[node_u].add(node_v)
        # self.nodes[node_v].add(node_u)
        
    @property
    def num_edges(self):
        return num(self.edges) // 2

    @property
    def num_nodes(self):
        return num(self.nodes)

    def doubly_linked_list(self):
        dlist = {}
        for node in self.nodes:
            dlist[node] = V(Edge(), Edge())
            
        for node_u, node_v in sorted(self.edges, key=self.edges.get):
            weight = self.edges[(node_u, node_v)]
            if node_u < node_v:
                if weight < dlist[node_u].next.weight:
                    dlist[node_u].next.node = node_v
                    dlist[node_u].next.weight = weight
                if weight < dlist[node_v].prev.weight:
                    dlist[node_v].prev.node = node_u
                    dlist[node_v].prev.weight = weight
            elif node_v < node_u:
                if weight < dlist[node_v].next.weight:
                    dlist[node_v].next.node = node_u
                    dlist[node_v].next.weight = weight
                if weight < dlist[node_u].prev.weight:
                    dlist[node_u].prev.node = node_v
                    dlist[node_u].prev.weight = weight
        return dlist
    
    # A utility function to find set of an element i
    # (uses path compression technique)
    def find(self, parent, node):
        if parent[node] == node:
            return node
        return self.find(parent, parent[node])

        # A function that does union of two sets of x and y
        # (uses union by rank)
    def union(self, parent, rank, node_u, node_v):
        root_u = self.find(parent, node_u)
        root_v = self.find(parent, node_v)

        # Attach smaller rank tree under root of
        # high rank tree (Union by Rank)
        if root_u == root_v:
            return
    
        elif rank[root_u] == rank[root_v]:
            parent[root_v] = root_u
            rank[root_u] += 1

        elif rank[root_u] < rank[root_v]:
            parent[root_u] = root_v

        elif rank[root_u] > rank[root_v]:
            parent[root_v] = root_u

        else:
            raise AssertionError('Impossible condition')

        
    def _dft_recursive(self, ancestor, maximize=False):
        descendants = [ancestor]
        self._visited.add(ancestor)
        # edges sorted by weight:
        for descendant in sorted(self.nodes[ancestor], key=lambda d: self.edges[(ancestor, d)], reverse=maximize):
            if descendant not in self._visited:
                descendants.extend(self._dft_recursive(descendant))
        return descendants

    
    def _dft_iterative(self, ancestor, maximize=False):
        minimize = not maximize
        ancestors = collections.deque()
        ancestors.append(ancestor)
        descendants = list()
        while ancestors:
            ancestor = ancestors.pop()

            if ancestor not in self._visited:
                descendants.append(ancestor)
                self._visited.add(ancestor)

            for descendant in sorted(self.nodes[ancestor], key=lambda d: self.edges[(ancestor, d)], reverse=minimize):
                if descendant not in self._visited:
                    ancestors.append(descendant)

        return descendants

    
    def depth_first_traversal(self):
        clusters = list()
        for node in self.nodes:
            if node not in self._visited:
                clusters.append(self._dft_iterative(node))
        self._visited = set()
        return clusters
        
    
    def kruskal_mst(self, max_dist=_maxuint):
        # Step 1: Sort all the edges in ascending order by their
        #         weights. If we are not allowed to change the
        #         given graph, we can create a copy of graph
        
        sorted_edges = sorted(self.edges, key=self.edges.get)
        mstgraph = self.__class__()  # This will store the resultant MST
        parent = dict()
        rank = dict()
        
        # Create V subsets with single elements
        for node in self.nodes:
            parent[node] = node
            rank[node] = 0
        
        edge_i = 0
        edge_j = 0
        while edge_j < self.num_edges:
            # Step 2: Pick the smallest edge and increment
            #         the index for next iteration
            if edge_i == self.num_edges:
                break

            node_u, node_v = sorted_edges[edge_i]  # edge nodes
            dist_i = self.edges[(node_u, node_v)]  # edge weight
            edge_i += 1

            if (node_u, node_v) in self._visited:
                continue

            root_u = self.find(parent, node_u)
            root_v = self.find(parent, node_v)
            
            # If including this edge does't cause a cycle,
            # include it in result and increment the index
            # of result for next edge
            if root_u != root_v and dist_i <= max_dist:
                self.union(parent, rank, root_u, root_v)
                mstgraph.add_edge(node_u, node_v, dist_i)
                edge_j += 1
            # Else discard the edge
            self._visited.add((node_u, node_v))
            self._visited.add((node_v, node_u))

        self._visited = set()
            
        return mstgraph


def _cluster_linked_list_prev(dlist, curr_node, cluster=collections.deque(), visited=set()):
    if curr_node not in dlist:
        return
    while curr_node is not None:
        prev_node = dlist[curr_node].prev.node
        if prev_node is None or \
           prev_node in visited:
            return
        cluster.appendleft(prev_node)
        visited.add(prev_node)
        curr_node = prev_node


def _cluster_linked_list_next(dlist, curr_node, cluster=collections.deque(), visited=set()):
    if curr_node not in dlist:
        return
    while curr_node is not None:
        next_node = dlist[curr_node].next.node
        if next_node is None or \
           next_node in visited:
            return
        cluster.append(next_node)
        visited.add(next_node)
        curr_node = next_node

        
def cluster_linked_list(dlist):
    visited = set()
    clusters = list()
    for node in sorted(dlist, key=lambda n: -n.matches):
        if node not in visited:
            cluster = collections.deque()
            cluster.append(node)
            _cluster_linked_list_prev(dlist, node, cluster, visited)
            _cluster_linked_list_next(dlist, node, cluster, visited)
            clusters.append(cluster)
            visited.add(node)
    return clusters


def _naturalize(string):
    return tuple((int(x) if x.isdigit() else x for x in digits.split(string)))


def _record_qry_pos(record):
    return (record.qry.chr, record.qry.beg, record.qry.end)


def _record_trg_pos(record):
    return (record.trg.chr, record.trg.beg, record.trg.end)


def _record_qry_chr(record):
    return record.qry.chr


def _record_trg_chr(record):
    return record.trg.chr


def _record_trg_idx(record):
    return (record.trg, record.index[0])


def _record_qry_idx(record):
    return (record.qry, record.index[1])


def _record_index_trg_pos(record):
    return (record.index, record.trg.chr, record.trg.beg, record.trg.end)


def _cluster_trg_pos(cluster):
    """Only used for nicely sorting output for writing"""
    return (
        _naturalize(cluster[0].trg.chr),
        _naturalize(cluster[0].qry.chr),
        cluster[0].trg.beg, cluster[-1].trg.end,
        cluster[0].qry.beg, cluster[-1].qry.end
    )


def _cluster_members(cluster):
    if cluster is None:
        return None
    
    members = []
    for member in cluster:
        if member.members:
            members.extend(_cluster_members(member.members))
        else:
            members.append(member)

    return members


def _cluster_num_members(cluster):
    num_members = 0
    for c in _cluster_members(cluster):
        num_members += c.num_members
    return num_members


def _cluster_ord_infer_strand(cluster, key=lambda x: (x, -1)):
    record0, idx0 = key(cluster[0])
    recordN, idxN = key(cluster[-1])
    if num(cluster) > 1:
        rev = record0.end - recordN.beg
        fwd = recordN.end - record0.beg
        if rev > fwd:
            return -1
        else:
            return +1
    else:
        return record0.strand.int

    
def sign(value):
    nvalue = float(value if value else 0)
    dvalue = float(value if value else 1)
    return int(nvalue / abs(dvalue))
        
    
def index_records(records, key=lambda x: x):
    index = 1
    indices = {}
    for record in sorted(records, key=key):
        record_key = key(record)
        if record_key not in indices:
            indices[record_key] = index
            index += 1

    return indices


def doublelinked_clusters(clusters):
    c = 0
    cluster_list = []
    cluster_dict = {}
    for cluster in clusters:
        for member in cluster:
            i = tuple(member.index)

            if i not in cluster_dict:
                cluster_dict[i] = c
                cluster_list.append([])
                c += 1

            cluster_list[cluster_dict[i]].append(member)

    return cluster_list


def nodes_on_same_chr(node_i, node_j):
    return \
        node_i.trg.chr == node_j.trg.chr and \
        node_i.qry.chr == node_j.qry.chr


def _ordinal_node_distance(node_i, node_j):
    # Reformulate to calculate distance from the next collinear
    # index (along the diagonal). The current scoring scheme
    # gives equal weight to hit pairs with the same X (or Y) value
    # as those along the diagonal, we should prefer diagonal hits.
    if nodes_on_same_chr(node_i, node_j):
        return max(abs(node_i.index[0] - node_j.index[0]),
                   abs(node_i.index[1] - node_j.index[1]))
    return _maxuint
    

def _dist2d(x, y):
    return pow(x**2 + y**2, 0.2)


def _interval_strand_consistent(node_i, node_j):
    dist_q = (node_j.qry.mid - node_i.qry.mid) or 1
    dist_t = (node_j.trg.mid - node_i.trg.mid) or 1
    joint_strand = sign(dist_q / dist_t)
    return \
        joint_strand == node_i.strand.int and \
        joint_strand == node_j.strand.int
    

def _interval_node_distance(node_i, node_j):
    if nodes_on_same_chr(node_i, node_j):
        dist_i = _dist2d(node_i.trg.end - node_j.trg.beg,
                         node_i.qry.end - node_j.qry.beg)
        dist_j = _dist2d(node_j.trg.end - node_i.trg.beg,
                         node_j.qry.end - node_i.qry.beg)
        return min(dist_i, dist_j)
    return _maxuint

    
def cluster_filter_score(
        clusters, min_members=1,
        min_average=_minuint, max_average=_maxuint, max_delta=_maxuint):
    filtered_clusters = []
    for cluster in clusters:
        num_members = _cluster_num_members(cluster)
        members = _cluster_members(cluster)
        avg_score = _cluster_average_score(members, num_members)
        
        if min_average <= avg_score <= max_average:
            filtered_members = []
            for member in members:
                avg_delta = abs(avg_score - member.score)
                if avg_delta <= max_delta:
                    filtered_members.append(member)
                    
            if num(filtered_members) >= min_members:
                filtered_clusters.append(
                    filtered_members
                )
                
    return filtered_clusters

    
def cluster_ord_nearly_collinear_records(
        records, min_members=1, max_distance=1,
        min_score=_minuint, max_score=_maxuint, max_delta=_maxuint, debug=False):
    tchr_indices = index_records(records, key=_record_trg_chr)
    qchr_indices = index_records(records, key=_record_qry_chr)
    tpos_indices = index_records(records, key=_record_trg_pos)
    qpos_indices = index_records(records, key=_record_qry_pos)
    pair_records = {}  # index of contig pairs
    clusters = []

    # Optimization: Partition records into trg-qry chromosome
    # pairs, as synteny (and therefore collinearity) cannot
    # exist on different chromosomes by definition.
    for record in sorted(records, key=_record_trg_pos):
        record.index[0] = tpos_indices[_record_trg_pos(record)]
        record.index[1] = qpos_indices[_record_qry_pos(record)]
        pair_key = (
            tchr_indices[_record_trg_chr(record)],
            qchr_indices[_record_qry_chr(record)]
        )
            
        if pair_key not in pair_records:
            pair_records[pair_key] = []
        pair_records[pair_key].append(record)

    # Now perform Minimum Spanning Tree clustering of within-
    # trg-qry pairs of chromosomes:
    for pair_key in sorted(pair_records):
        records = pair_records[pair_key]
        
        graph = Graph()
        for i in range(0, num(records)-1):
            for j in range(i+1, num(records)):
                _distance  = _ordinal_node_distance(records[i], records[j])
                _min_score = min(records[i].score,  records[j].score)
                _max_score = max(records[i].score,  records[j].score)
                _max_delta = abs(records[i].score - records[j].score)
                if _min_score < min_score or \
                   _max_score > max_score or \
                   _max_delta > max_delta:
                    continue
                
                graph.add_edge(records[i], records[j], _distance)

        if graph.num_edges < 1:
            continue
        
        _clusters = graph.kruskal_mst(max_distance).depth_first_traversal()
        _clusters = list(filter(lambda c: num(c) >= min_members, _clusters))

        for cluster in _clusters:
            cluster.sort(key=_record_trg_idx)
            #for j, member in enumerate(cluster):
            #    cluster[j] = pair_records[pair_key][member]

        clusters.extend(_clusters)
                
    return clusters
    
    
def _cluster_ord_strictly_collinear_records(
        records, max_distance=1,
        min_score=0, max_score=_maxuint, max_delta=_maxuint, stranded=False, i=0):
    collinear_records = []
    prev_record = []

    j = int(not i)

    for curr_record in records:
        if prev_record:
            if prev_record[-1].trg.chr == curr_record[0].trg.chr and \
               prev_record[-1].qry.chr == curr_record[0].qry.chr:
                if stranded and \
                   (prev_record[-1].trg.strand.int != curr_record[0].trg.strand.int or
                    prev_record[-1].qry.strand.int != curr_record[0].qry.strand.int):
                    collinear_records.append(prev_record)
                    prev_record = curr_record
                    continue
                
                _qry_dist = abs(curr_record[0].index[i] - prev_record[-1].index[i])
                _trg_dist = abs(curr_record[0].index[j] - prev_record[-1].index[j])

                _min_score = min(curr_record[0].score,  prev_record[-1].score)
                _max_score = max(curr_record[0].score,  prev_record[-1].score)
                _max_delta = abs(curr_record[0].score - prev_record[-1].score)
                if _min_score < min_score or \
                   _max_score > max_score or \
                   _max_delta > max_delta:
                    collinear_records.append(prev_record)
                
                elif _qry_dist <= max_distance and \
                     _trg_dist <= max_distance:
                    prev_record.extend(curr_record)
                    continue
                else:
                    collinear_records.append(prev_record)
            else:
                collinear_records.append(prev_record)
            
        prev_record = curr_record

    if prev_record:
        collinear_records.append(prev_record)
        
    return collinear_records


def cluster_ord_strictly_collinear_records(
        records, min_members=1, max_distance=1, 
        min_score=_minuint, max_score=_maxuint, max_delta=_maxuint, stranded=False, debug=False):
    tpos_indices = index_records(records, key=_record_trg_pos)
    qpos_indices = index_records(records, key=_record_qry_pos)

    trg_clusters = []
    qry_clusters = []
    for record in sorted(records, key=_record_trg_pos):
        record.index[0] = tpos_indices[_record_trg_pos(record)]
        trg_clusters.append([record])

    for record in sorted(records, key=_record_qry_pos):
        record.index[1] = qpos_indices[_record_qry_pos(record)]
        qry_clusters.append([record])

    for distance in range(max_distance):
        trg_clusters = _cluster_ord_strictly_collinear_records(
            trg_clusters, distance+1, min_score, max_score, max_delta, stranded, 0
        )

        qry_clusters = _cluster_ord_strictly_collinear_records(
            qry_clusters, distance+1, min_score, max_score, max_delta, stranded, 1
        )

        if min_members > 0:
            trg_clusters = list(filter(lambda c: num(c) >= min_members, trg_clusters))
            qry_clusters = list(filter(lambda c: num(c) >= min_members, qry_clusters))
            min_members = 0

    for i, cluster in enumerate(trg_clusters):
        for member in cluster:
            member.index[0] = i

    for j, cluster in enumerate(qry_clusters):
        for member in cluster:
            member.index[1] = j

    return doublelinked_clusters(trg_clusters)


def cluster_ord_collinear_records(
        records, min_members=1, max_distance=1,
        min_score=_minuint, max_score=_maxuint, max_delta=_maxuint, strictly=False, debug=False):
    if strictly:
        return cluster_ord_strictly_collinear_records(
            records,
            min_members,
            max_distance,
            min_score,
            max_score,
            max_delta,
            debug
        )
    else:
        return cluster_ord_nearly_collinear_records(
            records,
            min_members,
            max_distance,
            min_score,
            max_score,
            max_delta,
            debug
        )


def cluster_ord_classified_records(records):
    prev = None
    clusters = []
    for curr in sorted(records, key=_record_index_trg_pos):
        if prev is None:
            clusters.append([curr])
        elif curr.index[0] == prev.index[0]:
            clusters[-1].append(curr)
        else:
            clusters.append([curr])
        
        prev = curr

    return clusters


def collapse_clusters(clusters):
    collapsed_clusters = []
    for cluster in clusters:
        cluster.sort(key=_record_trg_pos)
        trg_strand  = _cluster_ord_infer_strand(cluster, key=_record_trg_idx)
        qry_strand  = _cluster_ord_infer_strand(cluster, key=_record_qry_idx)
        num_members = _cluster_num_members(cluster)
        members     = _cluster_members(cluster)
        
        if trg_strand < 0 and qry_strand < 0:
            trg_strand = +1
            qry_strand = +1

        pair = BEDPE(
            Interval(cluster[0].trg.chr,
                     min(map(lambda c: c.trg.beg, cluster)),
                     max(map(lambda c: c.trg.end, cluster)),
                     strand=trg_strand
            ),
            Interval(cluster[0].qry.chr,
                     min(map(lambda c: c.qry.beg, cluster)),
                     max(map(lambda c: c.qry.end, cluster)),
                     strand=qry_strand
            ),
            name=_null,
            score=None,
            index=[0,0],
            members=members
        )
        pair.num_members = num_members
        
        collapsed_clusters.append([pair])
        
    return collapsed_clusters


def expand_clusters(clusters):
    expanded_clusters = []
    for cluster in clusters:
        expanded_clusters.append(_cluster_members(cluster))

    return expanded_clusters


def uniqify_clusters(clusters,
                     max_trg_copies=-1, max_qry_copies=-1,
                     max_trg_overlap=-1, max_qry_overlap=-1, selfself=False):
    if max_trg_copies < 1 or max_qry_copies < 1:
        return clusters

    uniqified_clusters = []
    
    trg_intervals = {}
    qry_intervals = trg_intervals if selfself else {}
    
    for cluster in sorted(clusters, key=_cluster_num_members, reverse=True):
        cluster_trg = Interval(cluster[0].trg.chr,
                          min(map(lambda m: m.trg.beg, cluster)),
                          max(map(lambda m: m.trg.end, cluster)))
        cluster_qry = Interval(cluster[0].qry.chr,
                          min(map(lambda m: m.qry.beg, cluster)),
                          max(map(lambda m: m.qry.end, cluster)))

        if cluster_trg.chr not in trg_intervals:
            trg_intervals[cluster_trg.chr] \
                = list((intervals.IntervalList() for i in range(max_trg_copies)))
        if cluster_qry.chr not in qry_intervals:
            qry_intervals[cluster_qry.chr] \
                = list((intervals.IntervalList() for j in range(max_qry_copies)))

        add_overlap = [-1, -1]  # -1 == False
        if max_trg_overlap >= 0.0:
            for i in range(max_trg_copies):
                trg_overlap = trg_intervals[cluster_trg.chr][i].overlap_length(cluster_trg) 
                trg_overlap = float(trg_overlap) / len(cluster_trg)
                if trg_overlap <= max_trg_overlap:
                    add_overlap[0] = i
                    break
        else:
            add_overlap[0] = 0

        if max_qry_overlap >= 0.0:
            for j in range(max_qry_copies):
                qry_overlap = qry_intervals[cluster_qry.chr][j].overlap_length(cluster_qry)
                qry_overlap = float(qry_overlap) / len(cluster_qry)
                if qry_overlap <= max_qry_overlap:
                    add_overlap[1] = j
                    break
        else:
             add_overlap[1] = 0
             
        if add_overlap[0] >= 0 and add_overlap[1] >= 0:
            trg_intervals[cluster_trg.chr][add_overlap[0]].add(cluster_trg)
            qry_intervals[cluster_qry.chr][add_overlap[1]].add(cluster_qry)
            uniqified_clusters.append(cluster)

    return uniqified_clusters


def adjoin_clusters(clusters,
                    max_trg_copies=-1, max_qry_copies=-1,
                    max_trg_overlap=-1, max_qry_overlap=-1, selfself=False):
    clusters = uniqify_clusters(clusters,
                                max_trg_copies,
                                max_qry_copies,
                                max_trg_overlap,
                                max_qry_overlap)
    clusters = list(( cluster[0] for cluster in collapse_clusters(clusters) ))
    clusters = cluster_strictly_collinear_records(clusters, stranded=True)
    return expand_clusters(clusters)
    

def cluster_ordinals(
        records, clustered=False, selfself=False, min_members=1, max_distance=1,
        min_score=_minuint, max_score=_maxuint, max_score_delta=_maxuint,
        min_average=_minuint, max_average=_maxuint, max_average_delta=_maxuint,
        max_overlap=(-1, -1), max_ncopies=(-1, -1), strictly=False, uniquely=False,
        collapse=False, adjoin=False, debug=False):
        
    if clustered:
        clusters = cluster_classified_records(records)
    else:
        clusters = cluster_ord_collinear_records(
            records,
            min_members,
            max_distance,
            min_score,
            max_score,
            max_score_delta,
            strictly,
            debug
        )

    clusters = cluster_filter_score(
        clusters,
        min_members,
        min_average,
        max_average,
        max_average_delta
    )
        
    if adjoin and uniquely:
        clusters = adjoin_clusters(
            clusters,
            max_ncopies[0],
            max_ncopies[1],
            max_overlap[0],
            max_overlap[1],
            selfself
        )        
    elif uniquely:
        clusters = uniqify_clusters(
            clusters,
            max_ncopies[0],
            max_ncopies[1],
            max_overlap[0],
            max_overlap[1],
            selfself
        )
    elif adjoin:
        err.write("ERROR: --max-overlap must be specified when --adjoin enabled\n")
        sys.exit(1)
    
    if collapse:
        clusters = collapse_clusters(clusters)

    return clusters


def cluster_intervals(records, max_distance=50000, min_length=0, max_bandwidth=_maxuint):
    tchr_indices = index_records(records, key=_record_trg_chr)
    qchr_indices = index_records(records, key=_record_qry_chr)
    pair_records = {}  # index of contig pairs
    clusters = []
    
    for record in sorted(records, key=_record_trg_pos):
        pair_key = (
            tchr_indices[_record_trg_chr(record)],
            qchr_indices[_record_qry_chr(record)]
        )
        
        if pair_key not in pair_records:
            pair_records[pair_key] = []
            
        pair_records[pair_key].append(record)
        
    for pair_key in sorted(pair_records):
        records = sorted(pair_records[pair_key], key=_record_trg_pos)
        
        graph = Graph()
        num_records = num(records)
        for i in range(0, num_records-1):
            for j in range(i+1, min(i+max_bandwidth, num_records)):
                _distance = _interval_node_distance(records[i], records[j])
                if _interval_strand_consistent(records[i], records[j]):
                    graph.add_edge(i, j, _distance)

        _clusters = graph.kruskal_mst(max_distance).depth_first_traversal()
        
        for cluster in _clusters:
            cluster.sort()
            for j, member in enumerate(cluster):
                cluster[j] = records[member]

            length = sum(map(lambda m: m.length, cluster))
            if length >= min_length:
                clusters.append(cluster)

    return clusters

        
def read_sam(sam_filename):
    try:
        import pysam
    except ImportError as error:
        err.write("ERROR: %s\n" % str(error))
        sys.exit(1)

    STRAND = ('+','-')

    CIGAR_EQV_OPERATOR = {
        pysam.CMATCH,
        pysam.CEQUAL
    }
    CIGAR_PAD_OPERATORS = {
        pysam.CHARD_CLIP,
        pysam.CSOFT_CLIP
    }
    CIGAR_ALN_OPERATORS = {
        pysam.CMATCH,
        pysam.CINS,
        pysam.CDEL,
        pysam.CREF_SKIP,
        pysam.CPAD,
        pysam.CEQUAL,
        pysam.CDIFF
    }
    CIGAR_QRY_OPERATORS = {
        pysam.CMATCH,
        pysam.CINS,
        pysam.CSOFT_CLIP,
        pysam.CHARD_CLIP,
        pysam.CEQUAL,
        pysam.CDIFF
    }
    CIGAR_REF_OPERATORS = {
        pysam.CMATCH,
        pysam.CDEL,
        pysam.CREF_SKIP,
        pysam.CEQUAL,
        pysam.CDIFF
    }
    
    CIGAR_MAP = {
        pysam.CMATCH    : 'M',
        pysam.CINS      : 'I',
        pysam.CDEL      : 'D',
        pysam.CREF_SKIP : 'N',
        pysam.CSOFT_CLIP: 'S',
        pysam.CHARD_CLIP: 'H',
        pysam.CPAD      : 'P',
        pysam.CEQUAL    : '=',
        pysam.CDIFF     : 'X',
        pysam.CBACK     : 'B',
    }

    def trim_cigar(aln):
        cigar = []
        for cig in aln.cigartuples:
            if cig[0] in CIGAR_PAD_OPERATORS:
                continue
            else:
                cigar.append("%d%s" % (cig[1],CIGAR_MAP[cig[0]]))
        return _empty.join(cigar)


    def infer_align_length(cstats):
        length = 0
        for i in tuple(CIGAR_ALN_OPERATORS):
            length += cstats[i]
        return length


    def infer_query_length(cstats):
        length = 0
        for i in tuple(CIGAR_QRY_OPERATORS):
            length += cstats[i]
        return length


    def infer_query_5p_clip_length(aln):
        clip_length = 0
        for i in range(len(aln.cigartuples)):
            if aln.cigartuples[i][0] in CIGAR_PAD_OPERATORS:
                clip_length += aln.cigartuples[i][1]
            else:
                break
        return clip_length


    def infer_query_3p_clip_length(aln):
        clip_length = 0
        for i in range(len(aln.cigartuples)):
            if aln.cigartuples[~i][0] in CIGAR_PAD_OPERATORS:
                clip_length += aln.cigartuples[~i][1]
            else:
                break
        return clip_length
    

    sam_file = pysam.AlignmentFile(sam_filename)
    trg_lengths = dict(zip(sam_file.references, sam_file.lengths))

    records = []
    for record in sam_file:
        if record.is_unmapped or \
           record.is_secondary:
            continue

        cstats = record.get_cigar_stats()[0]
        
        qry_length = infer_query_length(cstats)
        qry_beg = infer_query_5p_clip_length(record)
        qry_end = infer_query_3p_clip_length(record)
        qry_end = qry_length - qry_end
        
        trg_length = trg_lengths[record.reference_name]
        trg_beg = record.reference_start
        trg_end = record.reference_end

        if cstats[pysam.CDIFF]:
            aln_matches = cstats[pysam.CEQUAL]
        else:
            aln_matches = cstats[pysam.CMATCH]
            if record.has_tag('NM'):
                aln_matches -= record.has_tag('NM')
                
        aln_length = infer_align_length(cstats)

        if self.strand.int < 0:
            self.qry.beg, self.qry.end \
                = self.qry.length - self.qry.end, \
                  self.qry.length - self.qry.beg
        
        _record = IntervalPair(
            Interval(
                record.reference_name,
                trg_length,
                trg_beg,
                trg_end,
                STRAND[0]
            ),
            Interval(
                record.query_name,
                qry_length,
                qry_beg,
                qry_end,
                STRAND[int(record.is_reverse)]
            ),
            matches=aln_matches,
            length=aln_length,
            mapq=record.mapping_quality,
            cigar=trim_cigar(record)
        )
        if record.has_tag('AS'):
            _record.score = record.get_tag('AS')

        records.append(_record)

    return records


def read_paf(paf_filename):
    paf_file = zopen(paf_filename, 'r')

    int_fields = (1,2,3,6,7,8,9,10,11)
    
    records = []
    for line in paf_file:
        field = line.strip().split(_tab)

        try:
            for i in int_fields:
                field[i] = int(field[i])
            assert field[2] <  field[1]
            assert field[3] <= field[1]
            assert field[2] <  field[3]
            assert field[7] <  field[6]
            assert field[8] <= field[6]
            assert field[7] <  field[8]
        except IndexError:
            err.write("ERROR: PAF: Too few fields (=%d)\n" % num(field))
            sys.exit(1)
        except:
            err.write("ERROR: PAF: Invalid record: '%s'\n" % line)
            sys.exit(1)
            
        record = PAF(
            Interval(
                field[0],
                field[2],
                field[3],
                field[1],
                field[4]
            ),
            Interval(
                field[5],
                field[7],
                field[8],
                field[6],
                +1
            ),
            matches=field[9],
            length=field[10],
            mapqual=field[11],
            index=[0,0]
        )

        for i in range(12, num(field)):
            record.set_auxtag(field[i])

        records.append(record)

    return records



def read_bedpe(bedpe_filename, clustered=False, selfself=False):
    bedpe_file = zopen(bedpe_filename, 'r')
    
    records = []
    initial_counts_tot = 0
    for line in bedpe_file:
        line = line.strip()

        if line is _empty or \
           line.startswith(_comment):
            continue

        field = line.split(_tab)

        try:
            for i in range(2):
                chr = 3*i+0
                beg = 3*i+1
                end = 3*i+2
                field[beg] = int(field[beg])
                field[end] = int(field[end])
                assert field[beg] <= field[end]
                
        except IndexError:
            err.write("ERROR: BEDPE: Too few fields (=%d)\n" % num(field))
            sys.exit(1)
        except:
            err.write("ERROR: BEDPE: Invalid record: '%s'\n" % line)
            sys.exit(1)

        record = BEDPE(
            Interval(
                field[0],
                field[1],
                field[2]
            ),
            Interval(
                field[3],
                field[4],
                field[5]
            ),
            index=[0,0]
        )

        if record.trg.chr is _null or record.trg.beg < 0 or record.trg.end < 0 or \
           record.qry.chr is _null or record.qry.beg < 0 or record.qry.end < 0:           
            continue

        if selfself > 1 and \
           record.trg.chr == record.qry.chr:
            continue
        
        if num(field) > 6:
            record.name = field[6]
        if num(field) > 7:
            record.score = float(field[7])
        if num(field) > 8:
            record.trg.strand = field[8]
        if num(field) > 9:
            record.qry.strand = field[9]
        
        if clustered:
            try:
                record.index = [int(field[10]), int(field[10])]
                record.num_members = int(field[11])
            except:
                err.write("ERROR: expected integer 11th and 12th columns\n")
                sys.exit(1)

        records.append(record)

    return records

        
def version(exitcode=0, stream=sys.stdout):
    stream.write("%s %s\n" % (__program__, __version__))
    sys.exit(exitcode)
    
    
def usage(message=None, exitcode=1, stream=sys.stderr):
    message = _empty if message is None else 'ERROR: %s\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] <input>\n" % __program__)
    stream.write("\n")
    stream.write("Options:\n")
    #------------|----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    #            0        10        20        30        40        50        60        70        80
    stream.write("\n")
    stream.write(" Ordinal clustering:\n")
    stream.write("  -a,--min-average <ufloat>  Minimum cluster-averaged score (hard threshold)\n")
    stream.write("  -A,--max-average <ufloat>  Maximum cluster-averaged score (hard threshold)\n")
    stream.write("  -B,--max-average-delta <ufloat>\n")
    stream.write("                             Refine node agglomeration by max cluster-average:\n")
    stream.write("                             Node i with |mean(score) - score[i]| > <ufloat>\n")
    stream.write("                             will be removed from the cluster [inf]\n")
    stream.write("  -C,--clustered             input file has been clustered in a previous\n")
    stream.write("                             run; no additional clustering will be performed,\n")
    stream.write("                             requires ordinal cluster integer and node count\n")
    stream.write("                             in 11th and 12th fields, respectively\n")
    stream.write("  -c,--collapse              Collapse clusters to their maximal genomic\n")
    stream.write("                             ranges; the (mean) average score is reported for\n")
    stream.write("                             each collapsed cluster\n")
    stream.write("  -D,--max-score-delta <ufloat>\n")
    stream.write("                             Restrict node agglomeration by max score delta.\n")
    stream.write("                             Nodes i and j with |score[i] - score[j]| > <ufloat>\n")
    stream.write("                             will not be linked [inf]\n")
    stream.write("  -d,--max-distance <uint>   Maximum inter-node ordinal index distance [1]\n")
    stream.write("  -j,--adjoin                Adjoin adjacent clusters if their order and\n")
    stream.write("                             orientations are consistent (requires -O)\n")
    stream.write("  -m,--min-members <uint>    Minimum member nodes to report a cluster [1]\n")
    stream.write("  -N,--max-clusters <uint>[,<uint>]\n")
    stream.write("                             Retain <uint> coverage of the longest collinear\n")
    stream.write("                             clusters per genomic segment on the target,query\n")
    stream.write("                             sequences, respectively (requires -O) [inf,inf]\n")
    stream.write("  -O,--max-overlap <ufloat>[,<ufloat>]\n")
    stream.write("                             Discard a smaller cluster if it overlaps a\n")
    stream.write("                             larger one by more than <ufloat> fraction of\n")
    stream.write("                             the smaller's genomic length on the target,query\n")
    stream.write("                             sequences, respectively [1.0,1.0]\n")
    stream.write("  -s,--min-score <ufloat>    Minimum node score (hard threshold)\n")
    stream.write("  -S,--max-score <ufloat>    Maximum node score (hard threshold)\n")
    stream.write("  -X,--self-self             input represents self-self all-v-all alignments\n")
    stream.write("  -x,--self-self-inter       Enable -X, only cluster inter-chromosomal hits\n")
    stream.write("  -y,--sensitive             More sensitive ordinal clustering, better suited\n")
    stream.write("                             for closely-related/highly-collinear genomes\n")
    stream.write("\n")
    #------------|----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    #            0        10        20        30        40        50        60        70        80
    stream.write(" Interval clustering:\n")
    stream.write("  -i,--intervals             Cluster input using interval-based clustering\n")
    stream.write("  -b,--max-bandwidth <uint>  Maximum number of neighbors to compute distances\n")
    stream.write("                             between intervals. An optimization [50]\n")
    stream.write("  -d,--max-distance <uint>   Maximum interval nucleotide distance [25000]\n")
    stream.write("  -l,--min-length <uint>     Minimum length of unclustered intervals [50000]\n")
    stream.write("\n")
    stream.write(" General options:\n")
    stream.write("  -o,--outfile <file>        Write clustered output to <file> [stdout]\n")
    stream.write("  -v,--version               Write version info and exit\n")
    stream.write("  -h,--help                  This help message\n")
    stream.write("\n")
    stream.write("Arguments:\n")
    stream.write("  <input>                    A BEDPE- or PAF-formatted file. Type inferred by\n")
    stream.write("                             its file suffix. Input may be gzip-, bzip2-, or\n")
    stream.write("                             xz-compressed.\n")
    stream.write("\n")
    stream.write("%s\n" % message)
    sys.exit(exitcode)
    #------------|----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    #            0        10        20        30        40        50        60        70        80


def main(argv):
    long_opts = (
        'min-average=',
        'max-average=',
        'max-average-delta=',
        'max-distance=',
        'max-overlap=',
        'min-members=',
        'outfile=',
        'collapse',
        'clustered=',
        'intervals',
        'max-bandwidth=',
        'max-clusters=',
        'max-score-delta=',
        'min-length=',
        'min-score=',
        'max-score=',
        'qry-sizes=',
        'trg-sizes=',
        'self-self',
        'self-self-inter',
        'sensitive',
        'strictly',
        'adjoin',
        'debug',
        'version',
        'help',
    )
    short_opts = 'a:A:B:b:CcD:d:hijl:m:N:O:o:S:s:uvXxy'
    
    try:
        options, arguments = getopt.getopt(argv, short_opts, long_opts)
    except getopt.GetoptError as error:
        usage(error)

    debug = False
    adjoin = False
    outfile = '-'
    selfself = 0
    strictly = False
    uniquely = False
    collapse = False
    clustered = False
    min_length = 0
    min_members = 1
    max_distance = -1
    min_score = _minuint
    max_score = _maxuint
    max_score_delta = _maxuint
    min_average = _minuint
    max_average = _maxuint
    max_average_delta = _maxuint
    max_ncopies = [-1, -1]
    max_overlap = [-1, -1]
    max_bandwidth = 50
    interval_clustering = False
    for flag, value in options:
        if   flag in ('-h','--help'):
            usage(exitcode=0)
        elif flag in ('-v','--version'):
            version(exitcode=0)
        elif flag in ('-a','--min-average'):
            min_average = float(value)
        elif flag in ('-A','--max-average'):
            max_average = float(value)
        elif flag in ('-B','--max-average-delta'):
            max_average_delta = float(value)
        elif flag in ('-C','--clustered'):
            clustered = True
        elif flag in ('-c','--collapse'):
            collapse = True
        elif flag in ('-i','--intervals'):
            interval_clustering = True
        elif flag in ('-j','--adjoin'):
            adjoin = True
        elif flag in ('-D','--max-score-delta'):
            max_score_delta = float(value)
        elif flag in ('-b','--max-bandwidth'):
            max_bandwidth = int(value)
        elif flag in ('-d','--max-distance'):
            max_distance = int(value)
        elif flag in ('-l','--min-length'):
            min_length = int(value)
        elif flag in ('-m','--min-members'):
            min_members = int(value)
        elif flag in ('-O','--max-overlap'):
            max_overlap = value.split(_comma)
            uniquely = True
        elif flag in ('-o','--outfile'):
            outfile = value
        elif flag in ('-N','--max-clusters'):
            max_ncopies = value.split(_comma)
            uniquely = True
        elif flag in ('-s','--min-score'):
            min_score = float(value)
        elif flag in ('-S','--max-score'):
            max_score = float(value)
        elif flag in ('-X','--self-self'):
            selfself = 1
        elif flag in ('-x','--self-self-inter'):
            selfself = 2
        elif flag in ('-y','--strictly','--sensitive'):
            strictly = True

    if num(arguments) != 1:
        usage("Unexpected number of arguments, one expected")
    if max_distance < 0:
        if interval_clustering:
            max_distance = 25000
        else:
            max_distance = 1
    if max_distance < 1:
        usage("--max-distance must be greater than 0")
    if min_members < 1:
        usage("--min-members must be greater than or equal to 1")
    if max_score < min_score:
        usage("--min-score must be less than or equal to --max-score")
    if max_score_delta < 0:
        usage("--max-score-delta must be greater than or equal to 0")
    if max_average < min_average:
        usage("--min-average must be less than or equal to --max-average")
    if max_average_delta < 0:
        usage("--max-average-delta must be greater than or equal to 0")
            
    if num(max_overlap) == 1:
        max_overlap = float(max_overlap[0])
        max_overlap = [max_overlap, max_overlap]
    elif num(max_overlap) == 2:
        max_overlap = list(map(float, max_overlap))
    else:
        usage("--max-overlap exepects two comma-separated fractional values")

    if max_overlap[0] < 0:
        max_overlap[0] = -1
    if max_overlap[1] < 0:
        max_overlap[1] = -1
        
    if num(max_ncopies) == 1:
        max_ncopies = int(max_ncopies[0])
        max_ncopies = [max_ncopies, max_ncopies]
    elif num(max_ncopies) == 2:
        max_ncopies = list(map(int, max_ncopies))
        if selfself and max_ncopies[0] != max_ncopies[1]:
            usage("--max-clusters requires target == query when --self-self enabled")
    else:
        usage("--max-clusters expects two comma-separated integer values")

    if max_ncopies[0] < 1:
        max_ncopies[0] = -1
    if max_ncopies[1] < 1:
        max_ncopies[1] = -1

    alnfile = arguments[0]
    outfile = zopen(outfile, 'w')
    tmpfile = zstrip(alnfile).lower()

    if tmpfile.endswith(PAF_SUFFIX):
        records = read_paf(alnfile)
        formatf = format_paf
#    elif tmpfile.endswith(SAM_SUFFIX):
#        records = read_sam(alnfile)
#        formatf = format_paf
    else:
        records = read_bedpe(alnfile, clustered, selfself)
        formatf = format_bedpe
        
    err.write("Command: %s %s\n" % (__program__, ' '.join(argv)))

    initial_counts_tot = len(records)
    
    if interval_clustering:
        clusters = cluster_intervals(
            records,
            max_distance,
            min_length,
            max_bandwidth
        )
    else:
        clusters = cluster_ordinals(
            records,
            clustered,
            selfself,
            min_members,
            max_distance,
            min_score,
            max_score,
            max_score_delta,
            min_average,
            max_average,
            max_average_delta,
            max_overlap,
            max_ncopies,
            strictly,
            uniquely,
            collapse,
            adjoin,
            debug
        )

    cluster_counts = []
    for i, cluster in enumerate(sorted(clusters, key=_cluster_trg_pos)):
        cluster_counts.append(0)
        for member in cluster:
            cluster_counts[-1] += member.num_members

            outfile.write(formatf(
                record=str(member),
                cluster=i+1,
                members=member.num_members
            ))
            
    cluster_counts.sort(reverse=True)
    cluster_counts_tot = sum(cluster_counts)
    cluster_counts_mid = 0.5 * cluster_counts_tot
    cluster_counts_sum = 0
    cluster_counts_N50 = 0
    while cluster_counts_sum < cluster_counts_mid:
        cluster_counts_sum += cluster_counts[cluster_counts_N50]
        cluster_counts_N50 += 1

    try:
        cluster_counts_L50 = cluster_counts[cluster_counts_N50-1]
    except IndexError:
        cluster_counts_L50 = 0
        
    err.write("Initial members: %d\n" % initial_counts_tot)
    err.write("Cluster members: %d\n" % cluster_counts_tot)
    err.write("Cluster count: %d\n" % num(cluster_counts))
    err.write("Cluster N/L50: %d/%d\n" % (  
        cluster_counts_N50, cluster_counts_L50
    ))
    

if __name__ == '__main__':
    main(sys.argv[1:])

