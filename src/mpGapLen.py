
import os
import re
import sys
import math
import pysam
import getopt
import collections

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Use read pairs to re-estimate gap sizes'


PYTHONVERSION = sys.version_info[:2]
STRAND_MAP = {'FR':'+-', 'RF':'-+', 'RR':'--', 'FF':'++'}
VALID_PAIRS = set(('FR','RF','FF','RR'))
PROGRAM_NAME = os.path.basename(sys.argv[0])

num = len

class Gap(object):
    def __init__(self, start, end):
        self.start = int(start)
        self.end  = int(end)

    @property
    def length(self):
        return self.end - self.start
        
class BEDPE(object):
    def __init__(self, read=1):
        self.contig = 3 * (read - 1)
        self.start  = 3 * (read - 1) + 1
        self.end    = 3 * (read - 1) + 2
        self.name   = 6
        self.mapping_quality = 7
        self.strand = 8 + (read - 1)

class Library(object):
    def __init__(self, file, mean, stddev, strand):
        self.file = file
        self.mean = mean
        self.stddev = stddev
        self.strand = strand

class Region(object):
    def __init__(self, contig='.', start=-1, end=-1, strand='.', is_gap=False):
        self.contig = contig
        self.start = start
        self.end = end
        self.strand = strand
        self.is_gap = is_gap

    def __str__(self):
        return "%s:%d-%d(%s) <%s>" % (self.contig, self.start, self.end, self.strand, 'gap' if self.is_gap else 'contig')

    @property
    def length(self):
        if self.start < 0 or self.end < 0:
            return 0
        return self.end - self.start


def mapped_to(contig, read_start, read_end):
    """test whether self has any kind of overlap with other"""
    if contig.start <= read_start < contig.end:
        #           self.beg ========= self.end
        # other.beg ============== other.end
        # (b/c other.beg < other.end, also True for 'contains' queries)
        return True
    
    if contig.start < read_end <= contig.end:
        #           self.beg ========= self.end
        # other.beg ============== other.end
        # (b/c other.beg < other.end, also True for 'contains' queries)
        return True
    
    if read_start < contig.end <= read_end:
        # self.beg ========= self.end
        #      other.beg ============== other.end
        # (b/c self.beg < self.end, also True for 'contained' queries)
        return True
    
    return False
    
    
def read_matepair_conf(configfile):
    config_dict = collections.OrderedDict()
    config_file = open(configfile, 'r')  # same format as SSPACE
    for line in config_file:
        # LIB_GRP IGNORED LIB_FILE LIB_MEAN LIB_SD LIB_ORI
        if line.isspace() or line.startswith('#'):
            continue
        field = line.rstrip().split()
        field[3] = float(field[3])
        field[4] = float(field[4])
        field[5] = field[5].upper()
        if field[5] not in VALID_PAIRS:
            raise ValueError("Invalid pair orientation: %s" % field[5])

        if field[0] not in config_dict:
            config_dict[field[0]] = []
            
        config_dict[field[0]].append(
            Library(pysam.TabixFile(field[2], parser=pysam.asTuple()),
                    field[3], field[4], STRAND_MAP[field[5]])
        )
    config_file.close()
        
    return config_dict



def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else 'ERROR: %s\n\n' % str(message)
    stream.write("""
Program: %s (%s)
Version: %s %s
Contact: %s

Usage:   %s [options] <genome.fasta> <lib.conf> <output-prefix>

Options: -q <uint>  Minimum mapping quality [1]
         -k <uint>  Minimum linking pairs  [3]
         -Z <uint>  Z-score over which pairs are ignored [inf]
         -S         Write output in SSPACE .evidence format
         -W         Calc gap length weighted by mapping quality
         -w         Calc gap length weighted by lib variance
         -h         This help message

Notes:
  
    1. lib.conf is similar to that from SSPACE, with columns:
       <LibID> <BEDPE> <FileName> <LibMean> <LibStdev> <LibPairOrientation>
    
    2. Input paired-end/mate-pair files must be in BEDPE format with upstream
       read occupying columns 1-3 and downtream read occupying columns 4-6, 
       the BEDPE must be sorted via `sort -k1,1 -k2,2n -k6,6n`, compressed
       using BGZIP, and indexed with Tabix (i.e., `tabix -0 -s1 -b2 -e6`)

%s
""" % (__program__, __purpose__, __pkgname__, __version__, __contact__,
       __program__, message))
    sys.exit(exitcode)

    

def read_scaffold_gaps(filename):
    nucleotides = re.compile('[acgturyswkmbdhvACGTURYSWKMBDHV]+')

    scaffolds = collections.OrderedDict()
    scaffold_file = pysam.FastxFile(filename)
    for scaffold in scaffold_file:
        prev = Region()

        if scaffold.name in scaffolds:
            raise ValueError("Duplicate reference name: %s" % scaffold.name)
        else:
            scaffolds[scaffold.name] = []

        for contig in nucleotides.finditer(scaffold.sequence):
            curr = Region(scaffold.name, contig.start(), contig.end())

            if prev.end > 0:
                this = Region(scaffold.name, prev.end, curr.start, is_gap=True)
                scaffolds[scaffold.name].append(prev)
                scaffolds[scaffold.name].append(this)
                    
            prev = curr

        scaffolds[scaffold.name].append(prev)
            
    scaffold_file.close()
            
    return scaffolds
    


def mpgaplen(scaffold_file, scaffolds, matepair_conf, outfile, outcheck, min_mapping_quality=0, min_linking_pairs=3, max_Z=None, weighted=False, sspace_format=False):
    read1 = BEDPE(read=1)
    read2 = BEDPE(read=2)

    scaffold_gap_nil = 0
    scaffold_gap_pos = 0
    scaffold_gap_neg = 0
    scaffold_gap_check = 0
    scaffold_gap_sized = 0
    scaffold_gap_total = 0
    scaffold_contig_total = 0
    for scaffold in scaffold_file:
        scaffold_length = len(scaffold.sequence)
        scaffold_contig_count = 1
        scaffold_gap_count = 0
        scaffold_gaps = []        
        
        for i in range(1, num(scaffolds[scaffold.name]), 2):
            assert not scaffolds[scaffold.name][i-1].is_gap, "Bug: Contig feature expected, got " + str(scaffolds[scaffold.name][i-1])
            assert     scaffolds[scaffold.name][i+0].is_gap, "Bug: Gap feature expected, got "    + str(scaffolds[scaffold.name][i+0])
            assert not scaffolds[scaffold.name][i+1].is_gap, "Bug: Contig feature expected, got " + str(scaffolds[scaffold.name][i+1])

            contig5p = scaffolds[scaffold.name][i-1]
            gap      = scaffolds[scaffold.name][i+0]
            contig3p = scaffolds[scaffold.name][i+1]

            scaffold_contig_count += 1
            scaffold_gap_count += 1
            
            num_links = 0
            num_check = 0
            gap_mean  = 0
            gap_sizes = []
            gap_weight = 0.0
            for group in matepair_conf:
                for library in matepair_conf[group]:
                    try:
                        spanning_pairs = library.file.fetch(scaffold.name, gap.start, gap.end)
                    except ValueError:
                        continue

                    for pair in spanning_pairs:
                        if pair[read1.contig] != pair[read2.contig]:
                            continue
                        if int(pair[read1.mapping_quality]) < min_mapping_quality:
                            continue
                        if ((pair[read1.strand] + pair[read2.strand]) != library.strand):
                            continue
                        
                        read1_start = int(pair[read1.start])
                        read1_end   = int(pair[read1.end])
                        read2_start = int(pair[read2.start])
                        read2_end   = int(pair[read2.end])

                        if read1_start < 0 or read1_end <= 0 or \
                           read2_start < 0 or read2_end <= 0:
                            continue
                        if read1_start >= gap.start or read2_end <= gap.end:
                            continue
                        if max_Z is not None and \
                           float(read2_end - read1_start - gap.length - library.mean) / library.stddev > max_Z:
                            continue

                        # Ideally we have fetch()'d only pairs that map to contigs adjacent
                        # to the gap, however this is not always the case (especially when 
                        # using fosmid-end and/or BAC-end pairs, which may span multiple
                        # contigs. We must, therefore explicitly filter for pairs that map
                        # to both of the two gap-flanking contigs:
                        if mapped_to(contig5p, read1_start, read1_end) and \
                           mapped_to(contig3p, read2_start, read2_end):
                            # Calculate gap size:
                            #                   |I|
                            #        ____--------------------____
                            # ================........=================
                            # |f.u| ^--------^       ^----------^ |f.d|
                            #                ^-------^
                            #                   |G|
                            #  |G| = |I| - |f.u| - |f.d|
                            gap_size = library.mean - (gap.start - read1_start + 1) - (read2_end - gap.end + 1)

                            pair_weight = 1.0
                            if weighted & 0x1:
                                pair_weight *= max(0.1, float(pair[read1.mapping_quality]))
                            if weighted & 0x2:
                                pair_weight *= 1.0 / float(pow(library.stddev, 2))
                            
                            gap_sizes.append((pair_weight, gap_size))
                            gap_weight += pair_weight
                            gap_mean  += pair_weight * gap_size
                            num_links += 1
                        else:
                            #elif mapped_to(contig5p, read1_start, read1_end) or \
                            # mapped_to(contig3p, read2_start, read2_end):
                            #
                            #        ____--------------------____
                            # ================...==...=================
                            #                     ^-small intervening contig
                            #
                            # This will prevent proper gap-closure, the user need to remove
                            # small, unlinked intervening contigs for best results
                            num_check += 1

                        
            if num_links >= min_linking_pairs:
                gap_mean /= gap_weight
                gap_sdev  = 0.0
                for gap_wt, gap_size in gap_sizes:
                    gap_sdev += gap_wt * pow(gap_size - gap_mean, 2)

                gap_sdev = pow(gap_sdev / float(max(1, gap_weight)), 0.5)
                gap_serr = gap_sdev / pow(num_links, 0.5)

                if -1.0 < gap_mean < 1.0:
                    scaffold_gap_nil += 1
                elif gap_mean > 0.0:
                    scaffold_gap_pos += 1
                elif gap_mean < 0.0:
                    scaffold_gap_neg += 1
                    
                scaffold_gaps.append([gap.start, gap.end, gap_mean, gap_serr, num_links])
                scaffold_gap_sized += 1

            elif num_check >= min_linking_pairs:
                outcheck.write("%s:%d-%d\t%d\n" % (
                    scaffold.name, gap.start+1, gap.end, num_check))

                scaffold_gaps.append([gap.start, gap.end, gap.length, 0.0, num_links])
                scaffold_gap_check += 1
                
            else:
                scaffold_gaps.append([gap.start, gap.end, gap.length, 0.0, num_links])
                
                
        if sspace_format:
            outfile.write(">%s|size%d|tigs%d\n" % (scaffold.name, scaffold_length, scaffold_contig_count))
            prev_end = 0
            for gap in scaffold_gaps:
                outfile.write("f_tig%d|size%d|links%d|gaps%d\n" % (
                    scaffold_contig_total+1, gap[0] - prev_end, gap[4], int(round(gap[2], 0))))
                prev_end = gap[1]
                scaffold_contig_total += 1
                scaffold_gap_total += 1
                
            outfile.write("f_tig%d|size%d\n\n" % (scaffold_contig_total+1, scaffold_length - prev_end))
            scaffold_contig_total += 1
            
        else:
            for gap in scaffold_gaps:
                outfile.write("%s\t%d\t%d\tGAP\t%0.1f\t%.3f\t%d\n" % (
                    scaffold.name, gap[0], gap[1], gap[2], gap[3], gap[4]))
                scaffold_contig_total += 1
                scaffold_gap_total += 1
                
            scaffold_contig_total += 1

            
    sys.stderr.write("[%s] Gaps successfully re-sized: %d (%0.2f%% of %d)\n" % (
        PROGRAM_NAME,
        scaffold_gap_sized,
        scaffold_gap_sized / float(max(1, scaffold_gap_total)) * 100.0,
        scaffold_gap_total
    ))
    sys.stderr.write("[%s]   Pos gaps: %d (%0.2f%%)\n" % (
        PROGRAM_NAME,
        scaffold_gap_pos,
        scaffold_gap_pos / float(max(1, scaffold_gap_sized)) * 100.0,
    ))
    sys.stderr.write("[%s]   Nil gaps: %d (%0.2f%%)\n" % (
        PROGRAM_NAME,
        scaffold_gap_nil,
        scaffold_gap_nil / float(max(1, scaffold_gap_sized)) * 100.0,
    ))
    sys.stderr.write("[%s]   Neg gaps: %d (%0.2f%%)\n" % (
        PROGRAM_NAME,
        scaffold_gap_neg,
        scaffold_gap_neg / float(max(1, scaffold_gap_sized)) * 100.0,
    ))    
    sys.stderr.write("[%s] Gaps requiring inspection: %d (%0.2f%%)\n" %(
        PROGRAM_NAME,
        scaffold_gap_check,
        scaffold_gap_check / float(max(1, scaffold_gap_total)) * 100.0,
    ))
    
    
    
def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'hSwWk:q:Z:', ('help',))
    except getopt.GetoptError as error:
        usage(error)

    if num(arguments) == 0:
        usage()

    max_Z = None  # <= +10 good for filtering spurious alignments
    sspace = False
    suffix = '.bed'
    weighted_mean = 0
    min_linking_pairs = 3
    min_mapping_quality = 1
    for flag, value in options:
        try:
            if   flag in {'-h', '--help'}: usage(exitcode=0)
            elif flag == '-k': min_linking_pairs = int(value)
            elif flag == '-q': min_mapping_quality = int(value)
            elif flag == '-W': weighted_mean |= 0x1
            elif flag == '-w': weighted_mean |= 0x2
            elif flag == '-Z': max_Z = float(value)
            elif flag == '-S':
                sspace = True
                suffix = '.evidence'
        except ValueError as error:
            usage(error)

    if min_linking_pairs <= 0:
        usage("Invalid value to '-k': %d" % min_linking_pairs)
    if min_mapping_quality < 0:
        usage("Invalid value to '-q': %d" % min_mapping_quality)
    if max_Z is not None and max_Z < 0.0:
        usage("Invalid value to '-Z': %s" % str(max_Z))
    if num(arguments) != 3:
        usage("Unexpected number of arguments")            

    gaps = read_scaffold_gaps(arguments[0])
    conf = read_matepair_conf(arguments[1])
    scaf = pysam.FastxFile(arguments[0])
    obed = open(arguments[2] + '.resized' + suffix, 'w')
    ochk = open(arguments[2] + '.inspect.intervals', 'w')

    sys.stderr.write("[%s] Command: %s %s\n" % (PROGRAM_NAME, PROGRAM_NAME, ' '.join(argv)))
    
    mpgaplen(scaf, gaps, conf, obed, ochk, min_mapping_quality, min_linking_pairs, max_Z, weighted_mean, sspace)

    scaf.close()
    obed.close()
    ochk.close()

    
    
if __name__ == '__main__':
    main(sys.argv[1:])
    
