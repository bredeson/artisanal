
import os
import sys

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Convert JBAT assembly file to MapTK join format'


class MalformedAssemblyFileError(BaseException):
    pass


def main(argv):
    if len(argv) != 2:
        sys.stderr.write("\n")
        sys.stderr.write("Program: %s (%s)\n" % (__program__, __purpose__))
        sys.stderr.write("Version: %s %s\n" % (__pkgname__, __version__))
        sys.stderr.write("Contact: %s\n" % __contact__)
        sys.stderr.write("\n")
        sys.stderr.write("Usage: %s <in.assembly> <gaplen>\n\n\n" % (
            __program__))
        sys.exit(1)

    assembly_file = open(argv[0], 'r')
    join_file = sys.stdout
    gap_size = int(argv[1])

    seen_head = False
    seen_body = False
    scaffold_index = 1    
    contig_name = dict()
    for line in assembly_file:
        if line.isspace() or line.startswith('#'):
            continue
        
        cols = line.rstrip().split()
        
        if cols[0].startswith('>'):
            contig_name[int(cols[1])] = cols[0].lstrip('>')
            seen_head = True

        elif not seen_head:
            raise MalformedAssemblyFileError("No cprops-style assembly file-header detected")
        
        else:
            gaplen = 0
            for index in cols:
                strand = '-' if index.startswith('-') else '+'
                index  = abs(int(index))

                join_file.write("Scaffold%d\t%d\t%s\t%s\n" % (
                    scaffold_index, gaplen, contig_name[index],strand))

                seen_body = True
                gaplen = gap_size

            scaffold_index += 1

    if not seen_body:
        raise MalformedAssemblyFileError("No asm-style assembly file-body detected")
    
    assembly_file.close()
    join_file.close()


if __name__ == '__main__':
    main(sys.argv[1:])

                

                
                
