
import os
import sys
import getopt
import collections

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Convert a MUMmer-like layout file to JBAT assembly format'


def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else '\nERROR: %s\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [-L] <in.cprops> <in.layout>\n" % (
        __program__))
    stream.write("\n")
    stream.write("%s\n" % message)
    sys.exit(exitcode)
                     

def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'hL', ['help',])
    except getopt.GetoptError as error:
        usage(error)
    
    if len(arguments) != 2:
        usage("Unexpected number of arguments")

    strict_layout = False
    for flag, value in options:
        if   flag in {'-h','--help'}: usage(exitcode=0)
        elif flag == '-L': strict_layout = True 
        
    cprops = open(arguments[0], 'r')
    layout = open(arguments[1], 'r')
    output = sys.stdout

    contig_dict = collections.OrderedDict()
    scaffold_list = []
    scaffold_indx = -1
    prev_scaffold = None
    for line in layout:
        if line.isspace() or \
           line.startswith('#'):
            continue
        
        fields = line.rstrip().split('\t')

        if fields[0] != prev_scaffold:
            scaffold_list.append([])
            scaffold_indx += 1

        scaffold_list[scaffold_indx].append((fields[1], fields[3]))
        prev_scaffold = fields[0]
        contig_dict[fields[1]] = 1
    
    is_assembly_file = False
    cprops_indx = 0
    cprops_dict = collections.OrderedDict()
    for line in cprops:
        if line.isspace() or \
           line.startswith('#'):
            continue
        
        line = line.strip()

        if line.startswith('>'): # .cprops header in .assembly file
            line = line.lstrip('>')
            is_assembly_file = True
        elif is_assembly_file:
            continue

        fields = line.split()
        if not strict_layout or fields[0] in contig_dict:
            cprops_indx += 1
            cprops_dict[fields[0]] = [cprops_indx, int(fields[2])]
            output.write(">%s %d %s\n" % (fields[0], cprops_indx, fields[2]))

    cprops.close()

    for scaffold in scaffold_list:
        sep = ''
        for contig, strand in scaffold:
            strand = -1 if strand == '-' else 1
            output.write("%s%d" % (sep, strand * cprops_dict[contig][0]))
            del(contig_dict[contig])
            sep = ' '
        output.write("\n")

    if not strict_layout:
        for contig in contig_dict:
            output.write("%d\n" % cprops_dict[ref][0])

    output.close()
    

if __name__ == '__main__':
    main(sys.argv[1:])
