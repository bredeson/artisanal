
import os
import sys
import pysam
import getopt
import logging
import tempfile
#import exceptions
import itertools
import subprocess
import collections
import multiprocessing
from intervals import BaseInterval
from fileutil import zopen
from fastxutil import format_fastx, format_fasta
from subprocess import check_call, CalledProcessError

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Patch errors in low-complexity regions of long-read assemblies'


BAMFILE = None
LOGGING_LEVEL  = logging.INFO
LOGGING_FORMAT = '%(asctime)s %(levelname)s  [' + \
                 __program__ + '] %(message)s'

MAFFT_EXECUTABLE = 'ginsi'

num = len
gap = '-'
empty = ''
space = ' '
nocall = 'n'
PYTHONVERSION = sys.version_info[:2]
if PYTHONVERSION < (3,0):
    range = xrange


class FileNotFoundError(EnvironmentError):
    pass
    
# ******************************************************************************
# this class and the following static function were taken from PacBio's
# Falcon assembler library falcon_kit.multiproc:
class PseudoPool(object):
    """Fake version of multiprocessing.Pool
    """
    def map(self, func, iterable, chunksize=None):
        return map(func, iterable)
    def imap(self, func, iterable, chunksize=None):
        return itertools.imap(func, iterable)
    def __init__(self, initializer=None, initargs=[], *args, **kwds):
        if initializer:
            initializer(*initargs)


def Pool(processes, *args, **kwds):
    """Pool factory.
    If 'not processes', return our FakePool;
    otherwise, a multiprocessing.Pool.
    """
    if processes:
        return multiprocessing.Pool(processes, *args, **kwds)
    else:
        return PseudoPool(*args, **kwds)

# ******************************************************************************

class BedInterval(BaseInterval):
    def __init__(self, chrom, beg, end, length=sys.maxsize):
        BaseInterval.__init__(self, chrom, beg, end)
        self.length = length
        assert 0 <= self.beg <= self.length, \
            "Beg coordinate out of range '%s'" % str(self)
        assert 0 <= self.end <= self.length, \
            "End coordinate out of range '%s'" % str(self)
        assert self.beg <= self.end, "End < Beg: '%s'" % str(self)

    @property
    def chr(self):
        return self.name

    def expand_width(self, width):
        return BedInterval(
            self.chr,
            max(self.beg - width, 0),
            min(self.end + width, self.length),
            self.length
        )

    
class SeqInterval(BaseInterval):
    def __init__(self, chrom, beg, end, sequence=''):
        BaseInterval.__init__(self, chrom, beg, end)
        self.sequence = sequence

    @property
    def chr(self):
        return self.name


def is_executable_regular_file(*filepath):
    filepath = os.path.sep.join(filepath)
    filename = os.path.basename(filepath)
    dirpath  = os.path.dirname(filepath)

    if dirpath is None or dirpath is empty:
        testpath = os.path.join(os.curdir, filename)
    else:
        testpath = filepath

    return os.path.isfile(testpath) and \
        os.access(testpath, os.F_OK) and \
        os.access(testpath, os.R_OK) and \
        os.access(testpath, os.X_OK)


def which(filepath, fatal=False):
    filename = os.path.basename(filepath)
    dirpath  = os.path.dirname(filepath)
    
    if dirpath is empty:
        if is_executable_regular_file(os.curdir, filename):
            return os.path.join(os.curdir, filename)

        # the following is NOT cross-platform compatible;
        # need to write in OS platform handling in the future
        # (for which os.name and platform.system() and
        # platform.release() may be useful)
        for dirpath in os.environ['PATH'].split(os.pathsep):
            if is_executable_regular_file(dirpath, filename):
                return os.path.join(dirpath, filename)

    elif os.path.isabs(filepath):
        if is_executable_regular_file(filepath):
            return filepath

    else:
        filepath = os.path.expanduser(filepath)
        filepath = os.path.abspath(filepath)
        if is_executable_regular_file(filepath):
            return filepath

    if fatal:
        raise FileNotFoundError(
            "%s does not exist or is not a regular, readable, and executable file" %(
                filename)
        )
    return None

        
def format_region_string(record):
    return "%s:%d-%d" % (record.chr, record.beg, record.end)


def align_sequences(unaligned_sequences):
    # Call mafft via pipe: write mafft input with this script,
    # read mafft output with this script and process results
    # (shell=True required, mafft executable is a shell script)
    #'--lop','-1.0',
    #'--op','0.5',

    mafft = subprocess.Popen(
        [
            which(MAFFT_EXECUTABLE, fatal=True),
            '--quiet',
            '--nuc',
            '--inputorder',
            '--maxiterate', '1000',
            '-'
        ],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=False  
    )
    mafft_output, mafft_errors = mafft.communicate(empty.join(unaligned_sequences))
    
    aligned_sequence = []
    aligned_sequences = collections.deque()
    for line in mafft_output.splitlines():
        line = line.strip()
        if line is empty:
            continue
        if line.startswith('>'):
            if aligned_sequence:
                aligned_sequences.append(
                    empty.join(aligned_sequence).replace(space, empty).lower()
                )
            aligned_sequence = []
        else:
            aligned_sequence.append(line)

    return aligned_sequences
            

def _call_consensus(alignments, col, i=0):
    char_counts = collections.OrderedDict(((char, 0) for char in (empty,'a','t','c','g')))
    for row in range(i, num(alignments)):
        char = alignments[row][col]
        if char is nocall:
            continue
        if char is gap:
            char = empty
        char_counts[char] += 1
        
    return max(char_counts, key=char_counts.get)


def call_consensus(input_data):
    inner_interval, outer_interval, max_depth = input_data

    consensus_interval = SeqInterval(
        inner_interval.chr,
        inner_interval.beg,
        inner_interval.end,
        None
    )
    
    alignment_region = BAMFILE.fetch(
        inner_interval.chr,
        inner_interval.beg,
        inner_interval.end,
        multiple_iterators=True
    )

    max_spans = max_depth
    max_colls = [max_depth, max_depth]
    spanning_sequences = collections.deque()
    collated_sequences = collections.deque()
    for record in alignment_region:
        if record.is_secondary or \
           record.is_duplicate or \
           record.is_supplementary:
            continue
        if max_spans == 0:
            break
        if ((record.reference_start - inner_interval.beg) <= -10 and
            (record.reference_end   - inner_interval.end) >=  10):
            spanning_sequences.append(
                format_fasta(
                    record.query_name,
                    record.query_alignment_sequence
                )
            )
            max_spans -= 1
            
        elif (((record.reference_end    > inner_interval.mid) and
               ((record.reference_start - inner_interval.beg) <= -10)) or
              ((record.reference_start  < inner_interval.mid) and
               ((record.reference_end   - inner_interval.end) >=  10))): 
            collated_sequences.append(
                (
                    record.query_name,
                    record.query_sequence,
                    int(inner_interval.beg <= record.reference_start)
                )
            )
            
                
    if num(spanning_sequences) > 0:
        unaligned_sequences = spanning_sequences
    elif num(collated_sequences) > 0:
        unaligned_sequences = collections.deque()
        for record in collated_sequences:
            if max_colls[record[2]] == 0:
                continue
            unaligned_sequences.append(
                format_fasta(
                    record[0],
                    record[1]
                )
            )
            max_colls[record[2]] -= 1
    else:
        return (consensus_interval, [])
           
    unaligned_sequences.appendleft(
        format_fastx(outer_interval)
    )

    aligned_sequences = align_sequences(unaligned_sequences)

    if num(aligned_sequences) < 2:
        return (consensus_interval, [])

    
    # calculate coordinates of target LCR on extracted sequence:
    # ---------==========---------
    # \o.beg   \i.beg    \i.end
    target_interval = BedInterval(
        inner_interval.chr,
        inner_interval.beg - outer_interval.beg,
        inner_interval.end - outer_interval.beg,
        len(outer_interval)
    )

    target_consensus = []
    target_position = 0
    target_sequence = aligned_sequences[0]
    for aligned_position, aligned_character in enumerate(target_sequence):
        if aligned_character == gap:
            if target_interval.beg <= target_position < target_interval.end:
                target_consensus.append(
                    _call_consensus(aligned_sequences, aligned_position, i=1)
                )
        else:
            if target_interval.beg <= target_position < target_interval.end:
                target_consensus.append(aligned_character)
            target_position += 1

    consensus_interval.sequence = empty.join(target_consensus)
            
    return (consensus_interval, aligned_sequences)


def distribute_intervals(inner_intervals, outer_intervals, max_depth=100):
    # 1. extract the reference region +/- max read length
    # 2. extract reads extending through region (+10 bp flank) and convert to fasta
    # 3. yield above as list of tuples: (seqname, sequence)
    
    for i in range(num(inner_intervals)):
        inner_interval = inner_intervals[i]
        outer_interval = outer_intervals[i]
        yield (inner_interval, outer_interval, max_depth)


def by_position(record):
    return record.beg


def run(bam_filename, fasta_file, bed_file, out_file, max_depth=-1, read_length=300, max_threads=1):
    references = collections.OrderedDict()
    for reference_name, reference_length in zip(fasta_file.references, fasta_file.lengths):
        references[reference_name] = BedInterval(
            reference_name,
            0,
            reference_length,
            reference_length
        )
    
    previous = None
    inner_intervals = {}
    outer_intervals = {}
    for line in bed_file:
        line = line.strip()

        if line is empty or \
           line.startswith('#'):
            continue

        field = line.split('\t')

        inner = BedInterval(
            field[0],
            int(field[1]),
            int(field[2]),
            references[field[0]].length
        )

        if previous is not None:
            if previous.chr == inner.chr and \
               previous.end >= inner.end:
                raise Exception(
                    "File not properly sorted or contains "+\
                    "overlapping intervals: %s" % (
                    bed_file.filename)
                )
            
        outer = inner.expand_width(read_length)
        outer = SeqInterval(
            outer.chr,
            outer.beg,
            outer.end,
            fasta_file.fetch(
                outer.chr,
                outer.beg,
                outer.end
            ).lower()
        )

        if inner.chr not in inner_intervals:
            inner_intervals[inner.chr] = []
            outer_intervals[outer.chr] = []

        inner_intervals[inner.chr].append(inner)
        outer_intervals[outer.chr].append(outer)
        previous = inner
        
        
    def InitializeWorker():
        logging.info("Initializing worker process (pid=%d)" % os.getpid())

    logging.info("In main process (pid=%d):" % os.getpid())

    pool = Pool(max_threads, initializer=InitializeWorker)
    for reference in references.values():
        logging.info("Traversing %s:" % reference.chr)
        try:
            inner = inner_intervals[reference.chr]
            outer = outer_intervals[reference.chr]
        except KeyError:
            inner = []
            outer = []

        sequence = []
        consensuses = []
        for consensus, alignments in pool.imap(call_consensus, distribute_intervals(inner, outer, max_depth)):
            logging.info("Locus complete: %s" % str(consensus))
            if num(alignments) > 0:
                for alignment in alignments:
                    logging.debug("ALN:  %s" % alignment)
                logging.debug("")
            else:
                logging.debug("ALN:  <none>")
                logging.debug("")
                
            if consensus.sequence is None:
                logging.debug("CNS:  <none> (= NA)")
                logging.debug("")
                continue
            else:
                logging.debug("CNS:  %s (= %+d)" % (consensus.sequence, len(consensus.sequence)-len(consensus)))
                logging.debug("")
                
            consensuses.append(consensus)

        if num(consensuses) > 0:
            for consensus in sorted(consensuses, key=by_position):
                sequence.append(fasta_file.fetch(reference.chr, reference.beg, consensus.beg))
                sequence.append(consensus.sequence)
                reference.beg = consensus.end

        if reference.beg < reference.end:
            sequence.append(fasta_file.fetch(reference.chr, reference.beg, reference.end))

        out_file.write(
            format_fasta(
                reference.chr,
                empty.join(sequence),
                width=100
            )
        )
        
    logging.info("Finished")

    
    
def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else "ERROR: %s\n\n" % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("Usage:   %s [options] <in.bam> <in.fasta> <in.bed>\n" % __program__)
    stream.write("\n")
    stream.write("Options:\n")
    stream.write("  -l,--read-length <uint>  Length of longest read in BAM [300]\n")
    stream.write("  -m,--max-depth <uint>    Use at most <uint> reads [inf]\n")
    stream.write("  -o,--outfile <file>      Write output to file [stdout]\n")
    stream.write("  -t,--threads <uint>      Use at most <uint> threads [1]\n")
    stream.write("  -v,--verbose             Write more vebose output to stderr\n")
    stream.write("  -h,--help                This help page\n")
    stream.write("\n")
    stream.write(message)
    sys.exit(exitcode)


def main(argv):
    global BAMFILE
    try:
        options, arguments = getopt.getopt(argv, 'D:o:t:l:hv', (
            'read-length=',
            'max-depth=',
            'threads=',
            'outfile=',
            'verbose',
            'help',
        ))
    except getopt.GetoptError as error:
        usage(error)

    outfile = '-'
    max_depth = -1
    num_threads = 1
    read_length = 300
    LOGGING_LEVEL  = logging.INFO
    for flag, value in options:
        if   flag in ('-h','--help'): usage(exitcode=0)
        elif flag in ('-v','--verbose'): LOGGING_LEVEL = logging.DEBUG
        elif flag in ('-o','--outfile'): outfile = value
        elif flag in ('-t','--threads'): num_threads = int(value)
        elif flag in ('-l','--read-length'): read_length = int(value)
        elif flag in ('-D','--max-depth'): max_depth = int(value)

    if num(arguments) != 3:
        usage("Unexpected number of arguments")
    if num_threads < 1:
        usage("Number of threads must be a positive non-zero value")
    if max_depth == 0:
        usage("Max depth must be a positive non-zero value")
    if read_length < 1:
        usage("Read length must be a positive non-zero value")
        
    logging.basicConfig(
        stream=sys.stderr,
        level=LOGGING_LEVEL,
        format=LOGGING_FORMAT
    )

    logging.info("Command: %s" % (space.join(sys.argv)))
    
    which(MAFFT_EXECUTABLE, fatal=True)
        
    outfile = zopen(outfile, 'w')
    bedfile = zopen(arguments[2], 'r')

    fastafile = pysam.FastaFile(arguments[1])
    BAMFILE = pysam.AlignmentFile(arguments[0])
    
    run(arguments[0], fastafile, bedfile, outfile, max_depth, read_length, num_threads)

    fastafile.close()
    outfile.close()
    
if __name__ == '__main__':
    main(sys.argv[1:])

# --+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
#       10        20        30        40        50        60        70        80
