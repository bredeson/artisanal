
import os
import re
import sys
import getopt
import intervals
import collections

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Reference-guided scaffolding with syntenic genes'

_tab = '\t'
_null = '.'
_empty = ''
_colon = ':'
_comma = ','
_comment = '#'
_asterisk = '*'
_maxuint = sys.maxsize

num = len
digits = re.compile('(\d+)')

class BED(intervals.BaseInterval):
    def __init__(self, chr=_null, beg=-1, end=-1, strand=_null):
        intervals.BaseInterval.__init__(self, chr, beg, end)
        self.strand = strand

    @property
    def strand(self):
        return self._strand

    @strand.setter
    def strand(self, strand):
        self._strand = intervals.Strand(strand)
        
    @property
    def mid(self):
        return float(self.beg + self.end) / 2.0

    @property
    def chr(self):
        return self.name

    @chr.setter
    def chr(self, name):
        self.name = name

        
class BEDPE(object):
    def __init__(self, trg, qry, name=_null, score=0.0, index=None, num_members=0):
        self.trg = trg
        self.qry = qry
        self.name = name
        self.score = score
        self.index = index
        self.num_members = num_members

    @property
    def strand(self):
        return intervals.Strand(self.trg.strand.int * self.qry.strand.int)
        
    def __str__(self):
        return "%s\t%d\t%d\t%s\t%d\t%d\t%s\t%.04f\t+\t%s" % (
            self.trg.chr,
            self.trg.beg,
            self.trg.end,
            self.qry.chr,
            self.qry.beg,
            self.qry.end,
            self.name,
            self.score,
            self.strand
        )

    def __eq__(self, other):
        return \
            self.trg == other.trg and \
            self.qry == other.qry and \
            self.name == other.name and \
            self.score == other.score

    __repr__ = __str__



def _naturalize(string):
    return tuple((int(x) if x.isdigit() else x for x in digits.split(string)))

    
def _record_qry_pos(record):
    return (record.qry.chr, record.qry.beg, record.qry.end)


def _record_trg_pos(record):
    return (record.trg.chr, record.trg.beg, record.trg.end)


def _record_qry_chr(record):
    return record.qry.chr


def _record_trg_chr(record):
    return record.trg.chr


def _record_trg_idx(record):
    return (record.trg, record.index[0])


def _record_qry_idx(record):
    return (record.qry, record.index[1])


def _record_index_trg_pos(record):
    return (record.index, record.trg.chr, record.trg.beg, record.trg.end)


def _cluster_num_members(cluster):
    return max(num(cluster), cluster[0].num_members)


def _cluster_trg_pos(cluster):
    return (_naturalize(cluster[0].trg.chr), cluster[0].trg.beg, cluster[-1].trg.end)
        

def _cluster_infer_strand(cluster, key=lambda x: (x, -1)):
    record0, idx0 = key(cluster[0])
    recordN, idxN = key(cluster[-1])
    if num(cluster) > 1:
        strand = sign(idxN - idx0)
        if not strand:
            strand = sign(recordN.end - record0.beg)
        return strand            
    else:
        return record0.strand.int


def sign(value):
    nvalue = float(value if value else 0)
    dvalue = float(value if value else 1)
    return int(nvalue / abs(dvalue))


def read_chrom_sizes(sizes_filename):
    sizes_file = open(sizes_filename, 'r')
    sizes = collections.OrderedDict()
    for line in sizes_file:
        line = line.strip()
        if line is _empty or \
           line.startswith(_comment):
            continue

        fields = line.split(_tab)

        if fields[0] in sizes:
            sys.stderr.write(
                "WARNING: Duplicate sequence detected, skipping '%s'\n" % (
                fields[0]))
            continue
        
        sizes[fields[0]] = int(fields[1])

    sizes_file.close()
    return sizes



def cluster_classified_records(records):
    prev = None
    clusters = []
    for curr in sorted(records, key=_record_index_trg_pos):
        if prev is None:
            clusters.append([curr])
        elif curr.index[0] == prev.index[0]:
            clusters[-1].append(curr)
        else:
            clusters.append([curr])
        
        prev = curr

    return clusters



def uniqify_clusters(clusters, max_trg_copies=1, max_qry_copies=1,
                     max_trg_overlap=_maxuint, max_qry_overlap=_maxuint):
    uniqified_clusters = []

    qry_intervals = {}
    trg_intervals = {}

    for cluster in sorted(clusters, key=_cluster_num_members, reverse=True):
        cluster_trg = BED(cluster[0].trg.chr,
                          min(map(lambda m: m.trg.beg, cluster)),
                          max(map(lambda m: m.trg.end, cluster)))
        cluster_qry = BED(cluster[0].qry.chr,
                          min(map(lambda m: m.qry.beg, cluster)),
                          max(map(lambda m: m.qry.end, cluster)))

        if cluster_trg.chr not in trg_intervals:
            trg_intervals[cluster_trg.chr] = \
                list((intervals.IntervalList() for i in range(max_trg_copies)))
        if cluster_qry.chr not in qry_intervals:
            qry_intervals[cluster_qry.chr] = \
                list((intervals.IntervalList() for j in range(max_qry_copies)))
            
        has_overlap = [-1, -1]
        for i in range(max_trg_copies):
            trg_overlap = trg_intervals[cluster_trg.chr][i].overlap_length(cluster_trg) 
            trg_overlap = float(trg_overlap) / len(cluster_trg)
            if trg_overlap <= max_trg_overlap:
                has_overlap[0] = i
                break

        for j in range(max_qry_copies):
            qry_overlap = qry_intervals[cluster_qry.chr][j].overlap_length(cluster_qry)
            qry_overlap = float(qry_overlap) / len(cluster_qry)
            if qry_overlap <= max_qry_overlap:
                has_overlap[1] = j
                break
            
        if has_overlap[0] >= 0 and has_overlap[1] >= 0:
            trg_intervals[cluster_trg.chr][has_overlap[0]].add(cluster_trg)
            qry_intervals[cluster_qry.chr][has_overlap[1]].add(cluster_qry)
            uniqified_clusters.append(cluster)

    return uniqified_clusters



def place_clusters(clusters):
    placed_clusters = []

    best = dict()
    hits = dict()
    for cluster in sorted(clusters, key=_cluster_num_members, reverse=True):
        qry = cluster[0].qry
        trg = cluster[0].trg
        key = (qry.chr, trg.chr)
        if qry.chr not in best:
            best[qry.chr] = (trg.chr, cluster[0].num_members, cluster[0].strand.int)
        if key not in hits:
            hits[key] = [0, 0, 0]
        hits[key][cluster[0].strand.int+1] += cluster[0].num_members

        num_members = sum(hits[key])
        if num_members > best[qry.chr][1]:
            best[qry.chr] = (
                trg.chr,
                num_members,
                max(range(num(hits[key])), key=lambda i: hits[key][i])-1
            )
        
    for cluster in clusters:
        qry = cluster[0].qry
        trg = cluster[0].trg
        if trg.chr == best[qry.chr][0] and \
           cluster[0].strand.int == best[qry.chr][2]:
            placed_clusters.append(cluster)

    return placed_clusters


def collapse_clusters(clusters):
    collapsed_clusters = []
    for cluster in clusters:
        cluster.sort(key=_record_trg_pos)
        trg_strand  = _cluster_infer_strand(cluster, key=_record_trg_idx)
        qry_strand  = _cluster_infer_strand(cluster, key=_record_qry_idx)
        num_members = _cluster_num_members(cluster)
        
        if trg_strand < 0 and qry_strand < 0:
            trg_strand = +1
            qry_strand = +1
            
        collapsed_clusters.append(
            [
                BEDPE(
                    BED(cluster[0].trg.chr,
                        min(map(lambda c: c.trg.beg, cluster)),
                        max(map(lambda c: c.trg.end, cluster)),
                        trg_strand
                    ),
                    BED(cluster[0].qry.chr,
                        min(map(lambda c: c.qry.beg, cluster)),
                        max(map(lambda c: c.qry.end, cluster)),
                        qry_strand
                    ),
                    name=_null,
                    score=sum(map(lambda c: c.score, cluster)) / num_members,
                    num_members=num_members
                )
            ]
        )
    return collapsed_clusters



def run(bedpe_file, trg_sizes, qry_sizes, min_gapsize=100,
        min_members=1, min_length=1, max_ncopies=(1, 1),
        max_overlap=(_maxuint, _maxuint), layout_only=False,
        asterisk=False, out=sys.stdout, err=sys.stderr, debug=False):
    clusters = []
    for line in bedpe_file:
        line = line.strip()

        if line is _empty or \
           line.startswith(_comment):
            continue

        field = line.split(_tab)
        
        if num(field) < 12:
            err.write("ERROR: Too few fields (=%d), 12 expected\n" % num(field))
            sys.exit(1)

        try:
            for i in range(2):
                chr = 3*i+0
                beg = 3*i+1
                end = 3*i+2
                field[beg] = int(field[beg])
                field[end] = int(field[end])
                assert field[beg] <= field[end]
                
            field[10] = int(field[10])
            field[11] = int(field[11])
            
        except IndexError:
            err.write("ERROR: Too few fields (=%d)\n" % num(field))
            sys.exit(1)
        except:
            err.write("ERROR: Invalid record: '%s'\n" % line)
            sys.exit(1)

        member = BEDPE(
            BED(field[0],
                field[1],
                field[2],
                field[8]
            ),
            BED(field[3],
                field[4],
                field[5],
                field[9]
            ),
            index=(field[10], field[10]),
            num_members=field[11]
        )

        if member.trg.chr is _null or member.trg.beg < 0 or member.trg.end < 0 or \
           member.qry.chr is _null or member.qry.beg < 0 or member.qry.end < 0:           
            continue

        clusters.append(member)
        
    clusters = cluster_classified_records(clusters)
    clusters = collapse_clusters(clusters)
    clusters = list(filter(
        lambda c: c[0].num_members >= min_members,
        clusters
    ))
    clusters = uniqify_clusters(
        clusters,
        max_trg_copies=max_ncopies[0],
        max_qry_copies=max_ncopies[1],
        max_trg_overlap=max_overlap[0],
        max_qry_overlap=max_overlap[1]
    )
    clusters = place_clusters(clusters)

    chain = 0
    chains = []
    qry_placd = dict()
    new_sizes = dict()
    clusters.sort(key=_cluster_trg_pos)
    for cluster in clusters:
        member = cluster[0]

        if qry_sizes[member.qry.chr] < min_length:
            continue
        if member.qry.chr in qry_placd:
            continue

        chain += 1

        if layout_only:
            chains.append(
                [
                    qry_sizes[member.qry.chr],
                    member.qry.chr,
                    qry_sizes[member.qry.chr],
                    '+',
                    0,
                    qry_sizes[member.qry.chr],
                    member.qry.chr,
                    qry_sizes[member.qry.chr],
                    member.strand.str,
                    0,
                    qry_sizes[member.qry.chr],
                    chain
                ]
            )
            new_sizes[member.qry.chr] = qry_sizes[member.qry.chr]
            qry_placd[member.qry.chr] = 1
        else:
            if member.trg.chr in new_sizes:
                new_sizes[member.trg.chr] += min_gapsize
            else:
                new_sizes[member.trg.chr] = 0

            chains.append(
                [
                    qry_sizes[member.qry.chr],
                    member.qry.chr,
                    qry_sizes[member.qry.chr],
                    '+',
                    0,
                    qry_sizes[member.qry.chr],
                    member.trg.chr,
                    0,
                    member.strand.str,
                    new_sizes[member.trg.chr],
                    new_sizes[member.trg.chr]+qry_sizes[member.qry.chr],
                    chain
                ]
            )
            new_sizes[member.trg.chr] += qry_sizes[member.qry.chr]
            qry_placd[member.qry.chr] = 1

    for qry_chr in qry_sizes:
        if qry_chr in qry_placd:
            continue

        chain += 1
        
        chains.append(
            [
                qry_sizes[qry_chr],
                qry_chr,
                qry_sizes[qry_chr],
                '+',
                0,
                qry_sizes[qry_chr],
                qry_chr,
                qry_sizes[qry_chr],
                '+',
                0,
                qry_sizes[qry_chr],
                chain
            ]
        )
        new_sizes[qry_chr] = qry_sizes[qry_chr]
        qry_placd[qry_chr] = 1
        
                
    for chain in chains:
        chain[7] = new_sizes[chain[6]]
        if chain[8] == '-':
            chain[9], chain[10] = new_sizes[chain[6]] - chain[10], new_sizes[chain[6]] - chain[9]
            if layout_only and asterisk:
                chain[6] = _asterisk + chain[6]
                
        out.write('chain ' + ' '.join(map(str, chain)) + '\n')
        out.write('%d\n\n' % chain[2])


        
def usage(message=None, exitcode=1, stream=sys.stderr):
    message = _empty if message is None else 'ERROR: %s\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] <input.bedpe> <trg.sizes> <qry.sizes>\n" % __program__)
    stream.write("\n")
    stream.write("Options:\n")
    stream.write("  -a,--asterisked           Asterisk rev comp'd query names with {-L}\n")
    stream.write("  -g,--min-gapsize <uint>   Minimum gap size [100]\n")
    stream.write("  -L,--layout-only          Order and orient queries along target only\n")
    stream.write("  -l,--min-length <uint>    Minimum sequence length to scaffold [1]\n")
    stream.write("  -m,--min-members <uint>   Minimum members to make a cluster [2]\n")
    stream.write("  -N,--max-clusters <uint>[,<uint>]\n")
    stream.write("                            Retain <uint> longest clusters per genomic\n")
    stream.write("                            position in target and query, respectively\n")
    stream.write("                            (requires either -a or -O) [1,1]\n")
    stream.write("  -O,--max-overlap <ufloat>,<ufloat>\n")
    stream.write("                            Discard a smaller cluster if it overlaps a larger one\n")
    stream.write("                            by more than <ufloat> fraction of the smaller's length\n")
    stream.write("                            in either target or query, respectively [1.0,1.0]\n")
    stream.write("  -o,--outfile <file>       Write output to <file> [stdout]\n")
    stream.write("  -D,--debug                Print debugging messages\n")
    stream.write("  -h,--help                 This help message\n")
    stream.write("\n")
    stream.write("%s\n" % message)
    sys.exit(exitcode)
    #----------------|----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    #                0         10         20       30        40        50        60        70        80

    
def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'haDg:l:Lm:N:o:O:', (
            'max-clusters=',
            'max-overlap=',
            'min-members=',
            'min-gapsize=',
            'min-length=',
            'layout-only',
            'asterisked',
            'outfile=',
            'debug',
            'help',
        ))
    except getopt.GetoptError as error:
        usage(error)

    debug = False
    errfile = None
    outfile = None
    asterisk = False
    min_length = 1
    min_members = 1
    min_gapsize = 100
    layout_only = False
    max_ncopies = (1,1)
    max_overlap = (_maxuint, _maxuint)
    for flag, value in options:
        if   flag in ('-h','--help'): usage(exitcode=0)
        elif flag in ('-D','--debug'): debug = True
        elif flag in ('-a','--asterisked'): asterisk = True
        elif flag in ('-g','--min-gapsize'): min_gapsize = int(value)
        elif flag in ('-L','--layout-only'): layout_only = True
        elif flag in ('-l','--min-length'): min_length = int(value)
        elif flag in ('-m','--min-members'): min_members = int(value)
        elif flag in ('-N','--max-clusters'): max_ncopies = value.split(_comma)
        elif flag in ('-O','--max-overlap'): max_overlap = value.split(_comma)
        elif flag in ('-o','--outfile'): outfile = value

    if num(arguments) == 0:
        usage()
    elif num(arguments) != 3:
        usage("Unexpected number of arguments, three expected")
    if min_members < 1:
        usage("--min-members must be greater than or equal to 1")
    if min_gapsize < 0:
        usage("--min-gapsize must be greater than or equal to 0")
    if min_length < 1:
        usage("--min-length must be greater than or equal to 1")
    if num(max_overlap) == 1:
        max_overlap = float(max_overlap[0])
        max_overlap = (max_overlap, max_overlap)
    elif num(max_overlap) == 2:
        max_overlap = tuple(map(float, max_overlap))
    else:
        usage("--max-overlap exepects two comma-separated fractional values")

    if num(max_ncopies) == 1:
        max_ncopies = int(max_ncopies[0])
        max_ncopies = (max_ncopies, max_ncopies)
    elif num(max_ncopies) == 2:
        max_ncopies = tuple(map(int, max_ncopies))
    else:
        usage("--max-clusters expects two comma-separated integer values")

    if max_overlap < (0.0, 0.0):
        usage("--max-overlap must be greater than or equal to 0")
    if max_ncopies < (1,1):
        usage("--max-clusters must be greater than 0")

    bedpe_file = open(arguments[0], 'r')
    trg_sizes = read_chrom_sizes(arguments[1])
    qry_sizes = read_chrom_sizes(arguments[2])

    if outfile is None:
        outfile = sys.stdout
        errfile = sys.stderr
    else:
        outfile = open(outfile, 'w')
        errfile = sys.stderr

    sys.stderr.write("Command: %s %s\n" % (__program__, ' '.join(argv)))
    run(bedpe_file,
        trg_sizes,
        qry_sizes,
        min_gapsize,
        min_members,
        min_length,
        max_ncopies,
        max_overlap,
        layout_only,
        asterisk,
        outfile,
        errfile,
        debug
    )
    sys.stderr.write("Finished\n")



if __name__ == '__main__':
    main(sys.argv[1:])
