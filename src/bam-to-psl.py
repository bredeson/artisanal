#!/usr/bin/env python

import os
import sys
import pysam

from getopt import getopt
from fileutil import zopen

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Convert SAM/BAM alignments to PSL format'


STRAND = ('+', '-')
CIGAR_EQV_OPERATOR = {
    pysam.CMATCH,
    pysam.CEQUAL
}
CIGAR_PAD_OPERATORS = {
    pysam.CHARD_CLIP,
    pysam.CSOFT_CLIP
}
CIGAR_ALN_OPERATORS = {
    pysam.CMATCH,
    pysam.CINS,
    pysam.CDEL,
    pysam.CREF_SKIP,
    pysam.CPAD,
    pysam.CEQUAL,
    pysam.CDIFF
}
CIGAR_QRY_OPERATORS = {
    pysam.CMATCH,
    pysam.CINS,
    pysam.CSOFT_CLIP,
    pysam.CHARD_CLIP,
    pysam.CEQUAL,
    pysam.CDIFF
}
CIGAR_REF_OPERATORS = {
    pysam.CMATCH,
    pysam.CDEL,
    pysam.CREF_SKIP,
    pysam.CEQUAL,
    pysam.CDIFF
}

CIGAR_MAP = {
    pysam.CMATCH    : 'M',
    pysam.CINS      : 'I',
    pysam.CDEL      : 'D',
    pysam.CREF_SKIP : 'N',
    pysam.CSOFT_CLIP: 'S',
    pysam.CHARD_CLIP: 'H',
    pysam.CPAD      : 'P',
    pysam.CEQUAL    : '=',
    pysam.CDIFF     : 'X',
    pysam.CBACK     : 'B',
}


num = len


def infer_align_length(aln):
    length = 0
    for cig in aln.cigartuples:
        if cig[0] in CIGAR_ALN_OPERATORS:
            length += cig[1]

    return length

            
def infer_query_length(aln):
    length = 0
    for cig in aln.cigartuples:
        if cig[0] in CIGAR_QRY_OPERATORS:
            length += cig[1]
    return length


def infer_query_5p_clip_length(aln):
    clip_length = 0
    for i in range(len(aln.cigartuples)):
        if aln.cigartuples[i][0] in CIGAR_PAD_OPERATORS:
            clip_length += aln.cigartuples[i][1]
        else:
            break
    return clip_length


def infer_query_3p_clip_length(aln):
    clip_length = 0
    for i in range(len(aln.cigartuples)):
        if aln.cigartuples[~i][0] in CIGAR_PAD_OPERATORS:
            clip_length += aln.cigartuples[~i][1]
        else:
            break
    return clip_length


format_psl = '\t'.join((
    '{matches:d}',
    '{misMatches:d}',
    '{repMatches:d}',
    '{nCount:d}',
    '{qNumInsert:d}',
    '{qBaseInsert:d}',
    '{tNumInsert:d}',
    '{tBaseInsert:d}',
    '{strand:s}',
    '{qName:s}',
    '{qSize:d}',
    '{qStart:d}',
    '{qEnd:d}',
    '{tName:s}',
    '{tSize:d}',
    '{tStart:d}',
    '{tEnd:d}',
    '{blockCount:d}',
    '{blockSizes:s}',
    '{qStarts:s}',
    '{tStarts:s}'
)).format

    
def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else "ERROR: %s\n\n\n" % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__,__purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__,__version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage: %s <in.bam> <out.psl>\n\n%s" % (
        os.path.basename(sys.argv[0]),
        message
    ))
    sys.exit(exitcode)


def main(argv):
    options, arguments = getopt(argv, 'h',('help',))
    for flag, value in options:
        if flag in {'-h','--help'}:
            usage(exitcode=0)
            
    if len(arguments) != 2:
        usage("Unexpected number of arguments")
    
    alignments = pysam.AlignmentFile(arguments[0])
    out = zopen(arguments[1], 'w')

    is_cigar_eqx = False
    ref_lengths = dict(zip(alignments.references, alignments.lengths))
    
    for record in alignments:
        if record.is_qcfail or \
           record.is_unmapped or \
           record.is_duplicate or \
           record.is_secondary:
            continue
        
        num_matches = 0
        num_mismatches = 0
        len_inserts = 0
        num_inserts = 0
        len_deletes = 0
        num_deletes = 0
        qry_length = infer_query_length(record)
        qry_strand = STRAND[int(record.is_reverse)]        
        qry_beg = infer_query_5p_clip_length(record)
        qry_end = qry_length - infer_query_3p_clip_length(record)
        qry_pos = qry_beg
        ref_beg = record.reference_start
        ref_pos = ref_beg
        qry_begs = []
        qry_ends = []
        ref_begs = []
        ref_ends = []
        blk_lens = []
        blk_len = 0
        for operation, length in record.cigartuples:
            if operation in CIGAR_EQV_OPERATOR:
                if operation == pysam.CEQUAL:
                    is_cigar_eqx = True
                num_matches += length
                qry_pos += length
                ref_pos += length
                blk_len += length
            elif operation == pysam.CDIFF:
                is_cigar_eqx = True
                num_mismatches += length
                qry_pos += length
                ref_pos += length                
                blk_len += length
            elif operation == pysam.CINS:
                qry_begs.append(qry_pos - blk_len)
                qry_ends.append(qry_pos)
                ref_begs.append(ref_pos - blk_len)
                ref_ends.append(ref_pos)
                blk_lens.append(blk_len)
                len_inserts += length
                num_inserts += 1                
                qry_pos += length
                blk_len = 0
            elif operation == pysam.CDEL:
                qry_begs.append(qry_pos - blk_len)
                qry_ends.append(qry_pos)
                ref_begs.append(ref_pos - blk_len)
                ref_ends.append(ref_pos)
                blk_lens.append(blk_len)
                len_deletes += length
                num_deletes += 1
                ref_pos += length
                blk_len = 0
            elif operation == pysam.CREF_SKIP:
                qry_begs.append(qry_pos - blk_len)
                qry_ends.append(qry_pos)
                ref_begs.append(ref_pos - blk_len)
                ref_ends.append(ref_pos)
                blk_lens.append(blk_len)
                len_deletes += length
                num_deletes += 1
                ref_pos += length
                blk_len = 0
            elif operation in CIGAR_PAD_OPERATORS:
                continue
            else:
                raise NotImplementedError(
                    "CIGAR: unsupported operation: '%s'" % operation
                )

        if blk_len:
            qry_begs.append(qry_pos - blk_len)
            qry_ends.append(qry_pos)
            ref_begs.append(ref_pos - blk_len)
            ref_ends.append(ref_pos)
            blk_lens.append(blk_len)

        if not is_cigar_eqx and record.has_tag('NM'):
            # NM tag includes insertions and deletions, subtract
            # those to get nucleotide match differences.
            num_mismatches = record.get_tag('NM') - len_deletes - len_inserts
            num_matches -= num_mismatches

        num_blocks = len(blk_lens)

        if num_matches < 0:
            raise ValueError("Parse error: [%s:%d-%d %s:%d-%d %s ]" % (
                record.query_name,
                record.query_alignment_start,
                record.query_alignment_end,
                record.reference_name,
                record.reference_start,
                record.reference_end,
                record.cigarstring
            ))
        if qry_strand == '-':
            #qry_pos = map(lambda p: qry_length - p, qry_ends)
            qry_beg, qry_end = qry_length - qry_end, qry_length - qry_beg

        qry_begs.append('')
        ref_begs.append('')
        blk_lens.append('')
        
        out.write(format_psl(
            matches=num_matches,
            misMatches=num_mismatches,
            repMatches=0,
            nCount=0,
            qNumInsert=num_inserts,
            qBaseInsert=len_inserts,
            tNumInsert=num_deletes,
            tBaseInsert=len_deletes,
            strand=qry_strand,
            qName=record.query_name,
            qSize=qry_length,
            qStart=qry_beg,
            qEnd=qry_end,
            tName=record.reference_name,
            tSize=ref_lengths[record.reference_name],
            tStart=record.reference_start,
            tEnd=record.reference_end,
            blockCount=num_blocks,
            blockSizes=','.join(map(str, blk_lens)),
            qStarts=','.join(map(str, qry_begs)),
            tStarts=','.join(map(str, ref_begs))
        ) + '\n')
            
            
main(sys.argv[1:])

