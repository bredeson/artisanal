
import os
import sys
import pysam
import getopt
import collections
from fileutil import zopen
from fastxutil import format_fastx, reverse_sequence

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Patch low-quality assembly sequence'


INTEGER_FIELDS = (1,2,4,5,7,8)

class Strand(object):
    def __init__(self, strand):
        if isinstance(strand, Strand):
            self.__strand = strand.int
        elif isinstance(strand, int):
            self.__strand = self._to_int(self._to_str(strand))
        elif isinstance(strand, str):
            self.__strand = self._to_int(strand)
        else:
            raise TypeError("Invalid strand type, int or str expected")

    def _to_int(self, strand):
        if strand == '.':
            return 0
        if strand == '-':
            return -1
        if strand == '+':
            return +1
        else:
            raise ValueError("Invalid strand value '%s'" % strand)

    def _to_str(self, strand):
        if strand == 0:
            return '.'
        if strand <  0:
            return '-'
        if strand >  0:
            return '+'
        else:
            raise ValueError("Invalid strand value '%s'" % str(strand))

    @property
    def int(self):
        return self.__strand

    @property
    def str(self):
        return self._to_str(self.__strand)

    def reverse(self):
        self.__strand *= -1


        
class GenomicInterval(object):
    def __init__(self, name='.', beg=-1, end=-1, strand='.', is_gap=False):
        self.name = name
        self.beg = beg
        self.end = end
        self.strand = Strand(strand)
        self.is_gap = is_gap
        
    def __str__(self):
        return "%s:%d-%d(%s) <%s>" % (
            self.name, self.beg, self.end, self.strand.str,
            'gap' if self.is_gap else 'contig')

    @property
    def length(self):
        if self.beg < 0 or self.end < 0:
            return 0
        return self.end - self.beg



class AlignedIntervalTriple(object):
    def __init__(self, gap, ref, qry, strand='.', cigar=None):
        self.gap = gap
        self.ref = ref
        self.qry = qry
        self.__cigar = cigar
        self.strand = Strand(strand)


def by_position(triple):
    return (triple.gap.name, triple.gap.beg)
        
def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else 'ERROR: %s\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage: %s [-x] <assembly.fa> <patches.fa> <patches.bed>\n\n%s\n" % (
        __program__, message))
    sys.exit(exitcode)

    
def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'hx', ('help',))
    except getopt.GetoptError as error:
        usage(error)
        
    if len(arguments) != 3:
        usage("Unexpected number of arguments")

    noclose = False
    for flag, value in options:
        if   flag in ('-h','--help'): usage(exitcode=0)
        elif flag == '-x': noclose = True
        
    ref_fasta = pysam.FastxFile(arguments[0])
    qry_fasta = pysam.FastaFile(arguments[1])
    patchfile = zopen(arguments[2])
    outfile = sys.stdout


    # Read in the patch.bed file to guide the splicing procedure
    # -----------------------------------------------------------
    patches = {}
    for line in patchfile:
        fields = line.rstrip().split('\t')

        chrom  = fields[0]
        for f in INTEGER_FIELDS:
            fields[f] = int(fields[f])
            
        # fields[0:3] are name, start, end of gap in ref;
        # fields[3:6] are name, start, end of ref patch edge
        # fields[5:9] are name, start, end of qry patch edge
        # fields[9] is strand
        
        if fields[3] == '.' or fields[4] < 0:
            continue

        patch = AlignedIntervalTriple(
            GenomicInterval(fields[0], fields[1], fields[2], '+'),
            GenomicInterval(fields[3], fields[4], fields[5], '+'),
            GenomicInterval(fields[6], fields[7], fields[8], fields[9]),
        )

        if chrom not in patches:
            patches[chrom] = []

        patches[chrom].append(patch)

        
    # Begin splicing the query assembly into the reference assembly
    # -------------------------------------------------------------
    
    for reference in ref_fasta:
        reference.quality = None
        reference.sequence = reference.sequence.upper()
        reference_length = len(reference.sequence)
        
        if reference.name not in patches:
            outfile.write(format_fastx(reference, width=100))
            continue

        # It is critical that patches are sorted by position
        patches[reference.name].sort(key=by_position)
        
        num_patches = len(patches[reference.name])

        prev = AlignedIntervalTriple(
            GenomicInterval(reference.name, len(reference.sequence), -1, '+'),
            GenomicInterval(reference.name, len(reference.sequence), -1, '+'),
            GenomicInterval(None, -1, -1, '.')
        )

        query_name = None
        query_sequence_fwd = None
        query_sequence_rev = None
        spliced_chains = collections.deque()
        spliced_sequence = collections.deque()
        for i in range(num_patches):
            j = num_patches - i - 1
            curr = patches[reference.name][j]
            # reference_start = patches[reference.name][j][1]
            # patch_name     = patches[reference.name][j][2]
            # patch_start    = patches[reference.name][j][3]
            # patch_end      = patches[reference.name][j][4]
            # patch_strand   = patches[reference.name][j][5]
            
            spliced_sequence.appendleft(reference.sequence[curr.ref.end:prev.ref.beg])
            
            if noclose:
                patch_sequence = ''
                if curr.qry.length - curr.ref.length < 0:  # deletion
                    if curr.gap.beg - curr.ref.beg >= 100:
                        patch_sequence = reference.sequence[curr.ref.beg:curr.gap.beg].lower() + 'n' * 100
                    else:
                        patch_sequence = reference.sequence[curr.ref.beg:curr.gap.end].lower()
                        
                else:
                    patch_sequence += reference.sequence[curr.gap.end:curr.ref.end]
                    if curr.qry.length - curr.ref.length < 100:
                        patch_sequence += 'N' * 100
                        curr.gap.beg = curr.gap.beg - 100 + (curr.qry.length - curr.ref.length)
                    else:
                        patch_sequence += 'N' * (curr.qry.length - curr.ref.length)

                    patch_sequence += reference.sequence[curr.ref.beg:curr.gap.beg]

                spliced_sequence.appendleft(patch_sequence)

            else:
                if curr.qry.name != prev.qry.name:
                    query_sequence_fwd = qry_fasta.fetch(curr.qry.name).lower()
                    query_sequence_rev = reverse_sequence(query_sequence_fwd)

                patch_sequence = ''
                if curr.qry.strand.str == '-':
                    patch_sequence = query_sequence_rev[curr.qry.beg:curr.qry.end]
                else:
                    patch_sequence = query_sequence_fwd[curr.qry.beg:curr.qry.end]
                
                spliced_sequence.appendleft(patch_sequence)

            prev = curr
                
        spliced_sequence.appendleft(reference.sequence[0:prev.ref.beg])

        reference.sequence = ''.join(spliced_sequence)

        outfile.write(format_fastx(reference, width=100))

    outfile.close()



if __name__ == '__main__':
    main(sys.argv[1:])
    
