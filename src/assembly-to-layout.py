
import os
import sys

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Convert JBAT assembly to MUMmer-like layout format'


class MalformedAssemblyFileError(BaseException):
    pass


def main(argv):
    if len(argv) != 1:
        sys.stderr.write("\n")
        sys.stderr.write("Program: %s (%s)\n" % (__program__, __purpose__))
        sys.stderr.write("Version: %s %s\n" % (__pkgname__, __version__))
        sys.stderr.write("Contact: %s\n" % __contact__)
        sys.stderr.write("\n")
        sys.stderr.write("Usage: %s <in.assembly>\n\n\n" % (__program__))
        sys.exit(1)

    assembly_file = open(argv[0], 'r')
    join_file = sys.stdout

    seen_head = False
    seen_body = False
    scaffold_index = 1    
    contig_name = dict()
    for line in assembly_file:
        if line.isspace() or line.startswith('#'):
            continue
        
        cols = line.rstrip().split()
        
        if cols[0].startswith('>'):
            contig_name[int(cols[1])] = [cols[0].lstrip('>'), int(cols[2])]
            seen_head = True

        elif not seen_head:
            raise MalformedAssemblyFileError("No cprops-style assembly file-header detected")
        
        else:
            for index in cols:
                strand = '-' if index.startswith('-') else '+'
                index  = abs(int(index))

                join_file.write("Scaffold%d\t%s\t%d\t%s\n" % (
                    scaffold_index, contig_name[index][0], contig_name[index][1], strand))

                seen_body = True
                
            scaffold_index += 1

    if not seen_body:
        raise MalformedAssemblyFileError("No asm-style assembly file-body detected")
        
    assembly_file.close()
    join_file.close()


if __name__ == '__main__':
    main(sys.argv[1:])

                

                
                
