#!/bin/bash
PROGRAM=$(basename $0 .sh);
PACKAGE='__PACKAGE_NAME__';
VERSION='__PACKAGE_VERSION__';
CONTACT='__PACKAGE_CONTACT__';
PURPOSE='A pipeline for generating .chain files between revised assemblies';


function log_info () { 
    printf "%s %s INFO  [$PROGRAM] $1" $(date '+%F %H:%M:%S,%3N') >>/dev/fd/2;
}

function log_warn () { 
    printf "%s %s WARN  [$PROGRAM] $1" $(date '+%F %H:%M:%S,%3N') >>/dev/fd/2;
}

function log_error () {
    printf "%s %s ERROR [$PROGRAM] $2.\n\n" $(date '+%F %H:%M:%S,%3N') >>/dev/fd/2;
    exit $1;
}

function error () {
    printf "Error: $2.\n\n" >&2;
    exit $1;
}

function check_depend () { 
    if [[ ! -e "$1" ]]; then
        log_error 1 "Missing dependency ($1), stage did not complete";
    fi
}

function check_exe () {
    if [ ! $(which "$1" 2>/dev/null) ]; then
        log_error 1 "External dependency '$1' not found in PATH"
    fi
}

function check_file () {
    if [ -e "$1" ]; then
        if [ -d "$1" ]; then
            if [[ ! -r "$1" || ! -x "$1" ]]; then
                log_error 1 "Directory not readable or executable: $1";
            fi
        elif [ -f "$1" ]; then
            if [[ ! -r "$1" ]]; then
                log_error 1 "File not readable: $1";
            fi
        else
            log_error 1 "Unexpected file type: $1";
        fi
    else
        log_error 1 "File or directory does not exist: $1";
    fi
}

function abspath () {
    if [ -z "$1" ]; then
	echo $(pwd);
    else
	file_path=$1;
	file_name=$(basename $file_path);
	file_path=$(dirname $file_path);
	post_path=""
	while [[ ! -d "${file_path}" ]]; do
	    post_path="/$(basename ${file_path})${post_path}";
	    file_path=$(dirname $file_path)
	done
	file_path=$(cd $file_path; pwd);
	file_path="${file_path}${post_path}";
	if [ $file_name != "." ]; then
	    file_path="${file_path}/${file_name}";
	fi
	echo $file_path;
    fi
}


if [[ $# -ne 3 ]]; then
#       10        20        30        40        50        60        70        80
#---+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    echo "
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage: $PROGRAM [options] <source.fa> <target.fa> <out-prefix>

Notes: 
  - UCSC Kent (https://hgdownload.soe.ucsc.edu/admin/exe/), BEDtools,
    SAMtools, and minimap2 are expected to be accessible via \$PATH.


" >>/dev/fd/2;
    exit 1;
#---+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
#       10        20        30        40        50        60        70        80
fi

commandline="$PROGRAM $@";

log_info "Command: $commandline\n";

missing_dependency=;
for exe in bedtools samtools minimap2 faToTwoBit bamToPsl liftUp pslToChain chainSort scaffold-contig-map; do
    if [[ ! $(which $exe 2>/dev/null) ]]; then
        echo "[$PROGRAM] ERROR: $exe not detected in PATH" >>/dev/fd/2;
        missing_dependency=1;
    fi
done
if [[ $missing_dependency ]]; then
    exit 1;
fi

sam_hardclip_to_softclip=$(cat <<EOL | tr '\n' ' ' | tr -s ' '
if (/^\@/) { 
  print; 
} else { 
  chomp();
  @F=split(/\t/);
  \$b=0; 
  \$e=0; 
  while (\$F[5] =~ s/^(\d+)[HS]//) { 
    \$b+=\$1; 
  } 
  while (\$F[5] =~ s/(\d+)[HS]$//) { 
    \$e+=\$1; 
  } 
  if (\$b) { 
    \$F[5]="\${b}S\$F[5]"; 
  } 
  if (\$e) { 
    \$F[5]="\$F[5]\${e}S"; 
  } 
  \$F[9]="*"; 
  print(join("\t",@F[0..10]),"\n"); 
}
EOL
)

source_compressed=;
source_fasta=$(abspath $1);
source_fasta_path=$(dirname $source_fasta);
source_fasta_name=$(basename $source_fasta);
source_prefix=$source_fasta_name;
source_prefix=$(echo $source_prefix | sed 's/\(.gz\|-gz\|.z\|-z\|_z\)$//');
if [ $source_prefix != $source_fasta_name ]; then
    source_compressed=1;
fi
source_prefix=$(echo $source_prefix | sed 's/.f\(asta\|nt\|na\|a\)$//');

check_file "$source_fasta"


target_compressed=;
target_fasta=$(abspath $2);
target_fasta_path=$(dirname $target_fasta);
target_fasta_name=$(basename $target_fasta);
target_prefix=$target_fasta_name;
target_prefix=$(echo $target_prefix | sed 's/\(.gz\|-gz\|.z\|-z\|_z\)$//');
if [ $target_prefix != $target_fasta_name ]; then
    target_compressed=1;
fi
target_prefix=$(echo $target_prefix | sed 's/.f\(asta\|nt\|na\|a\)$//');

check_file "$target_fasta"


output_prefix=$3
output_path=$(dirname $output_prefix);
output_name=$(basename $output_prefix);
mkdir -p $output_path 2>/dev/null;
check_file "$output_path"

output_path=$(cd $output_path; pwd);
output_prefix="$output_path/$output_name";


tmpfile_name="$output_name";  # -$$-$RANDOM;
tmpfile_path="$output_path/.$tmpfile_name";
tmpfile_prefix="$tmpfile_path/$tmpfile_name";

if [ ! -d "$tmpfile_path" ]; then
    mkdir -p $tmpfile_path 2>/dev/null;
fi
check_file "$tmpfile_path";

cd $tmpfile_path;


if [[ (! -s source.2bit) || (! -s source.sizes) ]]; then
    if [ $source_compressed ]; then
	gzip -c -d $source_fasta >source.fasta;
    else
	ln -sf $source_fasta source.fasta;
    fi
    source_prefix=source

    samtools faidx $source_prefix.fasta
    faToTwoBit \
        $source_prefix.fasta \
        $source_prefix.2bit.tmp \
    && mv $source_prefix.2bit.tmp $source_prefix.2bit;

    twoBitInfo \
        $source_prefix.2bit \
        $source_prefix.sizes.tmp \
    && mv $source_prefix.sizes.tmp $source_prefix.sizes;
fi
source_prefix=source

check_file "$source_prefix.fasta.fai"
check_file "$source_prefix.2bit"
check_file "$source_prefix.sizes"


if [[ (! -s target.2bit) || (! -s target.sizes) ]]; then
    if [ $target_compressed ]; then
	gzip -c -d $target_fasta >target.fasta;
    else
	ln -sf $target_fasta target.fasta;
    fi
    target_prefix=target
    
    samtools faidx $target_prefix.fasta
    faToTwoBit \
        $target_prefix.fasta \
        $target_prefix.2bit.tmp \
    && mv $target_prefix.2bit.tmp $target_prefix.2bit
        
    twoBitInfo \
        $target_prefix.2bit \
        $target_prefix.sizes.tmp \
    && mv $target_prefix.sizes.tmp $target_prefix.sizes

    twoBitInfo \
	$target_prefix.2bit \
	$target_prefix.gap.bed.tmp \
	-nBed \
    && mv $target_prefix.gap.bed.tmp $target_prefix.gap.bed;
fi
target_prefix=target

check_file "$target_prefix.fasta.fai"
check_file "$target_prefix.2bit"
check_file "$target_prefix.sizes"
check_file "$target_prefix.gap.bed"


bedtools complement \
    -i $target_prefix.gap.bed \
    -g $target_prefix.sizes \
> $target_prefix.ctg.bed.tmp \
&& mv $target_prefix.ctg.bed.tmp $target_prefix.ctg.bed;

check_file "$target_prefix.ctg.bed"


bedtools getfasta \
    -bed $target_prefix.ctg.bed \
    -fi  $target_prefix.fasta \
| fold -w 100 \
> $target_prefix.ctg.fasta.tmp \
&& mv $target_prefix.ctg.fasta.tmp $target_prefix.ctg.fasta

check_file "$target_prefix.ctg.fasta"



awk 'BEGIN{OFS="\t";FS="\t"} {if (ARGIND == 1) {lengths[$1]=$2} else {print $2,$1":"$2"-"$3,($3-$2),$1,lengths[$1]}}' \
    $target_prefix.sizes \
    $target_prefix.ctg.bed \
> $target_prefix.ctg.lift.tmp \
&& mv $target_prefix.ctg.lift.tmp $target_prefix.ctg.lift

check_file "$target_prefix.ctg.lift"


scaffold-contig-map \
    -C -q -t \
    -u unmatched.bed.tmp \
    -o matched.chain.tmp \
    $source_prefix.fasta \
    $target_prefix.fasta \
&& mv unmatched.bed.tmp unmatched.bed \
&& mv matched.chain.tmp matched.chain

check_file unmatched.bed
check_file matched.chain

bedtools getfasta \
    -bed unmatched.bed \
    -fi $target_prefix.fasta \
| fold -w100 \
> unmatched.fasta.tmp \
&& mv unmatched.fasta.tmp unmatched.fasta

check_file unmatched.fasta

if [[ -s unmatched.fasta ]]; then
    target_bases=$(cut -f2 $target_prefix.sizes | paste -s -d'+' - | bc);
    minimap2 \
	-t 4 -x asm5 \
	-a --secondary=no \
        -I $target_bases \
	$source_fasta \
	unmatched.fasta \
    | perl -ne "$sam_hardclip_to_softclip" \
    | samtools view -u - \
    | samtools sort \
	 -m1g \
	 -o unmatched.bam \
	 - \
    && samtools quickcheck unmatched.bam \
    && samtools index unmatched.bam

    check_file unmatched.bam.bai


    bamToPsl \
	-nohead \
	unmatched.bam \
	unmatched.tmp.psl \
    && mv unmatched.tmp.psl unmatched.psl

    check_file unmatched.psl


    liftUp \
	-pslQ \
	-nohead \
	unmatched.lift.tmp.psl \
	$target_prefix.ctg.lift \
	error \
	unmatched.psl \
    && mv unmatched.lift.tmp.psl unmatched.lift.psl

    check_file unmatched.lift.psl


    pslToChain \
	unmatched.lift.psl \
	unmatched.lift.tmp.chain \
    && mv unmatched.lift.tmp.chain unmatched.lift.chain

    check_file unmatched.lift.chain
    
else
    touch unmatched.lift.chain
fi

cat \
    matched.chain \
    unmatched.lift.chain \
| chainSort stdin stdout \
| tr -s '\n' \
| awk 'BEGIN{i=0;rs=""} {if ($1 == "chain") {$13=++i; print rs$0; rs="\n"} else {print}}' \
>final.chain \
&& cp final.chain $output_prefix.chain

check_file "$output_prefix.chain"  # && rm -r "$tmpfile_path";


log_info "Finished\n"


