
import os
import sys
import bisect

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_NAME__-__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Provide genomic interval searching support for __PACKAGE_NAME__'


num = len

class IntervalList(object):
    def __init__(self, intervals=[], setter=lambda x: x, getter=lambda x: x):
        self._set = setter
        self._get = getter
        self._len = 0  # an optimization
        self._length = 0
        self.intervals = []
        for interval in intervals:
            self.add(interval)

            
    def __getitem__(self, item):
        return self._get(self.intervals[item])

    
    def __delitem__(self, item):
        del(self.intervals[item])
        self._len = num(self.intervals)

        
    def __iter__(self):
        return iter(map(self._get, self.intervals))


    def __repr__(self):
        return str(self.intervals)
    
    
    def __len__(self):
        return self._len

    
    def __eq__(self, other):
        return self.intervals == other.intervals


    def __ne__(self, other):
        return self.intervals != other.intervals


    def __lt__(self, other):
        return ((self[0] < other[0]) or (self[0] == other[0] and self[-1] < other[-1]))

    
    def __le__(self, other):
        return self == other or self < other

    
    def __gt__(self, other):
        return ((self[0] > other[0]) or (self[0] == other[0] and self[-1] > other[-1]))


    def __ge__(self, other):
        return self == other or self > other
    
    
    @property
    def is_empty(self):
        return self._len == 0

    
    def add(self, interval):
        other = self._set(interval)
        
        if self.is_empty:
            self.intervals.append(other)
            
        else:
            # Using bisect.insort_right, all exact-match entries
            # will be inserted _after_ their match in the list;
            # bisect.insort_left would insert _before_
            bisect.insort_right(self.intervals, other)

        self._length += len(interval)
        self._len += 1
            
        return True

    
    def generic_search(self, interval, left=False):
        if left:
            return bisect.bisect_left(self.intervals, self._set(interval))
        else:
            return bisect.bisect_right(self.intervals, self._set(interval))

    
    def overlap_search(self, other):
        beg = 0
        end = num(self) - 1

        if beg > end:
            return -1
    
        while beg <= end:
            mid = (beg + end) // 2

            this = self[mid]
            
            if this.overlaps(other):
                # Our segment overlaps
                # another placed segment
                return mid

            elif this.beg >= other.end:
                # other is upstream, move mid in neg. direction
                end = mid - 1

            elif this.end <= other.beg:
                # other is downstream, move mid in pos. direction
                beg = mid + 1

            else:
                raise NotImplementedError(
                    "Indices (%d -- %d -- %d) unhandled: '%s', '%s'\n" % (
                        beg, mid, end, str(this), str(other))
                )

        return -1


    def overlap_range(self, others):
        """
        Returns a tuple containing the Pythonic 
        slice range of overlapping intervals on self.
        If input is a list, assumes items are sorted
        by common coordinate system.
        """
        if not hasattr(others, '__iter__'):
            others = (others,)

        beg = -1
        end = -1
        Len = num(self) - 1
        for other in others:
            index = self.overlap_search(other)
            if index < 0:
                continue

            b = index
            e = index
            if index > Len:
                raise IndexError("Out of bounds: %d > %d" % (index, Len))
        
            while b > 0:
                if not self[b-1].overlaps(other):
                    break
                b -= 1

            while e < Len:
                if not self[e+1].overlaps(other):
                    break
                e += 1
                
            if beg < 0 or b < beg:
                beg = b
            if end < 0 or e > end:
                end = e

        if 0 <= beg and end <= Len:
            return(beg, end+1)
        else:
            return(-1,-1)
        

    def overlap_length(self, others):
        """
        Returns the length of overlaps a query 
        Interval or IntervalList object has with
        this IntervalList object
        """
        if not hasattr(others, '__iter__'):
            others = (others,)

        Len = num(self) - 1
        overlap_length = 0
        for other in others:
            index = self.overlap_search(other)
            if index < 0:
                continue

            b = index
            e = index
            if index > Len:
                raise IndexError("Out of bounds: %d > %d" % (index, Len))

            overlap_length += other.overlap_length(self[index])
            while b > 0:
                this = self[b-1]
                if not other.overlaps(this):
                    break
                overlap_length += other.overlap_length(this)
                b -= 1

            while e < Len:
                this = self[e+1]
                if not other.overlaps(this):
                    break
                overlap_length += other.overlap_length(this)
                e += 1
                
        return overlap_length
            

    def overlap_fraction(self, others):
        """
        Returns the fraction of overlap a query 
        Interval or IntervalList object has with
        this IntervalList object
        """
        return self.overlap_length(others) / self._length
    
    
class Strand(object):
    def __init__(self, obj):
        if isinstance(obj, Strand):
            self._strand = obj._strand
        elif isinstance(obj, int):
            self._strand = obj // abs(obj) if obj else obj
        elif isinstance(obj, str):
            self._strand = self._to_int(obj)
        else:
            raise TypeError("Invalid strand type: int, str, or Strand obj expected")

    def _to_int(self, strand):
        if   strand == '.':
            return 0
        elif strand == '-':
            return -1
        elif strand == '+':
            return +1
        else:
            raise ValueError("Invalid strand value: `+', `-', or `.' expected")
        
    def __str__(self):
        if self._strand == 0:
            return '.'
        if self._strand < 0:
            return '-'
        if self._strand > 0:
            return '+'

    def __eq__(self, other):
        return self.int == other.int

    def __ne__(self, other):
        return self.int != other.int

    def __lt__(self, other):
        return self.int < other.int

    def __le__(self, other):
        return self == other or self < other
    
    def __gt__(self, other):
        return self.int > other.int

    def __ge__(self, other):
        return self == other or self > other
    
    @property
    def int(self):
        return self._strand

    @property
    def str(self):
        return str(self)
    
    def reverse(self):
        self._strand *= -1

        
class BaseInterval(object):
    def __init__(self, name, beg, end):
        self.name   = str(name)
        self.beg    = int(beg)
        self.end    = int(end)
        
    def __str__(self):
        return "%s:%d-%d" % (
            str(self.name), self.beg, self.end)

    def __repr__(self):
        return str(self)
    
    def __lt__(self, other):
        if self.name < other.name:
            return True
        
        if self.name == other.name:
            if self.beg < other.beg:
                return True
            if self.beg == other.beg:
                return self.end < other.end
        return False

    def __eq__(self, other):
        return self.name == other.name and \
            self.beg == other.beg and \
            self.end == other.end

    def __le__(self, other):
        return self < other or self == other

    def __gt__(self, other):
        if self.name > other.name:
            return True

        if self.name == other.name:
            if self.beg > other.beg:
                return True
            if self.beg == other.beg:
                return self.end > other.end
        return False

    def __ge__(self, other):
        return self > other or self == other

    def __len__(self):
        return self.end - self.beg

    @property
    def mid(self):
        return (self.beg + self.end) // 2

    string = __str__
    
    def overlaps(self, other):
        """test whether self has any kind of overlap with other"""
        if other.name == self.name:
            if other.beg < self.end <= other.end:
                # self.beg ========= self.end
                #      other.beg ============== other.end
                # (b/c self.beg < self.end, also True for 'contained' queries)
                return True

            elif self.beg < other.end <= self.end:
                #           self.beg ========= self.end
                # other.beg ============== other.end
                # (b/c other.beg < other.end, also True for 'contains' queries)
                return True

        return False
    
    def contains(self, other):
        """test whether self contains other"""
        if other.name == self.name:
            if self.beg <= other.beg and other.end <= self.end:
                return True

        return False

    def within(self, other):
        """test whether self is contained within other"""
        if other.name == self.name:
            if other.beg <= self.beg and self.end <= other.end:
                return True

        return False

    def overlaps_beg(self, other):
        """test whether self overlaps other.beg"""
        if other.name == self.name:
            if self.overlaps(other) and \
               other.beg < self.end < other.end:
                # self.beg ========= self.end
                #      other.beg ============== other.end
                return True

        return False

    def overlaps_end(self, other):
        """test whether self overlaps other.end"""
        if other.name == self.name:
            if self.overlaps(other) and \
               other.beg < self.beg < other.end:
                #           self.beg ========= self.end
                # other.beg ============== other.end
                return True

        return False

    def overlap_length(self, other):
        if not self.overlaps(other):
            return 0
            
        elif self.within(other):
            return self.end - self.beg
        
        elif self.contains(other):
            return other.end - other.beg
            
        elif self.overlaps_beg(other):
            return self.end - other.beg

        elif self.overlaps_end(other):
            return other.end - self.beg

        else:
            raise NotImplementedError("Unexpected overlap type")

        
    def overlap_fraction(self, other):
        return float(self.overlap_length(other)) / (self.end - self.beg)


class Interval(BaseInterval):
    pass
