#!/usr/bin/env python

import os
import re
import sys
import gzip
import bisect
import getopt
import logging
import collections

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Convert BAM to MUMmer .delta format'


# configure global logging:
logging.basicConfig(
    stream=sys.stderr,
    level=logging.INFO,
    format="[{0}] %(asctime)s [%(levelname)s] %(message)s".format(
        __program__)
)

try:
    # importing non-standard Python libraries:
    # (prefix module names with `_' to prevent pydoc API exposure)
    import pysam
except ImportError as error:
    logging.error("%s: %s" % (error.__class__.__name__, str(error)))
    sys.exit(1)
    

SELF_ALIGNED = False
MIN_MAPPING_QUALITY = 0
MIN_ALIGNED_ERROR = 0.8
MIN_ALIGNED_LENGTH = 1000
MIN_ALIGNED_CONCORD = 0.5
MIN_ALIGNED_IDENTITY = 0.95
MIN_CONTAIN_FRACTION = 0.95
MIN_ALIGNED_FRACTION = None
MIN_OVERLAP_FRACTION = 0.50
CIGAR_PADDING_OPERATOR = {4, 5}
QUERY_LENGTH_OPERATOR = {0, 1, 4, 5, 7, 8}
STRAND = ('+','-')
DEBUG = 0

num = len
format_debug_las = "  {0:s}: {1:s} ({2:d}) {3:s} ({4:d}) @ {5:.01f}% id, {6:.01f}% cov".format
format_debug_end = "    ({0:s}: {1:s} {2:s} {3:s} [pid: {4:0.2f}%, cov: {5:0.2f}%])".format    


class Strand(object):
    def __init__(self, obj):
        if isinstance(obj, Strand):
            self.__strand = obj.__strand
        elif isinstance(obj, int):
            self.__strand = obj // abs(obj) if obj else obj
        elif isinstance(obj, str):
            self.__strand = self.__to_int(obj)
        else:
            raise TypeError("Invalid strand type: int, str, or Strand obj expected")

    def __to_int(self, strand):
        if   strand == '.':
            return 0
        elif strand == '-':
            return -1
        elif strand == '+':
            return +1
        else:
            raise ValueError("Invalid strand value: '+', '-', or '.' expected")
        
    def __str__(self):
        if self.__strand < 0:
            return '-'
        if self.__strand > 0:
            return '+'
        if self.__strand == 0:
            return '.'
        
    @property
    def int(self):
        return self.__strand

    @property
    def str(self):
        return str(self)
    
    def reverse(self):
        self.__strand *= -1

        
class BaseInterval(object):
    def __init__(self):
        """
        Method class. To use these methods, the user must create
        a child class and define the following attributes at __init__:
        self.name : Name of sequence
        self.length : Length of unaligned sequence
        self.beg : The beginning/starting position of the alignment
        self.end : The ending/stopping position of the alignment
        self.aligned_length : Length of the aligned portion of the sequence
        """
        pass

    def __str__(self):
        return "%s:%d-%d" % (
            str(self.name),
            self.beg,
            self.end,
        )

    def __repr__(self):
        return str(self)
    
    def __lt__(self, other):
        if self.name < other.name:
            return True
        
        if self.name == other.name:
            if self.beg < other.beg:
                return True
            if self.beg == other.beg:
                return self.end < other.end
        return False

    def __eq__(self, other):
        return self.name == other.name and \
            self.beg == other.beg and \
            self.end == other.end

    def __le__(self, other):
        return self < other or self == other

    def __gt__(self, other):
        if self.name > other.name:
            return True

        if self.name == other.name:
            if self.beg > other.beg:
                return True
            if self.beg == other.beg:
                return self.end > other.end
        return False

    def __ge__(self, other):
        return self > other or self == other
    
    def overlaps(self, other):
        """test whether self has any kind of overlap with other"""
        if other.name == self.name:
            if other.beg < self.end <= other.end:
                # self.beg ========= self.end
                #      other.beg ============== other.end
                # (b/c self.beg < self.end, also True for 'within' queries)
                return True

            elif self.beg < other.end <= self.end:
                #           self.beg ========= self.end
                # other.beg ============== other.end
                # (b/c other.beg < other.end, also True for 'contains' queries)
                return True

        return False
    
    def contains(self, other):
        """test whether self contains other"""
        if other.name == self.name:
            if self.beg <= other.beg and other.end <= self.end:
                return True

        return False

    def within(self, other):
        """test whether self is contained within other"""
        if other.name == self.name:
            if other.beg <= self.beg and self.end <= other.end:
                return True

        return False

    def overlaps_beg(self, other):
        """test whether self overlaps 5'-end of other"""
        if other.name == self.name:
            if self.overlaps(other) and \
               other.beg < self.end < other.end:
                # self.beg ========= self.end
                #      other.beg ============== other.end
                return True

        return False

    def overlaps_end(self, other):
        """test whether self overlaps 3'-end other"""
        if other.name == self.name:
            if self.overlaps(other) and \
               other.beg < self.beg < other.end:
                #           self.beg ========= self.end
                # other.beg ============== other.end
                return True

        return False

    @property
    def p5_spur_length(self):
        return self.beg
    
    @property
    def p3_spur_length(self):
        return self.length - self.end    
    
    @property
    def alignable_length(self):
        if self._alignable_length < 0:
            return self.length
        else:
            return self._alignable_length

    @property
    def aligned_fraction(self):
        return float(self.aligned_length) / self.alignable_length
            
    def overlap_length(self, other):
        if self.overlaps(other):
            if self.within(other):
                return self.aligned_length
        
            elif self.contains(other):
                return other.aligned_length
            
            elif self.overlaps_beg(other):
                return self.end - other.beg

            elif self.overlaps_end(other):
                return other.end - self.beg

            else:
                raise AssertionError("Unhandled overlap type")
        else:
            return 0
            
    def overlap_fraction(self, other):
        return float(self.overlap_length(other)) / self.aligned_length

    
class AlignedInterval(BaseInterval):
    def __init__(self, name, length, beg, end):
        self.name   = name
        self.length = int(length)
        self.beg    = int(beg)
        self.end    = int(end)
            
    @property
    def aligned_length(self):
        return self.end - self.beg
    
        
class DeltaRecord(object):
    def __init__(self, qry, trg, length=-1, strand=0, matches=0, alignment=None):
        self.qry = qry
        self.trg = trg
        self.length = length
        self.strand = Strand(strand)
        self.matches = matches
        self.alignment = alignment
        
    @property
    def similarity(self):
        return float(self.matches) / max(1, self.length)


def _log_exception(exception, message, traceback):
    """
    Exception handler to redirect errors to the logging module
    """
    logging.error("%s: %s" % (exception.__name__, message))
    sys.exit(1)


def query_length(record):
    length = 0
    for cigar_op, cigar_len in record.cigartuples:
        if cigar_op in QUERY_LENGTH_OPERATOR:
            length += cigar_len
    
    return length


def query_5p_spurlen(record):
    spurlen = 0
    for cigar_op, cigar_len in record.cigartuples:
        if cigar_op not in CIGAR_PADDING_OPERATOR:
            break
        spurlen += cigar_len

    return spurlen


def query_3p_spurlen(record):
    spurlen = 0
    for cigar_op, cigar_len in reversed(list(record.cigartuples)):
        if cigar_op not in CIGAR_PADDING_OPERATOR:
            break
        spurlen += cigar_len
        
    return spurlen

    
    
def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else 'ERROR: %s\n\n\n' % message

    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] <in.bam> <trg.fa> <qry.fa>\n" % (
        os.path.basename(sys.argv[0])))
    stream.write("\n")
    stream.write("Options:\n")
    stream.write("    -q,--min-mapqual <uint>  Min. mapping quality [0]\n")
    stream.write("    -h,--help                This help message\n")
    stream.write("\n\n%s" % message)
    sys.exit(exitcode)
                     #----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
                     #        10        20        30        40        50        60        70        80


def main(argv):
    global MIN_MAPPING_QUALITY
    global MIN_ALIGNED_IDENTITY
    global MIN_ALIGNED_FRACTION
    global MIN_CONTAIN_FRACTION
    global MIN_OVERLAP_FRACTION
    global MIN_ALIGNED_CONCORD
    global MIN_ALIGNED_LENGTH
    global MIN_ALIGNED_ERROR
    global DEBUG

    short_options = 'hDq:'
    long_options = (
        'help',
        'debug',
        'min-mapqual='
    )
    
    try:
        options, arguments = getopt.getopt(argv, short_options, long_options)
    except getopt.GetoptError as error:
        usage(error)

    bamout = False
    contained_only = False
    for flag, value in options:
        try:
            if   flag in ('-h','--help'): usage(exitcode=0)
            elif flag in ('-D','--debug'): DEBUG += 1
            elif flag in ('-q','--min-mapqual'): MIN_MAPPING_QUALITY = abs(int(value))
        except ValueError:
            usage("Invalid value for %r option" % flag)
        
    if num(arguments) != 3:
        usage("Unexpected number of arguments")
    if DEBUG:
        logging.getLogger().setLevel(logging.DEBUG)        
    else:
        sys.excepthook = _log_exception
    
    logging.info("Starting")
    logging.info("Command: %s %s" % (sys.argv[0], ' '.join(argv)))

    inbam = pysam.AlignmentFile(arguments[0])
    trg_fna = pysam.FastaFile(arguments[1])
    qry_fna = pysam.FastaFile(arguments[2])
    delta = sys.stdout

    trg_lengths = collections.OrderedDict(zip(trg_fna.references, trg_fna.lengths))
    qry_lengths = collections.OrderedDict(zip(qry_fna.references, qry_fna.lengths))
    
    pairs = {}
    delta.write("%s %s\nNUCMER\n" % (arguments[1], arguments[2]))
    for trg_name in trg_lengths:
        trg_sequence = trg_fna.fetch(trg_name).upper()
        
        for record in inbam.fetch(trg_name):
            if record.cigarstring is None:
                continue
            if record.query_sequence is None:
                continue
            if record.mapping_quality < MIN_MAPPING_QUALITY:
                continue
            
            qry_sequence = record.query_sequence.upper()

            curr = DeltaRecord(
                AlignedInterval(
                    record.query_name,
                    qry_lengths[record.query_name],
                    query_5p_spurlen(record),
                    qry_lengths[record.query_name] - query_3p_spurlen(record)
                ),
                AlignedInterval(
                    trg_name,
                    trg_lengths[trg_name],
                    record.reference_start,
                    record.reference_end,
                ),
                length=0,
                strand=STRAND[int(record.is_reverse)],
                matches=0,
                alignment=[]
            )
            
            if curr.qry.name not in pairs:
                pairs[curr.qry.name] = {}
            if curr.trg.name not in pairs[curr.qry.name]:
                pairs[curr.qry.name][curr.trg.name] = []
                
            posmatch = 0
            negmatch = 0
            deletion = 0
            insrtion = 0
            align_count = 0
            align_length = 0
            align_operations = []
            qry_len = 0
            trg_len = 0
            qry_pos = 0
            trg_pos = record.reference_start
            qry_min = record.query_alignment_start  # 0-based
            qry_max = record.query_alignment_end    # 1-based
            vec = -1 if record.is_reverse else +1
            for qry_aln, trg_aln in record.get_aligned_pairs():
                qry_pos = qry_pos if qry_aln is None else qry_aln  # 0-based
                trg_pos = trg_pos if trg_aln is None else trg_aln  # 0-based

                if qry_pos < qry_min:
                    continue
                if qry_pos == qry_max:
                    break

                align_length += 1
                
                if trg_aln is None:
                    align_operations.append(-1 * (align_count+1))
                    align_count = 0
                    insrtion += 1
                    qry_len += 1
                elif qry_aln is None:
                    align_operations.append(+1 * (align_count+1))
                    align_count = 0
                    deletion += 1
                    trg_len += 1
                else:
                    qry_nt = qry_sequence[qry_pos]
                    trg_nt = trg_sequence[trg_pos]
                    align_count += 1
                    qry_len += 1
                    trg_len += 1
                    
                    if qry_nt == trg_nt:
                        posmatch += 1
                    else:
                        negmatch += 1

            # if record.is_reverse:
            #     align_operations.append(align_count)
            #     align_operations.reverse()
            #     align_operations[-1] = 0
            # else:
            align_operations.append(0)

            assert qry_len == record.query_alignment_length, \
                "Parse error: query lengths do not match: %s" % record.query_name
            assert trg_len == record.reference_length, \
                "Parse error: reference lengths do not match: %s" % record.query_name                        
            curr.alignment = align_operations
            curr.length = align_length
            curr.matches = posmatch
            
            if curr.strand.int < 0:
                curr.qry.beg, curr.qry.end = curr.qry.length - curr.qry.end, curr.qry.length - curr.qry.beg

            pairs[curr.qry.name][curr.trg.name].append(curr)


    for qry_name in qry_lengths:
        if qry_name not in pairs:
            continue
        for trg_name in sorted(pairs[qry_name]):
            delta.write(">%s %s %d %d\n" % (
                trg_name, qry_name, trg_lengths[trg_name], qry_lengths[qry_name]))

            for pair in sorted(pairs[qry_name][trg_name], key=lambda p: p.qry.beg):
                pair_qry_beg = pair.qry.beg + 1
                pair_qry_end = pair.qry.end
                
                if pair.strand.int < 0:
                    pair_qry_beg, pair_qry_end = pair_qry_end, pair_qry_beg

                delta.write("%d %d %d %d %d %d 0\n" % (
                    pair.trg.beg + 1, pair.trg.end,
                    pair_qry_beg, pair_qry_end,
                    pair.length - pair.matches,
                    pair.length - pair.matches
                ))
                for op in pair.alignment:
                    delta.write("%d\n" % op)


    qry_fna.close()
    trg_fna.close()
    inbam.close()
    delta.close()
    
    logging.info("Finished")

    

if __name__ == '__main__':
    main(sys.argv[1:])

