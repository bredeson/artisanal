
import os
import re
import sys
import pysam
import getopt
import hashlib
import collections

from fileutil import zopen
from fastxutil import format_fasta, format_fastx, reverse_sequence


__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Map identical sequence (sub)sets'


HELP_FLAGS = ('-h', '--help')
VERS_FLAGS = ('-v', '--version')

_empty = ''
_space = ' '
_null_str = ''
_null_chr = '.'
_null_pos = -1
_positive = '+'
_negative = '-'

_output_layout = 0x1
_format_layout = "{0}\t{1}\t{2:d}\t{3}\n".format
_output_bed3  = 0x8
_format_bed3  = "{0}\t{1:d}\t{2:d}\n".format
_output_bed6  = 0x0
_format_bed6  = "{0}\t{1:d}\t{2:d}\t{3}\t{4:d}\t{5}\n".format
_output_bedpe = 0x2
_format_bedpe = "{0}\t{1:d}\t{2:d}\t{3}\t{4:d}\t{5:d}\t.\t{6:d}\t+\t{7}\n".format
_output_chain = 0x4
_format_chain = "chain {9:d} {0} {1:d} + {2:d} {3:d} {4} {5:d} {6} {7:d} {8:d} {10:d}\n{9:d}\n\n".format

num = len

def _format_region(region):
    return "{0}:{1:d}-{2:d}".format(
        str(region[0]),
        int(region[1]),
        int(region[2])
    )


def usage(message=None, exitcode=1, stream=sys.stderr):
    message = _empty if message is None else 'ERROR: %s\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] <targets.fasta> <queries.fasta>\n" % (
        __program__))
    stream.write("\n")
    stream.write("Options: -o <file>     Write output to FILE [stdout]\n")
    stream.write("         -B            Write output in BEDPE format [BED]\n")
    stream.write("         -C            Write output in .chain format\n")
    stream.write("         -L            Write output in .layout format\n")
    stream.write("         -r            Write output query names as regions strings\n")
    stream.write("         -U            Unmatched contigs should be non-fatal\n")
    stream.write("         -u <file>     Write unmatched contigs to BED <file>\n")
    stream.write("         -D            Duplicate contigs should be non-fatal\n")
    # stream.write("         -d <file>     Write duplicate contigs to BED <file>\n")
    stream.write("         -t            Break target down into indv. contigs\n")
    stream.write("         -q            Break query down into indv. contigs\n")
    stream.write("         -h,--help     Print this help message and exit\n")
    stream.write("         -v,--version  Print version and exit\n")
    stream.write("\n")
    stream.write("%s\n" % message)
    sys.exit(exitcode)


def generate_hash(region, sequence, forward_hashes, reverse_hashes, duplicate_fatal=True):
    forward_hash = hashlib.md5(sequence.encode('ascii')).hexdigest()
    reverse_hash = hashlib.md5(reverse_sequence(sequence).encode('ascii')).hexdigest()
    if forward_hash in forward_hashes:
        sys.stderr.write("%s: Duplicate sequences: %s == %s\n" % (
            'Error' if duplicate_fatal else 'Warning',
            _format_region(forward_hashes[forward_hash]),
            _format_region(region)))
        if duplicate_fatal: sys.exit(1)
            
    elif reverse_hash in reverse_hashes:
        sys.stderr.write("%s: Duplicate sequences: %s == %s\n" % (
            'Error' if duplicate_fatal else 'Warning',
            _format_region(reverse_hashes[reverse_hash]),
            _format_region(region)))
        if duplicate_fatal: sys.exit(1)
            
    else:
        forward_hashes[forward_hash] = region
        reverse_hashes[reverse_hash] = region
    
    
def search_hash(region, sequence, forward_hashes, reverse_hashes, unmatched_fatal=True):
    target_hash = hashlib.md5(sequence.encode('ascii')).hexdigest()
    if target_hash in forward_hashes:
        return forward_hashes[target_hash], _positive
    elif target_hash in reverse_hashes:
        return reverse_hashes[target_hash], _negative
    else:
        sys.stderr.write("%s: Query does not match any targets: %s\n" % (
            'Error' if unmatched_fatal else 'Warning',
            _format_region(region)))
        if unmatched_fatal:
            sys.exit(1)
        else:
            return [_null_chr, _null_pos, _null_pos], _null_chr
            


def format_output(source_region,  target_region,
                  source_lengths, target_lengths,
                  source_strand,  output_format=0x0, chain_number=None, region_string=False):
    target_name = target_region[0]
    target_length = target_region[2] - target_region[1]
    if target_region[0] == _null_chr or \
       source_region[0] == _null_chr:
        if output_format & _output_bedpe:
            target_length = 0
        elif output_format & (_output_layout | _output_chain):
            return _null_str

    if region_string and target_region[0] != _null_chr:
        source_name = _format_region(source_region)
        target_name = _format_region(target_region)
        
    if output_format & _output_layout:
        return _format_layout(
            source_region[0],
            target_name,
            target_length,
            source_strand
        )
    elif output_format & _output_bedpe:
        return _format_bedpe(
            source_region[0],
            source_region[1],
            source_region[2],
            target_region[0],
            target_region[1],
            target_region[2],
            target_length,
            source_strand
        )
    elif output_format & _output_chain:
        if source_strand == _negative:
            target_region[1], target_region[2] = \
                target_lengths[target_region[0]] - target_region[2], \
                target_lengths[target_region[0]] - target_region[1]

        chain_number[0] += 1

        return _format_chain(
            source_region[0],
            source_lengths[source_region[0]],
            source_region[1],
            source_region[2],
            target_region[0],
            target_lengths[target_region[0]],
            source_strand,
            target_region[1],
            target_region[2],
            target_length,
            chain_number[0]
        )
    else:
        return _format_bed6(
            source_region[0],
            source_region[1],
            source_region[2],
            target_name,
            target_length,
            source_strand
        )

    
        
def contig_map(source_file, target_file, output_file,
               duplicate_file=None, unmatched_file=None,
               break_sources=False, break_targets=False,
               output_format=0, region_strings=False,
               duplicate_allowed=False, unmatched_allowed=False):

    nucleotides = re.compile('[acgturyswkmbdhvACGTURYSWKMBDHV]+')

    mappings = collections.OrderedDict()
    
    chain_number = [0]
    source_lengths = {}
    target_lengths = {}
    forward_hashes = {}
    reverse_hashes = {}
    duplicate_fatal = not duplicate_allowed
    unmatched_fatal = not unmatched_allowed
    for source in source_file:
        source.sequence = source.sequence.upper()
        source_length = len(source.sequence)
        source_lengths[source.name] = source_length

        mappings[source.name] = []
        
        if break_sources:
            for source_contig in nucleotides.finditer(source.sequence):
                source_region = [str(source.name), source_contig.start(), source_contig.end()]
                generate_hash(
                    source_region,
                    source_contig.group(0),
                    forward_hashes,
                    reverse_hashes,
                    duplicate_fatal
                )
            
        else:
            source_region = [str(source.name), 0, source_length]
            generate_hash(
                source_region,
                source.sequence,
                forward_hashes,
                reverse_hashes,
                duplicate_fatal
            )

    mappings[_null_chr] = []
            
    for target in target_file:
        target.sequence = target.sequence.upper()
        target_length = len(target.sequence)
        target_lengths[target.name] = target_length
        if break_targets:
            for target_contig in nucleotides.finditer(target.sequence):
                target_region = [
                    str(target.name),
                    int(target_contig.start()),
                    int(target_contig.end())
                ]
                source_region, source_strand = search_hash(
                    target_region,
                    target_contig.group(0),
                    forward_hashes,
                    reverse_hashes,
                    unmatched_fatal
                )
                
                mappings[source_region[0]].append((
                    source_region,
                    target_region,
                    source_strand
                ))
                if unmatched_file is not None and\
                   source_region[0] == _null_chr and \
                   source_region[1] == _null_pos and\
                   source_region[2] == _null_pos:
                    unmatched_file.write(
                        _format_bed3(
                            target_region[0],
                            target_region[1],
                            target_region[2]
                        )
                    )
        else:
            target_region = [target.name, 0, target_length]
            source_region, source_strand = search_hash(
                target_region,
                target.sequence,
                forward_hashes,
                reverse_hashes,
                unmatched_fatal
            )

            mappings[source_region[0]].append((
                source_region,
                target_region,
                source_strand
            ))
            if unmatched_file is not None and\
               source_region[0] == _null_chr and \
               source_region[1] == _null_pos and\
               source_region[2] == _null_pos:
                unmatched_file.write(_format_bed3(target_region))
                

    for source_name in mappings:
        for mapping in sorted(mappings[source_name], key=lambda m: m[0]):
            output_file.write(format_output(
                mapping[0],
                mapping[1],
                source_lengths,
                target_lengths,
                mapping[2],
                output_format,
                chain_number,
                region_strings
            ))

            

def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'BChvLDUtqro:u:',['help', 'version'])
    except getopt.GetoptError as message:
        usage(message)

    regstr = False
    outfile = '-'
    outformat = _output_bed6
    break_targets = False
    break_sources = False
    duplicate_file = None
    unmatched_file = None
    duplicate_nonfatal = False
    unmatched_nonfatal = False
    for flag, value in options:
        if   flag in HELP_FLAGS: usage(exitcode=0)
        elif flag in VERS_FLAGS: usage(exitcode=0)
        elif flag == '-o': outfile = str(value)
        elif flag == '-B': outformat = _output_bedpe
        elif flag == '-C': outformat = _output_chain
        elif flag == '-L': outformat = _output_layout
        elif flag == '-D': duplicate_nonfatal = True
        elif flag == '-d': duplicate_file = value
        elif flag == '-U': unmatched_nonfatal = True
        elif flag == '-u': unmatched_file = value
        elif flag == '-t': break_sources = True
        elif flag == '-q': break_targets = True
        elif flag == '-r': regstr = True
        
    if num(arguments) < 2:
        usage('Too few arguments')
    if num(arguments) > 2:
        usage('Too many arguments')
    if duplicate_file is not None:
        duplicate_nonfatal = True
    if unmatched_file is not None:
        unmatched_nonfatal = True

    source_file = pysam.FastxFile(arguments[0])  # mapping from
    target_file = pysam.FastxFile(arguments[1])  # mapping to
    output_file = zopen(outfile, 'w')

    u_file = zopen(unmatched_file, 'w') if unmatched_file else None
    d_file = zopen(duplicate_file, 'w') if duplicate_file else None
    
    contig_map(
        source_file,
        target_file,
        output_file,
        d_file,
        u_file,
        break_sources,
        break_targets,
        outformat,
        regstr,
        duplicate_nonfatal,
        unmatched_nonfatal
    )

    output_file.close()
    if duplicate_file is not None:
        d_file.close()
    if unmatched_file is not None:
        u_file.close()

        

if __name__ == '__main__':
    main(sys.argv[1:])
