
# NOTES:
# 1) requires _all_ features to have ID tag
# 2) exon and CDS features of coding loci _must_ have transcript/RNA Parent (not the gene)
PROGRAM=$(basename $0 .sh);
PACKAGE='__PACKAGE_NAME__';
VERSION='__PACKAGE_VERSION__';
CONTACT='__PACKAGE_CONTACT__';
PURPOSE='A pipeline for lifting GFF3 annotations from imperfect genomes';

function log_info () { 
    printf "%s %s INFO  [$PROGRAM] $1" $(date '+%F %H:%M:%S,%3N') >>/dev/fd/2;
}

function log_warn () { 
    printf "%s %s WARN  [$PROGRAM] $1" $(date '+%F %H:%M:%S,%3N') >>/dev/fd/2;
}

function log_error () {
    printf "%s %s ERROR [$PROGRAM] $2.\n\n" $(date '+%F %H:%M:%S,%3N') >>/dev/fd/2;
    exit $1;
}

function error () {
    printf "Error: $2.\n\n" >&2;
    exit $1;
}

function check_depend () { 
    if [[ ! -e "$1" ]]; then
        log_error 1 "Missing dependency ($1), stage did not complete";
    fi
}

function check_exe () {
    if [ ! $(which "$1" 2>/dev/null) ]; then
	log_error 1 "External dependency '$1' not found in PATH"
    fi
}

function check_file () {
    if [ -e "$1" ]; then
	if [ -d "$1" ]; then
	    if [[ ! -r "$1" || ! -x "$1" ]]; then
		log_error 1 "Directory not readable or executable: $1";
	    fi
	elif [ -f "$1" ]; then
	    if [[ ! -r "$1" ]]; then
		log_error 1 "File not readable: $1";
	    fi
	else
	    log_error 1 "Unexpected file type: $1";
	fi
    else
	log_error 1 "File or directory does not exist: $1";
    fi
}
	
archive=;
add_UTRs=;
add_SSCs=;
print_usage=;
num_threads=1;
max_tiled_dist=0;
commandline="$PROGRAM $@";
while [[ -n $@ ]]; do
    case "$1" in
        -p|--max-processes) shift; num_threads=$1;;
	-D|--max-tiled-dist) shift; max_tiled_dist=$1;;
	-A|--archive) archive=1;;
	-u|--add-UTRs) add_UTRs=1;;
	-s|--add-SSCs) add_SSCs=1;;
	-h|--help) print_usage=1;;
	*) break;;
    esac
    shift;
done


if [[ $print_usage ||  $# -ne 5 ]]; then
#       10        20        30        40        50        60        70        80
#---+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    echo "
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage: $PROGRAM [options] <input> <lift.chain> <source.fa> <target.fa> <out-prefix>
 
Options: 
  -A,--archive
       Upon successful execution, archive the run directory into a 
       .tar.gz file.

  -D --max-tiled-dist <uint>
       Top-level features with supplmentary mappings greater than this 
       length will be considered poorly mapped and not lifted to the 
       target genome, but will instead be output to the file of lost 
       features. (default: auto)

  -p --max-processes <uint>
       Max number of processing threads to use in child feature liftover
       step. (default=1)

  -s --add-SSCs
       Add explicit translational start_codon and stop_codon features to
       protein-coding loci (default: off)

  -u --add-UTRs
       Add explicit five_prime_UTR and three_prime_UTR features to protein
       -coding loci (default: off)

Notes: 
  - As of now, this version of $PROGRAM supports only GFF3 and BED input 
    files only.

  - UCSC Kent (https://hgdownload.soe.ucsc.edu/admin/exe/), Perl, and
    GNU parallel (https://www.gnu.org/software/parallel) are expected to 
    be accessible via \$PATH.


" >>/dev/fd/2;
    exit 1;
#---+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
#       10        20        30        40        50        60        70        80
fi 

log_info "Command: $commandline\n";

original_file=$1;
mapping_chain=$2;
source_fasta=$3;
target_fasta=$4;
output_prefix=$5;

check_file "$original_file";
check_file "$mapping_chain";
check_file "$source_fasta";
check_file "$target_fasta";

check_exe perl;
check_exe bedToPsl
check_exe faToTwoBit;
check_exe twoBitInfo;
check_exe gff3ToGenePred;
check_exe genePredToFakePsl;
check_exe genePredToGtf;
check_exe pslMap;
check_exe pslMapPostChain;
check_exe transMapPslToGenePred;
check_exe parallel;
#check_exe qbatch;


source_fasta_path=$(dirname $source_fasta);
source_fasta_path=$(cd $source_fasta_path; pwd);
source_fasta_name=$(basename $source_fasta);
source_fasta=$source_fasta_path/$source_fasta_name;
source_prefix=$(echo $source_fasta_name | sed -E 's/.f\(asta\|nt\|na\|a\)$//');

target_fasta_path=$(dirname $target_fasta);
target_fasta_path=$(cd $target_fasta_path; pwd);
target_fasta_name=$(basename $target_fasta);
target_fasta=$target_fasta_path/$target_fasta_name;
target_prefix=$(echo $target_fasta_name | sed -E 's/.f\(asta\|nt\|na\|a\)$//');

original_file_path=$(dirname $original_file);
original_file_path=$(cd $original_file_path; pwd);
original_file=$original_file_path/$(basename $original_file);

mapping_chain_path=$(dirname $mapping_chain);
mapping_chain_path=$(cd $mapping_chain_path; pwd);
mapping_chain_name=$(basename $mapping_chain);
mapping_chain=$mapping_chain_path/$mapping_chain_name;

host=$(hostname -f);
sys=$(uname -s);
ver=$(uname -r);
arch=$(uname -m);


log_info "Running $PROGRAM $VERSION as $USER@$host [$sys v$ver ($arch)]\n";
output_path=$(dirname $output_prefix);
output_name=$(basename $output_prefix);
if [ ! -d $output_path ]; then
    mkdir -p $output_path 2>/dev/null;
    check_depend "$output_path";
fi
output_path=$(cd $output_path; pwd);
output_prefix=$output_path/$output_name;


tmpfile_name=$output_name.$PROGRAM;  # -$$-$RANDOM;
tmpfile_path=$output_path/$tmpfile_name;
tmpfile_prefix=$output_path/$tmpfile_name/$tmpfile_name;

if [ ! -d "$tmpfile_path" ]; then
    log_info "Initializing workdir: $tmpfile_path\n";
    mkdir -p $tmpfile_path 2>/dev/null;
fi
check_depend "$tmpfile_path";
cd $tmpfile_path;


if [[ (! -s $source_prefix.2bit) || (! -s $source_prefix.sizes) ]]; then
    log_info "Linking and indexing source: $source_fasta_name\n";
    ln -sf $source_fasta $source_fasta_name;

    faToTwoBit \
	$source_fasta_name \
	$source_prefix.2bit.tmp \
    && mv $source_prefix.2bit.tmp $source_prefix.2bit;

    twoBitInfo \
	$source_prefix.2bit \
	$source_prefix.sizes.tmp \
    && mv $source_prefix.sizes.tmp $source_prefix.sizes;
fi
check_depend "$source_prefix.2bit";
check_depend "$source_prefix.sizes";


if [[ (! -s $target_prefix.2bit) || (! -s $target_prefix.sizes) ]]; then
    log_info "Linking and indexing target: $target_fasta_name\n";
    ln -sf $target_fasta $target_fasta_name;
    
    faToTwoBit \
	$target_fasta_name \
	$target_prefix.2bit.tmp \
    && mv $target_prefix.2bit.tmp $target_prefix.2bit
	
    twoBitInfo \
	$target_prefix.2bit \
	$target_prefix.sizes.tmp \
    && mv $target_prefix.sizes.tmp $target_prefix.sizes
fi
check_depend "$target_prefix.2bit";
check_depend "$target_prefix.sizes";


if [ ! -s "$mapping_chain_name" ]; then
    chainSort \
	$mapping_chain \
	stdout \
	| awk 'BEGIN{i=0} {if ($1 == "chain") {$13=++i} print $0}' \
	> $mapping_chain_name.tmp \
	&& mv $mapping_chain_name.tmp $mapping_chain_name;
fi
check_depend "$mapping_chain_name";


###BED-START###################################################################
if [ ${original_file##*.} == 'bed' ]; then

if [ ! -s "$tmpfile_prefix.source.map" ]; then
    awk 'BEGIN{FS="\t";OFS="\t"; i=0} {i++; print i,$4}' \
	$original_file >$tmpfile_prefix.source.map.tmp \
    && mv $tmpfile_prefix.source.map.tmp $tmpfile_prefix.source.map;
fi
check_depend "$tmpfile_prefix.source.map";


if [ ! -s "$tmpfile_prefix.source.bed" ]; then
    awk 'BEGIN{FS="\t";OFS="\t"; i=0} {i++; $4=i; print $0}' \
	$original_file >$tmpfile_prefix.source.bed.tmp \
    && mv $tmpfile_prefix.source.bed.tmp $tmpfile_prefix.source.bed;
fi
check_depend "$tmpfile_prefix.source.bed";


if [ ! -s "$tmpfile_prefix.source.psl" ]; then
    bedToPsl \
	$source_prefix.sizes \
	$tmpfile_prefix.source.bed \
	$tmpfile_prefix.source.psl.tmp \
    && mv $tmpfile_prefix.source.psl.tmp \
	  $tmpfile_prefix.source.psl;
fi
check_depend "$tmpfile_prefix.source.psl";


if [ ! -s "$tmpfile_prefix.target.lift.1.psl" ]; then
    pslMap \
	$tmpfile_prefix.source.psl \
	$mapping_chain_name \
	$tmpfile_prefix.target.lift.1.psl.tmp \
	-mapInfo=$tmpfile_prefix.target.lift.info \
	-chainMapFile \
	-swapMap \
    && mv $tmpfile_prefix.target.lift.1.psl.tmp \
	  $tmpfile_prefix.target.lift.1.psl;
fi
check_depend "$tmpfile_prefix.target.lift.1.psl";


if [ ! -s "$tmpfile_prefix.target.lift.2.psl" ]; then 
    pslMapPostChain \
	$tmpfile_prefix.target.lift.1.psl \
	$tmpfile_prefix.target.lift.2.psl.tmp \
    && mv $tmpfile_prefix.target.lift.2.psl.tmp \
	  $tmpfile_prefix.target.lift.2.psl;
fi
check_depend "$tmpfile_prefix.target.lift.2.psl";


if [ ! -s "$tmpfile_prefix.target.lift.2.bed" ]; then
    pslToBed \
	$tmpfile_prefix.target.lift.2.psl \
	$tmpfile_prefix.target.lift.2.bed.tmp \
    && mv $tmpfile_prefix.target.lift.2.bed.tmp \
	  $tmpfile_prefix.target.lift.2.bed;
fi
check_depend "$tmpfile_prefix.target.lift.2.bed";


if [ ! -s "$tmpfile_prefix.target.lost.list" ]; then
    awk 'BEGIN{FS="\t";OFS="\t"} {if ($10 == "") { print $1 }}' \
	$tmpfile_prefix.target.lift.info \
	| sort -k1,1V \
	| uniq \
	> $tmpfile_prefix.target.lost.list.tmp \
    && mv $tmpfile_prefix.target.lost.list.tmp \
	  $tmpfile_prefix.target.lost.list
fi
check_depend "$tmpfile_prefix.target.lost.list";


if [ ! -s "$tmpfile_prefix.lost.bed" ]; then
    perl -e \
'                                                                                                               
BEGIN
{
    open($LOST,"<'$tmpfile_prefix.target.lost.list'");
    open($NAME,"<'$tmpfile_prefix.source.map'");
    %Lost=map{chomp(); $_=>1}<$LOST>;
    %Name=map{chomp(); @f=split(/\t/); $f[0]=>$f[1]}<$NAME>;
    close($LOST);
    close($NAME);
}
while (<>) {
    if (/^\s*$/) { next }
    if (/^\#/) { next }
    chomp();    

    my @F = split(/\t/);

    if ($Lost{$F[3]}) {
        $F[3] = $Name{$F[3]};
        print(join("\t",@F),"\n");
    }
}
' $tmpfile_prefix.source.bed \
    > $tmpfile_prefix.lost.bed.tmp \
    && mv $tmpfile_prefix.lost.bed.tmp \
	  $tmpfile_prefix.lost.bed
fi
check_depend "$tmpfile_prefix.lost.bed";


if [ ! -s "$tmpfile_prefix.lift.bed" ]; then
    perl -e \
'
BEGIN
{
    open($SORT,"<'$target_prefix.sizes'");
    open($NAME,"<'$tmpfile_prefix.source.map'");
    %Sort=map{chomp(); @f=split(/\t/); $f[0]=>++$i}<$SORT>;
    %Name=map{chomp(); @f=split(/\t/); $f[0]=>$f[1]}<$NAME>;
    close($SORT);
    close($NAME);
    $i = 2**32;
}
while (<>) {
    if (/^\s*$/) { next }
    if (/^#/) { next }
    chomp();

    my @F = split(/\t/);

    $F[3] = $Name{$F[3]};

    if ($Sort{$F[0]}) {
        print(join("\t",$Sort{$F[0]},@F),"\n");
    } else {
        print(join("\t",$i,@F),"\n");
    }
}
' $tmpfile_prefix.target.lift.2.bed \
        | sort -k1,1n -k3,3n -k4,4n \
	| cut -f2-7 \
        > $tmpfile_prefix.lift.bed.tmp \
    && mv $tmpfile_prefix.lift.bed.tmp \
	  $tmpfile_prefix.lift.bed;
fi
check_depend "$tmpfile_prefix.lift.bed";


if [[ (-e $tmpfile_prefix.lift.bed) && (-e $tmpfile_prefix.lost.bed) ]]; then
    cp $tmpfile_prefix.lift.bed $output_prefix.lift.bed;
    cp $tmpfile_prefix.lost.bed $output_prefix.lost.bed;
fi
check_depend "$output_prefix.lift.bed";
check_depend "$output_prefix.lost.bed";

num_passed=$(cut -f4 $tmpfile_prefix.lift.bed | sort | uniq | wc -l);
num_failed=0;
num_lost=$(cut -f4 $tmpfile_prefix.lost.bed | sort | uniq | wc -l);

log_info "Loci mapped:\n";
log_info "  Loci passing liftover: $num_passed\n";
log_info "  Loci failing liftover: $num_failed\n";
log_info "Loci unmapped:   $num_lost\n";
log_info "Finished\n";


log_info "Finished\n";

    
###GFF3-START################################################################## 
elif [ ${original_file##*.} == 'gff3' ]; then
    
if [ $(head -1 $original_file | grep -cE '^##gff-version\s+3') -ne 1 ]; then
    log_error 1 "Malformed file, no `##gff-version 3` header detected: $original_file";
fi

mkdir -p features-on-source
mkdir -p features-on-target
mkdir -p liftover-chains


if [ ! -e "$tmpfile_path/liftover-chains.complete" ]; then
    log_info "Splitting chain file\n";
    perl -ane '
BEGIN
{
    our $FILE=\*STDOUT;
} 
{
    if ($F[0] eq "chain") { 
        close($FILE); 
        open($FILE,">'${tmpfile_path}'/liftover-chains/$F[12].chain")
    } 
    print($FILE $_); 
} 

END 
{
    close($FILE);
}' $mapping_chain_name \
    && touch "$tmpfile_path/liftover-chains.complete";
fi
check_depend "$tmpfile_path/liftover-chains.complete";


if [ ! -s "$tmpfile_prefix.toplevel.gff3" ]; then
    log_info "Splitting GFF file\n";
    perl -e \
'
BEGIN
{
    %Parent=();
    %Loci=();
    @Loci=();
    %Exons=();
    $attr_sep=" ";
    $ParentID="gene_id";
    $ChildID="transcript_id";
}

sub parse_attributes {
    my $attr_string = shift;
    my $sep = shift // "=";

    my $attr_hash = {};
    if ($attr_string eq ".") {
        return $attr_hash;
    }
    $attr_string =~ s/^\s+|\s+$//g;
    $attr_string =~ s/;$//;
    $attr_string =~ s/\s*;\s*/;/g;
    for my $keyval (split(/;/,$attr_string)) {
        if ($keyval =~ /$sep/) {
            my $idx = index($keyval, $sep);
            my $key = substr($keyval, 0, $idx);
            my $val = substr($keyval, $idx + 1);
            $attr_hash->{$key} = $val;
            $attr_hash->{$key} =~ s/\042//g;
        } else {
            my $key = $keyval;
            my $val = 1;
            $attr_hash->{$key} = $val;
        }
    }
    return $attr_hash;
}

sub set_attributes {
    my $attr_hash = shift;
    my @attr_list = ();
    for my $key (sort keys(%$attr_hash)) {
        my $val = $attr_hash->{$key};    
        push(@attr_list, "$key=$val");
    }
    
    if (@attr_list) {
        return join(";",@attr_list);
    } else {
        return ".";
    }
}

sub get_highest_level_parent {
    my $parents = shift;
    my $parent = shift;
        
    my $child = $parent;
    while (defined($parents->{$child})) {
        $parent = $parents->{$child};
        if ($parent eq $child) {
            return $parent;
        }
        $child = $parent;
    }
    return $parent;
}

while (<>) {
    if (/^\s*$/) { next }
    if (/^\##gff-version\s+(\d+)/) {
        if ($1 >= 3) {
	    $ParentID="Parent";
	    $ChildID="ID";
            $attr_sep="=";
        }
    }
    if (/^\#/) { next }

    my @F = split(/\t/);
    
    chomp($F[$#F]);
    
    $F[8] = parse_attributes($F[8], $attr_sep);
    if (defined($F[8]->{$ParentID})) {
        $Parent{$F[8]->{$ChildID}} = $F[8]->{$ParentID};
    } else {
        $Parent{$F[8]->{$ChildID}} = $F[8]->{$ChildID};
    }
    push(@Loci,\@F);
}

END {
    %has_children = ();
    open($ATTRS,">'$tmpfile_prefix.attr.tmp'");
    open($GENES,">'$tmpfile_prefix.toplevel.gff3.tmp'");
    print($GENES "##gff-version 3\n");
    for $field (@Loci) {
        $attrs = $field->[8];
        $parent = get_highest_level_parent(\%Parent, $attrs->{$ChildID});
	$attrs->{"original_region"} = "$field->[0]:$field->[3]-$field->[4]";
	$attrs->{"original_source"} = "$field->[1]";
	$attrs->{"original_type"}   = "$field->[2]";

	$write = 1;
	$Loci{$parent} //= [];
        if ($attrs->{$ChildID} eq $parent) {
            if ($field->[2] ne "gene") {
                $field->[2] =  "gene";
            }
	    $field->[8] = set_attributes($attrs);

	    print($ATTRS join("\t",$attrs->{$ChildID},$parent,$field->[8]),"\n");
	    print($GENES join("\t",@$field),"\n");
	    push(@{$Loci{$parent}}, join("\t",@$field));

	    $attrs->{$ParentID} = $attrs->{$ChildID};
            $attrs->{$ChildID} .= "!__TOPLEVEL__";
	    $field->[2] = "primary_transcript";

        } elsif ($attrs->{$ParentID} eq $parent) {
	    if ($field->[2] !~ /^(?:mRNA|ncRNA|rRNA|primary_transcript|transcript)/) {
	        $field->[2] = "transcript";
            }
            if ($attrs->{"original_type"} eq "exon") {
	        print($ATTRS join("\t",$attrs->{$ChildID}."!__DIRECTEXON__",$parent,set_attributes($attrs)),"\n");
                $attrs->{$ChildID} .= "!__DIRECTEXON__";
		$write = 0;
            }
	    $has_children{$parent} = 1;

        } else {
            if ($field->[2] !~ /^(?:exon|CDS|five_prime_UTR|three_prime_UTR)$/) {
                $field->[2] = "exon";
            }
	    $has_children{$parent} = 1;
        }
	$field->[8] = set_attributes($attrs);
	    
        print($ATTRS join("\t",$attrs->{$ChildID},$parent,$field->[8]),"\n") if $write;
	push(@{$Loci{$parent}}, join("\t",@$field));
    }
    for $locus (sort keys(%Loci)) {
        open($LOCUS,">features-on-source/$locus.gff3");
	print($LOCUS "##gff-version 3\n");
        for $feature (@{$Loci{$locus}}) {
	    if (! $has_children{$locus}) {
	        @field = split("\t",$feature);
		$attrs = parse_attributes($field[8], $attr_sep);
                if ($attrs->{$ChildID} =~ /__TOPLEVEL__/) {
                    $attrs->{"__SINGLETON__"} = 1;
                    $field[8] = set_attributes($attrs);
		    $feature  = join("\t",@field);
                    print($ATTRS join("\t",$attrs->{$ChildID},$locus,$field[8]),"\n");
                }
            }
	    print($LOCUS "$feature\n");
        }
	close($LOCUS);
    }
    close($ATTRS);
    close($GENES);
}
' $original_file \
    && mv $tmpfile_prefix.toplevel.gff3.tmp \
	  $tmpfile_prefix.toplevel.gff3 \
    && mv $tmpfile_prefix.attr.tmp \
	  $tmpfile_prefix.attr;
fi
check_depend "$tmpfile_prefix.toplevel.gff3";
check_depend "$tmpfile_prefix.attr";


if [ ! -s "$tmpfile_prefix.toplevel.source.gp" ]; then
    log_info "Lifting top-level features\n";
    gff3ToGenePred \
	$tmpfile_prefix.toplevel.gff3 \
	$tmpfile_prefix.toplevel.source.gp.tmp \
	-bad=$tmpfile_prefix.toplevel.source.bad \
	-allowMinimalGenes \
	-warnAndContinue \
    && mv $tmpfile_prefix.toplevel.source.gp.tmp \
	  $tmpfile_prefix.toplevel.source.gp;
fi
check_depend "$tmpfile_prefix.toplevel.source.gp";


if [ ! -s "$tmpfile_prefix.toplevel.source.psl" ]; then
    genePredToFakePsl \
	-chromSize=$source_prefix.sizes \
	$source_prefix.2bit \
	$tmpfile_prefix.toplevel.source.gp \
	$tmpfile_prefix.toplevel.source.psl.tmp \
	$tmpfile_prefix.toplevel.cds \
    && mv $tmpfile_prefix.toplevel.source.psl.tmp \
	  $tmpfile_prefix.toplevel.source.psl;
fi
check_depend "$tmpfile_prefix.toplevel.source.psl";


if [ ! -s "$tmpfile_prefix.toplevel.target.1.psl" ]; then
    pslMap \
	$tmpfile_prefix.toplevel.source.psl \
	$mapping_chain_name \
	$tmpfile_prefix.toplevel.target.1.psl.tmp \
	-mapInfo=$tmpfile_prefix.toplevel.info \
	-chainMapFile \
	-swapMap \
    && mv $tmpfile_prefix.toplevel.target.1.psl.tmp \
	  $tmpfile_prefix.toplevel.target.1.psl;
fi
check_depend "$tmpfile_prefix.toplevel.target.1.psl";


if [ ! -s "$tmpfile_prefix.toplevel.target.2.psl" ]; then
    pslMapPostChain \
	$tmpfile_prefix.toplevel.target.1.psl \
	$tmpfile_prefix.toplevel.target.2.psl.tmp \
    && mv $tmpfile_prefix.toplevel.target.2.psl.tmp \
	  $tmpfile_prefix.toplevel.target.2.psl
fi
check_depend "$tmpfile_prefix.toplevel.target.2.psl";


if [ ! -s "$tmpfile_prefix.toplevel.target.3.psl" ]; then
    # Select best mapping of top-level features to genome
    sort -k10,10V -k1,1nr $tmpfile_prefix.toplevel.target.2.psl |
        perl -ane '
BEGIN
{
    $SMAX='$max_tiled_dist';
    $smax=0;
    %tmax=();
    %seen=();
    @loci=();
} 
while (<>) {
    m/^#/ && next;
    chomp();

    my @F = split(/\t/); 

    if (! $seen{$F[9]}) {
        $F[18] =~ s/,$//;
        $F[19] =~ s/,$//;
        $F[20] =~ s/,$//;
        @blen = split(/,/,$F[18]);
        @sbeg = split(/,/,$F[19]);
        @tbeg = split(/,/,$F[20]);
        $tmax{$F[9]} = 0;
        for ($i = 1; $i < @blen; $i++) {
            $tdist = $tbeg[$i] - $tbeg[$i-1] - $blen[$i-1];
            $sdist = $sbeg[$i] - $sbeg[$i-1] - $blen[$i-1];
            if ($sdist > $smax) {
                $smax = $sdist;
            }
            if ($tdist > $tmax{$F[9]}) {
	        $tmax{$F[9]} = $tdist;
            }
        }
        push(@loci,\@F);
    }
    $seen{$F[9]} = 1;
}

END
{
    if ($SMAX <= 0) {
        $SMAX = 2 * $smax;
    }
    for $locus (@loci) {
        if ($tmax{$locus->[9]} <= $SMAX) {
            print(join("\t",@$locus),"\n");
        }
    }
}
' >$tmpfile_prefix.toplevel.target.3.psl.tmp \
    && mv $tmpfile_prefix.toplevel.target.3.psl.tmp \
	  $tmpfile_prefix.toplevel.target.3.psl;
fi
check_depend "$tmpfile_prefix.toplevel.target.3.psl";


if [ ! -s "$tmpfile_prefix.toplevel.target.3.bed" ]; then
    awk 'OFS="\t" {print $14,$16,$17,$10}' \
	$tmpfile_prefix.toplevel.target.3.psl \
	| sort -k1,1V -k2,2n -k3,3n \
	>$tmpfile_prefix.toplevel.target.3.bed.tmp \
    && mv $tmpfile_prefix.toplevel.target.3.bed.tmp \
	  $tmpfile_prefix.toplevel.target.3.bed;
fi
check_depend "$tmpfile_prefix.toplevel.target.3.bed";


# Collect best liftover chains for each locus by chain
# ID and distribute those chains into locus-specific
# files
if [ ! -s "$tmpfile_prefix.locus-chains.list" ]; then
    perl -F"\t" -ane '
BEGIN
{
    open($FILE,"'$tmpfile_prefix.toplevel.target.3.psl'"); 
    %Chain=();
    %Target=();
    %Strand=();
    map{chomp();@f=split(/\t/); $Target{$f[9]}=$f[13]; $Strand{$f[9]}=$f[8]} <$FILE>
} 
{ 
    chomp($F[$#F]); 
    if (defined($Target{$F[0]}) && ($F[20] eq $Target{$F[0]})) { 
        $Chain{$F[0]}{$F[16]}=$F[24];
    }
} 
END 
{ 
    for $locus (keys(%Chain)) { 
        $chain = $Chain{$locus}; 
        print(join("\t",$locus, $Strand{$locus}, join(",",sort {$chain->{$b}<=>$chain->{$a}} keys(%$chain)),"\n") )
    }
}
'   $tmpfile_prefix.toplevel.info \
    | sort -k1,1V -k2,2V \
    >$tmpfile_prefix.locus-chains.list.tmp \
    && mv $tmpfile_prefix.locus-chains.list.tmp \
	  $tmpfile_prefix.locus-chains.list;
fi
check_depend "$tmpfile_prefix.locus-chains.list";


utr_option='';
if [ $add_UTRs ]; then
    utr_option='-utr';
fi

ssc_option="
awk '{if ((\$3 == \"start_codon\") || (\$3 == \"stop_codon\")) { next } print }' \\
    features-on-target/\$prefix/\$prefix.lift.gtf >features-on-target/\$prefix/\$prefix.lift.gtf.tmp \\
    && mv features-on-target/\$prefix/\$prefix.lift.gtf.tmp features-on-target/\$prefix/\$prefix.lift.gtf
";
if [ $add_SSCs ]; then
    ssc_option='';
fi


if [ ! -x "$tmpfile_path/lift.sh" ]; then
#TODO: Add --max-intron-length parameter to disallow introns greater than 1Mb (as default) apart
cat <<EOF >$tmpfile_path/lift.sh;
#!/bin/bash

prefix=\$1;
chains=\$2;

mkdir -p features-on-target/\$prefix;

echo \$chains | tr ',' '\n' | while read chain_id; do
    cat liftover-chains/\$chain_id.chain;
done >features-on-target/\$prefix/\$prefix.chain

cp features-on-source/\$prefix.gff3 features-on-target/\$prefix/\$prefix.orig.gff3;

gff3ToGenePred \\
    features-on-target/\$prefix/\$prefix.orig.gff3 \\
    features-on-target/\$prefix/\$prefix.orig.gp \\
    -bad=features-on-target/\$prefix/\$prefix.orig.bad \\
    -processAllGeneChildren \\
    -allowMinimalGenes \\
    -warnAndContinue

genePredToFakePsl \\
    -chromSize=$source_prefix.sizes \\
    $source_prefix.2bit \\
    features-on-target/\$prefix/\$prefix.orig.gp \\
    features-on-target/\$prefix/\$prefix.orig.psl \\
    features-on-target/\$prefix/\$prefix.orig.cds

pslMap \\
    features-on-target/\$prefix/\$prefix.orig.psl \\
    features-on-target/\$prefix/\$prefix.chain \\
    features-on-target/\$prefix/\$prefix.lift.1.psl \\
    -mapInfo=features-on-target/\$prefix/\$prefix.lift.info \\
    -chainMapFile \\
    -swapMap

pslMapPostChain \\
    features-on-target/\$prefix/\$prefix.lift.1.psl \\
    features-on-target/\$prefix/\$prefix.lift.2.psl

grep __TOPLEVEL__ \\
    features-on-target/\$prefix/\$prefix.lift.2.psl \\
    | sort -k1,1nr \\
    | head -1 \\
    >features-on-target/\$prefix/\$prefix.lift.2.toplevel.psl

grep -v __TOPLEVEL__ \\
    features-on-target/\$prefix/\$prefix.lift.2.psl \\
    | sort -k14,14 -k16,16n -k17,17nr \\
    >features-on-target/\$prefix/\$prefix.lift.2.children.psl

perl -e '
BEGIN
{
    open(\$TOPLVL,"<'features-on-target/\$prefix/\$prefix.lift.2.toplevel.psl'");
    @Toplevel=map{chomp(); @f=split(/\t/); \@f} <\$TOPLVL>; 
    \$toplevel=\$Toplevel[0];
    print(join("\t",@\$toplevel),"\n");
    close(\$TOPLVL);
}
while (<>) {
    m/^\s*$/ && next;
    my @F = split(/\t/);
    chomp(\$F[\$#F]);
    
    if ((\$F[13] eq \$toplevel->[13]) && (\$F[8] eq \$toplevel->[8])) {
        if ((\$F[15] >= \$toplevel->[15]) && (\$F[16] <= \$toplevel->[16])) { 
            print;
	} elsif ((\$F[15] <= \$toplevel->[15]) && (\$F[16] >= \$toplevel->[16])) {
	    print;
        } elsif ((\$F[15] <= \$toplevel->[15]) && (\$F[16] > \$toplevel->[15])) { 
            print; 
        } elsif ((\$F[15] >= \$toplevel->[15]) && (\$F[15] < \$toplevel->[16])) { 
            print;
        }
    }
}
END
{
    print("");
}
' features-on-target/\$prefix/\$prefix.lift.2.children.psl \\
  >features-on-target/\$prefix/\$prefix.lift.3.psl

transMapPslToGenePred \\
    features-on-target/\$prefix/\$prefix.orig.gp \\
    features-on-target/\$prefix/\$prefix.lift.3.psl \\
    features-on-target/\$prefix/\$prefix.lift.gp

genePredToGtf \\
    file \\
    features-on-target/\$prefix/\$prefix.lift.gp \\
    features-on-target/\$prefix/\$prefix.lift.gtf \\
    -source=liftover $utr_option

$ssc_option

cat features-on-target/\$prefix/\$prefix.lift.gtf | perl -e \\
'
BEGIN
{
    open(\$STRAND,"<$tmpfile_prefix.locus-chains.list");
    open(\$ATTRS,"<$tmpfile_prefix.attr");
    open(\$LOST,">'features-on-target/\$prefix.lost.gtf'");
    \$LIFT=\*STDOUT;
    %Attrs=();
    %Parent=();
    %Strand=();
    @Lift=();
    @Lost=();
    map{chomp(); @f=split(/\t/); \$Strand{\$f[0]}=\$f[1]} <\$STRAND>;
    map{chomp(); @f=split(/\t/); \$Attrs{\$f[0]}=\$f[2]; \$Parent{\$f[0]}=\$f[1]} <\$ATTRS>;
    close(\$STRAND);
    close(\$ATTRS);
    \$original_source="$PROGRAM";
    \$seen_toplevel = 0;
    \$ParentID="gene_id";
    \$ChildID="transcript_id";
    \$attr_sep=" ";
    \$singleton=0;
}

sub parse_attributes {
    my \$attr_string = shift;
    my \$sep = shift // "=";

    my \$attr_hash = {};
    if (\$attr_string eq ".") {
        return \$attr_hash;
    }
    \$attr_string =~ s/^\s+|\s+$//g;
    \$attr_string =~ s/;$//;
    \$attr_string =~ s/\s*;\s*/;/g;
    for my \$keyval (split(/;/,\$attr_string)) {
        if (\$keyval =~ /\$sep/) {
            my \$idx = index(\$keyval, \$sep);
            my \$key = substr(\$keyval, 0, \$idx);
            my \$val = substr(\$keyval, \$idx + 1);
            \$attr_hash->{\$key} = \$val;
            \$attr_hash->{\$key} =~ s/\042//g;
        } else {
            my \$key = \$keyval;
            my \$val = 1;
            \$attr_hash->{\$key} = \$val;
        }
    }
    return \$attr_hash;
}

sub set_attributes {
    my \$attr_hash = shift;
    my @attr_list = ();
    for my \$key (sort keys(%\$attr_hash)) {
    	my \$val = \$attr_hash->{\$key};
        push(@attr_list, "\$key=\$val");
    }
    
    if (@attr_list) {
        return join(";", @attr_list);
    } else {
        return ".";
    }
}

while (<>) {
    if (/^\s*$/) { next }
    if (/^\##gff-version\s+(\d+)/) {
        if (\$1 >= 3) {
	    \$ParentID="Parent";
	    \$ChildID="ID";
            \$attr_sep="=";
        }
    }
    if (/^\#/) { next }

    my @F = split(/\t/);

    chomp(\$F[\$#F]);

    \$write = 0;
    \$Attrs = {};
    \$attrs = parse_attributes(\$F[8], \$attr_sep);
    if (defined(\$Parent{\$attrs->{\$ChildID}}) &&
        defined(\$Strand{\$Parent{\$attrs->{\$ChildID}}}) && 
        (\$F[6] eq \$Strand{\$Parent{\$attrs->{\$ChildID}}})) {
	\$write = 1;
    }
    if (defined(\$attrs->{"exon_id"})) {
        if (\$attrs->{"exon_id"} =~ /__(?:TOPLEVEL|DIRECTEXON|SINGLETON)__/) { 
            next;
        } elsif (\$F[2] eq "5UTR") {
            \$F[2] = "five_prime_UTR";
        } elsif (\$F[2] eq "3UTR") {
            \$F[2] = "three_prime_UTR";
        }
	\$Attrs = {
            "ID" => join(".",\$attrs->{"exon_id"},\$F[2]),
	    "Parent" => \$attrs->{\$ChildID}
        }
    } elsif (defined(\$Attrs{\$attrs->{\$ChildID}})) {
        \$Attrs = parse_attributes(\$Attrs{\$attrs->{\$ChildID}});
        if (\$write && \$attrs->{\$ChildID} =~ /__TOPLEVEL__/) {
 	    if (defined(\$Attrs->{"original_source"})) {
                \$original_source = \$Attrs->{"original_source"};
            }
	    \$seen_toplevel = 1;

        } elsif (\$F[2] eq "gene") {
            \$write = 0;
        }
    } elsif (\$write && \$attrs->{\$ChildID} =~ /__TOPLEVEL___\d+/) {
        if (!\$seen_toplevel) {
	    \$child = \$attrs->{\$ChildID};
	    \$child =~ s/_\d+$//;
            \$Attrs = parse_attributes(\$Attrs{\$child});
	    if (defined(\$Attrs->{"original_source"})) {
                \$original_source = \$Attrs->{"original_source"};
            }
            \$seen_toplevel = 1;
        }
    } else {
        \$write = 0;
    }
    if (\$write && !\$singleton) {
	if (defined(\$Attrs->{"original_source"})) {
	    delete(\$Attrs->{"original_source"});
        }
        if (defined(\$Attrs->{"original_type"})) {
            \$F[2] = \$Attrs->{"original_type"};
	    delete(\$Attrs->{"original_type"});
        }
	if (\$Attrs->{"__SINGLETON__"}) {
            delete(\$Attrs->{"__SINGLETON__"});
            \$singleton = 1;
        }
	if (\$Attrs->{"ID"} =~ /__(TOPLEVEL|DIRECTEXON)__/) {
	   \$Attrs->{"ID"} =~ s/\!__\${1}__(?:_\d+)?$//;
	   if (defined(\$Attrs->{"Parent"}) && (\$Attrs->{"ID"} eq \$Attrs->{"Parent"})) {
               delete(\$Attrs->{"Parent"});
           }
        }
	\$F[8] = set_attributes(\$Attrs);

        push(@Lift, \@F);
    } else {
        push(@Lost, \@F);
    }
}

END
{
    if (\$seen_toplevel) {
        for \$feature (@Lift) {
            \$feature->[1]=\$original_source;
	    print(\$LIFT join("\t", @\$feature),"\n");
        }
        for \$feature (@Lost) {
            \$feature->[1]=\$original_source;
	    print(\$LOST join("\t", @\$feature),"\n");
        }
    } else {
        for \$feature (@Lift, @Lost) {
	    print(\$LOST join("\t", @\$feature),"\n");
        }
    }
    close(\$LIFT);
    close(\$LOST);
}
' >features-on-target/\$prefix.lift.gff3.tmp \\
  && mv features-on-target/\$prefix.lift.gff3.tmp \\
        features-on-target/\$prefix.lift.gff3

if [ -e "features-on-target/\$prefix.lift.gff3" ]; then
    tar -czf features-on-target/\$prefix.tar.gz features-on-target/\$prefix && rm -r features-on-target/\$prefix
fi

EOF
fi
check_depend "$tmpfile_path/lift.sh";
chmod 750 $tmpfile_path/lift.sh;


if [ ! -s "$tmpfile_path/lift.batch" ]; then
    grep -v '^#' $tmpfile_prefix.locus-chains.list \
	| cut -f1,3 \
	| while read locus chains; do
	echo "$tmpfile_path/lift.sh $locus $chains &>features-on-target/$locus.lift.log";
    done >$tmpfile_path/lift.batch
fi
check_depend "$tmpfile_path/lift.batch";


parallel_options="\
--eta \
--progress \
--will-cite \
--max-procs $num_threads \
--joblog $tmpfile_prefix.lift.log \
";

num_failed=0;
num_passed=0;
if [ -s "$tmpfile_prefix.lift.log" ]; then
    num_failed=$(awk 'BEGIN{FS="\t";n=0} {if (/^Seq/) {next} if ($7 > 0) {n++}} END{print n}' $tmpfile_prefix.lift.log);
fi

if [[ (! -e "$tmpfile_path/lift.complete") || ($num_failed -gt 0) ]]; then
    log_info "Lifting child-level features, using $num_threads threads\n";
    if [ $num_failed -gt 0 ]; then
	parallel_options="${parallel_options} --resume-failed";
    fi
    # qbatch submit -T $num_threads lift.batch
    parallel $parallel_options < $tmpfile_path/lift.batch;
    #"$tmpfile_path/lift.sh {1} {3} &>features-on-target/{1}.lift.log"
    
    num_failed=$(awk 'BEGIN{FS="\t";n=0} {if (/^Seq/) {next} if ($7 > 0) {n++}} END{print n}' $tmpfile_prefix.lift.log);
    num_passed=$(awk 'BEGIN{FS="\t";n=0} {if (/^Seq/) {next} if ($7 == 0) {n++}} END{print n}' $tmpfile_prefix.lift.log);

    if [ $num_failed -eq 0 ]; then
	touch "$tmpfile_path/lift.complete";
    fi
fi
# check_depend "$tmpfile_path/lift.complete"

num_failed=$(awk 'BEGIN{FS="\t";n=0} {if (/^Seq/) {next} if ($7 > 0) {n++}} END{print n}' $tmpfile_prefix.lift.log);
num_passed=$(awk 'BEGIN{FS="\t";n=0} {if (/^Seq/) {next} if ($7 == 0) {n++}} END{print n}' $tmpfile_prefix.lift.log);


if [ ! -s "$tmpfile_prefix.lift.gff3" ]; then
    log_info "Merging child-level features\n";
    perl -ane \
'
BEGIN
{
  ($s,$m,$h,$d,$M,$Y) = localtime;
  $dt = sprintf("%02d-%02d-%02dT%02d:%02d:%02d",1900+$Y,1+$M,$d,$h,$m,$s);
  print("##gff-version 3\n");
  print("##file-version 1.0\n");
  print("##file-date $dt\n");
  print("##annotation-source liftover\n"); 
}
{
  m/^\s*$/ && next;
  print("##sequence-region $F[0] 1 $F[1]\n")
}
' $target_prefix.sizes >$tmpfile_prefix.lift.gff3.tmp;

    check_depend "$tmpfile_prefix.lift.gff3.tmp";


    cut -f4 $tmpfile_prefix.toplevel.target.3.bed | \
	while read locus; do
	    if [ -s features-on-target/$locus.lift.gff3 ]; then
		cat features-on-target/$locus.lift.gff3;
		echo "###";
	    else
		log_warn "Locus '$locus' failed to lift; For more info, see $tmpfile_name/features-on-target/$locus.lift.log\n";
	    fi
	done >>$tmpfile_prefix.lift.gff3.tmp && \
	    mv $tmpfile_prefix.lift.gff3.tmp \
	       $tmpfile_prefix.lift.gff3
fi
check_depend $tmpfile_prefix.lift.gff3;


if [ ! -s "$tmpfile_prefix.lost.gff3" ]; then
    perl -e \
'
BEGIN
{
    open($ATTRS,"<'$tmpfile_prefix.attr'");
    open($SIZES,"<'$source_prefix.sizes'");
    
    %Attrs=();
    %Lifted=();
    ($s,$m,$h,$d,$M,$Y) = localtime;
    $dt = sprintf("%02d-%02d-%02dT%02d:%02d:%02d",1900+$Y,1+$M,$d,$h,$m,$s);
    print("##gff-version 3\n");
    print("##file-version 1.0\n");
    print("##file-date $dt\n");
    map{chomp(); @f=split(/\t/); $Attrs{$f[0]}=$f[1]} <$ATTRS>;
    map{chomp(); @f=split(/\t/); print("##sequence-region $f[0] 1 $f[1]\n")} <$SIZES>;
    close($ATTRS);
    close($SIZES);
    $ParentID="gene_id";
    $ChildID="transcript_id";
    $attr_sep=" ";
}

sub parse_attributes {
    my $attr_string = shift;
    my $sep = shift // "=";

    my $attr_hash = {};
    if ($attr_string eq ".") {
        return $attr_hash;
    }
    $attr_string =~ s/^\s+|\s+$//g;
    $attr_string =~ s/;$//;
    $attr_string =~ s/\s*;\s*/;/g;
    for my $keyval (split(/;/,$attr_string)) {
        if ($keyval =~ /$sep/) {
            my $idx = index($keyval, $sep);
            my $key = substr($keyval, 0, $idx);
            my $val = substr($keyval, $idx + 1);
            $attr_hash->{$key} = $val;
            $attr_hash->{$key} =~ s/\042//g;
        } else {
            my $key = $keyval;
            my $val = 1;
            $attr_hash->{$key} = $val;
        }
    }
    return $attr_hash;
}

open($LIFT,"<'$tmpfile_prefix.lift.gff3'");
while (<$LIFT>) {
    if (/^\s*$/) { next }
    if (/^\##gff-version\s+(\d+)/) {
        if ($1 >= 3) {
	    $ParentID="Parent";
	    $ChildID="ID";
            $attr_sep="=";
        }
    }
    if (/^\#/) { next }

    my @F = split(/\t/);

    chomp($F[$#F]);

    $attrs = parse_attributes($F[8], $attr_sep);
    if (defined($Attrs{$attrs->{$ChildID}})) {
        $Lifted{$Attrs{$attrs->{$ChildID}}}=1;
    }
}

$ParentID="gene_id";
$ChildID="transcript_id";
$attr_sep=" ";
while (<>) {
    if (/^\s*$/) { next }
    if (/^\##gff-version\s+(\d+)/) {
        if ($1 >= 3) {
	    $ParentID="Parent";
	    $ChildID="ID";
            $attr_sep="=";
        }
    }
    if (/^\#/) { next }

    my @F = split(/\t/);

    chomp($F[$#F]);

    $attrs = parse_attributes($F[8], $attr_sep);
    if (defined($Attrs{$attrs->{$ChildID}}) && 
        (!$Lifted{$Attrs{$attrs->{$ChildID}}})) {
        print;
    }
}
' $original_file \
    >$tmpfile_prefix.lost.gff3.tmp \
    && mv $tmpfile_prefix.lost.gff3.tmp \
	  $tmpfile_prefix.lost.gff3
fi
check_depend "$tmpfile_prefix.lost.gff3";


num_lost=$(awk 'BEGIN{i=0} {if ($1 ~ /^#/) { next } if ($9 !~ /Parent=/) { i++ }} END{print i}' $tmpfile_prefix.lost.gff3);
num_lost=$(expr $num_lost - $num_failed);
    

if [[ (-e $tmpfile_prefix.lift.gff3) && (-e $tmpfile_prefix.lost.gff3) ]]; then
    cp $tmpfile_prefix.lift.gff3 $output_prefix.lift.gff3;
    cp $tmpfile_prefix.lost.gff3 $output_prefix.lost.gff3;
fi
check_depend "$output_prefix.lift.gff3";
check_depend "$output_prefix.lost.gff3";


if [ $archive ]; then
    log_info "Archiving run directory\n";
    if [[ -s $output_prefix.lift.gff3 ]]; then
	pushd $output_path \
	    && tar -czf $tmpfile_name.tar.gz $tmpfile_name \
	    && rm -r $tmpfile_name \
	    && popd;
	log_info "Archiving complete\n";
    else
	log_info "Archiving failed, run failed\n";
    fi
fi
log_info "Loci mapped:\n";
log_info "  Loci passing liftover: $num_passed\n";
log_info "  Loci failing liftover: $num_failed\n";
log_info "Loci unmapped:   $num_lost\n";
log_info "Finished\n";

###GFF3-END##################################################################

else
    log_error 1 "Unsupported format:" ${original_file##*.};
fi


## Sort the GFF file with genometools:
# 1) gt gff3 -tidy yes -checkids yes -retainids yes -sort yes unsorted.gff3 >sorted.gff3 
## Translate GFF3 CDS sequences to proteins:
# 2) gt suffixerator -dna -md5 -suf -bwt -db genome.fasta
# 3) gt extractfeat -translate yes -type CDS -join yes -retainids yes -matchdescstart -width 100 -seqfile genome.fasta sorted.gff3 >protein.fasta
