"""A pretty exception  stacktrace implementation"""

import sys
import os.path as path
from textwrap import fill

try:
    from threading import current_thread
except ImportError:
    from dummy_threading import current_thread

__authors__ = ('Jessen Bredeson <jessenbredeson@berkeley.edu>')
__version__ = '0.0-r002'
__all__ = ['prettystack']
__tag = 'ERROR'
__exe = sys.argv[0] # prevent downstream modification
__default_stackwidth__ = 79
stackwidth = int(__default_stackwidth__) # copies value


def _function():
    """Private. Return a string containing the name of the function in
    which it is called, analogous to the __name__ method for objects"""
    return sys._getframe(1).f_code.co_name


def badge():
    """Return '[scriptname]'."""
    return '[{0}]'.format(path.basename(__exe))


def __stackwidth():
    """Allow safe fallback if the stackwidth set by the user should 
    be set too small or is not an integer."""
    minwidth = 40
    if isinstance(stackwidth,int) or isinstance(stackwidth,float):
        return max(minwidth,len(badge()),int(stackwidth))
    else:
        return max(minwidth,len(badge()),__default_stackwidth__)


def _stacktrace_header():
    """Return stacktrace header."""
    return '{1:_<{0}}'.format(__stackwidth(),badge())


def _stacktrace_footer():
    """Private. Return stacktrace footer."""
    return '_'*__stackwidth()


def _format_traceback(traceback):
    """Private. Return a list of formatted traceback lines detailing the
    function, module/program, and position where an error has occurred."""
    
    if traceback == None:
        traceback = sys.exc_info()[2]
    
    stack = []
    while traceback is not None:
        frame     = traceback.tb_frame
        linenumb  = traceback.tb_lineno
        filename  = frame.f_code.co_filename
        funcname  = frame.f_code.co_name
        traceback = traceback.tb_next

        if  funcname == '<module>':
            funcname =  '__main__'
        else:
            funcname += '({0}:{1})'.format(filename,linenumb)

        stack.append('  at ' + funcname);

    stack.reverse()

    return stack


def _format_stacktrace(etype,message,traceback):
    """Private. Produce a message with stacktrace enclosed within a pretty 
    header and footer. The message line reports the exception type, the 
    thread in which the error occured, and the message string."""
    
    if  etype == None:
        etype = Exception().__class__
    else:
        etype = etype.__name__
    if  message == None:
        message = str(etype)
    elif not isinstance(message,str):
        message = str(message)        

    stack = []
    stack.append(_stacktrace_header())
    stack.append(fill('{0}: {1}: Thread {2}: {3}'.\
                          format(__tag,etype,current_thread().name,message),
                      width=__stackwidth(),
                      initial_indent=' ',
                      subsequent_indent=' '*4,
                      fix_sentence_endings=True))
    stack.append('\n'.join(_format_traceback(traceback)))
    stack.append(_stacktrace_footer())
    stack.append('\n\n')
    
    return '\n'.join(stack)


def pretty_stacktrace(etype,message,traceback):
    """Prettify exception traces globally with this hook (for more info. 
    see _format_stacktrace())."""
    sys.stderr.write(_format_stacktrace(etype,message,traceback))


# def throw(type,message):
#     """Produce pretty stacktrace with exception type and message, then
#     exit the current program."""
#     if type == None or type not in __valid_exceptions:
#         throw(TypeError,_function()+'() arg 1 must be an exception subclass')
#     if message == None or not isinstance(message,str):
#         message = str(message)

#     sys.exit(__format_stacktrace('EXCEPTION',type,message))


# def warn(type,message):
#     """Produce pretty stacktrace with warning type and message."""
#     if type == None or type not in __valid_exceptions:
#         throw(TypeError,_function()+'() arg 1 must be an exception subclass')
#     if message == None or not isinstance(message,str):
#         message = str(message)

#     sys.stderr.write(__format_stacktrace('WARNING',type,message))

#     return 1


# def debug(message,enable=False):
#     """Print a message only if 'enable' kwarg has been set to 'True'.
#     Prints to stderr."""
#     if message == None or not isinstance(message,str):
#         message = str(message)
#     if enable == None or not isinstance(enable,bool):
#         throw(TypeError,_function()+'() kwarg \'enable\' must be a bool')

#     if enable == True:
#         sys.stderr.write(__badge()+' DEBUG: '+message+'\n')

#     return 1


# def report(message):
#     """Print a reporting message to stderr.""" 
#     if message == None or not isinstance(message,str):
#         message = str(message)
    
#     sys.stderr.write(__badge()+' '+message+'\n')

#     return 1
