
import os
import sys

from fileutil import zopen

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_NAME__-__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Provide fasta/fastq parsing support for __PACKAGE_NAME__'


EMPTY = ''
SPACE = ' '

PYTHONVERSION = sys.version_info[:2]
if PYTHONVERSION < (3,):
    range = xrange
    import string
    NUCLEOTIDE_COMPLEMENT = \
        string.maketrans(
            'acgturyswkmbdhvnACGTURYSWKMBDHVN',
            'tgcaayrwsmkvhdbnTGCAAYRWSMKVHDBN'
        )
else:
    NUCLEOTIDE_COMPLEMENT = {
        65: 'T',  97: 't',
        66: 'V',  98: 'v',
        67: 'G',  99: 'g',
        68: 'H', 100: 'h',
        71: 'C', 103: 'c',
        72: 'D', 104: 'd',
        75: 'M', 107: 'm',
        77: 'K', 109: 'k',
        78: 'N', 110: 'n',
        82: 'Y', 114: 'y',
        83: 'W', 115: 'w',
        84: 'A', 116: 'a',
        85: 'A', 117: 'a',
        86: 'B', 118: 'b',
        87: 'S', 119: 's',
        89: 'R', 121: 'r',
    }


    
def _fold(linear_string, width=None):
    if width is None:
        return linear_string

    folded_string = []
    linear_length = len(linear_string)
    for offset in range(0, linear_length, width):
        if offset+width < linear_length:
            folded_string.append(linear_string[offset:offset+width])
        else:
            folded_string.append(linear_string[offset:])
            
    return '\n'.join(folded_string)


def _format_comment(record):
    comment=EMPTY
    if getattr(record, 'comment', None) is not None:
        comment = SPACE + record.comment
    return comment


def format_fasta(name, sequence, comment=EMPTY, width=None):
    return ">%s%s\n%s\n" % (name, comment, _fold(sequence, width=width))


def format_fastq(record):
    return "@%s%s\n%s\n+\n%s\n" % (record.name, _format_comment(record), record.sequence, record.quality)


def format_fastx(record, width=None):
    if getattr(record, 'quality', None) is None:
        return format_fasta(record.name, record.sequence, _format_comment(record), width)
    else:
        return format_fastq(record)


def pair_name(name):
    if name.endswith('/1') or name.endswith('/2'):
        return name[:-2]
    else:
        return name


def reverse_sequence(sequence):
    if sequence is None:
        return None
    return sequence[::-1].translate(NUCLEOTIDE_COMPLEMENT)


def reverse_quality(quality):
    if quality is None:
        return None
    return quality[::-1]


def reverse_record(record):
    record_quality = record.quality
    if record_quality is not None:
        record_quality = reverse_quality(record.quality)
    record.sequence = reverse_sequence(record.sequence)
    record.quality = record_quality
    return record



def _load_references_file(filename, indices, references):
    referencesfile = zopen(filename, 'r')

    for reference in referencesfile:
        reference = reference.rstrip().split('\t')[0]
        if reference in indices:
            references.add(reference)
        else:
            raise KeyError("Contig '%s' (via %s) not in reference file" % (
                reference, filename))

    referencesfile.close()
            


def load_references(fasta, referencelist):
    """Loads a list of reference names from the command-line or a file"""
    indices = dict(zip(fasta.references, range(len(fasta.references))))

    references = set()
    if len(referencelist) < 1:
        return fasta.references

    for reference in referencelist:
        if reference in indices:
            references.add(reference)
        elif os.path.isfile(reference):
            _load_references_file(reference, indices, references)
        else:
            raise KeyError("Contig '%s' not in reference file" % reference)

    return sorted(list(references), key=indices.get)


def load_listfile(listfiles):
    itemset = set()
    itemlist = list()
    for listfile in listfiles:
        inputfile = zopen(listfile, 'r')
        for line in inputfile:
            item = line.rstrip().split('\t')

            if item[0] in itemset:
                continue
        
            itemlist.append(item[0])
            itemset.add(item[0])

        inputfile.close()

    return itemlist


def h_to_numeric(value):
    value = str(value)
    coeff = value.upper()
    expon = 0
    
    if coeff.endswith('Y'):
        expon = 8
    elif coeff.endswith('Z'):
        expon = 7
    elif coeff.endswith('E'):
        expon = 6
    elif coeff.endswith('P'):
        expon = 5
    elif coeff.endswith('T'):
        expon = 4
    elif coeff.endswith('G'):
        expon = 3
    elif coeff.endswith('M'):
        expon = 2
    elif coeff.endswith('K'):
        expon = 1
    elif coeff.endswith('H'):
        expon = 2.0 / 3.0
    elif coeff.endswith('D'):
        expon = 1.0 / 3.0
    elif coeff.replace('.','',1).isdigit():
        expon = 0
    else:
        raise ValueError("Not a valid numeric value: '%s'"  % value)

    if expon:
        coeff = coeff[:-1]
    if coeff.replace('.','',1).isdigit():
        if '.' in coeff:
            return float(coeff) * int(round(1000 ** expon, 0))
        else:
            return int(coeff) * int(round(1000 ** expon, 0))
    else:
        raise ValueError("Not a valid numeric value: '%s'"  % value)


def __rcommasep(value):
    l = []
    L = len(value)
    for i in range(0, 3 * int(L / 3.0), 3):
        l.append(value[L - i - 3 : L - i])

    if L % 3 > 0:
        l.append(value[0 : L % 3])

    return ','.join(reversed(l))


def __lcommasep(value):
    l = []
    L = len(value)
    for i in range(0, L, 3):
        l.append(value[i : i + 3])
        
    return ','.join(l)


def commasep(value):
    value = str(value)

    if not value.replace('.','',1).isdigit():
        raise ValueError("Not a valid numeric value: '%s'"  % value)

    if '.' in value:
        value = value.split('.')
        return __rcommasep(value[0]) + '.' + __lcommasep(value[1])
    else:
        return __rcommasep(value)


