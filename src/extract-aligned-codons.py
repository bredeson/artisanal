#!/usr/bin/env python

import os
import sys
import pysam
import getopt

from fastxutil import format_fasta

DEBUG = False

gap = '-'
translate_single = {
    'TTT' : 'F', 'TCT' : 'S', 'TAT' : 'Y', 'TGT' : 'C',
    'TTC' : 'F', 'TCC' : 'S', 'TAC' : 'Y', 'TGC' : 'C',
    'TTA' : 'L', 'TCA' : 'S', 'TAA' : '*', 'TGA' : '*',
    'TTG' : 'L', 'TCG' : 'S', 'TAG' : '*', 'TGG' : 'W',
    'CTT' : 'L', 'CCT' : 'P', 'CAT' : 'H', 'CGT' : 'R',
    'CTC' : 'L', 'CCC' : 'P', 'CAC' : 'H', 'CGC' : 'R',
    'CTA' : 'L', 'CCA' : 'P', 'CAA' : 'Q', 'CGA' : 'R',
    'CTG' : 'L', 'CCG' : 'P', 'CAG' : 'Q', 'CGG' : 'R',
    'ATT' : 'I', 'ACT' : 'T', 'AAT' : 'N', 'AGT' : 'S',
    'ATC' : 'I', 'ACC' : 'T', 'AAC' : 'N', 'AGC' : 'S',
    'ATA' : 'I', 'ACA' : 'T', 'AAA' : 'K', 'AGA' : 'R',
    'ATG' : 'M', 'ACG' : 'T', 'AAG' : 'K', 'AGG' : 'R',
    'GTT' : 'V', 'GCT' : 'A', 'GAT' : 'D', 'GGT' : 'G',
    'GTC' : 'V', 'GCC' : 'A', 'GAC' : 'D', 'GGC' : 'G',
    'GTA' : 'V', 'GCA' : 'A', 'GAA' : 'E', 'GGA' : 'G',
    'GTG' : 'V', 'GCG' : 'A', 'GAG' : 'E', 'GGG' : 'G',
    '---' : '-'
}
translate_triple = {
    'TTT' : 'Phe', 'TCT' : 'Ser', 'TAT' : 'Tyr', 'TGT' : 'Cys',
    'TTC' : 'Phe', 'TCC' : 'Ser', 'TAC' : 'Try', 'TGC' : 'Cys',
    'TTA' : 'Leu', 'TCA' : 'Ser', 'TAA' : '***', 'TGA' : '***',
    'TTG' : 'Leu', 'TCG' : 'Ser', 'TAG' : '***', 'TGG' : 'Trp',
    'CTT' : 'Leu', 'CCT' : 'Pro', 'CAT' : 'His', 'CGT' : 'Arg',
    'CTC' : 'Leu', 'CCC' : 'Pro', 'CAC' : 'His', 'CGC' : 'Arg',
    'CTA' : 'Leu', 'CCA' : 'Pro', 'CAA' : 'Gln', 'CGA' : 'Arg',
    'CTG' : 'Leu', 'CCG' : 'Pro', 'CAG' : 'Gln', 'CGG' : 'Arg',
    'ATT' : 'Ile', 'ACT' : 'Thr', 'AAT' : 'Asn', 'AGT' : 'Ser',
    'ATC' : 'Ile', 'ACC' : 'Thr', 'AAC' : 'Asn', 'AGC' : 'Ser',
    'ATA' : 'Ile', 'ACA' : 'Thr', 'AAA' : 'Lys', 'AGA' : 'Arg',
    'ATG' : 'Met', 'ACG' : 'Thr', 'AAG' : 'Lys', 'AGG' : 'Arg',
    'GTT' : 'Val', 'GCT' : 'Ala', 'GAT' : 'Asp', 'GGT' : 'Gly',
    'GTC' : 'Val', 'GCC' : 'Ala', 'GAC' : 'Asp', 'GGC' : 'Gly',
    'GTA' : 'Val', 'GCA' : 'Ala', 'GAA' : 'Glu', 'GGA' : 'Gly',
    'GTG' : 'Val', 'GCG' : 'Ala', 'GAG' : 'Glu', 'GGG' : 'Gly',
    '---' : '---'
}

def format_phylip(name, linear_string, pad=2, width=None):
    if width is None:
        return linear_string

    pad = pad + 2
    
    folded_string = []
    linear_length = len(linear_string)
    for offset in range(0, linear_length, width):
        if offset+width < linear_length:
            folded_string.append("{0:<{1}}{2}".format(name, pad, linear_string[offset:offset+width]))
        else:
            folded_string.append("{0:<{1}}{2}".format(name, pad, linear_string[offset:]))
        name = ''
        pad = 0

    return '\n'.join(folded_string)


def usage(message=None, exitcode=1, stream=sys.stderr):
    message = "" if message is None else "\nERROR: %s\n\n\n" % message
    stream.write("Usage: %s [-P] <msa.fasta>\n%s" % (os.path.basename(__file__),message))
    sys.exit(exitcode)


def main(argv):
    try:
        options, arguments = getopt.getopt(argv,'Ph',('help',))
    except getopt.GetoptError as error:
        usage(error)

    write_phylip = False
    for flag, value in options:
        if   flag in ('-h','--help'): usage(exitcode=0)
        elif flag == '-P': write_phylip = True

    if len(arguments) != 1:
        usage("Unexpected number of arguments")
        
    aligned_fasta = pysam.FastxFile(arguments[0])
    trimmed_output = sys.stdout

    name_length = 0
    num_records = 0
    aligned_length = 0
    trimmed_length = 0
    aligned_records = []
    trimmed_records = []
    uppercase_nucleotides = set('ATCGN')
    for record in aligned_fasta:
        if aligned_length == 0:
            aligned_length = len(record.sequence)
        else:
            assert len(record.sequence) == aligned_length, "Input MSA not of equal lengths"
        
        if len(record.name) > name_length:
            name_length = len(record.name)
            
        aligned_records.append(
            [
                record.name,
                list(record.sequence)
            ]
        )
        trimmed_records.append(
            [
                record.name,
                []
            ]
        )
        num_records += 1

    for position in range(aligned_length):
        aligned_rows = 0
        for record in range(num_records):
            if aligned_records[record][1][position] in uppercase_nucleotides:
                aligned_rows += 1
            else:
                aligned_records[record][1][position] = gap

        if aligned_rows == num_records:
            for record in range(num_records):
                trimmed_records[record][1].append(aligned_records[record][1][position])
                
                
    for record in range(num_records):
        assert (len(trimmed_records[record][1]) % 3) == 0, "Trimmed MSA not a multiple of 3: %s" % argv[0]
        if trimmed_length == 0:
            trimmed_length = len(trimmed_records[record][1])
        else:
            assert len(trimmed_records[record][1]) == trimmed_length, "Trimmed MSA not of equal lengths: %s3" % argv[0]

        trimmed_records[record][1] = ''.join(trimmed_records[record][1])

        codon_record = []
        amino_record = []
        for i in range(0, trimmed_length, 3):
            try:
                codon = trimmed_records[record][1][i:(i+3)]
            except KeyError:
                if DEBUG:
                    codon = 'NNN'
                else:
                    codon = '---'

            try:
                amino = translate_single[codon]
            except KeyError:
                if DEBUG:
                    amino = 'Any'
                else:
                    codon = '---'
                    amino = '-'
            if amino.startswith('*'):
                codon = '---'
                amino = '-'
                
            codon_record.append(codon)
            amino_record.append(amino)

        trimmed_records[record][1] = ''.join(codon_record)



    if trimmed_length >= 3:
        if write_phylip:
            trimmed_output.write(" %d %d I\n" % (num_records, trimmed_length))

        for record in range(num_records):
            if write_phylip:
                trimmed_output.write(format_phylip(
                    trimmed_records[record][0],
                    trimmed_records[record][1],
                    pad=name_length,
                    width=80
                ) + "\n")
            else:
                trimmed_output.write(format_fasta(
                    trimmed_records[record][0],
                    trimmed_records[record][1],
                    width=100
                ))

            


main(sys.argv[1:])

