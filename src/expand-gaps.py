
import os
import re
import sys
import pysam
import getopt

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Expand gaps by inflation or contig clipping'


VALID_STRAND_VALUES = ('+', '-', '.')

PYTHONVERSION = sys.version_info[:2]
if PYTHONVERSION < (3,0):
    range = xrange

    
def _fold(linear_string, width=None):
    if width is None:
        return linear_string

    folded_string = []
    linear_length = len(linear_string)
    for offset in range(0, linear_length, width):
        if offset+width < linear_length:
            folded_string.append(linear_string[offset:offset+width])
        else:
            folded_string.append(linear_string[offset:])
            
    return '\n'.join(folded_string)    
    
    
def format_fastx(record, width=None):
    comment = '' if record.comment is None else ' '+record.comment

    if record.quality is None:
        return ">%s%s\n%s\n" % (record.name, comment, _fold(record.sequence, width=width))
    else:
        return "@%s%s\n%s\n+\n%s\n" % (record.name, comment, record.sequence, record.quality)



def asBED(bedstr):
    bed = bedstr.rstrip().split('\t')
    bed[1] = int(bed[1])
    bed[2] = int(bed[2])
    
    assert bed[1] <= bed[2], \
        "BED Start value > End value: %s:%d-%d" % bed[0:2]

    nfields = len(bed)
    if nfields > 4:
        bed[4] = float(bed[4])
    if nfields > 5:
        if bed[5] not in VALID_STRAND_VALUES:
            assert False, \
                "BED Strand value invalid: %s" % bed[5]

    return bed



def load_BED_file(filename):
    bed = None
    beddict = {}
    if filename.lower().endswith('gz'):
        bed = gzip.GzipFile(filename, 'r')
    else:
        bed = open(filename, 'r')
    
    for record in bed:
        record = asBED(record)
        
        if record[0] not in beddict:
            beddict[record[0]] = {}

        beddict[record[0]][ "%d-%d" % (record[1],record[2]) ] = record[4]
        
    return beddict



def expand_gaps(ofile, cfile, ifile, gapdict={}, mingap_len=25, expand_len=24, line_len=None):
    nucleotides = re.compile('[atcgATCG]+')

    num_bedexpanded = 0
    num_minexpanded = 0

    for record in ifile:
        record.quality = None
        record_len = len(record.sequence)
        masked_seq = []
        masked_bed = []
        masked_chn = []
        expand_off = 0
        pntr_start = 0
        prev_start = 0
        prev_end   = None

        for contig in nucleotides.finditer(record.sequence):
            curr_start = contig.start()
            curr_end   = contig.end()

            expand_gap = {}
            if record.name in gapdict:
                expand_gap = gapdict[record.name]
            
            if prev_end is not None:
                gap_pos = "%d-%d" % (prev_end, curr_start)
                gap_len = curr_start - prev_end

                if gap_pos in expand_gap:
                    # expand the gap to length
                    expand_gap_len = int(expand_gap[gap_pos])
                    if expand_gap_len < 0:
                        expand_gap_len = abs(expand_gap_len)
                        prev_len = prev_end - prev_start
                        curr_len = curr_end - curr_start
                        if prev_len > curr_len and prev_len > expand_len+expand_gap_len:
                            # 0         10        20        30        40        50
                            # |---------|---------|---------|---------|---------|
                            #  =============NNNNNNN==========NN============
                            #  ^---------^.........^--------^
                            masked_seq.append(record.sequence[pntr_start:prev_end-expand_gap_len-expand_len])
                            masked_seq.append('n' * expand_len)
                            masked_chn.append([pntr_start,  prev_end - expand_gap_len - expand_len,
                                               expand_off + pntr_start,
                                               expand_off + prev_end - expand_gap_len - expand_len])
                            expand_off -= gap_len + expand_gap_len
                            pntr_start = curr_start

                        elif curr_len > expand_len+expand_gap_len:
                            # 0         10        20        30        40        50
                            # |---------|---------|---------|---------|---------|
                            #  =============NNNNNNN==========NN============
                            #  ^-----------^.........^------^
                            masked_seq.append(record.sequence[pntr_start:prev_end])
                            masked_seq.append('n' * expand_len)
                            masked_chn.append([pntr_start,  prev_end,
                                               expand_off + pntr_start,
                                               expand_off + prev_end])
                            expand_off -= gap_len + expand_gap_len
                            pntr_start = curr_start + expand_len + expand_gap_len
                            
                        else:
                            raise ValueError("Contigs flanking gap (%s:%d-%d) smaller than expansion length" % (
                                record.name, prev_end, curr_start
                            ))
                    else:
                        # masked_bed.append([record.name, pntr_start, prev_end])
                        masked_seq.append(record.sequence[pntr_start:prev_end])
                        masked_seq.append('n' * expand_gap_len)
                        masked_chn.append([pntr_start,  prev_end,
                                           expand_off + pntr_start,
                                           expand_off + prev_end])
                        expand_off += expand_gap_len - gap_len
                        pntr_start = curr_start
                        num_bedexpanded += 1

                elif gap_len < mingap_len:
                    prev_len = prev_end - prev_start
                    curr_len = curr_end - curr_start
                    if prev_len > curr_len and prev_len > expand_len:
                        # 0         10        20        30        40        50
                        # |---------|---------|---------|---------|---------|
                        #  =============NNNNNNN==========NN============
                        #  ^---------^.........^--------^
                        masked_seq.append(record.sequence[pntr_start:prev_end-expand_len])
                        masked_seq.append('n' * int(gap_len + expand_len))
                        masked_chn.append([pntr_start,  prev_end - expand_len,
                                           expand_off + pntr_start, 
                                           expand_off + prev_end - expand_len])
                        pntr_start = curr_start

                    elif curr_len > expand_len:
                        # 0         10        20        30        40        50
                        # |---------|---------|---------|---------|---------|
                        #  =============NNNNNNN==========NN============
                        #  ^-----------^.........^------^
                        masked_seq.append(record.sequence[pntr_start:prev_end])
                        masked_seq.append('n' * int(gap_len + expand_len))
                        masked_chn.append([pntr_start,  prev_end,
                                           expand_off + pntr_start,
                                           expand_off + prev_end])
                        pntr_start = curr_start + expand_len
                
                    else:
                        # don't clip the flanking contigs, just expand gap
                        # 0         10        20        30        40        50
                        # |---------|---------|---------|---------|---------|
                        #  =============NNNNNNN==========NN============
                        #  ^-----------^.......^--------^
                        masked_seq.append(record.sequence[pntr_start:prev_end])
                        masked_seq.append('n' * int(gap_len + expand_len))
                        masked_chn.append([pntr_start,  prev_end,
                                           expand_off + pntr_start,
                                           expand_off + prev_end])
                        expand_off += expand_len
                        pntr_start = curr_start

                    num_minexpanded += 1
                # else:
                #     masked_chn.append([prev_start,  prev_end,
                #                        expand_off + prev_start,
                #                        expand_off + prev_end])

            prev_start = curr_start
            prev_end = curr_end


        masked_seq.append(record.sequence[pntr_start:record_len])
        # masked_bed.append([record.name, pntr_start, record_len])
        masked_chn.append([pntr_start, prev_end,
                           expand_off + pntr_start,
                           expand_off + record_len])

        record.sequence = ''.join(masked_seq)
    
        ofile.write(format_fastx(record, width=line_len))

        chain_num = 1
        masked_len = record_len + expand_off
        for chn in masked_chn:
            cfile.write("chain 999 %s %d + %d %d %s %d + %d %d %d\n" % (
                record.name, record_len, chn[0], chn[1], 
                record.name, masked_len, chn[2], chn[3],
                chain_num))
            cfile.write("%d\n\n" % (chn[1]-chn[0]))
            chain_num += 1

        # for bed in masked_bed:
        #     bed[1] = str(bed[1])
        #     bed[2] = str(bed[2])
        #     bfile.write('\t'.join(bed) + "\n")


    sys.stderr.write("bed-file expanded: %s\n" % num_bedexpanded)
    sys.stderr.write("min-length expanded: %s\n" % num_minexpanded)
            


def usage(message=None, exitcode=1, stream=sys.stderr):
    if message is None:
        message = ''
    else:
        message = 'ERROR: %s\n\n' % message

    stream.write("""
Program: %s (%s)
Version: %s %s
Contact: %s

Usage:   %s <in.fasta> <out.prefix>

Options: -g <file>  BED file of gap positions and expanded size
         -w <int>   Number of residues per line; 0 for Inf [0]
         -l <int>   Expand gaps smaller than {-l} Ns wide by {-x} [0]
         -x <int>   Expand gaps smaller than {-l} Ns wide by {-x} [0]

Outputs:

    out.prefix.fasta
    out.prefix.chain

Notes:
    
    The input BED file to {-b} contains the coordinates of the 
    gap prior to expansion in the 'start' and 'end' fields and the 
    expanded gap length in the 'score' field.

%s
""" % (__program__, __purpose__, __pkgname__, __version__, __contact__,
       __program__, message))
    sys.exit(exitcode)
    

def main(argv):
    gfile = None
    ofile = None
    line_len = 0
    expand_len = 0
    mingap_len = 0
    opts, args = getopt.getopt(argv, 'hg:l:x:w:', ['help'])
    for flag, val in opts:
        if   flag == '-w': line_len = int(val)
        elif flag == '-x': expand_len = int(val)
        elif flag == '-l': mingap_len = int(val)
        elif flag == '-g': gfile = str(val)
        elif flag in ('-h','--help'): usage(exitcode=0)

    if len(args) != 2:
        usage("Unexpected number of arugments")

    if line_len <= 0:
        line_len = None
    if mingap_len < 0:
        mingap_len = 0
    if expand_len < 0:
        expand_len = 0


    gapdict = {}
    if gfile is not None:
        gapdict = load_BED_file(gfile)

    ifile = pysam.FastxFile(args[0])
    ofile = open(args[1] + '.fasta', 'w')
    # bfile = open(args[1] + '.bed', 'w')
    cfile = open(args[1] + '.chain', 'w')
    

    expand_gaps(ofile, cfile, ifile, gapdict=gapdict, mingap_len=mingap_len, expand_len=expand_len, line_len=line_len)

    ofile.close()
    cfile.close()
    # bfile.close()
    

if __name__ == '__main__':
    main(sys.argv[1:])
