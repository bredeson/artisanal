#!/usr/bin/env python

import os
import sys
import pysam
import getopt
import intervals
import collections

__authors__ = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Parse BEDPE of syntenic segments into MCscan .simple format'

_tab = '\t'
_dash = '-'
_null = '.'
_empty = ''
_colon = ':'
_comma = ','
_comment = '#'
_maxuint = sys.maxsize

num = len

class BED(intervals.BaseInterval):
    def __init__(self, chr=_null, beg=-1, end=-1, id=_null, strand=_null):
        intervals.BaseInterval.__init__(self, chr, beg, end)
        self.strand = strand
        self.id = id

    @property
    def strand(self):
        return self._strand

    @strand.setter
    def strand(self, strand):
        self._strand = intervals.Strand(strand)
        
    @property
    def mid(self):
        return float(self.beg + self.end) / 2.0

    @property
    def chr(self):
        return self.name

    @chr.setter
    def chr(self, name):
        self.name = name


class BEDPE(object):
    def __init__(self, trg, qry, name=_null,
                 score=0.0, index=None, members=None, num_members=None):
        self.trg = trg
        self.qry = qry
        self.name = name
        self.members = members
        self.num_members = num_members
        self.index = index
        self.score = score

    @property
    def score(self):
        if self._score is None:
            if self.members is None:
                return 0.0
            else:
                return float(sum(map(
                    lambda m: float(m.score or 0.0)*m.num_members,
                    self.members
                ))) / self.num_members
        else:
            return self._score

    @score.setter
    def score(self, value):
        self._score = value
        
    @property
    def strand(self):
        return intervals.Strand(self.trg.strand.int * self.qry.strand.int)

    @property
    def num_members(self):
        if self._num_members is None:
            if self.members is None:
                return 1
            else:
                return num(self.members)
        else:
            return self._num_members

    @num_members.setter
    def num_members(self, value):
        self._num_members = value
    
    def __str__(self):
        return "%s\t%d\t%d\t%s\t%d\t%d\t%s\t%.04f\t%s\t%s" % (
            self.trg.chr,
            self.trg.beg,
            self.trg.end,
            self.qry.chr,
            self.qry.beg,
            self.qry.end,
            self.name,
            self.score,
            self.trg.strand,
            self.qry.strand
        )

    def __eq__(self, other):
        return \
            self.trg == other.trg and \
            self.qry == other.qry and \
            self.name == other.name and \
            self.score == other.score

    __repr__ = __str__


class SeqIDs(object):
    def __init__(self, tracks=None, indices=None):
        self.tracks = list() if tracks is None else tracks
        self.indices = dict() if indices is None else indices

    def index(self, i, j):
        return (j*(j-1))//2 + i
        
    @property
    def num_tracks(self):
        return num(self.tracks)

    @property
    def num_perms(self):
        return self.index(self.num_tracks-1, self.num_tracks-1)


def read_bed(bed_file):
    bed_file = open(bed_file)
    records = collections.OrderedDict()
    
    for line in bed_file:
        line = line.strip()

        if line is _empty or \
           line.startswith(_comment):
            continue

        field = line.split(_tab)

        bed = BED()
        
        try:
            bed.chr = str(field[0])
            bed.beg = int(field[1])
            bed.end = int(field[2])
            bed.id  = str(field[3])
            assert bed.beg <= bed.end
            
        except IndexError:
            sys.stderr.write("ERROR: Too few fields (=%d), expected 4 or more\n" % num(field))
            sys.exit(1)
        except:
            sys.stderr.write("ERROR: Invalid record: '%s'\n" % line)
            sys.exit(1)

        if bed.chr not in records:
            records[bed.chr] = intervals.IntervalList()

        records[bed.chr].add(bed)

    bed_file.close()
        
    return records


def read_sizes(filename):
    sizes_file = open(filename,'r')
    chrom_sizes = collections.OrderedDict()
    for line in sizes_file:
        line = line.strip()

        if line is _empty or \
           line.statswith(_comment):
            continue
        
        fields = line.split(_tab)

        chrom_sizes[fields[0]] = int(fields[1])

    sizes_file.close()
        
    return chrom_sizes


def read_seqids(filename):
    seqids_file = open(filename,'r')
    seqid = SeqIDs()
    for line in seqids_file:
        line = line.strip()

        if line is _empty or \
           line.startswith(_comment):
            continue

        seqids = line.split(_comma)
        seqids_ = []
        for id in seqids:
            if id is _empty:
                continue
            if id.endswith(_dash):
                id = id[:-1]  # remove strand info
            seqids_.append(id)

        seqids = seqids_
                
        if num(seqids) < 1:
            continue
        
        seqid.tracks.append(seqids)

    for track_i in range(0, seqid.num_tracks-1):
        for track_j in range(track_i+1, seqid.num_tracks):
            for chrom_i in seqid.tracks[track_i]:
                for chrom_j in seqid.tracks[track_j]:
                    seqid.indices[(chrom_i, chrom_j)] = seqid.index(track_i, track_j)

    return seqid

        
def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else 'ERROR: %s\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("Usage: %s [options] <input.bedpe> <trg.bed> <qry.bed> <out-prefix>\n" % __program__)
    stream.write("\n")
    stream.write("Options: -S,--seqids <file>  seqids track file for partitioning simple records\n")
    stream.write("         -X,--self-self      input.bedpe represents self-self alignments\n")
    stream.write("         -h,--help           This help message\n")
    stream.write("\n%s\n" % message)
    sys.exit(exitcode)
    

def main(argv):
    seqids = None
    selfself = False
    longoptions = (
        'help',
        'self-self',
        'seqids='
    )
    try:
        options, arguments = getopt.getopt(argv, 'XhS:', longoptions)
        for flag, value in options:
            if   flag in ('-h','--help'): usage(exitcode=0)
            elif flag in ('-S','--seqids'): seqids = read_seqids(value)
            elif flag in ('-X','--self-self'): selfself = True
    except getopt.GetoptError as error:
        usage(error)
    except:
        usage()
        
    if len(arguments) != 4:
        usage('Unexpected number of arguments')
        
    bedpe_file = open(arguments[0])
    trg_loci = read_bed(arguments[1])
    qry_loci = read_bed(arguments[2])
    outfiles = []
    if seqids:
        outfiles = [None] * seqids.num_perms
        for i in range(0, seqids.num_tracks-1):
            for j in range(i+1, seqids.num_tracks):
                outfiles[seqids.index(i, j)] = open(("%s.%d-%d.simple" % (arguments[3], i, j)), 'w')

    outfiles.append(open(("%s.simple" % arguments[3]), 'w'))

    
    for line in bedpe_file:
        line = line.strip()

        if line is _empty or \
           line.startswith(_comment):
            continue

        field = line.split(_tab)

        try:
            for i in range(2):
                chr = 3*i+0
                beg = 3*i+1
                end = 3*i+2
                field[beg] = int(field[beg])
                field[end] = int(field[end])
                assert field[beg] <= field[end]
                
        except IndexError:
            sys.stderr.write("ERROR: Too few fields (=%d), expected 12 or more\n" % num(field))
            sys.exit(1)
        except:
            sys.stderr.write("ERROR: Invalid record: '%s'\n" % line)
            sys.exit(1)

        cluster = BEDPE(
            BED(field[0],
                field[1],
                field[2]
            ),
            BED(field[3],
                field[4],
                field[5]
            ),
            index=[0,0]
        )

        if cluster.trg.chr is _null or cluster.trg.beg < 0 or cluster.trg.end < 0 or \
           cluster.qry.chr is _null or cluster.qry.beg < 0 or cluster.qry.end < 0:           
            continue
        
        if num(field) > 6:
            cluster.name = field[6]
        if num(field) > 7:
            cluster.score = float(field[7])
        if num(field) > 8:
            cluster.trg.strand = field[8]
        if num(field) > 9:
            cluster.qry.strand = field[9]
        
        try:
            cluster.index = [int(field[10]), int(field[10])]
            cluster.num_members = int(field[11])
        except:
            sys.stderr.write("ERROR: expected integer 11th and 12th columns\n" % num(field))
            sys.exit(1)
                

        trg_beg, trg_end = trg_loci[cluster.trg.chr].overlap_range(cluster.trg)
        qry_beg, qry_end = qry_loci[cluster.qry.chr].overlap_range(cluster.qry)

        if trg_beg < 0 or trg_end < 0 or qry_beg < 0 or qry_end < 0:
            sys.stderr.write("WARNING: Unmatched interval: '%s'\n" % str(cluster))
            continue

        if trg_end == num(trg_loci[cluster.trg.chr]):
            trg_end -= 1
        if qry_end == num(qry_loci[cluster.qry.chr]):
            qry_end -= 1

        out = outfiles[-1]
        if seqids:
            trgqry = (cluster.trg.chr, cluster.qry.chr)
            qrytrg = (cluster.qry.chr, cluster.trg.chr)
            if trgqry in seqids.indices:
                out = outfiles[seqids.indices[trgqry]]
            elif selfself and qrytrg in seqids.indices:
                trg_beg, trg_end, qry_beg, qry_end = qry_beg, qry_end, trg_beg, trg_end                
                cluster.trg, cluster.qry = cluster.qry, cluster.trg
                out = outfiles[seqids.indices[qrytrg]]
        
        out.write("%s\t%s\t%s\t%s\t%d\t%s\n" % (
            trg_loci[cluster.trg.chr][trg_beg].id,
            trg_loci[cluster.trg.chr][trg_end].id,
            qry_loci[cluster.qry.chr][qry_beg].id,
            qry_loci[cluster.qry.chr][qry_end].id,
            cluster.num_members,
            cluster.strand.str
        ))

    for outfile in outfiles:
        outfile.close()

        
main(sys.argv[1:])


        
