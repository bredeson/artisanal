
import os
import sys

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Sort debris sequences by descending length'


class MalformedAssemblyFileError(BaseException):
    pass


def main(argv):
    if len(argv) != 2:
        sys.stderr.write("\n")
        sys.stderr.write("Program: %s (%s)\n" % (__program__, __purpose__))
        sys.stderr.write("Version: %s %s\n" % (__pkgname__, __version__))
        sys.stderr.write("Contact: %s\n" % __contact__)
        sys.stderr.write("\n")
        sys.stderr.write("Usage:   %s <in.assembly> <first-debris-contig-name>\n\n\n" % (
            __program__))
        sys.exit(1)

    assembly_file = open(argv[0], 'r')
    first_debris = argv[1]
    outfile = sys.stdout


    seen_head = False
    seen_body = False
    assembly_scaffolds = []
    debris_scaffolds = []
    debris_lengths = []
    seen_debris = False
    scaffold_index = 1    
    contig_name = dict()
    for line in assembly_file:
        if line.isspace() or line.startswith('#'):
            continue
        
        if cols[0].startswith('>'):
            outfile.write(line)
            cols = line.rstrip().split()            
            contig_name[int(cols[1])] = (cols[0].lstrip('>'), int(cols[2]))
            seen_head = True

        elif not seen_head:
            raise MalformedAssemblyFileError("No cprops-style assembly file-header detected")
        
        else:
            cols = line.rstrip().split()
            assembly_contigs = []
            debris_contigs = []
            debris_length = 0
            for vector in cols:
                vector = int(vector)
                index  = abs(vector)
                strand = vector // index

                if contig_name[index][0] == first_debris:
                    seen_debris = True

                if seen_debris:
                    debris_contigs.append(vector)
                    debris_length += contig_name[index][1]
                else:
                    assembly_contigs.append(vector)
                    
                seen_body = True

            assembly_scaffolds.append(assembly_contigs)
            debris_scaffolds.append(debris_contigs)
            debris_lengths.append(debris_length)

    if not seen_body:
        raise MalformedAssemblyFileError("No asm-style assembly file-body detected")

    for scaffold in assembly_scaffolds:
        outfile.write(' '.join(map(str, scaffold)) + '\n')
        
    for l, scaffold in sorted(zip(debris_scafflen, debris_scaffold), reverse=True):
        outfile.write(' '.join(map(str, scaffold)) + '\n')
    
    assembly_file.close()
    outfile.close()


if __name__ == '__main__':
    main(sys.argv[1:])

                

                
                
