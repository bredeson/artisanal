
import os
import sys

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Convert JBAT assembly or cprops file to BED format'


def main(argv):
    if len(argv) != 1:
        sys.stderr.write("\n")
        sys.stderr.write("Program: %s (%s)\n" % (__program__, __purpose__))
        sys.stderr.write("Version: %s %s\n" % (__pkgname__, __version__))
        sys.stderr.write("Contact: %s\n" % __contact__)
        sys.stderr.write("\n")
        sys.stderr.write("Usage:   %s <in.cprops|in.assembly>\n" % __program__)
        sys.stderr.write("\n")
        sys.stderr.write("\n")
        sys.exit(1)

    cprops_file = open(argv[0], 'r')
    outbed_file = sys.stdout

    prev_name = None
    prev_start = 0
    is_assembly_file = False
    for line in cprops_file:
        if line.isspace() or \
           line.startswith('#'):
            continue

        if line.startswith('>'):
            # the .cprops portion of the .assembly file
            is_assembly_file = True
            line = line.lstrip('>')
            
        elif is_assembly_file:
            # the .asm portion of the .assembly file
            continue

        curr_name, curr_index, curr_length = line.rstrip().split()
        orig_name = curr_name.split(':::')
        curr_length = int(curr_length)
        
        if orig_name[0] != prev_name:
            prev_start = 0
        
        outbed_file.write("%s\t%d\t%d\t%s\t%d\t+\n" % (
            orig_name[0],
            prev_start,
            prev_start+curr_length,
            curr_name,
            curr_length
        ))

        prev_name = orig_name[0]
        prev_start += curr_length


main(sys.argv[1:])

