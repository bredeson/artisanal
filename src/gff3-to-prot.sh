#!/bin/bash -l

PROGRAM=$(basename $0 .sh);
PACKAGE='__PACKAGE_NAME__';
VERSION='__PACKAGE_VERSION__';
CONTACT='__PACKAGE_CONTACT__';
PURPOSE="Extract CDS and peptide sequences from GFF3";

# Assumes BEDtools and UCSC Kent Tools are installed. These may be obtained from:
# https://github.com/arq5x/bedtools2/releases   --and--
# https://hgdownload.cse.ucsc.edu/admin/exe/

function usage() {
    printf "\n" >>/dev/fd/2;
    printf "Program: $PROGRAM ($PURPOSE)\n" >>/dev/fd/2;
    printf "Version: $PACKAGE $VERSION\n" >>/dev/fd/2;
    printf "Contact: $CONTACT\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "Usage:   %s [options] <annot.gff3> <genome.fasta> <out-prefix>\n" $(basename $0 .sh) >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "Options: -C,--coding-only         Write protein-coding sequences only [all]\n" >>/dev/fd/2;
    printf "         -L,--longest <str>       Write longest isoform at locus {also,only}\n" >>/dev/fd/2;
    printf "         -G,--gene-name <str>     Gene feature attribute name [ID]\n" >>/dev/fd/2;
    printf "         -N,--tran-name <str>     Transcript/mRNA feature attribute name [Name]\n" >>/dev/fd/2;
    printf "         -g,--index-genes         Index gene loci numerically\n" >>/dev/fd/2;
    printf "         -t,--index-trans         Index transcripts at each locus numerically\n" >>/dev/fd/2;
    printf "         -P,--locus-prefix <str>  Prepend each locus with <str>\n" >>/dev/fd/2;
    printf "         -S,--stop-codon          Include stop codon in output peptide file\n" >>/dev/fd/2;
    printf "         -h,--help                This help document\n" >>/dev/fd/2;
    printf "         -v,--version             Print version info and exit\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "Notes: This converts:\n" >>/dev/fd/2;
    printf "  - annot.gff3 and genome.fasta may be b/gzip compressed\n" >>/dev/fd/2;
    printf "  - top-level gene records with RNA records\n" >>/dev/fd/2;
    printf "  - top-level RNA records\n" >>/dev/fd/2;
    printf "  - RNA records that contain:\n" >>/dev/fd/2;
    printf "    - exon and CDS\n" >>/dev/fd/2;
    printf "    - CDS, five_prime_UTR, three_prime_UTR\n" >>/dev/fd/2;
    printf "    - only exon for non-coding\n" >>/dev/fd/2;
    printf "  - top-level gene records with transcript records\n" >>/dev/fd/2;
    printf "  - top-level transcript records\n" >>/dev/fd/2;
    printf "  - transcript records that contain:\n" >>/dev/fd/2;
    printf "    - exon\n" >>/dev/fd/2;
    printf "  where RNA can be mRNA, ncRNA, rRNA, or tRNA and transcript can be either\n" >>/dev/fd/2;
    printf "  transcript or primary_transcript. C_gene_segment and V_gene_segment\n" >>/dev/fd/2;
    printf "  features will not be converted.\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    exit 1;
}

function version() {
    printf "$PROGRAM $VERSION\n" >>/dev/fd/2;
    exit 0;
}

longest=false;
coding_only=false;
gene_attribute='ID';  # gff3ToGenePred default
mrna_attribute='Name';
index_genes=false;
index_trans=false;
locus_prefix=false;
include_stop='';
while [[ -n $@ ]]; do
    case "$1" in
	-C|--coding-only) coding_only=true;;
	-L|--longest) shift; longest="$1";;
	-N|--tran-name|--mRNA-name|--name) shift; mrna_attribute="$1";;
	-G|--gene-name) shift; gene_attribute="$1";;
	-g|--index-genes) index_genes=true;;
	-t|--index-trans) index_trans=true;;
	-P|--locus-prefix) shift; locus_prefix="$1";;
	-S|--stop-codon) include_stop='-includeStop';;
	-v|--version) version;;
	-h|--help) usage;;
	-*) usage;;
	*) break;;
    esac
    shift;
done

if [[ $# -ne 3 ]]; then
    usage;
fi

missing_dependency=;
for exe in faToTwoBit twoBitInfo gff3ToGenePred genePredToBed genePredToProt bedtools; do
    if [[ ! $(which $exe 2>/dev/null) ]]; then
        echo "[$PROGRAM] ERROR: $exe not detected in PATH" >>/dev/fd/2;
        missing_dependency=1;
    fi
done
if [[ $missing_dependency ]]; then
    exit 1;
fi

gff_filename=$1;
gff_prefix=$(echo $gff_filename | sed 's/\(.gz\|-gz\|.z\|-z\|_z\)$//')

ref_filename=$2;
ref_prefix=$(echo $ref_filename | sed 's/\(.gz\|-gz\|.z\|-z\|_z\)$//')
ref_prefix=$(echo $ref_prefix | sed 's/.f\(asta\|nt\|na\|a\)$//');
prefix=$3;

if [[ ! -s "$ref_prefix.2bit" ]]; then
    # accepts b/gzip'd files as of when?
    faToTwoBit \
	$ref_filename \
	$ref_prefix.2bit;
fi
if [[ ! -s "$ref_prefix.chrom.sizes" ]]; then
    twoBitInfo \
	$ref_prefix.2bit \
	$ref_prefix.chrom.sizes
fi

CAT=cat
if [[ $gff_prefix != $gff_filename ]]; then
    if [[ $(which gzcat 2>/dev/null) ]]; then
	CAT=gzcat
    else
	CAT=zcat
    fi
fi

if [[ $($CAT $gff_filename | head -1 | grep -c -E '^##gff-version\s+3') -eq 1 ]]; then
    # accepts b/gzip'd files as of when?
    gff3ToGenePred \
	-geneNameAttr=$gene_attribute \
	-rnaNameAttr=$mrna_attribute \
	-bad=$prefix.bad \
	$gff_filename \
	$prefix.gp;


#	-warnAndContinue \    
else
    printf "[$PROGRAM] ERROR: '##gff-version 3' header missing or not a GFF3 file\n" >>/dev/fd/2;
    exit 1;
fi

if [[ $index_genes == true ]]; then
    sort -k2,2V -k4,4n -k5,5nr $prefix.gp \
	|  awk 'BEGIN{FS="\t";OFS="\t";i=0} {if (! count[$12]) { i++; count[$12]=i } o=$12; $12=count[$12]; print $0,o}' \
	>  $prefix.tmp.gp \
	&& mv $prefix.tmp.gp $prefix.gp
else
    sort -k2,2V -k4,4n -k5,5nr $prefix.gp \
	|  awk 'BEGIN{FS="\t";OFS="\t"} {print $0,$12}' \
	>  $prefix.tmp.gp \
	&& mv $prefix.tmp.gp $prefix.gp
fi

if [[ $index_trans == true ]]; then
    sort -k12,12V -k4,4n -k5,5nr $prefix.gp \
	|  awk 'BEGIN{FS="\t";OFS="\t"} {if (! count[$12]) { count[$12]=0 } count[$12]++; o=$1; $1=$12"."count[$12]; print $0,o}' \
	>  $prefix.tmp.gp \
	&& mv $prefix.tmp.gp $prefix.gp
else
    sort -k12,12V -k4,4n -k5,5nr $prefix.gp \
	|  awk 'BEGIN{FS="\t";OFS="\t"} {print $0,$1}' \
	>  $prefix.tmp.gp \
	&& mv $prefix.tmp.gp $prefix.gp
fi

if [[ "$locus_prefix" != false ]]; then
    awk -v pre="$locus_prefix" 'BEGIN{FS="\t";OFS="\t"} {$1=pre$1; $12=pre$12; print $0}' $prefix.gp >$prefix.tmp.gp \
	&& mv $prefix.tmp.gp $prefix.gp
fi

awk 'BEGIN{FS="\t";OFS="\t"} {print $16,$12,$17,$1}' $prefix.gp >$prefix.alias \
    && cut -f1-15 $prefix.gp >$prefix.tmp.gp \
    && mv $prefix.tmp.gp $prefix.gp

if [[ $coding_only == true ]]; then
    awk 'BEGIN{FS="\t";OFS="\t"} {if ($13 != "none") {print}}' $prefix.gp >$prefix.tmp.gp \
        && mv $prefix.tmp.gp $prefix.gp
fi


if [[ $longest != "only" ]]; then
    genePredToBed \
	$prefix.gp \
	stdout \
	| bedtools sort -g $ref_prefix.chrom.sizes -i - \
	> $prefix.bed;

    bedtools getfasta \
	-s -split -nameOnly \
	-bed $prefix.bed \
	-fi  $ref_filename \
	-fo - \
	| awk '{if (/^>/) {sub(/\([+\-]\)$/,"",$1); print} else {for (i=1; i <= length($1); i+=50) {print substr($1,i,50)}}}' \
	> $prefix.rna.fasta;
    
    genePredToProt \
	-cdsFa=$prefix.cds.fasta \
	$include_stop \
	$prefix.gp \
	$ref_prefix.2bit \
	$prefix.pep.fasta;

    awk 'BEGIN {
    FS="\t";
}
{
    cds_beg=$6;
    cds_end=$7;
    gsub(/,$/,"",$9);
    gsub(/,$/,"",$10);
    split($9,xon_beg,",");
    split($10,xon_end,",");
    if ($3 == "-") {
        BEG=0;
        LEN=0;
        for (i=$8; i >= 1; i--) {
            if (xon_beg[i] > cds_end) {
                BEG += (xon_end[i] - xon_beg[i]);
            } else if (xon_end[i] >= cds_end) {
                BEG += (xon_end[i] - cds_end);
                LEN += (cds_end - (cds_beg > xon_beg[i] ? cds_beg : xon_beg[i]));
            } else if (xon_beg[i] >= cds_beg) {
                LEN += (xon_end[i] - xon_beg[i]);
            } else if (xon_end[i] > cds_beg) {
                LEN += (xon_end[i] - cds_beg);
            }
        }
        print $1,"+",BEG+1,LEN;
    } else {
        BEG=0;
        LEN=0;
        for (i=1; i <= $8; i++) {
            if (xon_end[i] < cds_beg) {
                BEG += (xon_end[i] - xon_beg[i]);
            } else if (xon_beg[i] <= cds_beg) {
                BEG += (cds_beg - xon_beg[i]);
                LEN += ((cds_end < xon_end[i] ? cds_end : xon_end[i]) - cds_beg);
            } else if (xon_end[i] <= cds_end) {
                LEN += (xon_end[i] - xon_beg[i]);
            } else if (xon_beg[i] < cds_end) {
                LEN += (cds_end - xon_beg[i]);
            }
        }
        print $1,"+",BEG+1,LEN;
    }
}' $prefix.gp >$prefix.cds.txt
fi

if [[ $longest != false ]]; then
    # adds cds length as column 1, column 13 becomes locus name
    awk 'BEGIN{FS="\t";OFS="\t"} {split($9,beg,","); split($10,end,","); l=0; for(i=0; i <= $8; i++) {l+=(end[i]-beg[i])} print l,$0}' $prefix.gp \
	| sort -k13,13V -k1,1nr \
	| awk 'BEGIN{FS="\t";OFS="\t";p=""} {if ($13 != p) {print} p=$13}' \
	| cut -f2- \
	| sort -k1,1V \
	> $prefix.longest.gp;

    prefix="$prefix.longest";
    genePredToBed \
	$prefix.gp \
	stdout \
	| bedtools sort -g $ref_prefix.chrom.sizes -i - \
	> $prefix.bed;

    bedtools getfasta \
	-s -split -nameOnly \
	-bed $prefix.bed \
	-fi  $ref_filename \
	-fo - \
	| awk '{if (/^>/) {sub(/\([+\-]\)$/,"",$1); print} else {for (i=1; i <= length($1); i+=50) {print substr($1,i,50)}}}' \
	> $prefix.rna.fasta;
    
    genePredToProt \
	-cdsFa=$prefix.cds.fasta \
	$include_stop \
	$prefix.gp \
	$ref_prefix.2bit \
	$prefix.pep.fasta;

    awk 'BEGIN {
    FS="\t";
}
{
    cds_beg=$6;
    cds_end=$7;
    gsub(/,$/,"",$9);
    gsub(/,$/,"",$10);
    split($9,xon_beg,",");
    split($10,xon_end,",");
    if ($3 == "-") {
        BEG=0;
        LEN=0;
        for (i=$8; i >= 1; i--) {
            if (xon_beg[i] > cds_end) {
                BEG += (xon_end[i] - xon_beg[i]);
            } else if (xon_end[i] >= cds_end) {
                BEG += (xon_end[i] - cds_end);
                LEN += (cds_end - (cds_beg > xon_beg[i] ? cds_beg : xon_beg[i]));
            } else if (xon_beg[i] >= cds_beg) {
                LEN += (xon_end[i] - xon_beg[i]);
            } else if (xon_end[i] > cds_beg) {
                LEN += (xon_end[i] - cds_beg);
            }
        }
        print $1,"+",BEG+1,LEN;
    } else {
        BEG=0;
        LEN=0;
        for (i=1; i <= $8; i++) {
            if (xon_end[i] < cds_beg) {
                BEG += (xon_end[i] - xon_beg[i]);
            } else if (xon_beg[i] <= cds_beg) {
                BEG += (cds_beg - xon_beg[i]);
                LEN += ((cds_end < xon_end[i] ? cds_end : xon_end[i]) - cds_beg);
            } else if (xon_end[i] <= cds_end) {
                LEN += (xon_end[i] - xon_beg[i]);
            } else if (xon_beg[i] < cds_end) {
                LEN += (cds_end - xon_beg[i]);
            }
        }
        print $1,"+",BEG+1,LEN;
    }
}' $prefix.gp >$prefix.cds.txt
fi
