
import os
import sys
import math
import logging

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Collapse negative gaps between adjacent contigs'


num = len

# configure global logging:
logging.basicConfig(
    stream=sys.stderr,
    level=logging.INFO,
    format="[{0}] %(asctime)s [%(levelname)s] %(message)s".format(
        __program__)
)
try:
    # importing non-standard Python libraries:
    # (prefix module names with `_' to prevent pydoc API exposure)
    import re
    import gzip
    import pysam
    import bisect
    import getopt
    import random    
    import hashlib
    import subprocess
    import collections
except ImportError as error:
    logging.error("%s: %s" % (error.__class__.__name__, str(error)))
    sys.exit(1)

num = len

INT_FIELDS = (0,1,2,3,4,5,7,8)
FLOAT_FIELDS = (6,9,10)
CONTIG_EDGE = 1
FRAGMENT_NUMBER_TAG = ':::fragment_'
FRAGMENT_DEBRIS_TAG = ':::debris'
CIGAR_OPS = ['M','I','D','N','S','H','P','=','X','B']
CIGAR_MATCH = set('M=')
CIGAR_SUBST = set('X')
CIGAR_INDEL = set('ID')
CIGAR_SKIPS = set('NSHPB')
CIGAR_PARSE = re.compile('(\d+)([MIDNSHP=XB])')

# trg and qry positional args are swapped on purpose:
BLASR_5_CMD = ' {trg:s} {qry:s} --bestn 5 --allowAdjacentIndels --minAlnLength 100 --minPctSimilarity 80 -m 5'
BLASR_3_CMD = ' {trg:s} {qry:s} -bestn 5 -allowAdjacentIndels -minAlnLength 100 -minPctIdentity 80 -m 5'
MINIMAP_2_CMD = ' -x asm10 --eqx -c {qry:s} {trg:s}'


PYTHONVERSION = sys.version_info[:2]
if PYTHONVERSION < (3,):
    range = xrange
    import string
    NUCLEOTIDE_COMPLEMENT = \
        string.maketrans(
            'acgturyswkmbdhvnACGTURYSWKMBDHVN',
            'tgcaayrwsmkvhdbnTGCAAYRWSMKVHDBN'
        )
else:
    NUCLEOTIDE_COMPLEMENT = {
        65: 'T',  97: 't',
        66: 'V',  98: 'v',
        67: 'G',  99: 'g',
        68: 'H', 100: 'h',
        71: 'C', 103: 'c',
        72: 'D', 104: 'd',
        75: 'M', 107: 'm',
        77: 'K', 109: 'k',
        78: 'N', 110: 'n',
        82: 'Y', 114: 'y',
        83: 'W', 115: 'w',
        84: 'A', 116: 'a',
        85: 'A', 117: 'a',
        86: 'B', 118: 'b',
        87: 'S', 119: 's',
        89: 'R', 121: 'r',
    }


class MalformedAssemblyFileError(BaseException):
    pass
    
class Strand(object):
    def __init__(self, obj):
        if isinstance(obj, Strand):
            self.__strand = obj.__strand
        elif isinstance(obj, int):
            self.__strand = obj // abs(obj) if obj else obj
        elif isinstance(obj, str):
            self.__strand = self.__to_int(obj)
        else:
            raise TypeError("Invalid strand type: int, str, or Strand obj expected")

    def __to_int(self, strand):
        if   strand == '.':
            return 0
        elif strand == '-':
            return -1
        elif strand == '+':
            return +1
        else:
            raise ValueError("Invalid strand value: '+', '-', or '.' expected")
        
    def __str__(self):
        if self.__strand < 0:
            return '-'
        if self.__strand > 0:
            return '+'
        if self.__strand == 0:
            return '.'
        
    @property
    def int(self):
        return self.__strand

    @property
    def str(self):
        return str(self)
    
    def reverse(self):
        self.__strand *= -1


class BaseInterval(object):
    def __init__(self):
        """
        Method class. To use these methods, the user must create
        a child class and define the following attributes at __init__:
        self.name : Name of sequence
        self.length : Length of unaligned sequence
        self.beg : The beginning/starting position of the alignment
        self.end : The ending/stopping position of the alignment
        self.aligned_length : Length of the aligned portion of the sequence
        """
        pass

    def __str__(self):
        return "%s:%d-%d" % (
            str(self.name),
            self.beg,
            self.end,
        )

    def __repr__(self):
        return str(self)
    
    def __lt__(self, other):
        if self.name < other.name:
            return True
        
        if self.name == other.name:
            if self.beg < other.beg:
                return True
            if self.beg == other.beg:
                return self.end < other.end
        return False

    def __eq__(self, other):
        return self.name == other.name and \
            self.beg == other.beg and \
            self.end == other.end

    def __le__(self, other):
        return self < other or self == other

    def __gt__(self, other):
        if self.name > other.name:
            return True

        if self.name == other.name:
            if self.beg > other.beg:
                return True
            if self.beg == other.beg:
                return self.end > other.end
        return False

    def __ge__(self, other):
        return self > other or self == other

    def __len__(self):
        return abs(self.end - beg)
    
    @property
    def mid(self):
        return (self.beg + self.end) // 2
    
    def overlaps(self, other):
        """test whether self has any kind of overlap with other"""
        if other.name == self.name:
            if other.beg < self.end <= other.end:
                # self.beg ========= self.end
                #      other.beg ============== other.end
                # (b/c self.beg < self.end, also True for 'within' queries)
                return True
            
            elif self.beg < other.end <= self.end:
                #           self.beg ========= self.end
                # other.beg ============== other.end
                # (b/c other.beg < other.end, also True for 'contains' queries)
                return True

        return False
    
    def contains(self, other):
        """test whether self contains other"""
        if other.name == self.name:
            if self.beg <= other.beg and other.end <= self.end:
                return True

        return False

    def within(self, other):
        """test whether self is contained within other"""
        if other.name == self.name:
            if other.beg <= self.beg and self.end <= other.end:
                return True

        return False

    def overlaps_beg(self, other):
        """test whether self overlaps 5'-end of other"""
        if other.name == self.name:
            if self.overlaps(other) and \
               other.beg < self.end < other.end:
                # self.beg ========= self.end
                #      other.beg ============== other.end
                return True

        return False

    def overlaps_end(self, other):
        """test whether self overlaps 3'-end other"""
        if other.name == self.name:
            if self.overlaps(other) and \
               other.beg < self.beg < other.end:
                #           self.beg ========= self.end
                # other.beg ============== other.end
                return True

        return False

    @property
    def p5_spur_length(self):
        return self.beg
    
    @property
    def p3_spur_length(self):
        return self.length - self.end    
    
    @property
    def aligned_fraction(self):
        return float(self.aligned_length) / self.length
            
    def overlap_length(self, other):
        if self.overlaps(other):
            if self.within(other):
                return self.aligned_length
        
            elif self.contains(other):
                return other.aligned_length
            
            elif self.overlaps_beg(other):
                return self.end - other.beg

            elif self.overlaps_end(other):
                return other.end - self.beg

            else:
                raise AssertionError("Unhandled overlap type")
        else:
            return 0
            
    def overlap_fraction(self, other, index=None):
        return float(self.overlap_length(other, index)) / self.aligned_length

    
class Sequence(object):
    def __init__(self, name, sequence, comment=None, beg=0, end=0, strand=1, edges=0):
        self.name = name
        self.comment = comment
        self.sequence = sequence
        self.beg = beg or 0
        self.end = end or self.length
        self.strand = Strand(strand)
        self.edges = edges

    @property
    def length(self):
        return len(self.sequence)
        
    def reverse(self):
        self.sequence = self.sequence[::-1].translate(NUCLEOTIDE_COMPLEMENT)
        self.beg, self.end = self.length - self.end, self.length - self.beg
        self.strand.reverse()
        
    
class AlignedSegment(BaseInterval):
    def __init__(self, name, length, beg, end, strand=0):
        self.name   = name
        self.length = int(length)
        self.beg    = int(beg)
        self.end    = int(end)
        self.strand = Strand(strand)
        
    @property
    def aligned_length(self):
        return self.end - self.beg
    
        
class AlignedSegmentPair(object):
    def __init__(self, qry, trg, num_matches=0, num_mismatches=0, num_indels=0, cigar=None):
        self.qry = qry
        self.trg = trg
        self.num_matches = num_matches
        self.num_mismatches = num_mismatches
        self.num_indels = num_indels
        self.cigar = cigar

    @property
    def length(self):
        return self.num_matches + self.num_mismatches + self.num_indels
        
    @property
    def strand(self):
        return Strand(self.qry.strand.int * self.trg.strand.int)

    @property
    def similarity(self):
        return self.num_matches / max(1.0, float(self.length))
    

class AlignedTiling(BaseInterval):
    def __init__(self, key=lambda x: x):
        self.name = None
        self.matches = 0
        self.length = 0
        self.aligned_length = 0
        self.__len = 0
        self.pairs = []
        self.key = key
        
    def __getitem__(self, index):
        return self.pairs[index][-1]

    def __delitem__(self, item):
        del(self.pairs[item]);
        self.__len -= 1
    
    def __iter__(self):
        return iter(map(lambda a: a[-1], self.pairs))

    def __len__(self):
        return self.__len

    @property
    def beg(self):
        return self.key(self[0]).beg

    @property
    def end(self):
        return self.key(self[-1]).end
    
    @property
    def is_empty(self):
        return self.__len == 0
    
    def is_beg(self, pairL, stranded=False):
        if self.is_empty:
            return True
        pairR = self[0]
        segR  = self.key(pairR)
        segL  = self.key(pairL)
        if segL.mid <= segR.beg:
            if stranded:
                return pairL.strand.int == pairR.strand.int
            else:
                return True
        return False
            
    def is_mid(self, pairM, index, stranded=False):
        pairL = self[index-1]
        pairR = self[index]
        segL  = self.key(pairL)
        segM  = self.key(pairM)
        segR  = self.key(pairR)
        if segL.end <= segM.mid <= segR.beg:
            if stranded:
                return pairL.strand.int == pairM.strand.int and \
                    pairM.strand.int == pairR.strand.int
            else:
                return True
        return False
    
    def is_end(self, pairR, stranded=False):
        pairL = self[-1]
        segL  = self.key(pairL)
        segR  = self.key(pairR)
        if segL.end <= segR.mid:
            if stranded:
                return pairL.strand.int == pairR.strand.int
            else:
                return True
        return False

    def generic_search(self, pair, left=False, key=None):
        """
        Search an AlignedTiling object using an AlignedSegmentPair 
        object as the query
        """
        key = self.key if key is None else key
        item = (key(pair), pair)

        # TODO: pair is used in comparison when key(pair) is already
        #  in self.pairs. This raises AttributeError(s) via pair
        if left:
            return bisect.bisect_left(self.pairs, item)
        else:
            return bisect.bisect_right(self.pairs, item)

    def overlap_search(self, other):
        beg = 0
        end = num(self) - 1

        if beg > end:
            return -1

        that = self.key(other)
        
        while beg <= end:
            mid = (beg + end) // 2

            this = self.key(self[mid])

            if that.overlaps(this):
                return mid

            elif that.beg < this.beg:
                end = mid - 1

            elif that.end > this.end:
                beg = mid + 1

            else:
                raise NotImplementedError("Unhandled condition (%d -- %d -- %d)\n" % (beg, mid, end))

        return -1
    
    def add(self, pair):
        """
        Takes an AlignedSegmentPair object and accesses its AlignedSegment child
        of appropriate type to determine if the input AlignedSegmentPair represents
        a consistent alignment for tiling.
        """
        other = self.key(pair)
        item = (other, pair)

        if self.is_empty:
            self.name = other.name
            self.length = other.length
            self.pairs.append(item)
            self.aligned_length += pair.length
            self.matches += pair.num_matches
            self.__len += 1
            return True
            
        i = self.generic_search(pair)
        if i == 0:
            # insert at 5' end of tiled pairs
            bisect.insort_right(self.pairs, item)
            self.aligned_length += pair.length
            self.matches += pair.num_matches
            self.__len += 1
            return True

        elif i == num(self.pairs):
            # insert at 3' end of tiled pairs
            self.pairs.append(item)
            self.aligned_length += pair.length
            self.matches += pair.num_matches
            self.__len += 1
            return True
                
        elif i < num(self.pairs):
            # insert between two previously tiled pairs
            bisect.insort_right(self.pairs, item)
            self.aligned_length += pair.length
            self.matches += pair.matches
            self.__len += 1
            return True

        return False

    @property
    def similarity(self):
        return self.matches / max(1.0, float(self.aligned_length))
    
    def overlap_length(self, other, index=None):
        """
        Determine the amount of overlap between two AlignedTiling
        objects, with respect to self.
        """

        if index is None:
            index = self.overlap_search(other)
        if index < 0:
            return 0.0

        that = self.key(other)
        
        overlap_length = that.overlap_length(self.key(self[index]))

        b = index
        e = index
        L = num(self) - 1
        while b > 1:
            this = self.key(self[b-1])
            if not this.overlaps(that):
                break
            overlap_length += that.overlap_length(this)
            b -= 1

        while e < L:
            this = self.key(self[e+1])
            if not this.overlaps(that):
                break
            overlap_length += that.overlap_length(this)
            e += 1

        return overlap_length
        
    def overlap_fraction(self, other):
        return float(self.overlap_length(other)) / self.key(other).aligned_length
    

def _log_exception(exception, message, traceback):
    """
    Exception handler to redirect errors to the logging module
    """
    logging.error("%s: %s" % (exception.__name__, message))
    sys.exit(1)
        


def utf8(string):
    if PYTHONVERSION < (3,):
        return string
    else:
        return string.encode('utf-8')
    

def run_command(command):
    try:
        output = subprocess.check_output(command, shell=True, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError:
        output = None

    if hasattr(output, 'decode'):
        output = output.decode('utf-8')
    return output


def get_samtools_version(exe):
    output = run_command(exe)

    if output is not None:
        output = filter(lambda x: x.startswith('Version'), output.splitlines())
        return list(map(int, output.split()[1].replace('-','.').split('.')[:3]))
        
    raise Exception("Cannot determine samtools version number")
    
    
def get_blasr_version(exe):
    for version_flag in ('-version', '--version'):
        output = run_command('%s %s' % (exe, version_flag))

        if output is not None:
            return list(map(int, output.strip().split('\t')[1].split('.')[:2]))
    
    raise Exception("Cannot determine blasr version number")


def get_minimap2_version(exe):
    for version_flag in ('--version',):
        output = run_command('%s %s' % (exe, version_flag))

        if output is not None:
            return list(map(int, output.strip().replace('-','.').split('.')[:2]))
    
    raise Exception("Cannot determine blasr version number")


def tempfile(prefix='tmp', suffix='txt'):
    return "%s%s.%s" % (
        prefix,
        hashlib.sha1(utf8(str(random.randint(0, 100000000)))).hexdigest().replace(' ','_'),
        suffix
    )


def get_cigar_stats(cigar):
    num_matches = 0
    num_mismatches = 0
    num_indels = 0
    for operator in CIGAR_PARSE.finditer(cigar):
        op = operator.group(2)
        n  = operator.group(1)
        if op in CIGAR_MATCH:
            num_matches += int(n)
        elif op in CIGAR_SUBST:
            num_mismatches += int(n)
        elif op in CIGAR_INDEL:
            num_indels += int(n)

    return (num_matches, num_mismatches, num_indels)


def get_cigar_string(overlap, trg_seq, qry_seq, eqx=False):
    cigar = []
    if overlap.qry.trg_spur_length:
        cigar.append("%sS" % overlap.qry.p5_spur_length)

    num_op = 0
    prev_op = 0
    prev_op = -1
    for i in range(len(trg_seq)):
        if trg_seq[i] == '-' and qry_seq[i] == '-':
            curr_op = 6
        elif trg_seq[i] != '-' and qry_seq[i] != '-':
            if trg_seq[i] == qry_seq[i]:
                curr_op = 7 if eqx else 0
            else:
                curr_op = 8 if eqx else 0
        elif trg_seq[i] == '-':
            curr_op = 1
        elif qry_seq[i] == '-':
            curr_op = 2
        else:
            raise NotImplementedError("Unhandled alignment column: %s -- %s" % (
                trg_seq[i], qry_seq[i]))

        if prev_op >= 0 and \
           prev_op != curr_op:
            cigar.append("%d%s" % (num_op, CIGAR_OPS[prev_op]))
            num_op = 0
            
        prev_op = curr_op    
        num_op += 1
        
    if num_op:
        cigar.append("%d%s" % (num_op, CIGAR_OPS[prev_op]))
    else:
        cigar = ['*']

    if overlap.trg.p3_spur_length:
        cigar.append("%sS" % overlap.trg.p3_spur_length)
                
    return ''.join(cigar)
        

def parse_m5(line):
    field = line.strip().split(' ')
    logging.debug("      %s" % ' '.join(field[:17]))
    
    # BLASR m5 format:
    # [0]  qName     [6]  tName      [11] score
    # [1]  qLength   [7]  tLength    [12] numMatch
    # [2]  qStart    [8]  tStart     [13] mumMismatch
    # [3]  qEnd      [9]  tEnd       [14] numIns
    # [4]  qStrand   [10] tStrand    [15] numDel
    # [5]  <BLANK>                   [16] mapQV
    for f in (1,2,3, 7,8,9,11,12,13,14,15,16):
        field[f] = int(field[f])

    overlap = AlignedSegmentPair(
        AlignedSegment(field[6], field[7], field[8], field[9], field[10]),
        AlignedSegment(field[0], field[1], field[2], field[3], field[4]),
        num_matches=field[12],
        num_mismatches=field[13],
        num_indels=field[14] + field[15]
    )
    overlap.cigar = get_cigar_string(best_overlap, best_trg, best_qry)

    # m5 query coordinates are always on [+] strand; [-] strand
    # reported for target and coordinates not reported on matching
    # strand (i.e., they are reported on original input strand)
    if overlap.qry.strand.int < 0:
        # report coords according to original strand
        overlap.qry.beg, overlap.qry.end = \
            overlap.qry.length - overlap.qry.end, overlap.qry.length - overlap.qry.beg
    
    return overlap


def parse_paf(line):
    field = line.strip().split('\t')
    logging.debug("      %s" % ' '.join(field[:12]))
    
    for f in (1,2,3,6,7,8,9,10,11):
        field[f] = int(field[f])
    
    cigar = None
    for f in range(12, len(field)):
        if field[f].startswith('cg:Z:'):
            cigar = field[f][5:]
            break
        
    if cigar is None:
        raise Exception("PAF record without cg:Z: %r" % line)

    matches, mismatches, indels = get_cigar_stats(cigar)

    overlap = AlignedSegmentPair(
        AlignedSegment(field[5], field[6], field[7], field[8], field[4]),
        AlignedSegment(field[0], field[1], field[2], field[3], '+'),
        num_matches=matches,
        num_mismatches=mismatches,
        num_indels=indels,
        cigar=cigar
    )
    
    # PAF query coordinates are always on [+] strand; [-] strand
    # reported for target and coordinates not reported on matching
    # strand (i.e., they are reported on original input strand)
    if overlap.qry.strand.int < 0:
        # report coords according to original strand
        overlap.qry.beg, overlap.qry.end = \
            overlap.qry.length - overlap.qry.end, overlap.qry.length - overlap.qry.beg

    return overlap


def check_overlap(contig_5p, contig_3p, min_overlap_length=1, max_overlap_length=1000000,
                  max_spur_fraction=0.01, max_subst_rate=0.03, max_indel_rate=0.15, use_blasr=False):
    logging.info("  Checking overlap: %s - %s" % (contig_5p.name, contig_3p.name))

    max_length = min(
        contig_5p.length,
        contig_3p.length,
        max_overlap_length
    )
    
    contig_5p_beg = max(contig_5p.beg, contig_5p.end - max_length - 5000)
    contig_5p_end = contig_5p.length
    contig_3p_beg = 0
    contig_3p_end = min(contig_3p.end, contig_3p.beg + max_length + 5000)
    
    with open(tempfile(suffix='fasta'), 'w') as trg:
        trg.write(
            format_temp_fasta(
                "T/0/%d_%d" % (contig_5p_beg, contig_5p_end),
                contig_5p.sequence[contig_5p_beg:contig_5p_end],
                width=100
            )
        )
    with open(tempfile(suffix='fasta'), 'w') as qry:
        qry.write(
            format_temp_fasta(
                "Q/0/%d_%d" % (contig_3p_beg, contig_3p_end),
                contig_3p.sequence[contig_3p_beg:contig_3p_end],
                width=100
            )
        )

    # we run blasr or minimap2 with the qry and trg sequences swapped.
    # This keeps the flank sequences always in their original orientations
    # and _haplotig\d+ sequence reported in the orientation it should be
    # incorporated into the chromosome:
    input_block = run_command(ALIGN_CMD.format(trg=trg.name, qry=qry.name))

    best_sim = 0
    best_sub = 0
    best_ind = 0
    best_line = '<none>'
    best_score = -1
    best_overlap = None
    logging.debug("    Hits:")
    for input_line in input_block.splitlines():
        input_line = input_line.strip()
        
        if use_blasr:
            overlap = parse_m5(input_line)
        else:
            overlap = parse_paf(input_line)    
        
        obs_subst_rate = float(overlap.num_mismatches) / float(overlap.num_matches + overlap.num_mismatches)
        obs_indel_rate = float(overlap.num_indels) / float(overlap.length)
        
        if obs_subst_rate <= max_subst_rate and \
           obs_indel_rate <= max_indel_rate:

            overlap.trg.name   = contig_5p.name
            overlap.trg.length = contig_5p.length
            overlap.trg.beg   += contig_5p_beg
            overlap.trg.end   += contig_5p_beg
            
            overlap.qry.name   = contig_3p.name
            overlap.qry.length = contig_3p.length
            overlap.qry.beg   += contig_3p_beg
            overlap.qry.end   += contig_3p_beg

            max_spur_length = max(50, max_spur_fraction * overlap.length)
            
            if overlap.length >= min_overlap_length:
                if   overlap.qry.p5_spur_length <= max_spur_length and \
                     overlap.qry.p3_spur_length <= max_spur_length:
                    logging.debug("         <query-contained>")
                    
                elif overlap.trg.p5_spur_length <= max_spur_length and \
                     overlap.trg.p3_spur_length <= max_spur_length:
                    logging.debug("         <query-contains>")
                elif overlap.qry.p5_spur_length <= max_spur_length and \
                     overlap.trg.p3_spur_length <= max_spur_length:
                    if overlap.num_matches > best_score:
                        logging.debug("         <improved-score>")
                        best_score = overlap.num_matches
                        best_overlap = overlap
                        best_line = input_line.replace('\t',' ')
                        best_sim = overlap.similarity
                        best_sub = obs_subst_rate
                        best_ind = obs_indel_rate
                    else:
                        logging.debug("         <insufficient-score>")
                else:
                    logging.debug("         <insufficient-overlap-structure>")
            else:
                logging.debug("         <insufficient-overlap-length>")
        else:
            logging.debug("         <insufficient-similarity>")

            
    if best_overlap is None or \
       best_overlap.qry.strand.int < 0:
        best_line = '<none>'
        best_overlap = None
        best_score = -1
        
    logging.debug("    Overlap (%0.3f, %0.3f):" % (best_sub, best_ind));
    logging.debug("      %s" % best_line)
    
    os.unlink(qry.name)
    os.unlink(trg.name)
                    
    return (best_overlap, best_score)



def get_median_depth(bamfile, contig):
    total = 0
    count = {}
    for pile in bamfile.pileup(contig.name, contig.beg, contig.end, stepper='nofilter'):
        if pile.nsegments not in count:
            count[pile.nsegments] = 0
        count[pile.nsegments] += 1
        total += 1

    midpoint = total / 2.0
    total = 0
    median_depth = 0
    for depth in sorted(count):
        if count[depth]+total >= midpoint:
            median_depth = depth
            break
        total += count[depth]

    logging.debug("    Median depth of %s:%d-%d = %d" % (
        contig.name, contig.beg, contig.end, median_depth))
        
    return median_depth

        
def which(filename, fatal=False):
    if os.path.exists(filename):
        return filename
    for path in os.environ["PATH"].split(os.pathsep):
        testpath = os.path.join(path, filename)
        if os.path.exists(testpath) and \
           os.access(testpath, os.X_OK):
            return testpath

    if fatal:
        raise EnvironmentError("no %s in PATH" % filename)
    else:
        return None


def _fold(linear_string, width=None):
    if width is None:
        return linear_string

    folded_string = []
    linear_length = len(linear_string)
    for offset in range(0, linear_length, width):
        if offset+width < linear_length:
            folded_string.append(linear_string[offset:offset+width])
        else:
            folded_string.append(linear_string[offset:])
            
    return '\n'.join(folded_string)
    

def format_fasta(record, width=None):
    comment = '' if record.comment is None else " {%s}" % record.comment

    return ">%s%s\n%s\n" % (record.name, comment, _fold(record.sequence, width=width))


def format_temp_fasta(name, sequence, width=None):
    return ">%s\n%s\n" % (name, _fold(sequence, width=width))


def format_gfahead():
    return "H\tVN:Z:1.0\n"

def format_gfaseg(record):
    return "S\t%s\t*\tLD:i:%d\n" % (record.name, record.length)


def format_gfalink(overlap):
    return "L\t%s\t%s\t%s\t%s\t%s\n" % (
        overlap.trg.name, overlap.trg.strand.str,
        overlap.qry.name, overlap.qry.strand.str,
        overlap.cigar)


def format_gfapath(path_name, path_members):
    return "P\t%s\t%s\t*\n" % (
        path_name, ','.join(path_members))


def mixrange(string):
    # see: https://stackoverflow.com/questions/18759512/expand-a-range-which-looks-like-1-3-6-8-10-to-1-2-3-6-8-9-10
    res = []
    max_ = 0
    for val in string.split(','):
        if '-' in val:
            try:
                low, high = tuple(map(int, val.split('-')))
            except TypeError:
                raise ValueError("Invalid range spec: %s" % string)
            if low <= max_:
                raise ValueError("Invalid range spec: %s" % string)
            res.extend(list(range(low, high+1)))
            max_ = high
        else:
            val = int(val)
            if val < max_:
                raise ValueError("Invalid range spec: %s" % string)
            res.append(val)
            max_ = val
            
    return res


def htoi(value):
    value = str(value)
    coeff = value.upper()
    expon = 0
    
    if coeff.endswith('Y'):
        expon = 8
    elif coeff.endswith('Z'):
        expon = 7
    elif coeff.endswith('E'):
        expon = 6
    elif coeff.endswith('P'):
        expon = 5
    elif coeff.endswith('T'):
        expon = 4
    elif coeff.endswith('G'):
        expon = 3
    elif coeff.endswith('M'):
        expon = 2
    elif coeff.endswith('K'):
        expon = 1
    elif coeff.endswith('H'):
        expon = 2.0 / 3.0
    elif coeff.endswith('D'):
        expon = 1.0 / 3.0
    elif coeff.replace('.','',1).isdigit():
        expon = 0
    else:
        raise ValueError("Not a valid numeric value: '%s'"  % value)

    if expon:
        coeff = coeff[:-1]
    if coeff.replace('.','',1).isdigit():
        if '.' in coeff:
            return float(coeff) * int(round(1000 ** expon, 0))
        else:
            return int(coeff) * int(round(1000 ** expon, 0))
    else:
        raise ValueError("Not a valid numeric value: '%s'"  % value)
    

def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else 'ERROR: %s\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] <in.assembly> <in.fasta> <out-prefix>\n" % (
        __program__))
    stream.write("\n")
    stream.write("Options:\n")
    stream.write("  -B,--blasr                       Use blasr as aligner [minimap2]\n")
    stream.write("  -b,--bam <file>                  BAM file of aligned reads [null]\n")
    stream.write("  -L,--stitch-list <range>         Scaffold numbers to stitch [all]\n")
    stream.write("  -O,--max-overlap-length <uint>   Max length for overlap search [2^%d-1]\n" % (
        math.log(sys.maxsize, 2)+1))
    stream.write("  -o,--min-overlap-length <uint>   Min overlap length for merge [1000]\n")
    stream.write("  -I,--max-indel-rate <ufloat>     Max indel rate tolerance [0.15]\n")
    stream.write("  -S,--max-subst-rate <ufloat>     Max substitution rate tolerance [0.03]\n")
    stream.write("  -s,--max-spur-fraction <ufloat>  Max spur tolerance (< 1.0) [0.01]\n")
    stream.write("  -D,--debug                       Enable debugging output\n")
    stream.write("  -h,--help                        This help message\n")
    stream.write("\n")
    stream.write("%s\n" % message)
    sys.exit(exitcode)

    
def main(argv):
    global ALIGN_CMD
    long_flags = [
        'help',
        'debug',
        'blasr',
        'bam=',
        'max-subst-rate=',
        'max-indel-rate=',
        'min-overlap-length=',
        'max-overlap-length=',
        'max-spur-fraction=',
        'stitch-list='
    ]
    short_flags = 'BhDb:S:s:I:o:O:L:'
    
    try:
        options, arguments = getopt.getopt(argv, short_flags, long_flags)
    except getopt.GetoptError as error:
        usage(error)
        
    debug = False
    blasr = False
    alignments_file = None
    parse_cprops_names = False
    max_subst_rate = 0.03
    max_indel_rate = 0.15
    max_spur_frac = 0.01
    min_overlap_length = 1000
    max_overlap_length = sys.maxsize
    expected_half_depth = None
    expected_full_depth = None
    expected_rept_depth = None
    expected_poor_depth = None
    stitch_scaffolds_list = list()
    for flag, value in options:
        if   flag in {'-h','--help'}: usage(exitcode=0)
        elif flag in {'-D','--debug'}: debug = True
        elif flag in {'-B','--blasr'}: blasr = True
        elif flag in {'-I','--max-indel-rate'}: max_indel_rate = float(value)
        elif flag in {'-S','--max-subst-rate'}: max_subst_rate = float(value)
        elif flag in {'-s','--max-spur-fraction'}: max_spur_frac = float(value)
        elif flag in {'-O','--max-overlap-length'}: max_overlap_length = htoi(value)
        elif flag in {'-o','--min-overlap-length'}: min_overlap_length = htoi(value)
        elif flag in {'-L','--stitch-list'}: stitch_scaffolds_list.extend(mixrange(value))
        elif flag in {'-b','--bam'}: alignments_file = value
        
    if len(arguments) != 3:
        usage("Unexpected number of arguments, 3 expected")
        
    if debug:
        logging.getLogger().setLevel(logging.DEBUG)
    else:
        sys.excepthook = _log_exception

    if num(stitch_scaffolds_list) > 0:
        stitch_scaffolds_list = list(map(lambda i: i-1, stitch_scaffolds_list))
        if stitch_scaffolds_list[0] < 0:
            raise ValueError("Scaffold range spec is 1-based, detected 0-based")

    if blasr:
        blasr_executable = which('blasr', fatal=True)
        blasr_version = get_blasr_version(blasr_executable)
        ALIGN_CMD = blasr_executable + (BLASR_5_CMD if blasr_version > [5, 0] else BLASR_3_CMD)
    else:
        minimap_executable = which('minimap2', fatal=True)
        ALIGN_CMD = minimap_executable + MINIMAP_2_CMD

    logging.info("Starting")
    logging.info("Command: %s %s" % (os.path.basename(sys.argv[0]), ' '.join(argv)))
        
    assembly_ifile = open(arguments[0])
    contigs_ifile  = pysam.FastaFile(arguments[1])

    assembly_ofile = open(arguments[2] + '.assembly', 'w')
    contigs_ofile  = open(arguments[2] + '.fasta', 'w')
    graphfa_ofile  = open(arguments[2] + '.gfa', 'w')
    
    if alignments_file is not None:
        alignments_file = pysam.AlignmentFile(alignments_file)
        assert alignments_file.has_index(), \
            'Input BAM file must be coordinate sorted and indexed'
    
    contig_lengths = collections.OrderedDict(zip(
        contigs_ifile.references,
        contigs_ifile.lengths
    ))

    fragment = 0
    contigs = {}
    scaffolds = []
    seen_head = False
    seen_body = False
    scaffold_index = 0
    longest_contig = None
    contig_sequence = None
    prev_contig_name = None
    prev_contig_frag = 0
    prev_contig_offset = 0
    logging.info("Reading input .assembly and .fasta files")

    graphfa_ofile.write(format_gfahead())
    for line in assembly_ifile:
        line = line.strip()

        if len(line) == 0:
            continue

        if line.startswith('>'):
            contig_name, contig_index, contig_length = line.lstrip('>').split()
            contig_index  = int(contig_index)
            contig_length = int(contig_length)
            contig_alias = contig_name
            contig_frag = 0

            if FRAGMENT_NUMBER_TAG in contig_name:
                if contig_name.endswith(FRAGMENT_DEBRIS_TAG):
                    contig_name = contig_name[:-len(FRAGMENT_DEBRIS_TAG)]

                contig_name, contig_frag = contig_name.split(FRAGMENT_NUMBER_TAG)
                contig_frag = int(contig_frag)

                if ((contig_name == prev_contig_name) and \
                    (contig_frag - prev_contig_frag) != 1):
                    raise MalformedAssemblyFileError(
                        "Contig fragment .cprops records expected to be "+\
                        "consecutively ordered and numbered: %d, %d" % (
                            prev_contig_frag, contig_frag
                        )
                    )
                    
            if contig_name != prev_contig_name:
                contig_sequence = contigs_ifile.fetch(contig_name).upper()
                assert len(contig_sequence) == contig_lengths[contig_name], \
                    'Fasta index length != observed length : %s' % contig_name           
                prev_contig_frag = 0
                prev_contig_offset = 0
                
            contig = Sequence(
                contig_alias,
                contig_sequence[prev_contig_offset:(prev_contig_offset+contig_length)]
            )
                
            assert contig.length == contig_length, \
                'Fasta and assembly contig lengths inconsistent: %s' % contig_name

            if longest_contig is None or \
               longest_contig.length < contig.length:
                longest_contig = contig

            graphfa_ofile.write(format_gfaseg(contig))

            contigs[contig_index] = contig
            prev_contig_offset += contig_length
            prev_contig_name = contig_name
            prev_contig_frag = contig_frag
            seen_head = True
            
        elif not seen_head:
            raise MalformedAssemblyFileError("No cprops-style assembly file-header detected")
            
        else:
            scaffolds.append([])
            for vector in line.split():
                vector = int(vector)
                index  = abs(vector)
                strand = vector // index

                contig = contigs[index]
                
                if strand < 0:
                    contig.reverse()

                scaffolds[scaffold_index].append(contig)    
                seen_body = True
                
            scaffold_index += 1

    if not seen_body:
        raise MalformedAssemblyFileError("No asm-style assembly file-body detected")        
            
    del(contigs)

       
    if len(stitch_scaffolds_list) < 1:
        stitch_scaffolds_list = list(range(num(scaffolds)))
        
    if alignments_file:
        logging.debug("Calculating expected median depth")
        expected_full_depth = get_median_depth(alignments_file, longest_contig)
        expected_rept_depth = 2.50 * expected_full_depth
        expected_half_depth = 0.75 * expected_full_depth
        expected_poor_depth = 0.25 * expected_full_depth
        
    logging.info("Performing contig overlapping")
    for scaffold_index in stitch_scaffolds_list:
        logging.info(" Traversing scaffold %d" % (scaffold_index + 1))

        contigs = scaffolds[scaffold_index]
        for contig_index in range(1, num(contigs)):
            contig_5p = contigs[contig_index - 1]
            contig_3p = contigs[contig_index]

            overlap, score = \
                check_overlap(
                    contig_5p,  # trg
                    contig_3p,  # qry
                    min_overlap_length,
                    max_overlap_length,
                    max_spur_frac,
                    max_subst_rate,
                    max_indel_rate,
                    use_blasr=blasr
                ) 

            status_bits = 0
            status_tags = []
            if overlap is None:
                status_tags.append('no-overlap')
                
            elif alignments_file:
                contig_5p_depth = get_median_depth(alignments_file, overlap.trg)
                contig_3p_depth = get_median_depth(alignments_file, overlap.qry)
                
                if contig_5p_depth < expected_poor_depth:
                    status_tags.append('5p=poor-depth')
                    status_bits -= 2
                elif contig_5p_depth < expected_half_depth:
                    status_tags.append('5p=half-depth')
                    status_bits -= 1
                elif contig_5p_depth < expected_rept_depth:
                    status_tags.append('5p=full-depth')
                    status_bits += 1
                else:
                    status_tags.append('5p=repetitive')
                    status_bits -= 1
                    
                if contig_3p_depth < expected_poor_depth:
                    status_tags.append('3p=poor-depth')
                    status_bits -= 2
                elif contig_3p_depth < expected_half_depth:
                    status_tags.append('3p=half-depth')
                    status_bits -= 1
                elif contig_3p_depth < expected_rept_depth:
                    status_tags.append('3p=full-depth')
                    status_bits += 1
                else:
                    status_tags.append('3p=repetitive')
                    status_bits -= 1
                    
                if status_bits >= 0:
                    status_tags.insert(0, 'overlap-rejected')
                    overlap = None

            if overlap:
                status_tags.insert(0, 'overlap-collapsed')
                if overlap.qry.aligned_length >= overlap.trg.aligned_length:
                    status_tags.append('5p=3p-trimmed')
                    contig_5p.end = overlap.trg.beg
                    contig_5p.edges |= 0x2
                else:
                    status_tags.append('3p=5p-trimmed')
                    contig_3p.beg = overlap.qry.end
                    contig_3p.edges |= 0x1

                graphfa_ofile.write(format_gfalink(overlap))
                    
            logging.info("    Status: (%s)" % (', '.join(status_tags)))
            logging.debug("")


    contig_path = []
    contig_index = 1
    prev_contig = None
    assembly_body = []
    logging.info("Outputting collapsed .assembly and .fasta files")
    for scaffold_index in range(num(scaffolds)):
        logging.info(" Traversing scaffold %d" % (scaffold_index + 1))
        
        prev_contig = None
        assembly_body.append([])
        for curr_contig in scaffolds[scaffold_index]:
            if curr_contig.edges:
                logging.debug("Trimming contig: %s:%d-%d" % (
                    curr_contig.name, curr_contig.beg, curr_contig.end)
                )
                curr_contig.sequence = curr_contig.sequence[curr_contig.beg:curr_contig.end]

            if prev_contig:
                if ((curr_contig.edges & 0x1) or 
                    (prev_contig.edges & 0x2)):
                    logging.debug("Collapsing contig overlap: %s:%d-%d - %s:%d-%d" % (
                        prev_contig.name, prev_contig.beg, prev_contig.end,
                        curr_contig.name, curr_contig.beg, curr_contig.end)
                    )
                    if prev_contig.comment is None:
                        contig_path = ["%s%s" % (prev_contig.name, prev_contig.strand.str)]
                        prev_contig.name += '|collapsed'

                    contig_path.append("%s%s" % (curr_contig.name, curr_contig.strand.str))
                    prev_contig.sequence += curr_contig.sequence
                    prev_contig.end = prev_contig.length
                    prev_contig.edges = curr_contig.edges & 0x2

                else:
                    logging.info("  Writing contig: %s:%d-%d" % (
                        prev_contig.name, prev_contig.beg, prev_contig.end)
                    )
                    logging.debug("")
                    assembly_ofile.write(">%s %d %d\n" % (
                        prev_contig.name, contig_index, prev_contig.length)
                    )
                    assembly_body[-1].append(contig_index)
                    contigs_ofile.write(format_fasta(prev_contig, width=100))
                    if len(contig_path) > 0:
                        graphfa_ofile.write(format_gfapath(prev_contig.name, contig_path))
                    prev_contig = curr_contig
                    contig_index += 1
                    contig_path = []
            else:
                prev_contig = curr_contig

        if prev_contig:
            logging.info("  Writing contig: %s:%d-%d" % (
                prev_contig.name, prev_contig.beg, prev_contig.end)
            )
            logging.debug("")
            assembly_ofile.write(">%s %d %d\n" % (
                prev_contig.name, contig_index, prev_contig.length)
            )
            assembly_body[-1].append(contig_index)
            contigs_ofile.write(format_fasta(prev_contig, width=100))
            if len(contig_path) > 0:
                graphfa_ofile.write(format_gfapath(prev_contig.name, contig_path))
            contig_index += 1
            contig_path = []
                        
    for scaffold in assembly_body:
        assembly_ofile.write(' '.join(map(str, scaffold)) + '\n')

    contigs_ofile.close()
    graphfa_ofile.close()
    assembly_ofile.close()
    
    logging.info("Finished")
    
                    
if __name__ == '__main__':
    main(sys.argv[1:])
