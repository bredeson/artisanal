#!/usr/bin/env python

import os
import re
import sys
import getopt
import intervals
import collections

__author__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Calculate collinear gene block coverage'

_tab = '\t'
_null = '.'
_empty = ''
_colon = ':'
_comma = ','
_comment = '#'
_asterisk = '*'

num = len
digits = re.compile('(\d+)')

class BED(intervals.BaseInterval):
    def __init__(self, chr=_null, beg=-1, end=-1, id=_null, score=0, strand=_null):
        intervals.BaseInterval.__init__(self, chr, beg, end)
        self.strand = strand
        self.score = float(score)
        self.id = str(id)

    @property
    def strand(self):
        return self._strand

    @strand.setter
    def strand(self, strand):
        self._strand = intervals.Strand(strand)
        
    @property
    def mid(self):
        return float(self.beg + self.end) / 2.0

    @property
    def chr(self):
        return self.name

    @chr.setter
    def chr(self, name):
        self.name = name

    def __str__(self):
        return "%s\t%d\t%d\t%s\t%s\t%s" % (
            self.chr,
            self.beg,
            self.end,
            self.id,
            str(self.score),
            str(self.strand)
        )

        
class BEDPE(object):
    def __init__(self, trg, qry, name=_null, score=0.0, index=None, num_members=0):
        self.trg = trg
        self.qry = qry
        self.name = name
        self.score = score
        self.index = index
        self.num_members = num_members

    @property
    def strand(self):
        return intervals.Strand(self.trg.strand.int * self.qry.strand.int)
        
    def __str__(self):
        return "%s\t%d\t%d\t%s\t%d\t%d\t%s\t%.04f\t+\t%s" % (
            self.trg.chr,
            self.trg.beg,
            self.trg.end,
            self.qry.chr,
            self.qry.beg,
            self.qry.end,
            self.name,
            self.score,
            self.strand
        )

    def __len__(self):
        return self.trg.end - self.trg.beg
    
    def __eq__(self, other):
        return \
            self.trg == other.trg and \
            self.qry == other.qry and \
            self.name == other.name and \
            self.score == other.score

    def __gt__(self, other):
        return self.trg > other.trg

    def __ge__(self, other):
        return self == other or self > other
    
    def __lt__(self, other):
        return self.trg < other.trg

    def __le__(self, other):
        return self == other or self < other
    
    __repr__ = __str__



def _naturalize(string):
    return tuple((int(x) if x.isdigit() else x for x in digits.split(string)))

    
def _record_qry_pos(record):
    return (record.qry.chr, record.qry.beg, record.qry.end)


def _record_trg_pos(record):
    return (record.trg.chr, record.trg.beg, record.trg.end)


def _record_qry_chr(record):
    return record.qry.chr


def _record_trg_chr(record):
    return record.trg.chr


def _record_trg_idx(record):
    return (record.trg, record.index[0])


def _record_qry_idx(record):
    return (record.qry, record.index[1])


def _record_index_trg_pos(record):
    return (record.index, record.trg.chr, record.trg.beg, record.trg.end)


def _cluster_num_members(cluster):
    return max(num(cluster), cluster[0].num_members)


def _cluster_trg_pos(cluster):
    return (_naturalize(cluster[0].trg.chr), cluster[0].trg.beg, cluster[-1].trg.end)
        

def _cluster_infer_strand(cluster, key=lambda x: (x, -1)):
    record0, idx0 = key(cluster[0])
    recordN, idxN = key(cluster[-1])
    if num(cluster) > 1:
        strand = sign(idxN - idx0)
        if not strand:
            strand = sign(recordN.end - record0.beg)
        return strand            
    else:
        return record0.strand.int


def sign(value):
    nvalue = float(value if value else 0)
    dvalue = float(value if value else 1)
    return int(nvalue / abs(dvalue))


def read_chrom_sizes(sizes_filename):
    sizes_file = open(sizes_filename, 'r')
    sizes = collections.OrderedDict()
    for line in sizes_file:
        line = line.strip()
        if line is _empty or \
           line.startswith(_comment):
            continue

        fields = line.split(_tab)

        if fields[0] in sizes:
            sys.stderr.write(
                "WARNING: Duplicate sequence detected, skipping '%s'\n" % (
                fields[0]))
            continue
        
        sizes[fields[0]] = int(fields[1])

    sizes_file.close()
    return sizes



def cluster_classified_records(records):
    prev = None
    clusters = []
    for curr in sorted(records, key=_record_index_trg_pos):
        if prev is None:
            clusters.append([curr])
        elif curr.index[0] == prev.index[0]:
            clusters[-1].append(curr)
        else:
            clusters.append([curr])
        
        prev = curr

    return clusters



def uniqify_clusters(clusters, max_trg_overlap=sys.maxsize, max_qry_overlap=sys.maxsize):
    clusters.sort(key=_cluster_num_members, reverse=True)

    qry_intervals = {}
    trg_intervals = {}
    unique_clusters = []
    for cluster in clusters:
        cluster_trg = BED(cluster[0].trg.chr,
                          min(map(lambda m: m.trg.beg, cluster)),
                          max(map(lambda m: m.trg.end, cluster)))
        cluster_qry = BED(cluster[0].qry.chr,
                          min(map(lambda m: m.qry.beg, cluster)),
                          max(map(lambda m: m.qry.end, cluster)))

        if cluster_trg.chr not in trg_intervals:
            trg_intervals[cluster_trg.chr] = intervals.IntervalList()
        if cluster_qry.chr not in qry_intervals:
            qry_intervals[cluster_qry.chr] = intervals.IntervalList()

        trg_overlap = trg_intervals[cluster_trg.chr].overlap_length(cluster_trg) 
        trg_overlap = float(trg_overlap) / len(cluster_trg)

        qry_overlap = qry_intervals[cluster_qry.chr].overlap_length(cluster_qry)
        qry_overlap = float(qry_overlap) / len(cluster_qry)
        
        if trg_overlap <= max_trg_overlap and qry_overlap <= max_qry_overlap:
            trg_intervals[cluster_trg.chr].add(cluster_trg)
            qry_intervals[cluster_qry.chr].add(cluster_qry)
            unique_clusters.append(cluster)

    return unique_clusters



def place_clusters(clusters):
    placed_clusters = []

    best = dict()
    hits = dict()
    for cluster in sorted(clusters, key=_cluster_num_members, reverse=True):
        qry = cluster[0].qry
        trg = cluster[0].trg
        key = (qry.chr, trg.chr)
        if qry.chr not in best:
            best[qry.chr] = (trg.chr, cluster[0].num_members, cluster[0].strand.int)
        if key not in hits:
            hits[key] = [0, 0, 0]
        hits[key][cluster[0].strand.int+1] += cluster[0].num_members

        num_members = sum(hits[key])
        if num_members > best[qry.chr][1]:
            best[qry.chr] = (
                trg.chr,
                num_members,
                max(range(num(hits[key])), key=lambda i: hits[key][i])-1
            )
        
    for cluster in clusters:
        qry = cluster[0].qry
        trg = cluster[0].trg
        if trg.chr == best[qry.chr][0] and \
           cluster[0].strand.int == best[qry.chr][2]:
            placed_clusters.append(cluster)

    return placed_clusters


def collapse_clusters(clusters):
    collapsed_clusters = []
    for cluster in clusters:
        cluster.sort(key=_record_trg_pos)
        trg_strand  = _cluster_infer_strand(cluster, key=_record_trg_idx)
        qry_strand  = _cluster_infer_strand(cluster, key=_record_qry_idx)
        num_members = _cluster_num_members(cluster)
        
        if trg_strand < 0 and qry_strand < 0:
            trg_strand = +1
            qry_strand = +1
            
        collapsed_clusters.append(
            [
                BEDPE(
                    BED(cluster[0].trg.chr,
                        min(map(lambda c: c.trg.beg, cluster)),
                        max(map(lambda c: c.trg.end, cluster)),
                        trg_strand
                    ),
                    BED(cluster[0].qry.chr,
                        min(map(lambda c: c.qry.beg, cluster)),
                        max(map(lambda c: c.qry.end, cluster)),
                        qry_strand
                    ),
                    name=_null,
                    score=sum(map(lambda c: c.score, cluster)) / num_members,
                    num_members=num_members
                )
            ]
        )
    return collapsed_clusters


def index_clusters(clusters):
    indexed_clusters = collections.OrderedDict()

    for cluster in clusters:
        for member in cluster:
            if member.trg.chr not in indexed_clusters:
                indexed_clusters[member.trg.chr] = intervals.IntervalList(
                    setter=lambda i: (i.trg, i),
                    getter=lambda i :i[0]
                )
            indexed_clusters[member.trg.chr].add(member)

    return indexed_clusters


def read_bed(bed_file):
    bed_file = open(bed_file)
    records = collections.OrderedDict()
    
    for line in bed_file:
        line = line.strip()

        if line is _empty or \
           line.startswith(_comment):
            continue

        field = line.split(_tab)

        bed = BED()
        
        try:
            bed.chr = str(field[0])
            bed.beg = int(field[1])
            bed.end = int(field[2])
            bed.id  = str(field[3])
            assert bed.beg <= bed.end
            
        except IndexError:
            sys.stderr.write("ERROR: Too few fields (=%d), expected 4 or more\n" % num(field))
            sys.exit(1)
        except:
            sys.stderr.write("ERROR: Invalid record: '%s'\n" % line)
            sys.exit(1)

        if bed.chr not in records:
            records[bed.chr] = intervals.IntervalList()

        records[bed.chr].add(bed)

    bed_file.close()
        
    return records    


def run(bedpe_file, bed, bin_size, out=sys.stdout, err=sys.stderr, debug=False):
    clustered = []
    collapsed = []

    locusindex = collections.OrderedDict()
    for chr_name in bed:
        for i, record in enumerate(bed[chr_name]):
            locusindex[record.id] = i
    
    for line in bedpe_file:
        line = line.strip()

        if line is _empty or \
           line.startswith(_comment):
            continue

        field = line.split(_tab)
        
        if num(field) < 12:
            err.write("ERROR: Too few fields (=%d), 12 expected\n" % num(field))
            sys.exit(1)

        try:
            for i in range(2):
                chr = 3*i+0
                beg = 3*i+1
                end = 3*i+2
                field[beg] = int(field[beg])
                field[end] = int(field[end])
                assert field[beg] <= field[end]
                
            field[10] = int(field[10])
            field[11] = int(field[11])
            
        except IndexError:
            err.write("ERROR: Too few fields (=%d)\n" % num(field))
            sys.exit(1)
        except:
            err.write("ERROR: Invalid record: '%s'\n" % line)
            sys.exit(1)

        names = field[6].strip().split(':')
        member = BEDPE(
            BED(field[0],
                field[1],
                field[2],
                names[0],
                0,
                field[8]
            ),
            BED(field[3],
                field[4],
                field[5],
                names[1],
                0,
                field[9]
            ),
            index=(field[10], field[10]),
            num_members=field[11]
        )

        if member.trg.chr is _null or member.trg.beg < 0 or member.trg.end < 0 or \
           member.qry.chr is _null or member.qry.beg < 0 or member.qry.end < 0:           
            continue

        clustered.append(member)
        
    clustered = cluster_classified_records(clustered)
    collapsed = collapse_clusters(clustered)
    collapsed = index_clusters(collapsed)

    #import pprint
    #p = pprint.PrettyPrinter()
    #p.pprint(collapsed)

    for cluster in clustered:
        for member in cluster:
            bedpe = member
            member = member.trg
            member.score = 0
            key = (member.chr, member.beg, member.end)
            
            max_len = num(collapsed[member.chr])
            if member.chr in collapsed:
                #TODO: some members being dropped...
                ovl_beg, ovl_end = collapsed[member.chr].overlap_range(member)
                if (0 <= ovl_beg and ovl_end <= max_len):
                    member.score += ovl_end - ovl_beg
                    
                    
            out.write(str(member) + '\n')
    
    
        
def usage(message=None, exitcode=1, stream=sys.stderr):
    message = _empty if message is None else 'ERROR: %s\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] <input.bedpe>\n" % __program__)
    stream.write("\n")
    stream.write("Options:\n")
    stream.write("  -b,--bin-size <int>       Number of gene loci per bin [1]\n")
    stream.write("  -o,--outfile <file>       Write output to <file> [stdout]\n")
    stream.write("  -D,--debug                Print debugging messages\n")
    stream.write("  -h,--help                 This help message\n")
    stream.write("\n")
    stream.write("%s\n" % message)
    sys.exit(1)
    #----------------|----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    #                0         10         20       30        40        50        60        70        80

    
def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'hb:Do:', (
            'bin-size=',
            'outfile=',
            'debug',
            'help',
        ))
    except getopt.GetoptError as error:
        usage(error)

    debug = False
    errfile = None
    outfile = None
    bin_size = 1
    for flag, value in options:
        if   flag in ('-h','--help'): usage(exitcode=0)
        elif flag in ('-D','--debug'): debug = True
        elif flag in ('-o','--outfile'): outfile = value
        elif flag in ('-b','--bin-size'): bin_size = int(value)
        
    if num(arguments) == 0:
        usage()
    elif num(arguments) != 1:
        usage("Unexpected number of arguments, one expected")

    bedpe_file = open(arguments[0], 'r')
    bed_file = read_bed(arguments[1])
    
    if outfile is None:
        outfile = sys.stdout
        errfile = sys.stderr
    else:
        outfile = open(outfile, 'w')
        errfile = sys.stderr
        
    sys.stderr.write("Command: %s %s\n" % (__program__, ' '.join(argv)))
    run(bedpe_file,
        bed_file,
        bin_size,
        outfile,
        errfile,
        debug
    )
    sys.stderr.write("Finished\n")



if __name__ == '__main__':
    main(sys.argv[1:])
