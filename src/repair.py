
import os
import sys
import pysam
import getopt
import collections

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Add pairing information to indepently-aligned reads'


num = len

def _by_reference(record):
    return (record.reference_id, record.reference_start, record.reference_end)

def repair(queries, orientation=2, min_insert=0, max_insert=-1, min_mapq=0):
    pidx = -1
    primary = [None, None]
    for query in queries:
        query.flag |= 0x1  # PAIRED
        if query.is_secondary or query.is_supplementary:
            if pidx >= 0:
                query.flag |= 0x40 << pidx  # READ(1/2)
        else:
            pidx += 1
            primary[pidx] = query
            query.flag |= 0x40 << pidx  # READ(1/2)

    if primary[0] and primary[1]:
        primary = sorted(primary, key=_by_reference)
        if primary[0].is_unmapped or primary[1].is_unmapped:
            return
        if primary[0].reference_id != primary[1].reference_id:
            return
        if primary[0].mapping_quality < min_mapq or \
           primary[1].mapping_quality < min_mapq:
            for query in queries:
                query.flag |= (0x4 | 0x8)  # UNMAP | MUNMAP
            return
        
        len_insert = primary[1].reference_end - primary[0].reference_start
        if orientation >= 0 and \
           orientation != (int(primary[0].is_reverse) | int(primary[1].is_reverse) << 1):
            return
        if min_insert > len_insert:
            return
        if max_insert > min_insert and max_insert < len_insert:
            return
        
        for query in queries:
            query.flag |= 0x2  # PROPER_PAIR



def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else "ERROR: %s\n\n\n" % message
    #            |---------|---------|---------|---------|---------|---------|---------|
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] <in.bam> <out.bam>\n" % __program__)
    stream.write("\n")
    stream.write("Options: -I <uint>  Insert size maximum for proper pairing\n")
    stream.write("         -i <uint>  Insert size minimum for proper pairing\n")
    stream.write("         -p <str>   Proper pair orientation enum:{FF,FR,RF,RR} [all]\n")
    stream.write("         -q <uint>  Min mapping quality to consider reads mapped [0]\n")
    stream.write("         -1         Output primary alignments only\n")
    stream.write("         -h,--help  This help message\n")
    stream.write("\n")
    stream.write("Notes: Must run the samtools fixmate tool on out.bam\n")
    stream.write("\n\n%s" % message)
    sys.exit(exitcode)



def main(argv):
    if len(argv) == 0:
        usage()    
    try:
        options, arguments = getopt.getopt(argv, '1hi:I:p:q:', ('help',))
    except getopt.GetoptError as error:
        usage(error)
        
    min_mapq = 0
    min_isize = 0
    max_isize = -1
    primary_only = False
    proper_orientation = 'ALL'
    for flag, value in options:
        if   flag in ('-h','--help'): usage(exitcode=0)
        elif flag == '-I': max_isize = int(value)
        elif flag == '-i': min_isize = int(value)
        elif flag == '-q': min_mapq  = int(value)
        elif flag == '-p': proper_orientation = value.upper()
        elif flag == '-1': primary_only = True

    if num(arguments) != 2:
        usage("Unexpected number of arguments")
        
    if proper_orientation not in {'FF','FR','RF','RR','ALL'}:
        usage("Invalid enumerative value: %s\n" % proper_orientation)
    
    proper_orientation = -1 if proper_orientation == 'ALL' \
                         else (int(proper_orientation[0] == 'R') | int(proper_orientation[1] == 'R') << 1)

    ibam = pysam.AlignmentFile(arguments[0])
    obam = pysam.AlignmentFile(arguments[1], mode='wb', template=ibam)

    queries = collections.deque()
    num_primary = 0
    prev_query_name = None
    for curr in ibam:
        if primary_only and (curr.is_secondary or curr.is_supplementary):
            continue

        if curr.query_name != prev_query_name:
            if num_primary == 2:
                repair(queries, orientation=proper_orientation, min_insert=min_isize, max_insert=max_isize, min_mapq=min_mapq)

            for query in queries:
                obam.write(query)

            queries = collections.deque()
            num_primary = 0
        
        if not curr.is_secondary and not curr.is_supplementary:
            num_primary += 1

        prev_query_name = curr.query_name
        queries.append(curr)

    if num_primary == 2:
        repair(queries, orientation=proper_orientation, min_insert=min_isize, max_insert=max_isize, min_mapq=min_mapq)
    
    for query in queries:
        obam.write(query)

    ibam.close()
    obam.close()


if __name__ == '__main__':
    main(sys.argv[1:])
