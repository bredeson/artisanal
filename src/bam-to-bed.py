#!/usr/bin/env python

import os
import sys
import pysam
import getopt

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Convert a BAM to BED/BEDPE format'


num = len
CIGAR_PADDING_OPERATOR = {4, 5}
QUERY_LENGTH_OPERATOR = {0, 1, 4, 5, 7, 8}
ALIGNMENT_LENGTH_OPERATOR = {0, 1, 2, 3, 6, 7, 8}
STRAND = ['+','-']

def query_length(aln):
    length = 0
    for cig in aln.cigartuples:
        if cig[0] in QUERY_LENGTH_OPERATOR:
            length += cig[1]
    
    return length


def query_5p_spurlen(aln):
    spurlen = 0
    for cig in aln.cigartuples:
        if cig[0] in CIGAR_PADDING_OPERATOR:
            spurlen += cig[1]
        else:
            break

    return spurlen


def query_3p_spurlen(aln):
    spurlen = 0
    cigar = list(aln.cigartuples)
    cigar.reverse()
    for cig in cigar:
        if cig[0] in CIGAR_PADDING_OPERATOR:
            spurlen += cig[1]
        else:
            break

    return spurlen


def bamtobed(bam, bed, write_bedpe=False, write_lengths=False, write_regions=False):
    reference_lengths = dict(zip(bam.references, bam.lengths))
    
    for record in bam:
        if record.is_unmapped or \
           record.reference_id < 0:
            continue
        
        record_query_length = query_length(record)
        record_query_start = query_5p_spurlen(record)
        record_query_end = record_query_length - query_3p_spurlen(record)
        if record.is_reverse:
            record_query_start, record_query_end = \
                record_query_length - record_query_end, record_query_length - record_query_start

        if write_lengths:
            record_lengths = "\t%d\t%d" % (
                reference_lengths[record.reference_name],
                record_query_length
            )
        else:
            record_lengths = ""
            
        if write_bedpe:
            bed.write(
                "%s\t%d\t%d\t%s\t%d\t%d\t.\t%d\t+\t%s%s\n" % (
                    record.reference_name,
                    record.reference_start,
                    record.reference_end,
                    record.query_name,
                    record_query_start,
                    record_query_end,
                    record.mapping_quality,
                    STRAND[int(record.is_reverse)],
                    record_lengths
                )
            )
        else:
            if write_regions:
                record_query_locus = "%s:%d-%d" % (
                    record.query_name,
                    record_query_start,
                    record_query_end
                )
            else:
                record_query_locus = record.query_name
                
            bed.write(
                "%s\t%d\t%d\t%s\t%d\t%s%s\n" % (
                    record.reference_name,
                    record.reference_start,
                    record.reference_end,
                    record_query_locus,
                    record.mapping_quality,
                    STRAND[int(record.is_reverse)],
                    record_lengths
                )
            ) 


def readBedFile(bedfile, rod):
    bed = open(bedfile, 'r')
    records = []
    previous_tid = -1
    previous_pos = -1
    for record in bed:
        record = record.rstrip().split('\t')
        
        if len(record) < 3:
            raise Exception("BED record has too few columns: "+str(record))
        
        record[1] = int(record[1])
        record[2] = int(record[2])
        
        current_tid = rod.get_tid(record[0])
        if current_tid < previous_tid:
            raise Exception("BED file not sorted")
        elif current_tid > previous_tid:
            previous_pos = -1
        elif record[2] < record[1]:
            raise Exception("BED record start > stop: " + str(record))
        elif record[1] < previous_pos:
            raise Exception("BED file not sorted")

        records.append(tuple(record))
        previous_tid = current_tid
        previous_pos = record[1]

    bed.close()

    return records



def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else "ERROR: %s\n\n\n" % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] <in.bam>\n" % (
        os.path.basename(sys.argv[0])))
    stream.write("\n")
    stream.write("Options:\n")
    stream.write("    -b,--bedpe    Write in BEDPE format [BED]\n")
    stream.write("    -h,--help     Write this help message and exit\n")
    stream.write("    -r,--regions  Write query names as regions strings [Qname]\n")
    stream.write("    -l,--lengths  Write ref and query length fields [MapQ]\n")
    stream.write("\n")
    stream.write("\n%s" % message)
    sys.exit(exitcode)



def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'bhi:rl', ('help','bedpe','regions','lengths'))
    except getopt.GetoptError as err:
        usage(err)

    min_idnt = 0.0
    symmetric = False
    write_bedpe = False
    write_lengths = False
    write_regions = False
    for flag, value in options:
        if   flag in ('-h','--help'): usage(exitcode=0)
        elif flag in ('-b','--bedpe'): write_bedpe = True
        elif flag in ('-r','--regions'): write_regions = True
        elif flag in ('-l','--lengths'): write_lengths = True
        elif flag == '-i': min_idnt = float(value)

    if num(arguments) < 1:
        usage("Too few arguments")
    elif num(arguments) > 1:
        usage("Too many arguments")

    bed = sys.stdout
    bam = pysam.AlignmentFile(arguments[0])

    bamtobed(bam, bed, write_bedpe, write_lengths, write_regions)



main(sys.argv[1:])
