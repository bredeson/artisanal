#!/usr/bin/env python

# Look into pytaxonkit module: https://github.com/bioforensics/pytaxonkit

import os
import sys
import getopt
import collections

from fileutil import zopen

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Apply taxonomic rank(s) using taxid'

TAXONOMIC_RANK_MAP = collections.OrderedDict((
    (rank, i) for i, rank in enumerate(
        (
            'tax_id',
            'tax_name',
            'species',
            'genus',
            'family',
            'order',
            'class',
            'phylum',
            'kingdom',
            'superkingdom'
        )
    )
))

TAXONOMIC_CATEGORIES_MAP = {
    'archaea':      'A',
    'bacteria':     'B',
    'bacterial':    'B',
    'eukaryota':    'E',
    'eukaryotes':   'E',
    'eukaryote':    'E',
    'viruses':      'V',
    'virus':        'V',
    'viral':        'V',
    'viroids':      'V',
    'unclassified': 'U',
    'other':        'O'
}

num = len
tab = '\t'
pipe = '|'
empty = ''
comma = ','
comment = '#'
semicolon = ';'
commaspace = ', '
unspecified = 'unspecified'

def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else 'ERROR: %s\n\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] <in.tsv>\n" % (
        __program__))
    stream.write("\n")
    stream.write("Options: -L,--lineage-file <file>  The path to rankedlineage.dmp [./rankedlineage.dmp]\n")
    stream.write("         -o,--outfile <file>       Write output to <file> [stdout]\n")
    stream.write("         -r,--rank <str>           Specify the taxonomic rank (see Notes)\n")
    stream.write("         -t,--taxid <uint>         The taxid column in file (1-based)\n")
    stream.write("\n")
    stream.write("Notes:\n")
    stream.write("   1. Requires rankedlineage.dmp file available at:\n")
    stream.write("      https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz\n")
    stream.write("\n")
    stream.write("   2. Possible ranks are:\n")
    stream.write("      all, %s\n" % commaspace.join(TAXONOMIC_RANK_MAP.keys()))
    stream.write("\n\n%s" % message)
    sys.exit(exitcode)
    

def main(argv):
    lineage_filename = 'rankedlineage.dmp'
    output_filename = '-'
    taxid_column = 0
    ranks_list = None
    try:
        options, arguments = getopt.getopt(argv, 'L:r:t:o:h', (
            'lineage-file=',
            'outfile=',
            'taxid=',
            'rank=',
            'help'
        ))
    except getopt.GetoptError as message:
        usage(message)

    for flag, value in options:
        if   flag in ('-L','--lineage-file'): lineage_filename = value
        elif flag in ('-o','--outfile'): output_filename = value
        elif flag in ('-t','--taxid'): taxid_column = int(value)-1
        elif flag in ('-r','--rank'): ranks_list = value.lower()
        elif flag in ('-h','--help'): usage(exitcode=0)
        
    if num(arguments) < 1:
        usage('Too few arguments')
    if taxid_column is None:
        usage("Must specify taxid column number (1-based)\n")
    if ranks_list is None:
        usage(
            "One or more taxonomic ranks must be specified. "+\
            "Possible ranks are:\n  all, %s" % (
                commaspace.join(TAXONOMIC_RANK_MAP.keys())
            )
        )
    elif ranks_list == 'all':
        ranks_list = TAXONOMIC_RANK_MAP.keys()
    else:
        _ranks_list = []
        try:
            for rank in ranks_list.lower().split(','):
                _ranks_list.append(TAXONOMIC_RANK_MAP[rank])
        except KeyError:
            usage(
                "Invalid rank. Possible ranks are:\n  %s" % (
                    commaspace.join(TAXONOMIC_RANK_MAP.keys())
                )
            )
            sys.exit(1)
        ranks_list = _ranks_list
              
    lineage_file = zopen(lineage_filename)
    output_file = zopen(output_filename, 'w')
    blast_file = zopen(arguments[0])
    
    
    taxa = {}
    for line in lineage_file:
        line = line.strip()

        if line is empty or \
           line.startswith(comment):
            continue
        
        fields = line.split(tab)

        nodes = []
        taxid = int(fields[0])
        for rank in ranks_list:
            i = TAXONOMIC_RANK_MAP[rank]
            if fields[2*i] is pipe:
                continue
            if fields[2*i] is empty:
                fields[2*i] = unspecified

            nodes.append(fields[2*i])

        taxa[taxid] = comma.join(nodes)

    lineage_file.close()

    
    for line in blast_file:
        line = line.strip()

        if line is empty or \
           line.startswith(comment):
            continue

        fields = line.split(tab)
        
        try:
            taxid = fields[taxid_column]
        except IndexError:
            sys.stderr.write("ERROR: taxid column out of bounds: %d\n" % (
                taxid_column+1))
            sys.exit(1)

        lineage = []
        for tid in taxid.split(semicolon):
            try:
                tid = int(tid)
            except ValueError:
                sys.stderr.write("ERROR: invalid taxid: %s\n" % tid)
                sys.exit(1)

            try:
                lineage.append(taxa[tid])
            except KeyError:
                lineage.append(unspecified)

        fields[taxid_column] = semicolon.join(lineage)

        output_file.write(tab.join(fields) + '\n')

    
    output_file.close()
    blast_file.close()


main(sys.argv[1:])

