
import os
import re
import sys
import getopt
from collections import OrderedDict
from intervals import BaseInterval, IntervalList, Strand

__authors__ = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Tab-delimited alignments filtering tool'


num = len
err = sys.stderr

tab = '\t'
eol = '\n'
null = '.'
colon = ':'
empty= ''
comma = ','
comment = '#'
digits = re.compile('(\d+)')

class Interval(BaseInterval):
    def __init__(self, chr, beg, end, locus=None, strand=0, index=-1, alias=None):
        self.chr = chr
        self.beg = beg
        self.end = end
        self.locus  = locus
        self.strand = Strand(strand)
        self.index = index
        self.alias = alias

    @property
    def chr(self):
        return self.name

    @chr.setter
    def chr(self, name):
        self.name = name

    @property
    def length(self):
        return self.end - self.beg
        

class IntervalPair(object):
    def __init__(self, trg, qry, score=0):
        self.trg = trg
        self.qry = qry
        self.score = score

    @property
    def strand(self):
        return self.trg.strand.int == self.qry.strand.int

    def __str__(self):
        return "%s %s %0.3f" % (str(self.trg), str(self.qry), self.score)

    __repr__ = __str__


class IntervalCounts(BaseInterval):
    def __init__(self, chr, beg, end, counts=None):
        BaseInterval.__init__(self, chr, beg, end)
        self.counts = counts
        
    @property
    def chr(self):
        return self.name

    @chr.setter
    def chr(self, name):
        self.name = name

    def __str__(self):
        return "%s %s" % (BaseInterval.__str__(self), str(self.counts))

    __repr__ = __str__
    
        
def by_locus_and_score(record):
    return (record.qry.locus, -record.score)


def by_min_index(record):
    return min(record.trg.index, record.qry.index)


def by_trg_position(record):
    return (record.trg.beg, record.trg.end)


def read_chrom_sizes(chrom_filename):
    chrom_file = open(chrom_filename)

    chrom_size = OrderedDict()
    for line in chrom_file:
        line = line.strip()

        if line is empty or \
           line.startswith(comment):
            continue

        fields = line.split(tab)

        chrom_size[fields[0]] = int(fields[1])
        
    chrom_file.close()
    return chrom_size


def filter_by_ascore(hits_list, min_ascore=None, max_ascore=None):
    if min_ascore is None and max_ascore is None:
        return hits_list

    filtered_hits = list()

    min_not_none = min_ascore is not None
    max_not_none = max_ascore is not None
    for hit in hits_list:
        keep = True
        if min_not_none and hit.score < min_ascore:
            keep = False
        if max_not_none and hit.score > max_ascore:
            keep = False
        if keep:
            filtered_hits.append(hit)

    return filtered_hits
            
            
def filter_by_cscore(hits_list, min_cscore=0.0, minimize=False):
    opt_trg_score = dict()
    opt_qry_score = dict()
    filtered_hits = list()

    vec = -1
    opt = max
    if minimize:
        opt = min
        vec = +1
    
    hits_list.sort(key=lambda h: vec*h.score)
    for hit in hits_list:
        if hit.qry.locus not in opt_qry_score:
            opt_qry_score[hit.qry.locus] = sys.maxsize * vec
        if hit.score * vec < opt_qry_score[hit.qry.locus] * vec:
            opt_qry_score[hit.qry.locus] = hit.score

        if hit.trg.locus not in opt_trg_score:
            opt_trg_score[hit.trg.locus] = sys.maxsize * vec
        if hit.score * vec < opt_trg_score[hit.trg.locus] * vec:
            opt_trg_score[hit.trg.locus] = hit.score

    for hit in hits_list:
        opt_score = opt(opt_qry_score[hit.qry.locus], opt_trg_score[hit.trg.locus])
        hit_cscore = float(hit.score) / float(opt_score)
        
        if hit_cscore >= min_cscore:
            filtered_hits.append(hit)

    return filtered_hits


def filter_by_quota(hits_list, hit_quota=None):
    if hit_quota is None:
        return hits_list

    trg_counts = dict()
    qry_counts = dict()
    filtered_hits = list()
    for hit in hits_list:
        if hit.trg.locus not in trg_counts:
            trg_counts[hit.trg.locus] = set()
        trg_counts[hit.trg.locus].add(hit.qry.locus)

        if hit.qry.locus not in qry_counts:
            qry_counts[hit.qry.locus] = set()
        qry_counts[hit.qry.locus].add(hit.trg.locus)

    for hit in hits_list:
        if num(trg_counts[hit.trg.locus]) == hit_quota[0] and \
           num(qry_counts[hit.qry.locus]) == hit_quota[1]:
            filtered_hits.append(hit)

    return filtered_hits


def filter_by_singlebest(hits_list, minimize=False):
    vec = +1 if minimize else -1
    
    qry_hits = set()
    filtered_hits = []
    hits_list.sort(key=lambda h: vec*h.score)
    for hit in hits_list:
        if hit.qry.locus not in qry_hits:
            qry_hits.add(hit.qry.locus)
            filtered_hits.append(hit)

    return filtered_hits


def filter_by_mutualbest(hits_list, minimize=False):
    vec = +1 if minimize else -1

    trg_hits = {}
    qry_hits = {}
    filtered_hits = []
    hits_list.sort(key=lambda h: vec*h.score)
    for hit in hits_list:
        if hit.trg.locus not in trg_hits:
            trg_hits[hit.trg.locus] = (-1, set())
        if hit.score > trg_hits[hit.trg.locus][0]:
            trg_hits[hit.trg.locus] = (hit.score, set())
        if hit.score == trg_hits[hit.trg.locus][0]:
            trg_hits[hit.trg.locus][1].add(hit.qry.locus)
        if hit.qry.locus not in qry_hits:
            qry_hits[hit.qry.locus] = (-1, set())
        if hit.score > qry_hits[hit.qry.locus][0]:
            qry_hits[hit.qry.locus] = (hit.score, set())
        if hit.score == qry_hits[hit.qry.locus][0]:
            qry_hits[hit.qry.locus][1].add(hit.trg.locus)
    
    for hit in hits_list:
        hit_trgs = qry_hits[hit.qry.locus][1]
        hit_qrys = trg_hits[hit.trg.locus][1]
        if hit.trg.locus in hit_trgs and hit.qry.locus in hit_qrys:
            filtered_hits.append(hit)

    return filtered_hits


def filter_uniq(hits_list, selfalign=False, minimize=False):
    vec = +1 if minimize else -1
    seen = set()
    filtered_hits = []
    hits_list.sort(key=lambda h: vec*h.score)
    for hit in hits_list:
        pair_fwd = (hit.trg.locus, hit.qry.locus)
        pair_rev = (hit.qry.locus, hit.trg.locus)
        if pair_fwd not in seen:
            filtered_hits.append(hit)
            seen.add(pair_fwd)
            if selfalign:
                seen.add(pair_rev)

    return filtered_hits


def list_to_dict(hits_list):
    hits_dict = dict()
    for hit in hits_list:
        if hit.trg.chr not in hits_dict:
            hits_dict[hit.trg.chr] = []

        hits_dict[hit.trg.chr].append(hit)

    for chr in hits_dict:
        hits_dict[chr].sort(key=by_trg_position)

    return hits_dict


def _naturalize(string):
    return tuple((int(x) if x.isdigit() else x for x in digits.split(string)))


def _index_list(l):
    d = OrderedDict()
    for i, key in enumerate(l):
        d[key] = i

    return d


def irange(rmax, width=1, shift=1):
    beg = 0
    end = 0
    pos = 0
    while pos < rmax:
        pos = beg + width
        end = min(pos, rmax) - 1
        yield (beg, end)
        beg += shift
        

def format_message(message):
    if message is None:
        return empty
    else:
        return eol + eol + 'ERROR: ' + str(message) + eol


def format_anchor(hit, locus_id=False):    
    trg_anchor = hit.trg.locus if (locus_id or hit.trg.alias is None) else hit.trg.alias
    qry_anchor = hit.qry.locus if (locus_id or hit.qry.alias is None) else hit.qry.alias
    return "%s\t%s\t%s\n" % (trg_anchor, qry_anchor, str(hit.score))


def format_bedpe(hit, locus_id=False):
    trg_anchor = hit.trg.locus if (locus_id or hit.trg.alias is None) else hit.trg.alias
    qry_anchor = hit.qry.locus if (locus_id or hit.qry.alias is None) else hit.qry.alias
    return tab.join(map(str, (
        hit.trg.chr,
        hit.trg.beg,
        hit.trg.end,
        hit.qry.chr,
        hit.qry.beg,
        hit.qry.end,
        trg_anchor + comma + qry_anchor,
        hit.score,
        hit.trg.strand,
        hit.qry.strand
    ))) + eol



def usage(message=None, exitcode=1, stream=sys.stderr):
    stream.write(eol)
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write(eol)
    stream.write(eol)
    stream.write("Usage: %s [options] <input> <trg.bed> <qry.bed>\n" % (
        __program__))
    stream.write(eol)
    stream.write("Options:\n")
    stream.write("    -A                Write output in JCVI .anchors format [BEDPE]\n")
    stream.write("    -a <uint>         Alignment score column number (1-based) [3]\n")
    stream.write("    -b                Write best hit for each query\n")
    stream.write("    -c <ufloat>       Min C-score hit threshold [0.0]\n")
    stream.write("    -d <uint>         Min radius for self-self hits [0]\n")
    stream.write("    -I                Ignore qrys/trgs not in BED file [False]\n")
    stream.write("    -L                Write locus IDs (assumes -Q/-T set)\n");
    stream.write("    -m                Minimize the hit score [maximize]\n")
    stream.write("    -o <file>         Write output to <file> [stdout]\n")
    stream.write("    -Q <regex>        Remove isoform ID from query name using <regex>\n")
    stream.write("                      (To group isoforms for best-at-locus filtering)\n")
    stream.write("    -q <uint>         Query name column number (1-based) [1]\n")
    stream.write("    -R <uint>:<uint>  trg:qry hit quota (1:1, 1:2, etc) [None]\n")
    stream.write("    -r                Write only reciprocally-best hit pairs\n")
    stream.write("    -S <ufloat>       Hard max-score threshold [None]\n")
    stream.write("    -s <ufloat>       Hard min-score threshold [None]\n")
    stream.write("    -T <regex>        Remove isoform ID from target name using <regex>\n")
    stream.write("                      (To group isoforms for best-at-locus filtering)\n")
    stream.write("    -t <uint>         Target name column number (1-based) [2]\n")
    stream.write("    -u                Write hit pairs uniquely\n")
    stream.write("    -v                Output pair order as qry,trg [trg,qry]\n")
    stream.write("    -X                Input is self-self, all-vs-all comparison\n")
    #         |---------|---------|---------|---------|---------|---------|---------|---------|
    #         0        10        20        30        40        50        60        70        80
    stream.write(eol)
    stream.write("Notes:\n")
    stream.write("  1. Hit filters are applied in the following order: self-self, min radius,\n")
    stream.write("     min/max score, C-score, MBH/RBH, quota, unique\n")
    stream.write(eol)
    stream.write("  2. Option '-b' is incompatible with '-c' and '-r' options\n")
    stream.write(eol)
    stream.write("  3. Multiple top-scoring hits per query/target may be reported unless an\n")
    stream.write("     explicit hit quota is specified.\n")
    stream.write(eol)
    stream.write("  4. The '-Q' and '-T' options use Python-style regular expressions. For\n")
    stream.write("     example, the pattern '\.\d+$' will remove '.8' from transcript ID\n")
    stream.write("     'Ath.01G00100.8', producing the locus ID 'Ath.01G00100'. Details about\n")
    stream.write("     regex syntax can be found at: https://docs.python.org/3/library/re.html\n")
    stream.write(eol)
    stream.write("  5. Set '-a' argument to 0 to set the default score to 0\n")
    stream.write("%s\n" % format_message(message))
    stream.write(eol)
    sys.exit(exitcode)



def read_bedfile(bedfilename):
    bedfile = open(bedfilename,'r')
    bed = dict()
    index = 0
    for line in bedfile:
        line = line.strip()

        if line is empty or \
           line.startswith(comment):
            continue

        fields = line.split(tab)
        strand = fields[5] if num(fields) >= 6 else null
        
        bed[fields[3]] = (fields[0], int(fields[1]), int(fields[2]), strand, index)
        index += 1

    return bed


def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'Aa:Bbc:d:hILmo:Q:q:R:rS:s:T:t:uvX',('help',))
    except getopt.GetoptError as message:
        usage(message)

    min_dist = -1
    qry_best = False
    write_bedpe = True
    minimize = False
    out_file = None
    trg_column = 1
    qry_column = 2
    qry_isopat = None
    trg_isopat = None
    min_cscore = None
    hit_quota = None
    uniq_hits = False
    selfalign = False
    mutualbest = False
    swap_column = False
    score_column = 3
    ignore_no_bed = False
    min_ascore = None
    max_ascore = None
    write_locus_ids = False
    for flag, value in options:
        if flag in ('-h','--help'): usage(exitcode=0)
        elif flag == '-a': score_column = int(value)
        elif flag == '-A': write_bedpe = False
        elif flag == '-B': write_bedpe = True
        elif flag == '-b': qry_best = True
        elif flag == '-c': min_cscore = float(value)
        elif flag == '-d': min_dist = int(value)
        elif flag == '-I': ignore_no_bed = True
        elif flag == '-L': write_locus_ids = True
        elif flag == '-m': minimize = True
        elif flag == '-o': out_file = value
        elif flag == '-q': qry_column = int(value)
        elif flag == '-Q': qry_isopat = value
        elif flag == '-R': hit_quota = value.split(colon)
        elif flag == '-r': mutualbest = True
        elif flag == '-s': min_ascore = float(value)
        elif flag == '-S': max_ascore = float(value)
        elif flag == '-t': trg_column = int(value)
        elif flag == '-T': trg_isopat = value
        elif flag == '-u': uniq_hits = True
        elif flag == '-v': swap_column = True
        elif flag == '-X': selfalign = True

    if num(arguments) != 3:
        usage("Unexpected number of arguments")

    if qry_best:
        if min_cscore or mutualbest:
            usage("Option '-b' is incompatible with '-c' and '-r' options")
        
    if hit_quota is not None:
        if num(hit_quota) != 2:
            usage("Two colon-separated numeric values required for '-R'")
        try: 
            hit_quota = tuple(map(int, hit_quota))
        except (ValueError, TypeError):
            usage("Two colon-separated numeric values required for '-R'")

    if trg_isopat is not None:
        trg_isopat = re.compile(trg_isopat)
    if qry_isopat is not None:
        qry_isopat = re.compile(qry_isopat)
        
    trg_column -= 1
    qry_column -= 1
    score_column -= 1 

    if arguments[0] == '-':
        anchor_file = sys.stdin
    else:
        anchor_file = open(arguments[0])
        
    bed1 = read_bedfile(arguments[1])
    bed2 = read_bedfile(arguments[2])

    if out_file is None:
        out = sys.stdout
    else:
        out = open(out_file, 'w')
    
    err.write("Command: %s %s\n" % (__program__, ' '.join(argv)))
    
    hits = []
    prev = None
    for line in anchor_file:
        line = line.strip()

        if line is empty or \
           line.startswith(comment):
            continue

        fields = line.split(tab)

        try:
            trg_locus = fields[trg_column]
        except IndexError:
            err.write("ERROR: Target column index out of range: %d\n" % (trg_column+1))
            sys.exit(1)
        try:
            qry_locus = fields[qry_column]
        except IndexError:
            err.write("ERROR: Query column index out of range: %d\n" % (qry_column+1))
            sys.exit(1)

        if score_column < 0:
            hit_score = 0
        else:
            try:
                hit_score = float(fields[score_column])
            except IndexError:
                err.write("ERROR: Score column index out of range: %d\n" % (score_column+1))
                sys.exit(1)
            except ValueError:
                err.write("ERROR: Score column value non-numeric: %s\n" % fields[score_column])
                sys.exit(1)
            
        try:
            trg_bed = bed1[trg_locus]
        except KeyError:
            if ignore_no_bed:
                err.write("WARNING: No such name in Target BED: %s\n" % trg_locus)
                continue
            else:
                err.write("ERROR: No such name in Target BED: %s\n" % trg_locus)
                sys.exit(1)
        try:
            qry_bed = bed2[qry_locus]
        except KeyError:
            if ignore_no_bed:
                err.write("WARNING: No such name in Query BED: %s\n" % qry_locus)
                continue
            else:
                err.write("ERROR: No such name in Query BED: %s\n" % qry_locus)
                sys.exit(1)

            
        trg = Interval(
            trg_bed[0],
            trg_bed[1],
            trg_bed[2],
            trg_locus,
            trg_bed[3],
            trg_bed[4],
        )
        qry = Interval(
            qry_bed[0],
            qry_bed[1],
            qry_bed[2],
            qry_locus,
            qry_bed[3],
            qry_bed[4]
        )

        trg.alias = trg.locus
        if trg_isopat is not None:
            trg.locus = trg_isopat.sub(empty, trg.locus)

        qry.alias = qry.locus
        if qry_isopat is not None:
            qry.locus = qry_isopat.sub(empty, qry.locus)

        if selfalign:
            if qry.locus == trg.locus:
                continue
            if qry.chr == trg.chr and \
               abs(qry.mid - trg.mid) < min_dist:
                continue
            hits.append(
                IntervalPair(
                    qry,
                    trg,
                    hit_score
                )
            )
            
        hits.append(
            IntervalPair(
                trg,
                qry,
                hit_score
            )
        )

    if qry_best:
        hits = filter_by_singlebest(hits, minimize)
    else:
        if min_ascore or max_ascore:
            hits = filter_by_ascore(hits, min_ascore, max_ascore)
        if min_cscore:
            hits = filter_by_cscore(hits, min_cscore, minimize)
        if mutualbest:
            hits = filter_by_mutualbest(hits, minimize)

    if hit_quota:
        hits = filter_by_quota(hits, hit_quota)    
    if uniq_hits:
        hits = filter_uniq(hits, selfalign, minimize)

    for hit in sorted(hits, key=by_min_index):
        if selfalign and hit.qry.index < hit.trg.index:
            hit.qry, hit.trg = hit.trg, hit.qry
        if swap_column:
            hit.qry, hit.trg = hit.trg, hit.qry
            
        if write_bedpe:
            out.write(format_bedpe(hit, write_locus_ids))
        else:
            out.write(format_anchor(hit, write_locus_ids))
            
    out.close()


main(sys.argv[1:])
