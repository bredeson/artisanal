
PROGRAM=$(basename $0 .sh);
PACKAGE='__PACKAGE_NAME__';
VERSION='__PACKAGE_VERSION__';
CONTACT='__PACKAGE_CONTACT__';
PURPOSE='A pipeline to assess genome completeness using transcripts';


threads=1
fraction_identity=0.90
fraction_coverage=0.90

function usage() {
    printf "\n" >>/dev/fd/2;
    printf "Program: $PROGRAM ($PURPOSE)\n" >>/dev/fd/2;
    printf "Version: $PACKAGE $VERSION\n" >>/dev/fd/2;
    printf "Contact: $CONTACT\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "Usage:   $PROGRAM [options] <target.fasta> <query.fasta> <out-prefix>\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "Options:\n" >>/dev/fd/2;
    printf "  --min-coverage <float>  Min. transcript cov. to be complete [$fraction_coverage]\n" >>/dev/fd/2;
    printf "  --min-identity <float>  Min. transcript identity [$fraction_identity]\n" >>/dev/fd/2;
    printf "  --threads <uint>        Use <uint> number of parallel threads [$threads]\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "Notes:\n"
    printf "  Known GMAP issue: References with numeric IDs starting at '0'\n" >>/dev/fd/2;
    printf "  (e.g., Contig0 or Scaffold0) will not be mapped correctly\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    printf "\n" >>/dev/fd/2;
    exit 1;
}

HELP_MESSAGE=;
COMMAND="$PROGRAM $@";
while [[ -n $@ ]]; do
    case "$1" in
        '--min-identity') shift; fraction_identity="$1";;
        '--min-coverage') shift; fraction_coverage="$1";;
	'--threads') shift; threads="$1";;
        '--help') HELP_MESSAGE=1;;
        '-h') HELP_MESSAGE=1;;
        -*) usage;;
        *) break;;
    esac;
    shift;
done


if [[ $HELP_MESSAGE ]]; then
    usage;
fi

if [[ $# -ne 3 ]]; then
    usage;
fi

missing_dependency=;
for exe in blat bamToPsl gmap gmap_build minimap2 samtools; do
    if [[ -z `which $exe 2>/dev/null` ]]; then
	echo "[$PROGRAM] ERROR: $exe not detected in PATH" >>/dev/fd/2;
	missing_dependency=1;
    fi
done

if [[ $threads -gt 1 && -z `which parallel 2>/dev/null` ]]; then
    echo "[$PROGRAM] ERROR: Threads requested but GNU parallel not found" >>/dev/fd/2;
    missing_dependency=1;
fi

if [[ $missing_dependency ]]; then
    exit 1;
fi
    
echo "Command: $COMMAND" >>/dev/fd/2;

percent_identity=`echo "100 * $fraction_identity" | bc`;
percent_coverage=`echo "100 * $fraction_coverage" | bc`;

infasta=$1;
iprefix=$(basename $1 | sed 's/.f\(asta\|nt\|na\|a\)$//'); 
oprefix=$3;
queries=$2;
qprefix=$(basename $2 | sed 's/.f\(asta\|nt\|na\|a\)$//');

# Xtrop:
# 52k : /projectb/sandbox/plant/bredeson/Xenopus/tropicalis/annotation/gene_evidence/all-genes.nr.fasta;
# ort : /projectb/sandbox/plant/bredeson/Xenopus/tropicalis/annotation/gene_evidence/Xtrop.ort.fasta
# XB9 : /projectb/sandbox/plant/bredeson/Xenopus/tropicalis/annotation/gene_evidence/Xtrop-mRNA-XENBASE.fasta
# Xlaevis:
# NCBI: /projectb/sandbox/plant/bredeson/Xenopus/laevis/annotation/gene_evidence/ncbi-complete-cds.nt.20190617.nr.no-contaminants.fasta
# XB9 : /global/projectb/sandbox/plant/bredeson/Xenopus/laevis/assembly/v9.2/annotation/XENLA_9.2_Xenbase_cds.fa


if [ ! -e $queries.fai ]; then
    samtools faidx $queries;
fi
if [ ! -e $infasta.fai ]; then
    samtools faidx $infasta;
fi
if [ ! -d $iprefix.gmap.db ]; then
    gmap_build -D $PWD -d $iprefix.gmap.db $infasta &>$iprefix.gmap.build.err \
	&& rm $iprefix.gmap.build.err
fi

parse_best_weighted_alignment_script="./.$iprefix.pl";
echo '
BEGIN
{
  %aligns=();
}
{
  chomp($F[$#F]);
  $cov = $F[12] - $F[11];
  $pid = $F[0] / ($cov > 0 ? $cov : 1);
  $cov = $cov / ($F[10] > 0 ? $F[10] : 1);
  $weight = $cov*$pid;
  if ((! exists($aligns{$F[9]})) || ($aligns{$F[9]}{"weight"} < $weight)) {
    $aligns{$F[9]}{"weight"} = $weight;
    $aligns{$F[9]}{"len"} = $F[10];
    $aligns{$F[9]}{"cov"} = sprintf("%.05f",$cov);
    $aligns{$F[9]}{"pid"} = sprintf("%.05f",$pid);
    $aligns{$F[9]}{"qry"} = "$F[9]\t$F[11]\t$F[12]";
    $aligns{$F[9]}{"ref"} = "$F[13]\t$F[15]\t$F[16]";
    $aligns{$F[9]}{"strand"} = "+\t$F[8]";
    $aligns{$F[9]}{"alg"} = $F[21];
    $aligns{$F[9]}{"record"} = join("\t",@F)."\n";
  }
}
END
{
  map {
    print join("\t",
      $aligns{$_}{"ref"},
      $aligns{$_}{"qry"},
      $aligns{$_}{"alg"},
      $aligns{$_}{"len"},
      $aligns{$_}{"strand"},
      $aligns{$_}{"cov"},
      $aligns{$_}{"pid"},
    ),"\n";
  } keys(%aligns);
  open($psl,">'$oprefix.completeness.psl'") || die "Cannot open '$oprefix.completeness.psl' for writing\n";
  map {
    print($psl $aligns{$_}{"record"});
  } keys(%aligns);
}' >$parse_best_weighted_alignment_script;



perl -F"\t" -ane '
{
  print join("\t",0,0,0,0,0,0,0,0,".",@F[0,1],-1,-1,".",0,-1,-1,0,",",",",",","nohit"),"\n";
}' $queries.fai >$oprefix.init.psl


gmap \
    --batch=4 \
    --npaths=0 \
    --nthreads=$threads \
    --format=samse \
    --sam-extended-cigar \
    --read-group-id="$qprefix.gmap" \
    --read-group-name="$qprefix" \
    --read-group-library=gmap \
    --read-group-platform=ILLUMINA \
    --min-identity=$fraction_identity \
    --dir=$PWD \
    --db=$iprefix.gmap.db \
    $queries \
    2>$oprefix.gmap.align.err \
    | samtools view -u - >$oprefix.gmap.bam

bamToPsl \
    -nohead \
    $oprefix.gmap.bam \
    stdout \
    2>$oprefix.gmap.parse.err \
    | awk 'OFS="\t" {print $0,"gmap"}' >$oprefix.gmap.psl


minimap2 \
    -aY \
    -x splice \
    --eqx \
    --secondary=no \
    -t $threads \
    -R "@RG\tID:$qprefix.mini\tSM:$qprefix\tLB:mini\tPL:ILLUMINA" \
    $infasta \
    $queries \
    2>$oprefix.mini.align.err \
    | samtools view -u - >$oprefix.mini.bam

bamToPsl \
    -nohead \
    $oprefix.mini.bam \
    stdout \
    2>$oprefix.mini.parse.err \
    | awk 'OFS="\t" {print $0,"mini"}' >$oprefix.mini.psl


samtools cat $oprefix.mini.bam $oprefix.gmap.bam \
    |  samtools sort -m 1G -o $oprefix.bam - \
    && samtools index $oprefix.bam \
    && rm $oprefix.mini.bam $oprefix.mini.align.err $oprefix.mini.parse.err \
	  $oprefix.gmap.bam $oprefix.gmap.align.err $oprefix.gmap.parse.err


perl -F"\t" \
    -an $parse_best_weighted_alignment_script \
    $oprefix.init.psl \
    $oprefix.gmap.psl \
    $oprefix.mini.psl \
    | sort -k11,11gr -k12,12nr -k8,8nr \
    >$oprefix.completeness.bedpe


awk -v min_pid=$fraction_identity \
    -v min_cov=$fraction_coverage \
    'OFS="\t" {if (($11 < min_cov) || ($12 < min_pid)) { print $4 }}' \
    $oprefix.completeness.bedpe \
    >$oprefix.blat.list


perl -ane '
BEGIN {
  %Q=();
  $parse=0;
  $print=0;
} 
{
  if (/^>/) {
    $parse=1;
  } 
  if ($parse) {
    if (/^>\s*(\S+)/) {
      $print=int($Q{$1});
    } 
    print if $print;
  } else { 
    $Q{$F[0]}=1;
  }
}' $oprefix.blat.list $queries >$oprefix.blat.fasta && \
    samtools faidx $oprefix.blat.fasta && \
    rm $oprefix.blat.list

N=$(cat $oprefix.blat.fasta.fai | wc -l);
n=$(expr $N / $threads);
if [[ $N -lt $threads ]]; then
    threads=$N;
    n=1;
fi

M=$threads;
m=$(expr $N % $threads);
if [[ $m -eq 0 ]]; then
    # because `split' uses 0-based indices we subtract 1.
    # normally we would add 1 to $M if the number of
    # sequences is not divisible by $threads;
    echo "Setting range to $M" >>/dev/fd/2;
    M=$(expr $M - 1);
fi

if [[ $threads -gt 1 ]]; then
    cut -f1 $oprefix.blat.fasta.fai | sort -k1,1R | split -d -a 4 -l $n - $oprefix.blat.
    for i in $(seq -f '%04g' 0 $M); do
	mv $oprefix.blat.$i $oprefix.blat.$i.list;
	perl -ne '
BEGIN{
  open($f,"<'$oprefix.blat.$i.list'")||die;
  %h=map{chomp(); $_=>1}<$f>;
  $ok=1;
}
{
  if (/^>\s*(\S+)/) {
    $ok=int($h{$1});
  }
  print if $ok;
}' $oprefix.blat.fasta >$oprefix.blat.$i.fasta;
    done

    for i in $(seq -f '%04g' 0 $M); do
	echo "blat $infasta $oprefix.blat.$i.fasta -trimHardA -tileSize=11 -stepSize=3 -minIdentity=$percent_identity -noHead -out=psl $oprefix.blat.$i.tmp.psl &>$oprefix.blat.$i.tmp.log && mv $oprefix.blat.$i.tmp.psl $oprefix.blat.$i.psl && rm $oprefix.blat.$i.fasta $oprefix.blat.$i.list $oprefix.blat.$i.tmp.log";
    done | parallel --will-cite --max-procs $threads --joblog $oprefix.blat.err;

    merge_files="";
    for i in $(seq -f '%04g' 0 $M); do
	if [[ -e "$oprefix.blat.$i.psl" ]]; then
	    merge_files="$merge_files $oprefix.blat.$i.psl";
	else
	    echo "[$PROGRAM] ERROR: blat subprocess did not complete: $i" >>/dev/fd/2;
	    exit 255;
	fi
    done
    cat $merge_files >$oprefix.blat.psl && rm $merge_files;
else
    blat \
	$infasta \
	$oprefix.blat.fasta \
	-trimHardA \
	-tileSize=11 \
	-stepSize=3 \
	-minIdentity=$percent_identity \
	-noHead \
	-out=psl \
	$oprefix.blat.tmp.psl \
	&>$oprefix.blat.err \
    && mv $oprefix.blat.tmp.psl $oprefix.blat.psl
fi


perl -F"\t" \
    -an $parse_best_weighted_alignment_script \
    $oprefix.init.psl \
    $oprefix.gmap.psl \
    $oprefix.mini.psl \
    $oprefix.blat.psl \
    | sort -k11,11gr -k12,12nr -k8,8nr \
    >$oprefix.completeness.bedpe

LOID=`awk -v cov=$fraction_coverage -v fid=$fraction_identity '{if ($12 < fid) { print }}' $oprefix.completeness.bedpe | wc -l`;
COMP=`awk -v cov=$fraction_coverage -v fid=$fraction_identity '{if (($12 >= fid) && ($11 >= cov)) { print }}' $oprefix.completeness.bedpe | wc -l`;
FRAG=`awk -v cov=$fraction_coverage -v fid=$fraction_identity '{if (($12 >= fid) && ($11 >= 0.5 && $11 < cov)) { print }}' $oprefix.completeness.bedpe | wc -l`
MISS=`awk -v cov=$fraction_coverage -v fid=$fraction_identity '{if (($12 >= fid) && ($11 <  0.5)) { print }}' $oprefix.completeness.bedpe | wc -l` 

echo "##Command: $0 $@" >$oprefix.completeness.summary
echo "##C=complete  (>= $percent_coverage%)"  >>$oprefix.completeness.summary
echo "##F=fragmented (< $percent_coverage%)"  >>$oprefix.completeness.summary
echo "##M=missing    (< 50.00%)"              >>$oprefix.completeness.summary
echo "##I=identity   (< $percent_identity%)"  >>$oprefix.completeness.summary
printf "#file-name C F M I:\n"                >>$oprefix.completeness.summary
printf "%s %d %d %d %d\n" $(basename $oprefix) $COMP $FRAG $MISS $LOID >>$oprefix.completeness.summary

       
       
       

echo >>$oprefix.completeness.summary

printf "Min\tNumber\tFrac\ncov\tof\tof\nthresh\tqueries\tqueries\n==========================\n" >>$oprefix.completeness.summary
for f in `seq 0.50 0.05 1.00`; do
    awk -v minf=$f -v mini=$fraction_identity 'BEGIN {c=0;t=0} {if (($12 >= mini) && ($11 >= minf)) { c++ } t++} END { printf("%.2f\t%d\t%.4f\n",minf,c,c/t) }' $oprefix.completeness.bedpe;
done >>$oprefix.completeness.summary
