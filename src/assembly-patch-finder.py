
import os
import re
import sys
import math
import pysam
import bisect
import getopt
import logging

from intervals import Strand, BaseInterval, IntervalList

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Find patches for low-quality assembly sequence'


HELP_FLAGS = ('-h', '--help')
VERS_FLAGS = ('-v', '--version')

STRAND = ('+','-')
STRICT_RADIUS = 0x1
EXTEND_FLANK = 0x2
GLOBAL_TILING = 0x4
OUTER_DIMENSION = 0x8
INCLUDE_INDELS = 0x10
DEFAULT_MAPQUAL = 0
DEFAULT_RADIUS = 20000
DEFAULT_ALIGN_LENGTH = 1000
DEFAULT_TILED_LENGTH = 10000
DEFAULT_ERROR_RATE = 1.0
CIGAR_ALIGNED_OPERATOR = set((0, 7, 8))
CIGAR_PADDING_OPERATOR = set((4, 5))
QUERY_LENGTH_OPERATOR = set((0, 1, 4, 5, 7, 8))
REFERENCE_LENGTH_OPERATOR = set((0, 2, 3, 7, 8))
REFERENCE_GAP = set('Nn')
LOGGING_LEVEL = logging.INFO
PYTHONVERSION = sys.version_info[:2]


class GenomicInterval(BaseInterval):
    def __init__(self, contig='.', beg=-1, end=-1, strand='.', is_gap=False):
        BaseInterval.__init__(self, contig, beg, end)
        self.strand = Strand(strand)
        self.is_gap = is_gap

    @property
    def contig(self):
        return self.name

    @contig.setter
    def contig(self, name):
        self.name = name
        
    def __str__(self):
        return "%s:%d-%d(%s) <%s>" % (
            self.contig, self.beg, self.end, self.strand.str,
            'gap' if self.is_gap else 'contig')

    @property
    def length(self):
        if self.beg < 0 or self.end < 0:
            return 0
        return self.end - self.beg


class AlignedIntervalPair(object):
    def __init__(self, qry, ref, cigartuples=tuple(), strand='.', mapqual=0, aligns=[0,0]):
        self.qry = qry
        self.ref = ref
        self.cigartuples = cigartuples
        self.strand = Strand(strand)
        self.mapqual = mapqual
        self.aligns = aligns
        
    def __lt__(self, other):
        if self.ref.name < other.ref.name:
            return True
        
        if self.ref.name == other.ref.name:
            if self.ref.beg < other.ref.beg:
                return True
            if self.ref.beg == other.ref.beg:
                return self.ref.end < other.ref.end
        return False

    def __eq__(self, other):
        return self.ref.name == other.ref.name and \
            self.ref.beg == other.ref.beg and \
            self.ref.end == other.ref.end

    def __ne__(self, other):
        return self != other
    
    def __le__(self, other):
        return self < other or self == other

    def __gt__(self, other):
        if self.ref.name > other.ref.name:
            return True

        if self.ref.name == other.ref.name:
            if self.ref.beg > other.ref.beg:
                return True
            if self.ref.beg == other.ref.beg:
                return self.ref.end > other.ref.end
        return False

    def __ge__(self, other):
        return self > other or self == other

    
class AlignedInterval(BaseInterval):
    def __init__(self, contig, beg, end, contig_length=-1):
        BaseInterval.__init__(self, contig, beg, end)
        self.contig_length = int(contig_length)

    @property
    def contig(self):
        return self.name
    
    @contig.setter
    def contig(self, name):
        self.name = name
        
    @property
    def aligned_length(self):
        return self.end - self.beg

    @property
    def aligned_fraction(self):
        return float(self.aligned_length) / self.contig_length
    
    @property
    def spur5p_length(self):
        return self.beg
    
    @property
    def spur3p_length(self):
        return self.contig_length - self.end



class AlignedIntervalTiling(IntervalList):
    def __init__(self, alignments=[], setter=lambda x:x, getter=lambda x:x.ref):
        IntervalList.__init__(self, setter=setter, getter=getter)
        self._length = 0
        self._longest = None
        self._mapqual = 0
        for alignment in sorted(alignments, key=lambda a: -1.0*a.length*a.similarity):
            self.add(alignment)
            
    def add(self, alignment):
        v = self._set(alignment)
        if self.is_empty:
            if self._longest is None or \
               alignment.ref.aligned_length > self._longest.ref.aligned_length:
                self._longest = alignment
            self._mapqual += alignment.ref.aligned_length * alignment.mapqual
            self._length += alignment.ref.aligned_length
            self._len += 1
            self.intervals.append(v)
            return True
        else:
            i = bisect.bisect_left(self.intervals, v)
            if i == 0 and self.is_beg(alignment):
                # insert at 5' end
                if self._longest is None or \
                   alignment.ref.aligned_length > self._longest.ref.aligned_length:
                    self._longest = alignment
                self._mapqual += alignment.ref.aligned_length * alignment.mapqual
                self._length += alignment.ref.aligned_length
                self._len += 1
                bisect.insort_left(self.intervals, v)
                return True

            elif i == len(self.intervals) and self.is_end(alignment):
                # insert at 3' end
                if self._longest is None or \
                   alignment.ref.aligned_length > self._longest.ref.aligned_length:
                    self._longest = alignment
                self._mapqual += alignment.ref.aligned_length * alignment.mapqual
                self._length += alignment.ref.aligned_length                
                bisect.insort_right(self.intervals, v)
                self._len += 1
                return True
            
            elif i < len(self.intervals) and self.is_mid(alignment, i):
                # insert between two tiled alignments
                if self._longest is None or \
                   alignment.ref.aligned_length > self._longest.ref.aligned_length:
                    self._longest = alignment
                self._mapqual += alignment.ref.aligned_length * alignment.mapqual
                self._length += alignment.ref.aligned_length
                self._len += 1
                bisect.insort_left(self.intervals, v)
                return True

        return False

    @property
    def longest(self):
        return self._longest

    @property
    def mapqual(self):
        return self._mapqual // self._length

    @property
    def length(self):
        return self._length

    
    def is_beg(self, alignment):
        alignment_rght = self.intervals[0]
        alignment_qmid = (alignment.qry.end + alignment.qry.beg) / 2.0
        alignment_rmid = (alignment.ref.end + alignment.ref.beg) / 2.0
        # Using the mid-point in the following conditions disallows
        # alignments contained within others
        if alignment_qmid <= alignment_rght.qry.beg and \
           alignment_rmid <= alignment_rght.ref.beg:
            return True
        return False
    

    def is_mid(self, alignment, i):
        alignment_left = self.intervals[i-1]
        alignment_rght = self.intervals[i]
        alignment_qmid = (alignment.qry.end + alignment.qry.beg) / 2.0
        alignment_rmid = (alignment.ref.end + alignment.ref.beg) / 2.0
        if alignment_left.qry.end <= alignment_qmid <= alignment_rght.qry.beg and \
           alignment_left.ref.end <= alignment_rmid <= alignment_rght.ref.beg:
            return True
        return False


    def is_end(self, alignment):
        alignment_left = self.intervals[-1]
        alignment_qmid = (alignment.qry.end + alignment.qry.beg) / 2.0
        alignment_rmid = (alignment.ref.end + alignment.ref.beg) / 2.0
        # Using the mid-point in the following conditions disallows
        # alignments contained within others
        if alignment_left.qry.end <= alignment_qmid and \
           alignment_left.ref.end <= alignment_rmid:
            return True
        return False


def calc_ref_from_qry(record, position, zerobased=False):
    if zerobased:
        position += 1
        
    qry_off = qry_end = record.qry.beg
    ref_off = ref_end = record.ref.beg
    for cig in record.cigartuples:
        if cig[0] in QUERY_LENGTH_OPERATOR:
            qry_end += cig[1]
        if cig[0] in REFERENCE_LENGTH_OPERATOR:
            ref_end += cig[1]

        if qry_off < position <= qry_end:
            position = ref_off + (position - qry_off)
            if zerobased:
                position -= 1;
            return position

        qry_off = qry_end
        ref_off = ref_end
        
    return -1


def calc_qry_from_ref(record, position, zerobased=False):
    if zerobased:
        position += 1
        
    qry_off = qry_end = record.qry.beg
    ref_off = ref_end = record.ref.beg
    for cig in record.cigartuples:
        if cig[0] in QUERY_LENGTH_OPERATOR:
            qry_end += cig[1]
        if cig[0] in REFERENCE_LENGTH_OPERATOR:
            ref_end += cig[1]

        if ref_off < position <= ref_end:
            position = qry_off + (position - ref_off)
            if zerobased:
                position -= 1;
            return position

        qry_off = qry_end
        ref_off = ref_end
        
    return -1

    
def by_reference_position(alignment):
    return alignment.ref.beg



def infer_query_len(alignment):
    query_len = 0
    for cig in alignment.cigartuples:
        if cig[0] in QUERY_LENGTH_OPERATOR:
            query_len += cig[1]
    
    return query_len


def infer_query_beg(alignment):
    query_beg = 0
    for cig in alignment.cigartuples:
        if cig[0] in CIGAR_PADDING_OPERATOR:
            query_beg += cig[1]
        else:
            break

    return query_beg


def infer_query_end(alignment, query_len=-1):
    query_end = 0
    if query_len < 0:
        seen_alignment = False
        for cig in alignment.cigartuples:
            if cig[0] in CIGAR_PADDING_OPERATOR:
                if seen_alignment: break
                query_end += cig[1]
            elif cig[0] in CIGAR_LENGTH_OPERATOR:
                seen_alignment = True
                query_end += cig[1]

        return query_end
    else:
        # query_len is available, so go faster by 
        num_operations = num(alignment.cigartuples)
        for i in range(num_operations):
            cig = alignment.cigartuples[num_operations - i - 1]
            if cig[0] in CIGAR_PADDING_OPERATOR:
                query_end += cig[1]
            else:
                return query_len - query_end
                


def calc_bridge_error_rate(tiling):
    aligns = [0, 0]
    for aln in tiling.intervals:
        aligns[0] += aln.aligns[0]
        aligns[1] += aln.aligns[1]
        
    return float(aligns[0]) / max(1, aligns[1])



def calc_splint_error_rate(tiling, gap):
    matches5p = [0, 0]
    matches3p = [0, 0]
    for aln in tiling.intervals:
        if aln.ref.end < gap.end:
            matches5p[0] += aln.aligns[0]
            matches5p[1] += aln.aligns[1]
        else:
            matches3p[0] += aln.aligns[0]
            matches3p[1] += aln.aligns[1]

    return max(matches5p[0]/float(max(1, matches5p[1])), matches3p[0]/float(max(1, matches3p[1])))



def calc_aligns(read, ref_seq, qry_seq, include_indels=False):
    exclude_indels = not include_indels
    gap_bases = set('nN')
    aligns = [0, 0]
    qry_pos = 0
    qry_beg = read.query_alignment_start
    qry_end = read.query_alignment_end
    ref_pos = read.reference_start
    for position in read.get_aligned_pairs(matches_only=exclude_indels):
        qry_pos = qry_pos if position[0] is None else position[0]
        ref_pos = ref_pos if position[1] is None else position[1]

        if qry_pos <= qry_beg:
            continue
        if qry_pos >= qry_end:
            break

        if position[0] is None or \
           position[1] is None:
            aligns[0] += 1
            aligns[1] += 1
        else:
            ref_nt = ref_seq[ref_pos]
            qry_nt = qry_seq[qry_pos]                
            if ref_nt in gap_bases or \
               qry_nt in gap_bases:
                continue
            if ref_nt != qry_nt:
                aligns[0] += 1
            aligns[1] +=1

    return aligns




num = len

rejected_because = "    REJECTED {0} because {1}".format

def format_2bed(ref, qry, strand='.'):
    formatted_string = "{0.contig:s}\t{0.beg:d}\t{0.end:d}\t{1.contig:s}:{1.beg:d}-{1.end:d}\t0\t{strand:s}\n".format(
        ref, qry, strand=strand)
    if PYTHONVERSION < (3,):
        return formatted_string
    else:
        return formatted_string  #.decode('ASCII')


def format_3bed(gap, ref, qry, strand='.'):
    assert gap.beg <= gap.end, str(gap)
    assert ref.beg <= ref.end, str(ref)
    assert qry.beg <= qry.end, str(qry)
    formatted_string = "{0.contig:s}\t{0.beg:d}\t{0.end:d}\t{1.contig:s}\t{1.beg:d}\t{1.end:d}\t{2.contig:s}\t{2.beg:d}\t{2.end:d}\t{strand:s}\t{net:d}\n".format(
        gap, ref, qry, strand=strand, net=((qry.end-qry.beg)-(ref.end-ref.beg)))
    if PYTHONVERSION < (3,):
        return formatted_string
    else:
        return formatted_string  #.decode('ASCII')


        
def find_patches(alignment_file, reference_file, patches_file, support_file, repeats_file,
                 max_error_rate=1.0, min_align_mapping_quality=0, min_tiled_mapping_quality=0,
                 min_align_length=1, min_tiled_length=1, alignment_radius=0, switches=0):

    logging.info("Starting traversal of %d references:" % reference_file.nreferences)

    if switches & STRICT_RADIUS:
        lower = lambda x, y: y
        upper = lambda x, y: y
    elif switches & EXTEND_FLANK:
        lower = min
        upper = max
    else:
        lower = max
        upper = min

    filter_globally = switches & GLOBAL_TILING
    include_indels  = switches & INCLUDE_INDELS
    
    total_gaps = 0
    total_alignments = 0
    outer_dimension = 0
    nucleotides = re.compile('[acgturyswkmbdhvACGTURYSWKMBDHV]+')
    for ref_name, ref_length in zip(reference_file.references, reference_file.lengths):
        logging.info("Traversal of %s :" % ref_name)
        
        # The inability to fetch() the reference sequence should be fatal,
        # as it should ALWAYS be available, but a reference sequence may
        # have no BED features (gaps) to fetch, so empty fetch() is not fatal.
        try:
            ref_sequence = reference_file.fetch(ref_name).upper()
        except ValueError:
            raise IOError("Could not fetch reference sequence: "+ref_name)

        prev = GenomicInterval()
        ref_intervals = []
        for contig in nucleotides.finditer(ref_sequence):
            curr = GenomicInterval(ref_name, contig.start(), contig.end())

            if prev.end >= 0:
                this = GenomicInterval(ref_name, prev.end, curr.beg, is_gap=True)
                ref_intervals.append(prev)
                ref_intervals.append(this)
                logging.debug(str(prev))
                logging.debug(str(this))
                    
            prev = curr
            
        ref_intervals.append(prev)
        logging.debug(str(prev))
        logging.debug('')

        repeats = AlignedIntervalTiling()
        if repeats_file is not None:
            try:
                for repeat in repeats_file.fetch(ref_name):
                    repeats.add(
                        AlignedIntervalPair(
                            AlignedInterval(
                                repeat.contig,
                                repeat.start,
                                repeat.end
                            ),
                            AlignedInterval(
                                repeat.contig,
                                repeat.start,
                                repeat.end
                            )
                        )
                    )
            except ValueError:
                pass

        window = [None, None]
        closures = []
        num_gaps = 0
        num_alignments = 0
        alignment_files = [None, None]
        for i in range(1, num(ref_intervals), 2):
            assert not ref_intervals[i-1].is_gap, "Contig feature expected, got "+str(ref_intervals[i-1])
            assert     ref_intervals[i+0].is_gap, "Gap feature expected, got "   +str(ref_intervals[i+0])
            assert not ref_intervals[i+1].is_gap, "Contig feature expected, got "+str(ref_intervals[i+1])
            
            gap = ref_intervals[i]
            logging.debug(str(gap))

            window[0] = GenomicInterval(
                ref_name,
                lower(
                    ref_intervals[i-1].beg,
                    max(0, gap.beg - alignment_radius)
                ),
                gap.beg
            )
            window[1] = GenomicInterval(
                ref_name,
                gap.end,
                upper(
                    ref_intervals[i+1].end,
                    min(ref_length, gap.end + alignment_radius)
                )
            )
            logging.debug("  Flanks: "+str(window[0])+" - "+str(window[1]))
            
            try:
                alignment_files[0] = alignment_file.fetch(window[0].contig, window[0].beg, window[0].end)
                alignment_files[1] = alignment_file.fetch(window[1].contig, window[1].beg, window[1].end)
            except ValueError:
                # Also thrown when no .bai file is found...
                patches_file.write(format_3bed(gap, GenomicInterval(), GenomicInterval()))
                continue

            splints = []
            bridges = []
            presplints = []
            prebridges = []
            tiled_queries = {}
            tiled_locally = {}
            tiled_globally = {}
            alignments = [[], []]
            for flank in range(2):
                for record in alignment_files[flank]:
                    if record.is_duplicate or record.is_secondary:
                        continue
                    if record.mapping_quality < min_align_mapping_quality:
                        continue
                    if record.reference_length < min_align_length:
                        continue
                    
                    query_len = infer_query_len(record)
                    query_beg = infer_query_beg(record)
                    query_end = infer_query_end(record, query_len)
                    
                    pair = AlignedIntervalPair(
                        AlignedInterval(
                            record.query_name,
                            query_beg,
                            query_end,
                            query_len
                        ),
                        AlignedInterval(
                            record.reference_name,
                            record.reference_start,
                            record.reference_end,
                            ref_length
                        ),
                        cigartuples=record.cigartuples,
                        strand=STRAND[int(record.is_reverse)],
                        mapqual=record.mapping_quality,
                        aligns=calc_aligns(record, ref_sequence, record.query_sequence, include_indels)
                    )
                    alignments[flank].append(pair)
                    num_alignments += 1


            if filter_globally:
                _alignments = [[],[]]
                for pair in sorted(alignments[0]+alignments[1], key=lambda x: x.aligns[0]-x.aligns[1]):
                    if pair.qry.contig not in tiled_globally:
                        tiled_globally[pair.qry.contig] = {}
                    if pair.strand.str not in tiled_globally[pair.qry.contig]:
                        tiled_globally[pair.qry.contig][pair.strand.str] = AlignedIntervalTiling()
                    if tiled_globally[pair.qry.contig][pair.strand.str].add(pair):
                        _alignments[int(pair.ref.beg > gap.beg)].append(pair)

                alignments = _alignments
                        
            for flank in range(2):
                # prioritize alignments by length:
                for pair in sorted(alignments[flank], key=lambda x: x.aligns[0]-x.aligns[1]):
                    if pair.qry.contig not in tiled_queries:
                        tiled_locally[pair.qry.contig] = {}
                        tiled_queries[pair.qry.contig] = {}

                    if pair.strand.str not in tiled_queries[pair.qry.contig]:
                        tiled_locally[pair.qry.contig][pair.strand.str] = AlignedIntervalTiling()
                        tiled_queries[pair.qry.contig][pair.strand.str] = [
                            AlignedIntervalTiling(), AlignedIntervalTiling()
                        ]
                    
                    if pair.ref.beg < gap.beg and gap.end < pair.ref.end:
                        # Gap fully spanned, merge tilings
                        #   ______________________
                        # ========...........========
                        if tiled_locally[pair.qry.contig][pair.strand.str].add(pair):
                            if flank == 0:
                                tiled_queries[pair.qry.contig][pair.strand.str][1] \
                                    = tiled_queries[pair.qry.contig][pair.strand.str][0]
                            tiled_queries[pair.qry.contig][pair.strand.str][flank].add(pair)
                            
                    else:
                        # upstream- or downstream-flanking alignment:
                        #  ________--------
                        # =========.............=========
                        if tiled_locally[pair.qry.contig][pair.strand.str].add(pair):
                            tiled_queries[pair.qry.contig][pair.strand.str][flank].add(pair)

            for qry_name in tiled_queries:
                for strand in tiled_queries[qry_name]:
                    pairs5p = tiled_queries[qry_name][strand][0]
                    pairs3p = tiled_queries[qry_name][strand][1]

                    if num(pairs5p) < 1 or num(pairs3p) < 1:
                        logging.debug(rejected_because(qry_name, 'aligns to only one flank of the gap'))
                        continue
                    if pairs5p.mapqual < min_tiled_mapping_quality:
                        logging.debug(rejected_because(qry_name, 'has low mapQ (5p)'))
                        continue
                    if pairs3p.mapqual < min_tiled_mapping_quality:
                        logging.debug(rejected_because(qry_name, 'has low mapQ (3p)'))
                        continue                    
                    if pairs5p.length - pairs5p.overlap_length(repeats) < min_tiled_length or \
                       pairs3p.length - pairs3p.overlap_length(repeats) < min_tiled_length:
                        logging.debug(rejected_because(qry_name, 'overlaps too little non-repetitive sequence'))
                        continue
                    
                    # First test the fully-spanned case:
                    if pairs5p is pairs3p:
                        if pairs3p.intervals[-1].ref.end - gap.end >=  min_tiled_length and \
                           pairs5p.intervals[ 0].ref.beg - gap.beg <= -min_tiled_length:
                            pair_error_rate = 0.0
                            if max_error_rate < 1.0:
                                pair_error_rate = calc_splint_error_rate(pairs5p, gap)
                                
                            if pair_error_rate > max_error_rate:
                                logging.debug(rejected_because(qry_name, 'has high error rate'))
                                continue

                            splint = GenomicInterval(contig=qry_name, strand=strand)
                            anchor = GenomicInterval(contig=ref_name)
                            for pair in pairs5p.intervals:
                                if pair.ref.beg < gap.beg and gap.end < pair.ref.end:
                                    splint = GenomicInterval(qry_name, pair.qry.beg, pair.qry.end, strand=strand)
                                    anchor = GenomicInterval(ref_name, pair.ref.beg, pair.ref.end)
                                    break

                            if splint.beg < 0 or splint.end < 0 or anchor.beg < 0 or anchor.end < 0:
                                logging.debug(rejected_because(qry_name, 'doesnt sufficiently span gap'))
                                continue

                            splints.append((-min(pairs5p.length, pairs3p.length), anchor, splint))
                            presplints.append(pairs5p)
                        else:
                            logging.debug(rejected_because(qry_name, 'doesnt sufficiently span gap'))
                                

                    elif pairs5p.length >= min_tiled_length and pairs3p.length >= min_tiled_length:
                        pairs5p_error_rate = 0.0
                        pairs3p_error_rate = 0.0
                        if max_error_rate < 1.0:
                            pairs5p_error_rate = calc_bridge_error_rate(pairs5p)
                            pairs3p_error_rate = calc_bridge_error_rate(pairs3p)
                        
                        if pairs5p_error_rate > max_error_rate or \
                           pairs3p_error_rate > max_error_rate:
                            logging.debug(rejected_because(qry_name, 'has high error rate'))
                            continue

                        query_contig_length   = pairs5p.longest.qry.contig_length
                        pairs5p_spur5p_length = pairs5p.longest.qry.beg  # SELECT LONGEST?
                        pairs5p_spur3p_length = query_contig_length - pairs5p.longest.qry.end
                        pairs3p_spur5p_length = pairs3p.longest.qry.beg  # SELECT LONGEST?
                        pairs3p_spur3p_length = query_contig_length - pairs3p.longest.qry.end
                        
                        if pairs3p_spur5p_length < pairs5p_spur5p_length:
                            # alignment is inconsistent
                            logging.debug(rejected_because(qry_name, 'is inconsistently aligned'))
                            continue

                        if outer_dimension:
                            bridge = GenomicInterval(qry_name, pairs5p.longest.qry.beg, pairs3p.longest.qry.end, strand=strand)
                            anchor = GenomicInterval(ref_name, pairs5p.longest.ref.beg, pairs3p.longest.ref.end)
                            bridge_length = pairs3p.longest.qry.end - pairs5p.longest.qry.beg
                        else:
                            bridge = GenomicInterval(qry_name, pairs5p.longest.qry.end, pairs3p.longest.qry.beg, strand=strand)
                            anchor = GenomicInterval(ref_name, pairs5p.longest.ref.end, pairs3p.longest.ref.beg)
                            bridge_length = query_contig_length \
                                            - pairs5p.longest.qry.aligned_length \
                                            - pairs3p.longest.qry.aligned_length \
                                            - pairs5p_spur5p_length \
                                            - pairs3p_spur3p_length

                        if bridge.beg >	bridge.end and \
                           anchor.beg > anchor.end:
                            # BOTH were negative...
                            logging.debug(rejected_because(qry_name, 'has complex tiling'))
                            continue

                        elif bridge.beg > bridge.end:
                            # too much sequence in the ref, must remove sequence
                            if pairs5p.longest.ref.aligned_length > pairs3p.longest.ref.aligned_length:
                                # subtract from 5p alignment
                                # anchor.beg = anchor.beg - (bridge.beg - bridge.end)
                                anchor.beg = calc_ref_from_qry(pairs5p.longest, bridge.end, zerobased=False)
                                bridge.beg = bridge.end
                            else:
                                # subtract from 3p alignment
                                anchor.end = calc_ref_from_qry(pairs3p.longest, bridge.beg, zerobased=True)
                                bridge.end = bridge.beg

                        elif anchor.beg > anchor.end:
                            # too little sequence in ref, must insert sequence
                            if pairs5p.longest.qry.aligned_length > pairs3p.longest.qry.aligned_length:
                                bridge.beg = calc_qry_from_ref(pairs5p.longest, anchor.end, zerobased=False)
                                anchor.beg = anchor.end
                            else:
                                bridge.end = calc_qry_from_ref(pairs3p.longest, anchor.beg, zerobased=True)
                                anchor.end = anchor.beg

                        if anchor.beg < 0 or anchor.end < 0 or anchor.beg > anchor.end or \
                           bridge.beg < 0 or bridge.end < 0 or bridge.beg > bridge.end:
                            # BOTH were negative...
                            logging.debug(rejected_because(qry_name, 'could not correct by adjusting coordinates'))
                            continue
                            
                        bridges.append((-min(pairs5p.length, pairs3p.length), anchor, bridge))
                        prebridges.append(pairs5p)
                        prebridges.append(pairs3p)
                        

            if num(splints) > 0 or num(bridges) > 0:
                if num(splints) > 0:
                    logging.debug("  Gap closed with a splint")
                    patches = splints
                elif num(bridges) > 0:
                    logging.debug("  Gap closed with a bridge")
                    patches = bridges
                else:
                    assert False, "Unexpected condition"

                patches.sort()
                score, anchor, patch = patches[0]
                if num(closures) > 0 and anchor.overlaps(closures[-1]):
                    logging.debug("  Gap already closed")
                    patches_file.write(format_3bed(gap, GenomicInterval(), GenomicInterval()))                
                    continue
                else:                
                    patches_file.write(format_3bed(gap, anchor, patch, strand=patch.strand.str))
                    closures.append(anchor)
            else:
                logging.debug("  Gap unclosed")
                patches_file.write(format_3bed(gap, GenomicInterval(), GenomicInterval(), strand='.'))


            for tile in sorted(prebridges + presplints):
                for pair in tile.intervals:
                    support_file.write(format_2bed(pair.ref, pair.qry, strand=pair.strand.str))

            num_gaps += 1

            if num_gaps % 100 == 0:
                logging.info("  Processed %d gaps, %d alignments ..." % (num_gaps, num_alignments))

        total_alignments += num_alignments
        total_gaps += num_gaps
        logging.info("  Processed %d gaps, %d alignments ..." % (num_gaps, num_alignments))
        logging.info("  Processed %d total gaps, %d total alignments" % (total_gaps, total_alignments))
        logging.info("Traversal of %s complete" % ref_name)
        logging.info("")

    logging.info("Finished traversal")


                
def usage(message=None, exitcode=1, stream=sys.stderr):
    #                 0        10        20        30        40        50        60        70        80
    #                 |--------|---------|---------|---------|---------|---------|---------|---------|
    message = '' if message is None else '\nERROR: %s\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage: %s [options] <patches.bam> <assembly.fa> <out.prefix>\n\n" % (
        __program__))
    stream.write("Options: -E <ufloat>   Max gap-flanking alignment error rate [%.1f]\n" % (
        DEFAULT_ERROR_RATE,))
    stream.write("         -F            Extend search radius to include all alignments on\n")
    stream.write("                       the gap-flanking contig if it is longer than {-r}\n")
    stream.write("         -G            Tile alignments globally within search radius\n")
    stream.write("         -I            Include indels in alignment error rate estimate\n")
    stream.write("         -L <uint>     Min flanking tiled alignment length [%d]\n" % (
        DEFAULT_TILED_LENGTH,))
    stream.write("         -l <uint>     Min flanking HSP alignment length [%d]\n" % (
        DEFAULT_ALIGN_LENGTH,))
    stream.write("         -Q <uint>     Min flanking tiled alignment mapQ [%d]\n" % (
        DEFAULT_MAPQUAL,))
    stream.write("         -q <uint>     Min flanking HSP alignment mapQ [%d]\n" % (
        DEFAULT_MAPQUAL,))
    stream.write("         -r <uint>     Analyze alignments overlapping a {-r}-bp radius\n")
    stream.write("                       within the boundaries of the upstream/downstream\n")
    stream.write("                       flanking contig (search disallowed across next\n")
    stream.write("                       upstream/downstream gap, see {-R}) [%d]\n" % (
        DEFAULT_RADIUS,))
    stream.write("         -R            Enforce radius {-r} strictly (permits search beyond\n")
    stream.write("                       multiple upstream/downstream gaps)\n");
    stream.write("         -x <file>     A bgzip'd and tabix'd BED of repeat regions\n")
    stream.write("         -V            Run in verbose debugging mode\n")
    stream.write("         -h,--help     Print help message and exit\n")
    stream.write("         -v,--version  Print version and exit\n\n")
    stream.write("Notes:\n")
    stream.write("  1. patch.bam contains aligned candidate patch sequences. It must be\n")
    stream.write("     sorted by reference position and indexed with `samtools sort` and\n")
    stream.write("     `samtools index`\n\n")
    stream.write("  2. assembly.fa contains the genome assembly sequences to be patched\n");
    stream.write("     and must be indexed with `samtools faidx`\n\n")
    stream.write("%s\n\n" % message)
    sys.exit(exitcode)



def version(exitcode=1, stream=sys.stderr):
    stream.write("%s %s\n" % (__program__, __version__))
    sys.exit(exitcode)


    
def main(argv):
    switches = 0
    logging_level = LOGGING_LEVEL
    alignment_radius = DEFAULT_RADIUS
    max_error_rate = DEFAULT_ERROR_RATE
    min_align_mapqual = DEFAULT_MAPQUAL
    min_tiled_mapqual = DEFAULT_MAPQUAL
    min_align_length = DEFAULT_ALIGN_LENGTH
    min_tiled_length = DEFAULT_TILED_LENGTH
    repeats_filename = None
    logging_format  = "[{0}] %(asctime)s [%(levelname)s] %(message)s".format(
        __program__)
    try:
        options, arguments = getopt.getopt(argv, 'E:FGhIVvL:l:Rr:Q:q:S:x:',('help', 'version'))
    except getopt.GetoptError as message:
        usage(message)

    for flag, value in options:
        if   flag in HELP_FLAGS: usage(exitcode=0)
        elif flag in VERS_FLAGS: version(exitcode=0)
        elif flag == '-V': logging_level = logging.DEBUG
        elif flag == '-E': max_error_rate = float(value)
        elif flag == '-S': max_error_rate = float(value)  # deprecated
        elif flag == '-r': alignment_radius  = int(value)
        elif flag == '-Q': min_tiled_mapqual = int(value)
        elif flag == '-q': min_align_mapqual = int(value)
        elif flag == '-L': min_tiled_length = int(value)
        elif flag == '-l': min_align_length = int(value)
        elif flag == '-x': repeats_filename = value
        elif flag == '-R': switches |= STRICT_RADIUS
        elif flag == '-F': switches |= EXTEND_FLANK
        elif flag == '-G': switches |= GLOBAL_TILING
        elif flag == '-I': switches |= INCLUDE_INDELS

    if num(arguments) != 3:
        usage('Unexpected number of arguments');

    logging.basicConfig(
        stream=sys.stderr,
        level=logging_level,
        format=logging_format
    )

    reference_file = pysam.FastaFile(arguments[1])
    alignment_file = pysam.AlignmentFile(arguments[0])
    patches_file = open(arguments[2]+'.patches.bed', 'w', 1)
    support_file = open(arguments[2]+'.support.bed', 'w', 1)
    repeats_file = None
    
    if not alignment_file.has_index():
        raise Exception("Cannot find or read SAM/BAM/CRAM file index") 

    if repeats_filename is not None:
        repeats_file = pysam.TabixFile(repeats_filename, parser=pysam.asBed())
    
    logging.info("Command: " + " ".join(sys.argv))

    find_patches(
        alignment_file,
        reference_file,
        patches_file,
        support_file,
        repeats_file,
        max_error_rate,
        min_align_mapqual,
        min_tiled_mapqual,
        min_align_length,
        min_tiled_length,
        alignment_radius,
        switches
    )

    alignment_file.close()
    patches_file.close()
    support_file.close()
    
    
    
if __name__ == '__main__':
    main(sys.argv[1:])
