
import os
import sys
import getopt
import collections

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Move debris sequences to the end of a JBAT assembly file'


class MalformedAssemblyFileError(BaseException):
    pass


def read_debris_list_file(list_file):
    list_set = set()
    with open(list_file) as lfile:
        for line in lfile:
            if line.isspace() or line.startswith('#'):
                continue

            line = line.rstrip().split("\t")

            list_set.add(line[0])

    return list_set


def usage(message=None, exitcode=1, stream=sys.stderr):
    message = "" if message is None else "ERROR: %s\n\n\n" % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [-gs] [--group] [--sorted] <in.assembly> <debris.list>\n\n\n%s" % (
        __program__, message))
    sys.exit(exitcode)    

    
def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'ghs', ('help','group','sorted'))
    except getopt.GetoptError as error:
        usage(error)

    write_sorted = False
    write_grouped = False
    for flag, value in options:
        if flag in ('-h','--help'): usage(exitcode=0)
        if flag in ('-s','--sorted'): write_sorted = True
        if flag in ('-g','--group'): write_grouped = True
        
    if len(arguments) != 2:
        usage("Unexpected number of arguments")

    assembly_file = open(arguments[0], 'r')
    list_file = read_debris_list_file(arguments[1])
    join_file = sys.stdout
    
    prev_name = None
    prev_start = 0
    seen_head = False
    seen_body = False
    debris_list = []
    contig_name = collections.OrderedDict()
    contig_length = collections.OrderedDict()
    for line in assembly_file:
        if line.isspace() or \
           line.startswith('#'):
            continue
        
        cols = line.strip().split()
        
        if cols[0].startswith('>'):
            join_file.write(line)
            
            curr_name, curr_index, curr_length = cols
            curr_name = curr_name.lstrip('>')
            curr_index = int(curr_index)
            curr_length = int(curr_length)

            orig_name = curr_name.split(':::')

            if orig_name[0] != prev_name:
                prev_start = 0
            
            contig_name[curr_index] = curr_name
            contig_length[curr_index] = curr_length
            
            prev_name = orig_name[0]
            prev_start += curr_length

            seen_head = True

        elif not seen_head:
            raise MalformedAssemblyFileError("No cprops-style assembly file-header detected")
            
        else:
            sep = ''
            scaffold_index = 0
            for index in cols:
                vector = int(index)
                index  = abs(vector)

                if contig_name[index] in list_file:
                    debris_list.append(vector)
                else:
                    join_file.write("%s%d" % (sep, vector))
                    scaffold_index += 1
                    sep = ' '

                seen_body = True

            if scaffold_index > 0:
                join_file.write("\n")

    if not seen_body:
        raise MalformedAssemblyFileError("No asm-style assembly file-body detected")
                
    if write_sorted:
        debris_list.sort(key=contig_length.get, reverse=True)

    if write_grouped:
        sep = ''
        for index in debris_list:
            join_file.write("%s%d" % (sep, index));
            sep = ' '
        join_file.write("\n")
    else:
        for index in debris_list:
            join_file.write("%d\n" % index)
            
    assembly_file.close()
    join_file.close()


if __name__ == '__main__':
    main(sys.argv[1:])

                

                
                
