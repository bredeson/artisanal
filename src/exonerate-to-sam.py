
import os
import re
import sys
import pysam
import getopt

from fileutil import zopen as open
from fastxutil import reverse_sequence
from collections import OrderedDict


__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Convert Exonerate alignment file to SAM format'


EXONERATE_MODELS = {
    'protein2dna'   : (3, 3, 1, 1), # M
    'protein2genome': (3, 3, 1, 1), # M
    'coding2genome' : (1, 1, 1, 1), # C
    'cdna2genome'   : (1, 1, 1, 1), # M & C
    'est2genome'    : (1, 1, 1, 1)  # M
}

DEBUG = False
VOPS_COMPONENT = re.compile('(\S+)\s+(\d+)\s+(\d+)')
VOPS_SUPPORTED = set('CMSGF5I3')
VOPS_QMOVE = set('CMSG')
VOPS_TMOVE = set('CMSGF5I3')
VOPS_BLOCK = set('CM5I3')
VOPS_MATCH = set('CM')
VOPS_INDEL = set('GF')
VOPS_SPLIT = set('S')

CIGAR_COMPONENT = re.compile('(\d+)([MIDNSHP=XB])')
CIGAR_MATCH  = pysam.CMATCH
CIGAR_INSERT = pysam.CINS
CIGAR_DELETE = pysam.CDEL
CIGAR_RSKIP  = pysam.CREF_SKIP
CIGAR_SCLIP  = pysam.CSOFT_CLIP
CIGAR_HCLIP  = pysam.CHARD_CLIP
CIGAR_RPAD   = pysam.CPAD
CIGAR_EQUAL  = pysam.CEQUAL
CIGAR_DIFF   = pysam.CDIFF
CIGAR_RBACK  = pysam.CBACK
CIGAR_QMOVE = {
    CIGAR_MATCH,
    CIGAR_INSERT,
    CIGAR_SCLIP,
    CIGAR_EQUAL,
    CIGAR_DIFF
}
CIGAR_TMOVE = {
    CIGAR_MATCH,
    CIGAR_DELETE,
    CIGAR_RSKIP,
    CIGAR_RPAD,
    CIGAR_EQUAL,
    CIGAR_DIFF,
    CIGAR_RBACK
}
CIGAR_QCLIP = {
    CIGAR_SCLIP,
    CIGAR_HCLIP
}
CIGAR_EQUIV = {
    CIGAR_MATCH,
    CIGAR_EQUAL,
    CIGAR_DIFF
}
CIGAR_INDEL = {
    CIGAR_INSERT,
    CIGAR_DELETE
}

CIGAR_MAP_INT = {
    CIGAR_MATCH  : 'M',
    CIGAR_INSERT : 'I',
    CIGAR_DELETE : 'D',
    CIGAR_RSKIP  : 'N',
    CIGAR_SCLIP  : 'S',
    CIGAR_HCLIP  : 'H',
    CIGAR_RPAD   : 'P',
    CIGAR_EQUAL  : '=',
    CIGAR_DIFF   : 'X',
    CIGAR_RBACK  : 'B'
}

CIGAR_MAP_STR = {
    'M' : CIGAR_MATCH,
    'I' : CIGAR_INSERT,
    'D' : CIGAR_DELETE,
    'N' : CIGAR_RSKIP,
    'S' : CIGAR_SCLIP,
    'H' : CIGAR_HCLIP,
    'P' : CIGAR_RPAD,
    '=' : CIGAR_EQUAL,
    'X' : CIGAR_DIFF,
    'B' : CIGAR_RBACK
}

VOPS_CIGAR_MAP_STR = {
    'C' : CIGAR_MATCH,
    'M' : CIGAR_MATCH,
    'S' : CIGAR_MATCH,
    'I' : CIGAR_RSKIP,
    '5' : CIGAR_RSKIP,
    '3' : CIGAR_RSKIP,
    'G' : -1,
    'F' : -2
}

VOPS_FSHIFT = 'F'

CODON_MAP = {
    1 : {
        'X':'NNN',
        'Z':'SAR',
        'B':'RAY',
        '*':'TRR',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATH',
        'K':'AAR',
        'L':'YTN',
        'M':'ATG',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'MGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGG',
        'Y':'TAY',
    },
    2 : {
        'X':'NNN',
        '*':'WRR',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATY',
        'K':'AAR',
        'L':'YTN',
        'M':'ATR',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'CGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGR',
        'Y':'TAY',
    },
    3 : {
        'X':'NNN',
        '*':'TAR',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATY',
        'K':'AAR',
        'L':'TTR',
        'M':'ATR',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'MGN',
        'S':'WSN',
        'T':'MYN',
        'V':'GTN',
        'W':'TGR',
        'Y':'TAY',
    },
    4 : {
        'X':'NNN',
        '*':'TAR',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATH',
        'K':'AAR',
        'L':'YTN',
        'M':'ATG',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'MGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGR',
        'Y':'TAY',
    },
    5 : {
        'X':'NNN',
        '*':'TAR',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATY',
        'K':'AAR',
        'L':'YTN',
        'M':'ATR',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'CGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGR',
        'Y':'TAY',
    },
    6 : {
        'X':'NNN',
        '*':'TGA',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATH',
        'K':'AAR',
        'L':'YTN',
        'M':'ATG',
        'N':'AAY',
        'P':'CCN',
        'Q':'YAR',
        'R':'MGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGG',
        'Y':'TAY',
    },
    9 : {
        'X':'NNN',
        '*':'TAR',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATH',
        'K':'AAG',
        'L':'YTN',
        'M':'ATG',
        'N':'AAH',
        'P':'CCN',
        'Q':'CAR',
        'R':'CGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGR',
        'Y':'TAY',
    },
    10 : {
        'X':'NNN',
        '*':'TAR',
        'A':'GCN',
        'C':'TGH',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATH',
        'K':'AAR',
        'L':'YTN',
        'M':'ATG',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'MGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGG',
        'Y':'TAY',
    },
    11 : {
        'X':'NNN',
        '*':'TRR',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATH',
        'K':'AAR',
        'L':'YTN',
        'M':'ATG',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'MGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGG',
        'Y':'TAY',
    },
    12 : {
        'X':'NNN',
        '*':'TRR',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATH',
        'K':'AAR',
        'L':'YTN',
        'M':'ATG',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'MGN',
        'S':'HBN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGG',
        'Y':'TAY',
    },
    13 : {
        'X':'NNN',
        '*':'TAR',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'RGN',
        'H':'CAY',
        'I':'ATY',
        'K':'AAR',
        'L':'YTN',
        'M':'ATR',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'CGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGR',
        'Y':'TAY',
    },
    14 : {
        'X':'NNN',
        '*':'TAG',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATH',
        'K':'AAG',
        'L':'YTN',
        'M':'ATG',
        'N':'AAH',
        'P':'CCN',
        'Q':'CAR',
        'R':'CGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGR',
        'Y':'TAH',
    },
    16 : {
        'X':'NNN',
        '*':'TRA',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATH',
        'K':'AAR',
        'L':'YWN',
        'M':'ATG',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'MGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGG',
        'Y':'TAY',
    },
    21 : {
        'X':'NNN',
        '*':'TAR',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATY',
        'K':'AAG',
        'L':'YTN',
        'M':'ATR',
        'N':'AAH',
        'P':'CCN',
        'Q':'CAR',
        'R':'CGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGR',
        'Y':'TAY',
    },
    22 : {
        'X':'NNN',
        '*':'TVA',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATH',
        'K':'AAR',
        'L':'YWN',
        'M':'ATG',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'MGN',
        'S':'WSB',
        'T':'ACN',
        'V':'GTN',
        'W':'TGG',
        'Y':'TAY',
    },
    23 : {
        'X':'NNN',
        '*':'TDR',
        'A':'GCN',
        'C':'TGY',
        'D':'GAY',
        'E':'GAR',
        'F':'TTY',
        'G':'GGN',
        'H':'CAY',
        'I':'ATH',
        'K':'AAR',
        'L':'YTN',
        'M':'ATG',
        'N':'AAY',
        'P':'CCN',
        'Q':'CAR',
        'R':'MGN',
        'S':'WSN',
        'T':'ACN',
        'V':'GTN',
        'W':'TGG',
        'Y':'TAY',
    }
}

TSS_CODONS = {
    1  : {'ATG','TTG','CTG'},
    2  : {'ATT','ATC','ATA','ATG','GTG'},
    3  : {'ATA','ATG','GTG'},
    4  : {'TTA','TTG','CTG','ATT','ATC','ATA','ATG','GTG'},
    5  : {'TTG','ATT','ATC','ATA','ATG','GTG'},
    6  : {'ATG',},
    9  : {'ATG','GTG'},
    11 : {'TTG','CTG','ATT','ATC','ATA','ATG','GTG'},
    12 : {'CTG','ATG'},
    13 : {'TTG','ATA','ATG','GTG'},
    23 : {'ATT','ATG','GTG'}
}
TSS_CODONS[10] = TSS_CODONS[6]
TSS_CODONS[14] = TSS_CODONS[6]
TSS_CODONS[16] = TSS_CODONS[6]
TSS_CODONS[21] = TSS_CODONS[9]
TSS_CODONS[22] = TSS_CODONS[6]


TTS_CODONS = {
    1  : {'TAA','TAG','TGA'},
    2  : {'TAA','TAG','AGA','AGG'},
    3  : {'TAA','TAG'},
    6  : {'TGA',},
    11 : {'TAA','TAG','TGA'},
    16 : {'TAA','TGA'},
    22 : {'TCA','TAA','TGA'},
    23 : {'TTA','TAA','TAG','TGA'}
}
TTS_CODONS[4] = TTS_CODONS[3]
TTS_CODONS[5] = TTS_CODONS[3]
TTS_CODONS[9] = TTS_CODONS[3]
TTS_CODONS[10] = TTS_CODONS[3]
TTS_CODONS[12] = TTS_CODONS[11]
TTS_CODONS[13] = TTS_CODONS[3]
TTS_CODONS[14] = TTS_CODONS[6]
TTS_CODONS[21] = TTS_CODONS[3]


# Fore a more complete list, see:
# https://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi


class FileFormatError(Exception):
    pass


class Strand(object):
    def __init__(self, obj):
        if isinstance(obj, Strand):
            self.__strand = obj.__strand
        elif isinstance(obj, int):
            self.__strand = obj // abs(obj) if obj else obj
        elif isinstance(obj, str):
            self.__strand = self._to_int(obj)
        else:
            raise TypeError("Invalid strand type: int, str, or Strand obj expected")

    def _to_int(self, strand):
        if   strand == '.':
            return 0
        elif strand == '-':
            return -1
        elif strand == '+':
            return +1
        else:
            raise ValueError("Invalid strand value: `+', `-', or `.' expected")
        
    def __str__(self):
        if self.__strand == 0:
            return '.'
        if self.__strand < 0:
            return '-'
        if self.__strand > 0:
            return '+'

    @property
    def int(self):
        return self.__strand

    @property
    def str(self):
        return str(self)
    
    def reverse(self):
        self.__strand *= -1



class Annotation(object):
    def __init__(self, name, strand, beg, length):
        self.name = name
        self.strand = Strand(strand)
        self.beg = int(beg) - 1
        self.length = int(length)
        self.end = self.beg + self.length

    def __str__(self):
        return "%s %s %d %d" % (
            self.name,
            self.strand.str,
            self.beg,
            self.length
        )

class AlignedSegment(object):
    def __init__(self, name, length, beg, end, strand, seq=None, qual=None):
        self.name   = str(name)
        self.length = int(length)
        self.beg    = int(beg)
        self.end    = int(end)
        self.strand = strand
        self.seq    = seq
        self.qual   = qual

    def __str__(self):
        return "%s:%d-%d[%s]" % (
            self.name,
            self.beg,
            self.end,
            self.strand.str
        )
    
    @property
    def aligned_length(self):
        return self.end - self.beg
    
    @property
    def aligned_fraction(self):
        return float(self.aligned_length) / float(self.length)

    @property
    def strand(self):
        return self._strand
    
    @strand.setter
    def strand(self, strand):
        self._strand = Strand(strand)

    

class AlignedSegmentPair(object):
    def __init__(self, qry, trg, cigar=None, matches=0, length=-1, mapq=255, score=0.0):
        if isinstance(qry, AlignedSegment):
            self.qry = qry
        else:
            raise TypeError("Expected AlignedSegment object as query")
        if isinstance(trg, AlignedSegment):
            self.trg = trg
        else:
            raise TypeError("Expected AlignedSegment object as target")            
        self.matches = matches
        self.length = length
        self.cigar = cigar
        self.mapq  = mapq
        self.score = score

    def __str__(self):
        if self.cigar is None:
            cigar = '*'
        else:
            cigar = ''.join(('%d%s' % (l,CIGAR_MAP_INT[o]) for l,o in self.cigar))
        return "%s:%d-%d %s:%d-%d %s" % (
            self.qry.name,
            self.qry.beg,
            self.qry.end,
            self.trg.name,
            self.trg.beg,
            self.trg.end,
            cigar
        )

    __repr__ = __str__
    
    @property
    def strand(self):
        return Strand(self.qry.strand.int * self.trg.strand.int)

    @property
    def similarity(self):
        if self.length > 0:
            return float(self.matches) / self.length
        else:
            return float(self.matches) / max(self.qry.aligned_length, self.trg.aligned_length)
        

        
def is_valid_rg_string(rg_string):
    return rg_string.startswith('@RG') and get_rg_id(rg_string) is not None


def get_rg_id(rg_string):
    match = re.search('ID:([!-~][ -~]*)', rg_string)
    return match.group(1) if match else None
        
        
def format_sam_record(pair, cq=None, ct=None, fs=None, ix=None,  ts=0, rg=None):
    record = '%s\t%d\t%s\t%d\t%d\t%s\t*\t0\t0\t%s\t%s\tAS:i:%d' % (
        pair.qry.name,
        16 if pair.strand.int < 0 else 0,
        pair.trg.name,
        pair.trg.beg+1,
        pair.mapq,
        ''.join(('%d%s' % (l,CIGAR_MAP_INT[o]) for l,o in pair.cigar)),
        pair.qry.seq,
        pair.qry.qual,
        pair.score
    )
    if rg is not None:
        record += '\tRG:Z:%s' % rg
    if fs:
        record += '\tfs:Z:%s' % (';'.join(('%s:%d-%d' % (pair.trg.name,beg+1,end) for beg,end in fs)))
    if ix:
        record += '\tix:Z:%s' % (';'.join(('%s:%d-%d' % (pair.trg.name,beg+1,end) for beg,end in ix)))
    if cq:
        record += '\tcq:Z:%s:%d-%d' % (pair.qry.name, cq[0]+1, cq[1])
    if ct:
        record += '\tct:Z:%s:%d-%d' % (pair.trg.name, ct[0]+1, ct[1])
    record += '\tts:i:%d' % ts
    record += '\n'

    return record


def format_sam_header(fasta=None, read_group=None):
    header = []
    header.append("@HD\tVN:1.0\tSO:unknown")
    if fasta:
        for name, length in zip(fasta.references, fasta.lengths):
            header.append("@SQ\tSN:%s\tLN:%d" % (name, length))

    if read_group:
        read_group = read_group.replace('\\\\','\\').replace('\\t','\t').replace('\\n','\n')
        read_group = read_group.rstrip('\n')
        if not is_valid_rg_string(read_group):
            usage("Invalid @RG line %r" % read_group)
        header.append(read_group)

    header.append("@PG\tID:%s\tPN:%s\tVN:%s\tCL:%s" % (
        __program__, __program__, __pkgname__ + '-' + __version__, ' '.join(sys.argv)))

    return header


def reverse_translate(protein, table=1):
    codons = []
    for aa in protein:
        codons.append(CODON_MAP[table][aa])
    return ''.join(codons)

    
def find_stop_codons(pair, offset, length, split=None, table=1):
    vec = pair.trg.strand.int

    
    stops = []
    if split:
        # a split represents a single codon split across a splice
        # boundary, so we must handle it as a special case
        exon0 = split[0]
        exon1 = split[1]
        if DEBUG:
            sys.stderr.write("%s..%s\n" % (str(split[0]),str(split[1])))
        codon = pair.trg.seq[exon0[0]:exon0[0]+exon0[1]] + \
                pair.trg.seq[exon1[0]:exon1[0]+exon1[1]]
        if codon in TTS_CODONS[table]:
            for exon in split:
                tbeg = pair.trg.beg + vec * (exon[0] + 0)
                tend = pair.trg.beg + vec * (exon[0] + exon[1])
                stops.append((tbeg, tend))
    else:
        assert ((length % 3) == 0), 'Coding alignment block not a multiple of 3'
        for cbeg in range(0, length, 3):
            cend = cbeg + 3
            codon = pair.trg.seq[offset+cbeg:offset+cend]
            if codon in TTS_CODONS[table]:
                tbeg = pair.trg.beg + vec * (offset + cbeg)
                tend = pair.trg.beg + vec * (offset + cend)
                stops.append((tbeg, tend))
    return stops
        

def parse_cigar_to_tuples(cigar_string, pair):
    cigar = []
    qpos = pair.qry.beg
    qlen = pair.qry.length
    if qpos > 0:
        cigar.append([qpos, 4])
        
    for match in CIGAR_COMPONENT.finditer(cigar_string):
        length = int(match.group(1))
        label  = str(match.group(2))
        label  = CIGAR_MAP_STR[label]
        if label in CIGAR_QMOVE:
            qpos += length
        cigar.append((length, label))

    if qpos > qlen:
        raise Exception(
            "%s CIGAR string longer than query: %d > %d" % (
            str(pair.qry), qpos, qlen)
        )
    if qpos < qlen:
        cigar.append((qlen - qpos, 4))
    
    return cigar

            
def parse_vulgar_to_tuples(vstring, pair, cigar0M=False, qscale=1, qshift=1, tscale=1, tshift=1, table=1, annotation=None):
    # see : https://biopython.org/DIST/docs/api/Bio.SearchIO.ExonerateIO.exonerate_vulgar-pysrc.html
    # compute all coordinates in DNA space:
    qlength = pair.qry.length * qscale  # full sequence
    tlength = pair.trg.length * tscale  # aligned extent
    qstrand = pair.qry.strand.int
    tstrand = pair.trg.strand.int
    qstart  = pair.qry.beg * qscale
    tstart  = pair.trg.beg * tscale
    qblock  = (qlength - qstart) if qstrand < 0 else qstart
    tblock  = 0
    qoffset = 0
    toffset = 0
    tannot  = 0
    qtss = None
    ttss = None
    qtts = None
    ttts = None
    qtr = None
    ttr = None

    vops_coding = 'M' if qscale == 3 else 'C'

    # input sequences are (possibly degenerate) DNA
    has_both_sequences = bool(pair.qry.seq) and bool(pair.trg.seq)

    index = -1
    cigar = []
    clength = 0
    prev_cigop = None
    prev_split = None
    curr_split = None
    frameshifts = []
    stopcodons = []
    if qblock > 0:
        cigar.append([qblock, CIGAR_SCLIP])
        qoffset += qblock
        prev_cigop = CIGAR_SCLIP
        index += 1
        
    if DEBUG:
        sys.stderr.write(">%s %s %d(x%d) %d(x%d)\n" % (
            pair.qry.name,
            pair.trg.name,
            pair.qry.length, qshift,
            pair.trg.length, tshift
        ))
        sys.stderr.write("  %d %d\n" % (
            qstart + qstrand * qoffset,
            tstart + tstrand * toffset
        ))

    for match in VOPS_COMPONENT.finditer(vstring): 
        vlabel =     match.group(1)
        qblock = int(match.group(2)) * qshift
        tblock = int(match.group(3)) * tshift
        
        assert vlabel in VOPS_SUPPORTED, \
            "Unsupported vulgar label: '%s'" % vlabel

        curr_cigop = VOPS_CIGAR_MAP_STR[vlabel]
        
        if curr_cigop != prev_cigop:
            cigar.append([0, curr_cigop])
            index += 1
            
        if vlabel in VOPS_BLOCK:
            if vlabel == vops_coding:  # M|C
                if qtss is None:
                    qtss = qoffset
                qtts = qoffset + qblock
                if ttss is None:
                    ttss = toffset
                ttts = toffset + tblock
                
                if has_both_sequences:
                    # protein alignments use M,
                    # translated DNAs use C
                    if prev_split:
                        stopcodons.extend(
                            find_stop_codons(
                                pair,
                                toffset,
                                tblock,
                                split=(prev_split, curr_split),
                                table=table
                            )
                        )
                    stopcodons.extend(
                        find_stop_codons(
                            pair,
                            toffset,
                            tblock,
                            table=table
                        )
                    )
                curr_split = None
                prev_split = None
            else:  # 5|I|3
                prev_split = curr_split
                    
            if vlabel in VOPS_QMOVE:
                qoffset += qblock
            if vlabel in VOPS_TMOVE:
                toffset += tblock
            cigar[index][0] += tblock

        elif vlabel in VOPS_SPLIT:
            # if the query is a protein, track the
            # query bases by target and vice versa
            if qscale == tscale:
                curr_split = (toffset, tblock)
                qoffset += qblock
                toffset += tblock
                cigar[index][0] += qblock
            elif qscale == 3:
                curr_split = (toffset, tblock)
                qoffset += tblock  # <= !!! not a typo !!!
                toffset += tblock
                cigar[index][0] += tblock
            elif tscale == 3:
                curr_split = (toffset, qblock)
                qoffset += qblock
                toffset += qblock  # <= !!! not a typo !!!
                cigar[index][0] += qblock
            else:
                raise NotImplementedError("Unhandled frameshift case: [%s]" % pair)

        elif vlabel in VOPS_INDEL:
            if cigar0M and prev_cigop != CIGAR_MATCH:
                cigar[index][1] = prev_cigop = CIGAR_MATCH
                cigar.append([0, curr_cigop])
                index += 1
                
            if qblock > tblock:
                cigar[index][1] = curr_cigop = CIGAR_INSERT
                cigar[index][0] += qblock
            else:
                cigar[index][1] = curr_cigop = CIGAR_DELETE
                cigar[index][0] += tblock

            if vlabel == VOPS_FSHIFT:
                frameshifts.append((
                    tstart + tstrand * (toffset + 0),
                    tstart + tstrand * (toffset + tblock)
                ))
                
            if vlabel in VOPS_QMOVE:
                qoffset += qblock
            if vlabel in VOPS_TMOVE:
                toffset += tblock
        else:
            raise AssertionError("Unhandled vulgar label: '%s'" % vlabel)

        if DEBUG:
            sys.stderr.write("  %d %d\n" % (
                qstart + qstrand * qoffset,
                tstart + tstrand * toffset
            ))
        
        prev_cigop = curr_cigop

    if qoffset > qlength:
        raise Exception("%s vulgar string longer than query: %d > %d" % (
            str(pair.qry), qoffset, qlength))
    if qoffset < qlength:
        cigar.append((qlength - qoffset, CIGAR_SCLIP))

    #TODO: start & stop codons split by introns not handled properly        
    if qtss is not None and ttss is not None:
        qtr = (qstart + qstrand * qtss, qstart + qstrand * qtts)
        ttr = (tstart + tstrand * ttss, tstart + tstrand * ttts)
        if has_both_sequences:
            if pair.qry.seq[qtss:qtss+3] in TSS_CODONS[table]:
                if annotation:
                    if qtss == annotation.beg:
                        tannot |= 0x1
                elif pair.trg.seq[ttss:ttss+3] in TSS_CODONS[table]:
                    tannot |= 0x1
            if pair.qry.seq[qtts-3:qtts] in TTS_CODONS[table]:
                if annotation:
                    if qtts == annotation.end:
                        stopcodons.pop()
                        tannot |= 0x2
                elif pair.trg.seq[ttts-3:ttts] in TTS_CODONS[table]:
                    stopcodons.pop()
                    tannot |= 0x2
                
    return list(map(tuple, cigar)), qtr, ttr, stopcodons, frameshifts, tannot
        


def read_annotation_file(filename):
    annotations = OrderedDict()
    annotation_file = open(filename, 'r')
    for line in annotation_file:
        line = line.strip()
        if len(line) < 1 or line.startswith('#'):
            continue
        fields = line.split()
        annotation = Annotation(fields[0], fields[1], fields[2], fields[3])
        annotations[annotation.name] = annotation
    annotation_file.close()
    return annotations
            
    

def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else '\nERROR: %s\n\n' % message
    #------------|----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    #            0        10        20        30        40        50        60        70        80
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage: %s [options] <exonerate.ryo>\n" % __program__)
    stream.write("\n")
    stream.write("Options:\n")
    stream.write("    -0,--cigar-0M\n")
    stream.write("        Insert 0M ops in the CIGAR string between adjacent insertions\n")
    stream.write("        and deletions (caveat: not compatible with all downstream tools)\n")
    stream.write("\n")
    stream.write("    -A,--annotation <file>\n")
    stream.write("        Exonerate-style annotation file (see `man exonerate`) with 1-based\n")
    stream.write("        start positions; for models aligning translated queries.\n")
    stream.write("\n")
    stream.write("    -b,--base-quality <uint>\n")
    stream.write("        Set the default base quality score (range 0-93; -1 to disable) [-1]\n")
    stream.write("\n")
    stream.write("    -g,--geneticcode <uint>\n")
    stream.write("        Specify genetic code (range 1-23). The default is Standard [1]\n")
    stream.write("        See: http://www.ncbi.nlm.nih.gov/Taxonomy/Utils/wprintgc.cgi\n")
    stream.write("\n")
    stream.write("    -m,--model <str>\n")
    stream.write("        Exonerate model used: %s\n" % ', '.join(sorted(EXONERATE_MODELS)))
    stream.write("\n")
    stream.write("    -P,--protein-query-fasta <file>\n")
    stream.write("        Input a query fasta file of peptide sequences; compatible with only\n")
    stream.write("        protein2* models\n")
    stream.write("\n")
    stream.write("    -Q,--query-fasta <file>\n")
    stream.write("        Input a query fasta file of DNA sequences; with protein2* models,\n")
    stream.write("        CDS sequences by the same names as their proteins must be provided\n")
    stream.write("\n")
    stream.write("    -R,--rg <str>\n")
    stream.write("        Define a SAM header read group line, e.g. '@RG\\tID:foo\\tSM:bar'\n")
    stream.write("\n")
    stream.write("    -T,--target-fasta <file>\n")
    stream.write("        Input the target/reference fasta file used as exonerate input;\n")
    stream.write("        SAM header sequence (@SQ) lines are written to output\n")
    stream.write("\n")
    stream.write("    -h,--help\n")
    stream.write("        Print this help message and exit\n")
    stream.write("\n")
    stream.write("Notes:\n")
    stream.write("  - Assumes input exonerate.ryo report was generated with AT LEAST the\n")
    stream.write("    following options:\n")
    stream.write("      --verbose 0 --showalignment FALSE --showvulgar FALSE --ryo <ryo-fmt>\n")
    stream.write("    Where <ryo-fmt> MUST be:\n")
    stream.write("    '%qi\\t%ql\\t%qab\\t%qae\\t%tS\\t%ti\\t%tl\\t%tab\\t%tae\\t%es\\t%et\\t255\\tAS:i:%s\\tvg:Z:%V\\n'\n")
    stream.write("\n%s\n" % message)
    sys.exit(exitcode)
    #------------|----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
    #            0        10        20        30        40        50        60        70        80


    
def main(argv):
    long_flags = (
        'help',
        'rg=',
        'model=',
        'cigar-0M',
        'genetic-code=',
        'query-fasta=',
        'target-fasta=',
        'protein-query',
        'protein-query-fasta=',
        'base-quality=',
        'annotation=',
        'output='
    )
    try:
        options, arguments = getopt.getopt(argv, 'h0pA:b:g:m:o:P:Q:R:T:', long_flags)
    except getopt.GetoptError as message:
        usage(message)

    output = '-'
    cigar0M = False
    read_group = None
    query_scale = 1
    query_shift = 1
    query_fasta = None
    query_trans = False
    target_scale = 1
    target_shift = 1
    target_fasta = None
    target_index = None
    basequal_char = None
    basequal_value = -1
    genetic_code = 1
    annotations = dict()
    model = 'protein2genome'
    for flag, value in options:
        if   flag in {'-h','--help'}:
            usage(exitcode=0)
        elif flag in {'-0','--cigar-0M'}:
            cigar0M = True
        elif flag in {'-A','--annotation'}:
            annotations = read_annotation_file(value)
        elif flag in {'-g','--genetic-code'}:
            genetic_code = int(value)
        elif flag in {'-m','--model'}:
            model = value
        elif flag in {'-P','--protein-query-fasta'}:
            query_trans = True
            query_fasta = value
        elif flag in {'-p','--protein-query'}:
            query_scale = 3
            query_shift = 3
        elif flag in {'-R','--rg'}:
            read_group = value
        elif flag in {'-Q','--query-fasta'}:
            query_fasta = value
        elif flag in {'-T','--target-fasta'}:
            target_fasta = value
        elif flag in {'-b','--base-quality'}:
            basequal_value = int(value)
        elif flag in {'-o','--output'}:
            output = value

    if len(arguments) != 1:
        usage("Unexpected number of arguments")

    paf = open(arguments[0], 'r')
    out = open(output, 'w')

    if model in EXONERATE_MODELS:
        query_scale, query_shift, target_scale, target_shift = EXONERATE_MODELS[model]
    else:
        usage("Unsupported exonerate model '%s'. Valid models are: %s" % (
            exonerate_model, ', '.join(sorted(EXONERATE_MODELS))
        ))
    if query_trans and not model.startswith('protein2'):
        usage("Protein query sequences incompatible with non-protein2* models")

    if basequal_value < 0:
        pass
    elif basequal_value > 93:
        usage("--base-quality out of range: %d > 93\n" % basequal_value)
    else:
        basequal_char = chr(basequal_value+33)
        
    if query_fasta:
        query_fasta = pysam.FastaFile(query_fasta)

    if target_fasta:
        target_fasta = pysam.FastaFile(target_fasta)
        target_index = {(k,v) for v,k in enumerate(target_fasta.references)}

    if target_fasta or read_group:
        sys.argv[0] = __program__
        out.write("\n".join(format_sam_header(target_fasta, read_group)))
        out.write("\n")
                
    if read_group:
        read_group = get_rg_id(read_group)


    prev = None
    num_lines = 0
    pair_trg_seq = None
    prev_trg_seen = set()
    for line in paf:
        num_lines += 1
        if line.isspace() or \
           line.startswith('#'):
            continue

        # Parse standard PAF fields:
        # +----+--------+---------------------------------------------------------+
        # |Col |  Type  |                       Description                       |
        # +----+--------+---------------------------------------------------------+
        # |  1 | string | Query sequence name                                     |
        # |  2 |  int   | Query sequence length                                   |
        # |  3 |  int   | Query start coordinate (0-based)                        |
        # |  4 |  int   | Query end coordinate (0-based)                          |
        # |  5 |  char  | `+' if query/target on the same strand; `-' if opposite |
        # |  6 | string | Target sequence name                                    |
        # |  7 |  int   | Target sequence length                                  |
        # |  8 |  int   | Target start coordinate on the original strand          |
        # |  9 |  int   | Target end coordinate on the original strand            |
        # | 10 |  int   | Number of matching bases in the mapping                 |
        # | 11 |  int   | Number bases, including gaps, in the mapping            |
        # | 12 |  int   | Mapping quality (0-255 with 255 for missing)            |
        # +----+--------+---------------------------------------------------------+
        field = line.rstrip().split('\t')

        pair = AlignedSegmentPair(
            AlignedSegment(
                str(field[0]),
                int(field[1]),
                int(field[2]),
                int(field[3]),
                '+',
            ),
            AlignedSegment(
                str(field[5]),
                int(field[6]),
                int(field[7]),
                int(field[8]),
                str(field[4]),
            ),
            matches=int(field[9]),
            length=int(field[10]),
            mapq=int(field[11])
        )
        if pair.qry.beg > pair.qry.end:
            pair.qry.strand = '-'
        if pair.trg.beg > pair.trg.end:
            pair.trg.strand = '-'
        
        if target_fasta:
            if not prev or \
               target_index[prev.trg.name] != target_index[pair.trg.name]:
                if target_index[pair.trg.name] in prev_trg_seen:
                    raise FileFormatError("PAF file must be reference-ordered, line %d" % num_lines)    
                pair_trg_seq = target_fasta.fetch(pair.trg.name).upper()
                prev_trg_seen.add(target_index[pair.trg.name])

            if pair.trg.strand.int < 0:
                pair.trg.seq = pair_trg_seq[pair.trg.end:pair.trg.beg]
                pair.trg.seq = reverse_sequence(pair.trg.seq)
            else:
                pair.trg.seq = pair_trg_seq[pair.trg.beg:pair.trg.end]

        if query_fasta:
            pair.qry.seq = query_fasta.fetch(pair.qry.name).upper()
            if query_trans:
                pair.qry.seq = reverse_translate(pair.qry.seq, genetic_code)
            if pair.qry.strand.int < 0:
                pair.qry.seq = reverse_sequence(pair.qry.seq)
            assert (len(pair.qry.seq) == (query_scale * pair.qry.length)), \
                'Observed and expected query lengths are different, line %d: [%s]' % (num_lines, pair)
        else:
            pair.qry.seq = 'N' * (query_scale * pair.qry.length)
        if basequal_value < 0:
            pair.qry.qual = '*'
        else:
            pair.qry.qual = basequal_char * len(pair.qry.seq)

                
        # Now parse PAF tags:
        # +----+------+-------------------------------------------------------+
        # |Tag | Type |                      Description                      |
        # +----+------+-------------------------------------------------------+
        # | tp |  A   | Type of aln: P/primary, S/secondary and I,i/inversion |
        # | cm |  i   | Number of minimizers on the chain                     |
        # | s1 |  i   | Chaining score                                        |
        # | s2 |  i   | Chaining score of the best secondary chain            |
        # | NM |  i   | Total number of mismatches and gaps in the alignment  |
        # | MD |  Z   | To generate the ref sequence in the alignment         |
        # | AS |  i   | DP alignment score                                    |
        # | SA |  Z   | List of other supplementary alignments                |
        # | ms |  i   | DP score of the max scoring segment in the alignment  |
        # | nn |  i   | Number of ambiguous bases in the alignment            |
        # | ts |  A   | Transcript strand (splice mode only)                  |
        # | cg |  Z   | CIGAR string (only in PAF)                            |
        # | cs |  Z   | Difference string                                     |
        # | dv |  f   | Approximate per-base sequence divergence              |
        # | de |  f   | Gap-compressed per-base sequence divergence           |
        # | rl |  i   | Length of query regions harboring repetitive seeds    |
        # +----+------+-------------------------------------------------------+
        tsstts = 0
        isvulgar = False
        annotation = None
        internalstops = []    # internal STOP codons
        tcodingregion = None  # terminal start/stop codons
        qcodingregion = None
        frameshifts = []
        if pair.qry.name in annotations:
            annotation = annotations[pair.qry.name]
        for i in range(12, len(field)):
            if field[i].startswith('AS:i:'):
                pair.score = int(field[i][5:])
            elif field[i].startswith('cg:Z:'):
                pair.cigar = parse_cigar_to_tuples(field[i][5:], pair)
                break
            elif field[i].startswith('vg:Z:'):
                pair.cigar, qcodingregion, tcodingregion, internalstops, frameshifts, tsstts \
                    = parse_vulgar_to_tuples(
                        field[i][5:], pair,
                        cigar0M=cigar0M,
                        qscale=query_scale,
                        qshift=query_shift,
                        tscale=target_scale,
                        tshift=target_shift,
                        table=genetic_code,
                        annotation=annotation
                )
                isvulgar = True
                break

        # perform checks on the outputs
        if pair.cigar is None:
            raise FileFormatError("No CIGAR (cg:Z:) or VULGAR (vg:Z:) tag detected, line %d" % num_lines)

        query_length = 0
        for l, o in pair.cigar:
            if o in CIGAR_QMOVE:
                query_length += l

        assert (query_length == len(pair.qry.seq)), \
            'Query CIGAR and sequence are different lengths, line %d: [%s]' % (num_lines, pair)

        if isvulgar and pair.trg.strand.int < 0:
            # exonerate ryo reports coordinates on each sequence's original strand, but
            # negatively-stranded coordinates are swapped. To convert to SAM, we must
            # rev-comp queries on negatively-stranded targets. On such targets, query
            # orientations may be:
            # --model protein2dna: qry={+}
            # --model protein2genome: qry={+}
            # --model est2genome: qry={+}
            # --model cdna2genome: qry={+,-}
            # --model coding2genome: qry={+,-}
            # --model coding2coding: qry={+,-}
            pair.qry.beg, pair.qry.end = pair.qry.length - pair.qry.end, pair.qry.length - pair.qry.beg
            pair.trg.beg, pair.trg.end = pair.trg.end, pair.trg.beg
            pair.qry.seq  = reverse_sequence(pair.qry.seq)
            frameshifts   = [(b,e) for e,b in reversed(frameshifts)]
            internalstops = [(b,e) for e,b in reversed(internalstops)]
            if tcodingregion is not None:
                tcodingregion = tuple(reversed(tcodingregion))
            if qcodingregion is not None:
                qcodingregion = tuple(map(lambda p: query_length-p, reversed(qcodingregion)))
            
            pair.cigar.reverse()
        
        out.write(
            format_sam_record(
                pair,
                cq=qcodingregion,
                ct=tcodingregion,
                fs=frameshifts,
                ix=internalstops,
                ts=tsstts,
                rg=read_group
            )
        )
        prev = pair
        
main(sys.argv[1:])

