
import os
import io
import sys
import codecs

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_NAME__-__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Utilities for handling files/filenames'


XZ_SUFFIX = ('.xz', '.lzma')
GZ_SUFFIX = ('.z', '-z', '_z', '.gz', '-gz')
BZ_SUFFIX = ('.bz', '.bz2', '.bzip2')


def _codec(mode):
    if 'b' in mode:
        if 'w' in mode:
            return codecs.getwriter('utf-8')
        else:
            return codecs.getreader('utf-8')
    else:
        return lambda s: s


def stripsuffix(filename, suffixes=()):
    tempname = filename.lower()
    for suffix in sorted(suffixes, key=len, reverse=True):
        if tempname.endswith(suffix):
            filename = filename[:-len(suffix)]
    return filename

    
def zstrip(filename):
    tempname = filename.lower()
    if tempname.endswith(GZ_SUFFIX):
        return stripsuffix(filename, GZ_SUFFIX)
    elif tempname.endswith(BZ_SUFFIX):
        return stripsuffix(filename, BZ_SUFFIX)
    elif tempname.endswith(XZ_SUFFIX):
        return stripsuffix(filename, XZ_SUFFIX)
    else:
        return filename
    
    
def zopen(filename, mode='r'):
    if filename == '-':
        codec = _codec(mode)
        if 'w' in mode:
            return codec(sys.stdout)
        else:
            return codec(sys.stdin)
    
    lcfilename = filename.lower()
    codec = None
    handler = None
    if lcfilename.endswith(GZ_SUFFIX):
        import gzip as handler
        codec = _codec(mode+'b')
    
    elif lcfilename.endswith(BZ_SUFFIX):
        # NOTE: Py2 std lib bz2 does not support multistream
        import bz2file as handler
        codec = _codec(mode+'b')
    
    elif lcfilename.endswith(XZ_SUFFIX):
        # NOTE: not in Py2 std lib
        import lzma as handler
        codec = _codec(mode+'b')
    
    else:
        import io as handler
        codec = _codec(mode)

    return codec(handler.open(filename, mode))

