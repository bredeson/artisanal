
PROGRAM=$(basename $0 .sh);
PACKAGE='__PACKAGE_NAME__';
VERSION='__PACKAGE_VERSION__';
CONTACT='__PACKAGE_CONTACT__';
PURPOSE='Create liftover chain files between genomes';


function usage() {
#---+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
#        10        20        30        40        50        60        70        80
    echo "
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage: $PROGRAM [options] <source.fasta> <target.fasta> <output-prefix>

Options: 
    -b,--bridge <uint>
        Bridge tiled chains spanning gaps less than <uint> bp
         
     -B,--no-break
        Dont break target.fasta into contigs (SLOW)

     -K,--break-source
        Break source.fasta into contigs to perform search
     
     -S,--synteny-bedpe <file>
        Restrict to syntenic source:target pairs with input BEDPE <file>
        (currently supports subsetting by chr names only)
      
     -t,--threads <uint>
        Perform alignments using <uint> threads [1]

     -h,--help
        Print this help message

     -v,--version
        Print version info and exit

Notes:
  1. Source is the assembly to lift from; target, the assembly to lift to.

  2. UCSC Kent Tools, SAMtools, BEDtools, and Minimap2 must be accessible
     via the PATH env variable. Minimap2 v2.24+ highly recommended over
     previous versions.

  3. Input FASTA files may be compressed with bgzip, gzip, bzip2, or xz.
     Compression type determined by file suffix (i.e., .gz, .bz2, .xz).


" >>/dev/fd/2;
    exit 1
}

function version() {
    printf "$PROGRAM $VERSION\n" >>/dev/fd/2;
    exit 0;
}

threads=1;
bridge=false;
breaktrg=true;
breaksrc=false;
synteny_bedpe=;
COMMANDLINE="$PROGRAM $@";
while [[ -n $@ ]]; do
    case "$1" in
	-B|--no-break) breaktrg=false;;
	-b|--bridge) shift; bridge=$1;;
	-S|--synteny-bedpe) shift; synteny_bedpe=$1;;
	-K|--break-source|--break-old) breaksrc=true;;
	-t|--threads) shift; threads=$1;;
	-h|--help) usage;;
	-v|--version) version;;
	-*) usage;;
	*) break;;
    esac;
    shift;
done

if [ $# -ne 3 ]; then
    usage;
fi

printf "[$PROGRAM] Command: $COMMANDLINE\n" >>/dev/fd/2;
set -e 

export PATH="$(dirname $0):$PATH";

missing_dependency=;
for exe in liftover-filter-chain \
	   minimap2 samtools bedtools faToTwoBit bamToPsl \
	   twoBitInfo liftUp pslToChain chainSwap chainSort; do
    if [[ ! $(which $exe 2>/dev/null) ]]; then
        echo "[$PROGRAM] ERROR: $exe not detected in PATH" >>/dev/fd/2;
        missing_dependency=1;
    fi
done
if [[ $missing_dependency ]]; then
    exit 1;
fi


src_fasta=$1;
trg_fasta=$2;
out_prefix=$3;

# get full path to src genome fasta
src_fasta_compression=none;
src_fasta_dir=$(dirname $src_fasta);
src_fasta_dir=$(cd $src_fasta_dir; pwd);
src_fasta=$src_fasta_dir/$(basename $src_fasta);
src_prefix=$src_fasta_dir/$(basename $src_fasta);
if [[ $(echo $src_fasta | grep '.bz2$') ]]; then
    src_fasta_compression='bz2';
    
elif [[ $(echo $src_fasta | grep '.bgz$') ]]; then
    src_fasta_compression='gz';

elif [[ $(echo $src_fasta | grep '.gz$') ]]; then
    src_fasta_compression='gz';

elif [[ $(echo $src_fasta | grep '.xz$') ]]; then
    src_fasta_compression='xz';
fi
src_prefix=$(echo $src_fasta | sed "s/.f\(asta\|nt\|na\|a\)\(.$src_fasta_compression\)\?$//");


# get full path to trg genome fasta
trg_fasta_compression=none;
trg_fasta_dir=$(dirname $trg_fasta);
trg_fasta_dir=$(cd $trg_fasta_dir; pwd);
trg_fasta=$trg_fasta_dir/$(basename $trg_fasta);
trg_prefix=$(echo $trg_fasta | sed 's/.f\(asta\|nt\|na\|a\)\(.gz\)\?$//');
if [[ $(echo $trg_fasta | grep '.bz2$') ]]; then
    trg_fasta_compression='bz2';
    
elif [[ $(echo $trg_fasta | grep '.gz$') ]]; then
    trg_fasta_compression='gz';

elif [[ $(echo $trg_fasta | grep '.bgz$') ]]; then
    trg_fasta_compression='gz';

elif [[ $(echo $trg_fasta | grep '.xz$') ]]; then
    trg_fasta_compression='xz';
fi
trg_prefix=$(echo $trg_fasta | sed "s/.f\(asta\|nt\|na\|a\)\(.$trg_fasta_compression\)\?$//");


# if the output prefix directory doesn't exist, create it
tmp_prefix_dir=$out_prefix.align;
if [[ ! -d "$tmp_prefix_dir" ]]; then
    mkdir -p $tmp_prefix_dir;
fi
tmp_prefix_dir=$(cd $tmp_prefix_dir; pwd);
tmp_prefix=$tmp_prefix_dir/$(basename $out_prefix);

out_prefix_dir=$(dirname $out_prefix);
out_prefix_dir=$(cd $out_prefix_dir; pwd);
out_prefix=$out_prefix_dir/$(basename $out_prefix);


# link the src genome fasta into prefix dir and index
if [[ ! -e "$tmp_prefix.src.fasta" ]]; then
    if [[ $src_fasta_compression == none ]]; then
	ln -sf $src_fasta $tmp_prefix.src.fasta;

    elif [[ $src_fasta_compression == 'bz2' ]]; then
	bzip2 -dc $src_fasta >$tmp_prefix.src.fasta;

    elif [[ $src_fasta_compression == 'gz' ]]; then
	gzip -dc $src_fasta >$tmp_prefix.src.fasta;

    elif [[ $src_fasta_compression == 'xz' ]]; then
	xz -dc $src_fasta >$tmp_prefix.src.fasta;
    fi
fi
src_prefix=$tmp_prefix.src
src_fasta=$src_prefix.fasta

samtools \
    faidx \
    $src_fasta;

faToTwoBit \
    $src_fasta \
    $src_prefix.2bit;

twoBitInfo \
    $src_prefix.2bit \
    $src_prefix.chr.sizes;

twoBitInfo \
    $src_prefix.2bit \
    $src_prefix.ctg.sizes \
    -noNs;


# link the trg genome fasta into prefix dir and index
if [[ ! -e "$tmp_prefix.trg.fasta" ]]; then
    if [[ $trg_fasta_compression == none ]]; then
	ln -sf $trg_fasta $tmp_prefix.trg.fasta;

    elif [[ $trg_fasta_compression == 'bz2' ]]; then
	bzip2 -dc $trg_fasta >$tmp_prefix.trg.fasta;

    elif [[ $trg_fasta_compression == 'gz' ]]; then
	gzip -dc $trg_fasta >$tmp_prefix.trg.fasta;

    elif [[ $trg_fasta_compression == 'xz' ]]; then
	xz -dc $trg_fasta >$tmp_prefix.trg.fasta;
    fi
fi
trg_prefix=$tmp_prefix.trg
trg_fasta=$trg_prefix.fasta

samtools \
    faidx \
    $trg_fasta;

faToTwoBit \
    $trg_fasta \
    $trg_prefix.2bit;

twoBitInfo \
    $trg_prefix.2bit \
    $trg_prefix.chr.sizes;

twoBitInfo \
    $trg_prefix.2bit \
    $trg_prefix.ctg.sizes \
    -noNs;


# break trg genome into contigs for faster alignment and
# create .lift file for liftUp back into scaffold space
trg_query=none;
if [[ $breaktrg == true ]]; then
    twoBitInfo $trg_prefix.2bit $trg_prefix.gap.bed -nBed;
    bedtools complement -i $trg_prefix.gap.bed -g $trg_prefix.chr.sizes >$trg_prefix.ctg.bed;
    bedtools getfasta -bed $trg_prefix.ctg.bed -fi $trg_fasta | fold -w 100 >$trg_prefix.ctg.fasta
    
    awk 'BEGIN{OFS="\t";FS="\t"} {if (ARGIND == 1) {lengths[$1]=$2} else {print $2,$1":"$2"-"$3,($3-$2),$1,lengths[$1]}}' \
	$trg_prefix.chr.sizes $trg_prefix.ctg.bed >$trg_prefix.ctg.lift

    trg_query=$trg_prefix.ctg.fasta
else
    trg_query=$trg_fasta;
fi

if [[ $breaksrc == true ]]; then
    twoBitInfo $src_prefix.2bit $src_prefix.gap.bed -nBed;
    bedtools complement -i $src_prefix.gap.bed -g $src_prefix.chr.sizes >$src_prefix.ctg.bed;
    bedtools getfasta -bed $src_prefix.ctg.bed -fi $src_fasta | fold -w 100 >$src_prefix.ctg.fasta
    
    awk 'BEGIN{OFS="\t";FS="\t"} {if (ARGIND == 1) {lengths[$1]=$2} else {print $2,$1":"$2"-"$3,($3-$2),$1,lengths[$1]}}' \
	$src_prefix.chr.sizes $src_prefix.ctg.bed >$src_prefix.ctg.lift

    src_target=$src_prefix.ctg.fasta
    
else
    src_target=$src_fasta
fi
# Use UCSC's bamToPsl tool to convert the following minimap2
# alignments in BAM format to PSL format. We need to include
# minimap2's -Y flag to add soft-clips to all supplementary
# alignments, or bamToPsl will report the query sequence lengths
# incorrectly. 

## Orig: -x asm10 -a -M0.2 -t12
## asm10: -k19 -w19 -A1 -B9 -O16,41 -E2,1 -s200 -z200 --min-occ-floor=100
## better: -x asm10 -M0.2 -O41,41 -E1,1 --end-bonus=40 --no-end-flt -s200 -z800 -a -Y --secondary=no

## --eqx creates fragmented chain files, don't use it
minimap2 \
    -x asm10 -M0.2 -O16,16 -E1,1 --end-bonus=40 --no-end-flt -s200 -z800 \
    -a --secondary=no -t$threads \
    $src_target \
    $trg_query \
    > $tmp_prefix.sam

# Supplementary alignments must have soft-clips and cannot
# have hard-clips, or bamToPsl reports the query lengths
# incorrectly:
perl -F"\t" -ane '{if (/^\@/){print} else {chomp($F[$#F]); $S=0; $E=0; $F[9]="*"; $F[10]="*"; while($F[5]=~s/^(\d+)[HS]//){$S+=$1} while($F[5]=~s/(\d+)[HS]$//){$E+=$1} if($S){$F[5]=$S."S".$F[5]} if($E){$F[5]=$F[5].$E."S"} print(join("\t",@F),"\n")}}' $tmp_prefix.sam \
    |  samtools view -1 - \
    >  $tmp_prefix.tmp.bam \
    && rm $tmp_prefix.sam
# awk 'BEGIN{OFS="\t";FS="\t"} {if ($1 !~ /^\@/) {$10="*";$11="*"} print $0}' \

bamToPsl \
    -nohead \
    $tmp_prefix.tmp.bam \
    $tmp_prefix.tmp.psl


if [[ $breaktrg == true ]]; then
    liftUp \
	-pslQ \
	-nohead \
	$tmp_prefix.psl \
	$trg_prefix.ctg.lift warn \
	$tmp_prefix.tmp.psl
else
    mv $tmp_prefix.tmp.psl $tmp_prefix.psl
fi

if [[ $breaksrc == true ]]; then
    mv $tmp_prefix.psl $tmp_prefix.tmp.psl && \
    liftUp \
	-nohead \
	$tmp_prefix.psl \
	$src_prefix.ctg.lift warn \
	$tmp_prefix.tmp.psl
fi

if [[ -n "$synteny_bedpe" ]]; then
    mv $tmp_prefix.psl $tmp_prefix.tmp.psl
    awk 'BEGIN{OFS="\t";FS="\t"} {if (ARGIND == 1) {src[$1]=$4; trg[$4]=$1} else { if ((src[$14] == $10) || (trg[$10] == $14) || (src[$14] == ".") || (trg[$10] == ".")){print} }}' \
	$synteny_bedpe $tmp_prefix.tmp.psl >$tmp_prefix.psl
fi


pslToChain \
    $tmp_prefix.psl \
    $out_prefix.fwd.chain


src_fasta_length=$(cut -f2 $src_prefix.ctg.sizes | paste -s -d'+' | bc);
trg_fasta_length=$(cut -f2 $trg_prefix.ctg.sizes | paste -s -d'+' | bc);

#touch $out_prefix_dir/.pipeline;


filter_options=""
if [[ $bridge != false ]]; then
    filter_options="--bridge $bridge";
fi

# Usage: liftover-filter-chain <input.chain> <source.fasta> <target.fasta> <output-prefix>


liftover-filter-chain \
    --submodule \
    $filter_options \
    $out_prefix.fwd.chain \
    $src_fasta \
    $trg_fasta \
    $out_prefix.fwd

chainSwap $out_prefix.fwd.filter.chain $out_prefix.rev.chain


liftover-filter-chain \
    --submodule \
    $filter_options \
    $out_prefix.rev.chain \
    $trg_fasta \
    $src_fasta \
    $out_prefix.rev

chainSwap $out_prefix.rev.filter.chain stdout \
    | chainSort stdin $out_prefix.final.chain

# samtools sort -m 1G -o $tmp_prefix.srt.bam $tmp_prefix.bam
# samtools index $tmp_prefix.srt.bam

# awk 'OFS="\t" {if ($1 == "chain") {b=$11;e=$12; if ($10 == "-") {b=$9-$12;e=$9-$11} print $3,$6,$7,$8":"b"-"e,$13,$10}}' \
#     $out_prefix.final.chain | sort -k1,1V -k2,2n -k3,3n >$tmp_prefix.final.bed

# bedtools intersect -nonamecheck -s -F 1.0 \
#     -a $tmp_prefix.srt.bam \
#     -b $tmp_prefix.final.bed \
#     >  $out_prefix.final.bam

# samtools index $out_prefix.final.bam \
#     && touch $out_prefix.final.chain


awk \
    -v S=$src_fasta_length \
    -v T=$trg_fasta_length \
'BEGIN{
  s=0; 
  t=0;
} 
{
  if ($1 == "chain") { 
    s+=($7-$6); 
    t+=($12-$11)
  }
} 
END { 
  printf("\
Source genome total bases  = %d\n\
Source genome mapped bases = %d (%0.2f%%)\n\
Target genome total bases  = %d\n\
Target genome mapped bases = %d (%0.2f%%)\n",\
S, s, 100*s/(S ? S : 1), \
T, t, 100*t/(T ? T : 1))
}' $out_prefix.final.chain

printf "[$PROGRAM] Finished" >>/dev/fd/2;


