
import os
import re
import sys
import gzip
import bisect
import getopt
import logging
import subprocess

from fileutil import zopen

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Identify redundant haplotigs via alignment'

# configure global logging:
logging.basicConfig(
    stream=sys.stderr,
    level=logging.INFO,
    format="[{0}] %(asctime)s [%(levelname)s] %(message)s".format(
        __program__)
)    


INT_FIELDS = (1,2,3,6,7,8,9,10)
MINUS_STRAND = {'minus','-',-1}
FLOAT_FIELDS = (11,)
LONG_READ_ASSEMBLY = False
SELF_ALIGNED = False
FIELD_SEPERATOR = '\t'
MIN_ALIGNED_RATE = 0.8
MIN_ALIGNED_ERROR = 0.8
MIN_ALIGNED_LENGTH = 1000
MIN_ALIGNED_CONCORD = 0.5
MIN_ALIGNED_IDENTITY = 0.95
MIN_ALIGNED_SIMILARITY = 0.95
MIN_CONTAIN_FRACTION = 0.95
MIN_ALIGNED_FRACTION = None
MIN_OVERLAP_FRACTION = 0.50
MAX_OVERLAP_EXTENSION = 0
CIGAR_OPERATORS = re.compile('(\d+)([MIDNSHP=XB])')
ALIGN_OPERATORS = set('MID=X')
INDEL_OPERATORS = set('ID')
MATCH_OPERATORS = set('M=')
SUBST_OPERATORS = set('X')
DEBUG = 0

num = len
format_debug_las = "  {0:s}: {1:s} ({2:d}) {3:s} ({4:d}) @ id:{5:0.1f}%, sim:{6:0.1f}%, cov:{7:0.1f}%".format
format_debug_end = "{0:s}: {1:s}: {2:s}, {3:s} {4:s} @ id:{5:0.2f}%, sim:{6:0.1f}%, cov:{7:0.2f}%, con:{8:0.4f}".format


class ContactAuthorError(Exception):
    pass

class MalformedFileError(Exception):
    pass

class Strand(object):
    def __init__(self, obj):
        if isinstance(obj, Strand):
            self.__strand = obj.__strand
        elif isinstance(obj, int):
            self.__strand = obj // abs(obj) if obj else obj
        elif isinstance(obj, str):
            self.__strand = self._to_int(obj)
        else:
            raise TypeError("Invalid strand type: int, str, or Strand obj expected")

    def _to_int(self, strand):
        if   strand == '.':
            return 0
        elif strand == '-':
            return -1
        elif strand == '+':
            return +1
        else:
            raise ValueError("Invalid strand value: `+', `-', or `.' expected")
        
    def __str__(self):
        if self.__strand == 0:
            return '.'
        if self.__strand < 0:
            return '-'
        if self.__strand > 0:
            return '+'

    @property
    def int(self):
        return self.__strand

    @property
    def str(self):
        return str(self)
    
    def reverse(self):
        self.__strand *= -1


        
class AlignedSegment(object):
    def __init__(self, name, length, strand, beg, end, alignable_length=-1):
        self.name   = str(name)
        self.length = int(length)
        self.beg    = int(beg)
        self.end    = int(end)
        self.strand = Strand(strand)
        self._alignable_length = alignable_length
        
    def __str__(self):
        return "%s:%d-%d[%s]" % (
            self.name, self.beg, self.end, self.strand.str)

    @property
    def alignable_length(self):
        if self._alignable_length < 0:
            return self.length
        else:
            return self._alignable_length
    
    @property
    def aligned_length(self):
        return self.end - self.beg
    
    @property
    def aligned_fraction(self):
        return float(self.aligned_length) / self.alignable_length
    
    @property
    def spur5p_length(self):
        return self.beg
    
    @property
    def spur3p_length(self):
        return self.length - self.end
    
    def overlaps(self, other):
        """test whether self has any kind of overlap with other"""
        if other.name == self.name:
            if other.beg < self.end <= other.end:
                # self.beg ========= self.end
                #      other.beg ============== other.end
                # (b/c self.beg < self.end, also True for 'contained' queries)
                return True

            elif self.beg < other.end <= self.end:
                #           self.beg ========= self.end
                # other.beg ============== other.end
                # (b/c other.beg < other.end, also True for 'contains' queries)
                return True

        return False
    
    def contains(self, other):
        """test whether self contains other"""
        if other.name == self.name:
            if self.beg <= other.beg and other.end <= self.end:
                return True

        return False

    def within(self, other):
        """test whether self is contained within other"""
        if other.name == self.name:
            if other.beg <= self.beg and self.end <= other.end:
                return True

        return False

    def overlaps_beg(self, other):
        """test whether self overlaps other.beg"""
        if other.name == self.name:
            if self.overlaps(other) and \
               other.beg < self.end < other.end:
                # self.beg ========= self.end
                #      other.beg ============== other.end
                return True

        return False

    def overlaps_end(self, other):
        """test whether self overlaps other.end"""
        if other.name == self.name:
            if self.overlaps(other) and \
               other.beg < self.beg < other.end:
                #           self.beg ========= self.end
                # other.beg ============== other.end
                return True

        return False

    def overlap_fraction(self, other):
        if not self.overlaps(other):
            return 0.0
            
        elif self.within(other):
            return 1.0
        
        elif self.contains(other):
            return 1.0
            
        elif self.overlaps_beg(other):
            return float(self.end - other.beg) / self.aligned_length

        elif self.overlaps_end(other):
            return float(other.end - self.beg) / self.aligned_length

        else:
            raise ContactAuthorError("Unhandled overlap type")

    def bisect_search(self, others, key=lambda x: x):
        beg = 0
        end = num(others) - 1

        if beg > end:
            return -1
    
        while beg <= end:  # end >= beg:
            mid = (beg + end) // 2
            
            other = key(others[mid])

            if self.overlaps(other):
                # Our segment overlaps
                # another placed segment
                return mid

            elif other.beg < self.beg:
                # Midpoint is too far left
                beg = mid + 1

            elif other.end > self.end:
                # Midpoint is too far right
                end = mid - 1
            else:
                raise ContactAuthorError("Unhandled condition (%d -- %d -- %d)\n" % (beg, mid, end))

        return -1

    def nearest_neighbors_range(self, others, index=None, K=1, key=lambda x: x):
        if index is None:
            index = self.bisect_search(others, key)
        if index < 0:
            return (index, index)

        n = K
        nn = 0
        i = index
        j = index
        k = index
        l = index
        while ((0 <= k) or (l < num(others))):
            if nn == n:
                break
            if nn < n and 0 <= i-1:
                other = key(others[i-1])
                if self.overlaps(other):
                    nn += 1
                    i -= 1
            if nn < n and j+1 < num(others):
                other = key(others[j+1])
                if self.overlaps(other):
                    nn += 1
                    j += 1
            k -= 1
            l += 1

        return (i, j)
        
    def neighborhood_overlap_fraction(self, others, index=None, key=lambda x: x):
        overlap_fraction = 0.0

        if index is None:
            index = self.bisect_search(others, key)
        if index < 0:
            return overlap_fraction

        i = index - 1
        while 0 <= i:
            other = key(others[i])
            if not self.overlaps(other):
                break
            overlap_fraction += self.overlap_fraction(other)
            i -= 1

        i = index
        while i < num(others):
            other = key(others[i])
            if not self.overlaps(other):
                break
            overlap_fraction += self.overlap_fraction(other)
            i += 1
    
        # can be >1.0 if the neighbors were overlapping
        return min(1.0, overlap_fraction)


    
class AlignedSegmentPair(object):
    def __init__(self, qry, trg, matches=0, mismatches=0, indels=0, length=0, score=0.0):
        if isinstance(qry, AlignedSegment):
            self.qry = qry
        else:
            raise TypeError("Expected AlignedSegment object as query")
        if isinstance(trg, AlignedSegment):
            self.trg = trg
        else:
            raise TypeError("Expected AlignedSegment object as target")            
        self.matches = matches
        self.mismatches = mismatches
        self.indels = indels
        self.length = length
        self.score = score

    @property
    def strand(self):
        return Strand(self.qry.strand.int * self.trg.strand.int)

    @property
    def identity(self):
        return float(self.matches) / float(self.matches + self.mismatches)

    @property
    def similarity(self):
        return float(self.matches) / float(self.length)
    
    @property
    def indel_rate(self):
        return float(self.indels) / float(self.length)
    
    @property
    def align_rate(self):
        return 1.0 - self.indel_rate    
    


class TiledSegmentPair(object):
    def __init__(self, alignments=[], key=lambda a: a):
        self.qry_tiled_length = 0
        self.trg_tiled_length = 0
        self.tiled_length = 0
        self.alignments = []
        self.dispersion = 0
        self.mismatches = 0
        self.matches = 0
        self.indels = 0

        for pair in sorted(alignments, key=lambda a: -1.0*a.identity*a.aligned_fraction):
            self.add(pair, key)

    def __getitem__(self, item):
        return self.alignments[item][1]

    def __iter__(self):
        return iter(map(lambda a: a[1], self.alignments))

    def __len__(self):
        return len(self.alignments)

    @property
    def is_empty(self):
        return num(self.alignments) == 0

    @property
    def identity(self):
        return float(self.matches) / float(self.matches + self.mismatches)

    @property
    def similarity(self):
        return float(self.matches) / float(self.tiled_length)
    
    @property
    def indel_rate(self):
        return float(self.indels) / float(self.tiled_length)

    @property
    def align_rate(self):
        return 1.0 - self.indel_rate

    def qry_tiled_fraction(self, alignable=False):
        if self.is_empty:
            return 0.0
        if alignable:
            return float(self.qry_tiled_length) / float(self[0].qry.alignable_length)
        else:
            return float(self.qry_tiled_length) / float(self[0].qry.length)

    def trg_tiled_fraction(self, alignable=False):
        if self.is_empty:
            return 0.0
        if alignable:
            return float(self.trg_tiled_length) / float(self[0].trg.alignable_length)
        else:
            return float(self.trg_tiled_length) / float(self[0].trg.length)

    def is_beg(self, pair):
        pair_rght = self[0]
        pair_qmid = 0.5 * (pair.qry.end + pair.qry.beg)
        pair_tmid = 0.5 * (pair.trg.end + pair.trg.beg)
        return pair.strand.int == pair_rght.strand.int and \
            pair_qmid <= pair_rght.qry.beg and \
            pair_tmid <= pair_rght.trg.beg

    def is_mid(self, pair, i):
        pair_left = self[i-1]
        pair_rght = self[i]
        pair_qmid = 0.5 * (pair.qry.end + pair.qry.beg)
        pair_tmid = 0.5 * (pair.trg.end + pair.trg.beg)
        return pair.strand.int == pair_left.strand.int and \
            pair_left.qry.end <= pair_qmid <= pair_rght.qry.beg and \
            pair_left.trg.end <= pair_tmid <= pair_rght.trg.beg

    def is_end(self, pair):
        pair_left = self[-1]
        pair_qmid = 0.5 * (pair.qry.end + pair.qry.beg)
        pair_tmid = 0.5 * (pair.trg.end + pair.trg.beg)
        return pair.strand.int == pair_left.strand.int and \
            pair_left.qry.end <= pair_qmid and \
            pair_left.trg.end <= pair_tmid

    def add(self, pair, key=lambda x: x):
        member = (key(pair).beg, pair)
        tiled = False
        
        if self.is_empty:
            self.alignments.append(member)
            tiled = True
            
        else:
            i = bisect.bisect_left(self.alignments, member)
            if i == 0 and self.is_beg(pair):
                # insert at 5' end of tiled pairs
                self.dispersion += key(self[0]).beg - key(pair).end                
                bisect.insort_left(self.alignments, member)
                tiled = True

            elif i == num(self.alignments) and self.is_end(pair):
                # insert at 3' end of tiled pairs
                self.dispersion += key(pair).beg - key(self[-1]).end
                bisect.insort_right(self.alignments, member)
                tiled = True
                
            elif i < num(self.alignments) and self.is_mid(pair, i):
                # insert between two previously tiled pairs
                self.dispersion -= key(pair).aligned_length
                bisect.insort_left(self.alignments, member)
                tiled = True
                
        if tiled:
            self.qry_tiled_length += pair.qry.aligned_length
            self.trg_tiled_length += pair.trg.aligned_length
            self.tiled_length += pair.length
            self.matches += pair.matches
            self.mismatches += pair.mismatches
            self.indels += pair.indels
            return True
        else:
            return False

    
def _log_exception(exception, message, traceback):
    """
Exception handler hook to redirect errors to be reported 
by the logging module
    """
    logging.error("%s: %s" % (exception.__name__, message))
    sys.exit(1)
        
def format_output(pair, annot=None):
    pair_trg_beg = pair.trg.beg
    pair_trg_end = pair.trg.end
    if pair.strand.int < 0:
        pair_trg_beg, pair_trg_end = \
            pair.trg.length - pair_trg_end, pair.trg.length - pair_trg_beg
        
    return FIELD_SEPERATOR.join(map(str, (
        pair.qry.name,
        pair.qry.length,
        pair.qry.beg,
        pair.qry.end,
        pair.strand.str,
        pair.trg.name,
        pair.trg.length,
        pair_trg_beg,
        pair_trg_end,
        pair.matches,
        pair.length,
        int(pair.score)  # blastn reports float
    ))) + ("\n" if annot is None else ("%san:Z:%s\n" % (FIELD_SEPERATOR,annot)))

def qry(pair):
    return pair.qry

def trg(pair):
    return pair.trg

def pair_qry_position(pair):
    return (pair.qry.name, pair.qry.beg, pair.qry.end)

def logging_debugL1(message):
    if DEBUG > 0:
        logging.debug(message)

def logging_debugL2(message):
    if DEBUG > 1:
        logging.debug(message)

def logging_debugL3(message):
    if DEBUG > 2:
        logging.debug(message)

def logging_debugL4(message):
    if DEBUG > 3:
        logging.debug(message)
        
def logging_debug_pairs(tag, pairs, level=logging_debugL1):
    for pair in pairs:
        level(format_debug_las(
            tag,
            str(pair.qry), pair.qry.length, 
            str(pair.trg), pair.trg.length,
            100.0*pair.identity,
            100.0*pair.similarity,
            100.0*pair.qry.aligned_fraction,
            100.0*pair.identity*pair.qry.aligned_fraction)
        )
        
def redund_contigs(pairs, outfile, query_ids=set(), excluded=set(), contained_only=False):
    if num(pairs) == 0:
        return

    qry_btree = {}
    trg_btree = {}
    tiling_scores = {}
    tiling_concord = {}
    logging_debugL2(">%s" % pairs[0].qry.name)
    logging_debugL3(" LAS::")
    for pair in sorted(pairs, key=lambda p: -1.0*p.identity*p.qry.aligned_fraction):
        # The dicts qry_btree and trg_btree prevent redundant alignments from
        # occurring from the same segments of query or target sequences in
        # a qry-trg pair, as can occur in low-complexity regions.

        pkey = (pair.trg.name, pair.strand.int)
        if pkey not in trg_btree:
            qry_btree[pkey] = TiledSegmentPair()
            trg_btree[pkey] = TiledSegmentPair()
            tiling_scores[pkey] = 0.0

        # Block addition of suboptimal/redundant qry-trg pairs:
        qry_ovl_frac = pair.qry.neighborhood_overlap_fraction(qry_btree[pkey], key=qry)
        trg_ovl_frac = pair.trg.neighborhood_overlap_fraction(trg_btree[pkey], key=trg)
        
        if qry_ovl_frac < 0.5 and trg_ovl_frac < 0.5:
            logging_debugL3(format_debug_las(
                'LAS',
                str(pair.qry), pair.qry.length, 
                str(pair.trg), pair.trg.length,
                100.0*pair.identity,
                100.0*pair.similarity,
                100.0*pair.qry.aligned_fraction,
                100.0*pair.identity*pair.qry.aligned_fraction)
            )
            if qry_btree[pkey].add(pair, key=qry) and \
               trg_btree[pkey].add(pair, key=trg):
                tiling_scores[pkey] -= pair.identity * pair.qry.aligned_fraction
            
    logging_debugL3("  --")

    if num(tiling_scores) < 1:
        logging_debugL2("")
        return
    
    tiling_ranked = sorted(tiling_scores, key=tiling_scores.get)

    logging_debugL3(" GRP::")    
    for n, pkey in enumerate(tiling_ranked):
        logging_debug_pairs('GRP%d' % (n+1,), qry_btree[pkey], level=logging_debugL3)
        
        qry_concord = float(qry_btree[pkey].qry_tiled_length) \
                      / float(qry_btree[pkey][-1].qry.end - qry_btree[pkey][0].qry.beg)
        trg_concord = float(trg_btree[pkey].trg_tiled_length) \
                      / float(trg_btree[pkey][-1].trg.end - trg_btree[pkey][0].trg.beg)
        tiling_concord[pkey] = min(qry_concord / (trg_concord if trg_concord > 0.0 else 1.0),
                                   trg_concord / (qry_concord if qry_concord > 0.0 else 1.0))

    logging_debugL3("  --")
        
    top_tiling = None
    top_concord = 0.0
    overlap_clas = "single"
    overlap_type = "no"
    overlap_stat = "hit"
    for pkey in tiling_ranked:
        qry_name   = qry_btree[pkey][0].qry.name
        qry_length = qry_btree[pkey][0].qry.length
        trg_name   = trg_btree[pkey][0].trg.name
        trg_length = trg_btree[pkey][0].trg.length        

        if SELF_ALIGNED and qry_length > trg_length:
            continue
        if SELF_ALIGNED and trg_name in excluded:
            # may need to remove this. After looking at HiC contact maps
            # it becomes apparent that A => B, B => C transitivity exists
            continue
        if top_tiling is None:
            top_tiling = qry_btree[pkey]
            top_concord = tiling_concord[pkey]
            break

    if top_tiling is None:
        logging_debugL1(format_debug_end(
            'REJECTED', qry_name,
            overlap_clas, overlap_type, overlap_stat,
            0.0, 0.0, 0.0, 0.0)
        )
        logging_debugL2("")
        return        

    logging_debugL2(" TOP::")
    logging_debug_pairs('TOP', top_tiling, level=logging_debugL2)
    
    
    max_qry_spurlen = max(20, 0.5 * (1.0 - MIN_CONTAIN_FRACTION) * qry_length)
    max_trg_spurlen = max(20, 0.5 * (1.0 - MIN_CONTAIN_FRACTION) * trg_length)
    
    passes_all = False    
    passes_pid = top_tiling.identity   >= MIN_ALIGNED_IDENTITY and \
                 top_tiling.similarity >= MIN_ALIGNED_SIMILARITY
    passes_len = top_tiling.tiled_length >= MIN_ALIGNED_LENGTH
    passes_cov = top_tiling.qry_tiled_fraction(alignable=True) >= MIN_CONTAIN_FRACTION
    passes_con = top_concord >= MIN_ALIGNED_CONCORD

    if not passes_con:
        logging_debugL4("    => fails con")
        overlap_type = "contained"
        overlap_stat = "concordance"
        
    elif passes_pid and passes_cov:
        logging_debugL4("    => passes contained 1")
        # Query is contained within the target without bubbles:
        #               discard
        #   --__________________________-
        #     ::::::::::::::::::::::::::
        #  =====================================
        #                 keep
        # Dont enforce min length above; small queries are
        # only allowed to be contained.
        overlap_type = "contained"
        overlap_stat = "complete"
        passes_all = True
        
    elif top_tiling[ 0].qry.spur5p_length <= max_qry_spurlen and \
         top_tiling[-1].qry.spur3p_length <= max_qry_spurlen:
        passes_cov = top_tiling.qry_tiled_fraction(alignable=True) >= MIN_ALIGNED_FRACTION
        overlap_type = "contained"
        if passes_pid and passes_cov:
            logging_debugL4("    => passes contained 2")
            # Query is contained within the target with bubbles
            # (this case is necessary when comparing long-read datasets):
            #               discard
            #     --______-________-__________--
            #       :::::: :::::::: ::::::::::
            #  =====================================
            #                 keep
            overlap_stat = "partial"
            passes_all = True

        elif passes_pid:
            logging_debugL4("    => fails contained cov")
            overlap_stat = "coverage"
        else:
            logging_debugL4("    => fails contained id")
            overlap_stat = "identity"
            
    elif contained_only:
        logging_debugL4("    => fails contained only, next")
        # This condition prevents evaluating the rest of the condition tree
        # so that we can drop down into reporting the results
        overlap_type = "contain"
        overlap_stat = "coverage"

    elif passes_pid and passes_len:
        logging_debugL4("    => passes overlap pid and len")
        passes_cov = top_tiling.qry_tiled_fraction(alignable=True) >= MIN_OVERLAP_FRACTION
        overlap_type = "overlap"
        
        if passes_cov:
            logging_debugL4("    => passes overlap cov")
            if   top_tiling[ 0].qry.spur5p_length <= max_qry_spurlen and \
                 top_tiling[-1].trg.spur3p_length <= max_trg_spurlen and \
                 top_tiling[-1].qry.spur3p_length <= MAX_OVERLAP_EXTENSION:
                logging_debugL4("    => passes overlap 5p")
                # top_tiling[ 0].qry.spur5p_length <= top_tiling[ 0].trg.spur5p_length:
                # The 5'-end of the query overlaps,
                # the 3'-end of some larger target:
                #        v-q.beg       v-q.end
                #        _______________--------~
                #        :::::::::::::::
                # ~=====================
                #        ^-t.beg       ^-t.end
                overlap_stat = "beg"
                passes_all = True
                
            elif top_tiling[-1].qry.spur3p_length <= max_qry_spurlen and \
                 top_tiling[ 0].trg.spur5p_length <= max_trg_spurlen and \
                 top_tiling[ 0].qry.spur5p_length <= MAX_OVERLAP_EXTENSION:
                logging_debugL4("    => passes overlap 3p")
                # top_tiling[-1].qry.spur3p_length <= top_tiling[-1].trg.spur3p_length:
                # The 3'-end of the query overlaps,
                # the 5'-end of some larger query:
                #          v-q.beg      v-q.end
                #  ~-------______________
                #          ::::::::::::::
                #          ========================~
                #          ^-t.beg      ^-t.end
                overlap_stat = "end"
                passes_all = True
            else:
                logging_debugL4("    => fails overlap spur or ext")

        else:
            logging_debugL4("    => fails overlap cov")
            overlap_stat = "coverage"
            
    else:
        logging_debugL4("    => fails single")
        overlap_stat = "unknown"

    logging_debugL2("  --")
        
    if passes_all:
        logging_debugL1(format_debug_end(
            'ACCEPTED', qry_name,
            overlap_clas, overlap_type, overlap_stat,
            100.0 * top_tiling.identity,
            100.0 * top_tiling.similarity,
            100.0 * top_tiling.qry_tiled_fraction(alignable=True),
            100.0 * top_concord)
        )
        logging_debugL2('')
            
        for record in top_tiling:
            outfile.write(format_output(record, annot=overlap_type))

        excluded.add(qry_name)
        return

    elif contained_only:
        logging_debugL1(format_debug_end(
            'REJECTED', qry_name,
            overlap_clas, overlap_type, overlap_stat,
            100.0 * top_tiling.identity,
            100.0 * top_tiling.similarity,
            100.0 * top_tiling.qry_tiled_fraction(alignable=True),
            100.0 * top_concord)
        )
        logging_debugL1('')
        return

        
    # Check if query spans two or more other contigs,
    # each query-target pair aligned fraction < 0.5:
    #               discard
    #      __________----________
    #      ::::::::::    ::::::::
    # ===============    ==============
    #         keep            keep
    #
    # - OR - 
    #                        keep
    #      __________------________---________
    #      ::::::::::      ::::::::   ::::::::
    # ===============      ========   ==============
    #         keep          discard      keep

    tiled_ends = 0
    tiled_qry_con = 0
    tiled_trg_con = 0
    tiled_length = 0.0
    tiled_indels = 0.0
    tiled_matches = 0.0
    tiled_fraction = 0.0
    tiled_identity = 0.0
    overlap_clas = "multi"
    overlap_type = "none"
    overlap_stat = ""
    multi_tiling = {'BEG':[], 'MID':[], 'END':[]}
    logging_debugL2(" MLT::")
    for pkey in tiling_ranked:
        qry_tiling = qry_btree[pkey]
        trg_tiling = trg_btree[pkey]
        qry_name   = qry_tiling[0].qry.name
        qry_length = qry_tiling[0].qry.length        
        trg_name   = trg_tiling[0].trg.name
        trg_length = trg_tiling[0].trg.length
        
        passes_len = trg_tiling.tiled_length >= MIN_ALIGNED_LENGTH
        passes_pid = trg_tiling.identity     >= MIN_ALIGNED_IDENTITY and \
                     trg_tiling.similarity   >= MIN_ALIGNED_SIMILARITY
        passes_con = tiling_concord[pkey]    >= MIN_ALIGNED_CONCORD
        passes_pid = passes_pid and trg_tiling.align_rate >= MIN_ALIGNED_RATE

        logging_debug_pairs('MLT', qry_tiling, level=logging_debugL4)
        
        if SELF_ALIGNED and trg_name in excluded:
            # trg already seen as a query and it's
            # already been excluded, so
            logging_debugL4("    => fails target excluded")
            continue
        if not passes_len:
            logging_debugL4("    => fails len")
            continue
        if not passes_pid:
            logging_debugL4("    => fails id")
            continue
        if not passes_con:
            logging_debugL4("    => fails con")
            continue
            
        max_spurlen = min(0.5 * (1.0 - MIN_CONTAIN_FRACTION) * qry_length,
                          0.5 * (1.0 - MIN_CONTAIN_FRACTION) * trg_length)
        max_spurlen = max(20, max_spurlen)
        if qry_tiling[ 0].qry.spur5p_length <= max_spurlen and \
           trg_tiling[-1].trg.spur3p_length <= max_spurlen and \
           trg_tiling[-1].trg.spur3p_length <= qry_tiling[-1].qry.spur3p_length:
            # Less than half of the 5'-end of the query is anchored
            # to (overlaps) the 3'-end of some larger target:
            #        v-q.beg       v-q.end
            #        _______________--------~
            #        :::::::::::::::
            # ~=====================
            #        ^-t.beg       ^-t.end
            if tiled_ends & 0x1:
                logging_debugL4("    => ignored tiled beg")
                continue

            logging_debugL4("    => passes beg")
            tiled_qry_con  += float(qry_tiling.qry_tiled_length) / float(qry_tiling[-1].qry.end - qry_tiling[0].qry.beg)
            tiled_trg_con  += float(trg_tiling.trg_tiled_length) / float(trg_tiling[-1].trg.end - trg_tiling[0].trg.beg)
            tiled_fraction += trg_tiling.qry_tiled_fraction(alignable=True)
            tiled_length   += trg_tiling.tiled_length
            tiled_indels   += trg_tiling.indels
            tiled_matches  += trg_tiling.matches
            multi_tiling['BEG'].extend(qry_tiling)
            tiled_ends |= 0x1

        elif trg_length < qry_length and \
             trg_tiling.trg_tiled_fraction(alignable=True) >= MIN_CONTAIN_FRACTION:
            # Query contains trg without bubbles
            #       v-q.beg     v-q.end
            #  ~----_____________-----~
            #       :::::::::::::
            #       =============
            #       ^-t.beg     ^-t.end
            logging_debugL4("    => passes single mid")
            if not SELF_ALIGNED:
                logging_debugL4("    => not self-self")
                tiled_qry_con  += float(qry_tiling.qry_tiled_length) / float(qry_tiling[-1].qry.end - qry_tiling[0].qry.beg)
                tiled_trg_con  += float(trg_tiling.trg_tiled_length) / float(trg_tiling[-1].trg.end - trg_tiling[0].trg.beg)
                tiled_fraction += trg_tiling.qry_tiled_fraction(alignable=True)
                tiled_length   += trg_tiling.tiled_length
                tiled_indels   += trg_tiling.indels
                tiled_matches  += trg_tiling.matches
            multi_tiling['MID'].extend(qry_tiling)
            
        elif trg_length < qry_length and \
             trg_tiling[ 0].trg.spur5p_length <= max_spurlen and \
             trg_tiling[-1].trg.spur3p_length <= max_spurlen and \
             trg_tiling.trg_tiled_fraction(alignable=True) >= MIN_ALIGNED_FRACTION:
            # Query contains trg with bubbles
            #       v-q.beg     v-q.end
            #  ~----___-___-_____-----~
            #       ::: ::: :::::
            #       =============
            #       ^-t.beg     ^-t.end
            logging_debugL4("    => passes tiled mid")
            if not SELF_ALIGNED:
                logging_debugL4("    => not self-self")
                tiled_qry_con  += float(qry_tiling.qry_tiled_length) / float(qry_tiling[-1].qry.end - qry_tiling[0].qry.beg)
                tiled_trg_con  += float(trg_tiling.trg_tiled_length) / float(trg_tiling[-1].trg.end - trg_tiling[0].trg.beg)
                tiled_fraction += trg_tiling.qry_tiled_fraction(alignable=True)
                tiled_length   += trg_tiling.tiled_length
                tiled_indels   += trg_tiling.indels
                tiled_matches  += trg_tiling.matches
            multi_tiling['MID'].extend(qry_tiling)

        elif qry_tiling[-1].qry.spur3p_length <= max_qry_spurlen and \
             trg_tiling[ 0].trg.spur5p_length <= max_trg_spurlen and \
             trg_tiling[ 0].trg.spur5p_length <= qry_tiling[ 0].qry.spur5p_length:
            # Less than half of the 3'-end of the query is anchored
            # to (overlaps) the 5'-end of some larger target:
            #        v-q.beg      v-q.end
            #  ~-----______________
            #        ::::::::::::::
            #        ========================~
            #        ^-t.beg      ^-t.end
            if tiled_ends & 0x2:
                logging_debugL4("    => ignored tiled end")
                continue

            logging_debugL4("    => passes tiled end")
            tiled_qry_con  += float(qry_tiling.qry_tiled_length) / float(qry_tiling[-1].qry.end - qry_tiling[0].qry.beg)
            tiled_trg_con  += float(trg_tiling.trg_tiled_length) / float(trg_tiling[-1].trg.end - trg_tiling[0].trg.beg)
            tiled_fraction += trg_tiling.qry_tiled_fraction(alignable=True)
            tiled_length   += trg_tiling.tiled_length
            tiled_indels   += trg_tiling.indels
            tiled_matches  += trg_tiling.matches
            multi_tiling['END'].extend(qry_tiling)
            tiled_ends |= 0x2

        else:
            logging_debugL4("    => fail multi all")
            
    tiled_similarity = float(tiled_matches) / max(1, tiled_length)            
    tiled_identity = float(tiled_matches) / max(1, tiled_length - tiled_indels)
    tiled_concord = min(tiled_qry_con / (tiled_trg_con if tiled_trg_con > 0.0 else 1.0),
                        tiled_trg_con / (tiled_qry_con if tiled_qry_con > 0.0 else 1.0))

    tiled_spur5p_length = qry_length
    tiled_spur3p_length = qry_length
    for tag in ('END','MID','BEG'):
        if num(multi_tiling[tag]) > 0:
            tiled_spur5p_length = multi_tiling[tag][ 0].qry.spur5p_length
    for tag in ('BEG','MID','END'):
        if num(multi_tiling[tag]) > 0:
            tiled_spur3p_length = multi_tiling[tag][-1].qry.spur3p_length
    
    
    if (tiled_identity   >= MIN_ALIGNED_IDENTITY and \
        tiled_similarity >= MIN_ALIGNED_SIMILARITY and \
        (tiled_fraction  >= MIN_ALIGNED_FRACTION or \
         (tiled_fraction >= MIN_OVERLAP_FRACTION and \
          tiled_spur5p_length <= MAX_OVERLAP_EXTENSION and \
          tiled_spur3p_length <= MAX_OVERLAP_EXTENSION))):
        if tiled_ends == 3:
            logging_debugL4("    => passes full bridge")
            # both 5' and 3' ends anchored, discard this hit:
            overlap_type = "complete"
            overlap_stat = "bridge"
            passes_all = True

        else:
            logging_debugL4("    => fails full bridge")
            overlap_type = "partial"
            overlap_stat = "coverage"

    elif tiled_identity < MIN_ALIGNED_IDENTITY:
        logging_debugL4("    => fails part bridge id")
        overlap_type = "partial"
        overlap_stat = "identity"

    else:
        logging_debugL4("    => fails part bridge cov")
        overlap_type = "partial"
        overlap_stat = "coverage"


    for tag in ('BEG','MID','END'):
        logging_debug_pairs(tag, multi_tiling[tag], level=logging_debugL2)
    logging_debugL2("  --")
        
    if passes_all:
        logging_debugL1(format_debug_end(
            'ACCEPTED', qry_name,
            overlap_clas, overlap_type, overlap_stat,
            100.0 * tiled_identity,
            100.0 * tiled_similarity,
            100.0 * tiled_fraction,
            100.0 * tiled_concord)
        )
        for tag in ('BEG','MID','END'):
            for record in sorted(multi_tiling[tag], key=pair_qry_position):
                outfile.write(format_output(record, annot=overlap_type))

        excluded.add(qry_name)

    else:
        logging_debugL1(format_debug_end(
            'REJECTED', qry_name,
            overlap_clas, overlap_type, overlap_stat,
            100.0 * tiled_identity,
            100.0 * tiled_similarity,
            100.0 * tiled_fraction,
            100.0 * tiled_concord)
        )

    logging_debugL2('')

    return
        

def read_fasta_lengths(fasta_name):
    import pysam

    fasta_bases = {}
    fasta_file = pysam.FastxFile(fasta_name)
    nucleotides = re.compile('[acgturyswkmbdhvACGTURYSWKMBDHV]+')
    for record in fasta_file:
        contig_start = 0
        contig_count = 1
        prev_start = 0
        prev_end = None

        fasta_bases[record.name] = 0
        for contig in nucleotides.finditer(record.sequence):
            fasta_bases[record.name] += contig.end() - contig.start()

    fasta_file.close()
            
    return fasta_bases


def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else '[ERROR] %s\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] <in.paf> <out.paf>\n" % __program__)
    stream.write("\n")
    stream.write("Options:\n")
    stream.write("   -a,--min-aligned <ufloat>     Min frac of tiled query aligned [{-c}]\n")
    stream.write("   -c,--min-contain <ufloat>     Min frac of tiled query contained [%0.2f]\n" % (
        MIN_CONTAIN_FRACTION))
    stream.write("   -d,--min-concord <ufloat>     Min tiled dispersion concordance [%0.2f]\n" % (
        MIN_ALIGNED_CONCORD))
    stream.write("   -o,--min-overlap <ufloat>     Min frac of tiled query overlapped [%0.2f]\n" % (
        MIN_OVERLAP_FRACTION))
    stream.write("   -i,--min-identity <ufloat>    Min tiled alignment identity (excludes\n")
    stream.write("                                 indel positions in the denominator) [%0.2f]\n" % (
        MIN_ALIGNED_IDENTITY))
    stream.write("   -s,--min-similarity <ufloat>  Min tiled alignment similarity (includes\n")
    stream.write("                                 indel positions in the denominator) [%0.2f]\n" % (
        MIN_ALIGNED_SIMILARITY))
    stream.write("   -l,--min-length <uint>        Min tiled alignment length [%d]\n" % (
        MIN_ALIGNED_LENGTH))
    stream.write("   -Q,--query-fasta <file>       Query fasta file path\n")
    stream.write("   -T,--target-fasta <file>      Target fasta file path\n")
    stream.write("   -B,--blastn-paf               Input file is in blastn paf-like format\n")
    stream.write("   -E,--max-extension <uint>     Max overlap extension length to consider\n")
    stream.write("                                 one sequence redundant to another [%d]\n" % (
        MAX_OVERLAP_EXTENSION))
    # stream.write("   -L,--lr-assembly              Input contigs assembled using long reads\n")
    stream.write("   -X,--all-vs-all               Input is all-vs-all self-self alignments\n")
    stream.write("   -C,--contained-only           Write only full-length contained alignments\n")
    stream.write("   -D,--debug                    Run in debugging mode\n")
    stream.write("   -h,--help                     This help message\n")
    stream.write("\n")
    stream.write("Notes:\n")
    stream.write("   1. in.paf must be sorted by query length and query name, e.g.\n")
    stream.write("      sort -k2,2n -k1,1 unsorted.paf >in.paf\n")
    stream.write("\n")
    stream.write("   2. The `--query-fasta' and `--target-fasta' flags are optional, but\n")
    stream.write("      are highly recommended for scaffolded sequences. These options\n")
    stream.write("      allow normalization of the `--min-contain', `--min-aligned', and\n")
    stream.write("      `--min-overlap' parameters by the amount of non-gap sequence.\n")
    stream.write("\n")
    stream.write("   3. minimap2 recommended to be run with `-c' and `--eqx' options\n")
    stream.write("\n")
    stream.write("\n%s" % message)
    sys.exit(exitcode)
                 #----+----|----+----|----+----|----+----|----+----|----+----|----+----|----+----|
                 #        10        20        30        40        50        60        70        80


def main(argv):
    global MIN_ALIGNED_SIMILARITY
    global MIN_ALIGNED_IDENTITY
    global MIN_ALIGNED_FRACTION
    global MIN_CONTAIN_FRACTION
    global MAX_OVERLAP_EXTENSION
    global MIN_OVERLAP_FRACTION
    global MIN_ALIGNED_CONCORD
    global MIN_ALIGNED_LENGTH
    global MIN_ALIGNED_ERROR
    global LONG_READ_ASSEMBLY
    global SELF_ALIGNED
    global QRY_REGEX
    global TRG_REGEX
    global DEBUG

    short_options = 'hBCDLXa:c:E:o:i:l:M:Q:T:d:s:'
    long_options = (
        'help',
        'debug',
        'all-vs-all',
        'blastn-paf',
        'lr-assembly',
        'contained-only',
        'min-concord=',
        'min-aligned=',
        'min-contain=',
        'min-overlap=',
        'min-identity=',
        'min-similarity=',
        'min-length=',
        'max-extension=',
        'query-regex=',
        'query-fasta=',
        'target-regex=',
        'target-fasta='
    )
    
    try:
        options, arguments = getopt.getopt(argv, short_options, long_options)
    except getopt.GetoptError as error:
        usage(error)

    qry_fasta = None
    trg_fasta = None
    qry_regex = re.compile('(\S+)')
    trg_regex = re.compile('(\S+)')
    is_blastn_paf = False
    contained_only = False
    daligner_options = []
    for flag, value in options:
        try:
            if   flag in ('-h','--help'): usage(exitcode=0)
            elif flag in ('-D','--debug'): DEBUG += 1            
            elif flag in ('-X','--all-vs-all'): SELF_ALIGNED = True
            elif flag in ('-C','--contained-only'): contained_only = True
            elif flag in ('-d','--min-concord'): MIN_ALIGNED_CONCORD = float(value)
            elif flag in ('-i','--min-identity'): MIN_ALIGNED_IDENTITY = float(value)
            elif flag in ('-a','--min-aligned'): MIN_ALIGNED_FRACTION = float(value)
            elif flag in ('-c','--min-contain'): MIN_CONTAIN_FRACTION = float(value)
            elif flag in ('-o','--min-overlap'): MIN_OVERLAP_FRACTION = float(value)
            elif flag in ('-l','--min-length'): MIN_ALIGNED_LENGTH = int(value)
            elif flag in ('-s','--min-similarity'): MIN_ALIGNED_SIMILARITY = float(value)
            elif flag in ('-E','--max-extension'): MAX_OVERLAP_EXTENSION = int(value)
            elif flag in ('-T','--target-fasta'): trg_fasta = value
            elif flag in ('-Q','--query-fasta'): qry_fasta = value
            elif flag in ('-B','--blastn-paf'): is_blastn_paf = True
            elif flag in ('-L','--lr-assembly'): LONG_READ_ASSEMBLY = True
        except ValueError:
            usage("Invalid value for '%s' option" % flag)

    if MIN_ALIGNED_ERROR > 1.0:
        MIN_ALIGNED_ERROR /= 100.0
    if MIN_ALIGNED_CONCORD > 1.0:
        MIN_ALIGNED_CONCORD /= 100.0
    if MIN_ALIGNED_IDENTITY > 1.0:
        MIN_ALIGNED_IDENTITY /= 100.0
    if MIN_CONTAIN_FRACTION > 1.0:
        MIN_CONTAIN_FRACTION /= 100.0
    if MIN_ALIGNED_FRACTION is None:
        MIN_ALIGNED_FRACTION = MIN_CONTAIN_FRACTION
    if MIN_ALIGNED_FRACTION > 1.0:
        MIN_ALIGNED_FRACTION /= 100.0
    if MIN_OVERLAP_FRACTION > 1.0:
        MIN_OVERLAP_FRACTION /= 100.0
    if MIN_ALIGNED_SIMILARITY > 1.0:
        MIN_ALIGNED_SIMILARITY /= 100.0


    if len(arguments) == 0:
        usage()
    if len(arguments) != 2:
        usage("Unexpected number of arguments")
    if DEBUG:
        logging.getLogger().setLevel(logging.DEBUG)        
    else:
        sys.excepthook = _log_exception

    
    logging.info("Starting")
    logging.info("Command: %s %s" % (sys.argv[0], ' '.join(argv)))

    qry_set = read_fasta_lengths(qry_fasta) if qry_fasta is not None else {}
    trg_set = read_fasta_lengths(trg_fasta) if trg_fasta is not None else {}

    coords = zopen(arguments[0], 'r')
    redund = zopen(arguments[1], 'w')

    pair = None
    pairs = []
    excluded = set()
    prev_qry_name = None
    prev_qry_length = 0
    prev_qry_processed = set()
    logging.info("Filtering and tiling coords...")
    for line in coords:
        if line.isspace() or \
           line.startswith('#'):
            continue
        
        field = line.rstrip().split()

        if num(field) < 12:
            raise MalformedFileError("Input PAF file expected to be twelve columns wide")
        
        # PAF file fields contain the following info:
        #  [0] str(query name)
        #  [1] int(query length)
        #  [2] int(query beg)
        #  [3] int(query end)
        #  [4] str(relative strand)
        #  [5] str(target name)
        #  [6] int(target length)
        #  [7] int(target beg)
        #  [8] int(target end)
        #  [9] int(num identities)
        # [10] int(alignment/mapping length)
        # [11] float([bit]score)
                
        qry_name = field[0]  # qry_regex.search(field[12])
        trg_name = field[5]  # trg_regex.search(field[11])

        if qry_name is None:
            raise ValueError("Bad --query-regex pattern")
        if trg_name is None:
            raise ValueError("Bad --target-regex pattern")

        if SELF_ALIGNED and \
           qry_name == trg_name:
            continue
        
        for f in INT_FIELDS:
            try:
                field[f] = int(field[f].strip())
            except ValueError as error:
                raise MalformedFileError(str(error))

        for f in FLOAT_FIELDS:
            try:
                field[f] = float(field[f].strip())
            except ValueError as error:
                raise MalformedFileError(str(error))

        if qry_name not in qry_set:
            qry_set[qry_name] = field[1]
        if trg_name not in trg_set:
            trg_set[trg_name] = field[6]
            
        qry_alignable = qry_set[qry_name]
        trg_alignable = trg_set[trg_name]
        
        curr = AlignedSegmentPair(
            AlignedSegment(
                qry_name,
                field[1],  # qry_length
                +1,        # qry_strand
                field[2],  # qry_beg
                field[3],  # qry_end
                qry_alignable
            ),
            AlignedSegment(
                trg_name,
                field[6],  # trg_length
                -1 if field[4] in MINUS_STRAND else +1, # trg_strand
                field[7],  # trg_beg
                field[8],  # trg_end
                trg_alignable
            ),
            score=field[11]
        )

        if curr.trg.beg > curr.trg.end:
            if is_blastn_paf:
                # Using blastn's `-outfmt` option with the custom formatting string
                # '6 qseqid qlen qstart qend sstrand sseqid slen sstart send nident length bitscore'
                # reports alignments in a PAF-like format with some important differences.
                # Handle those here:
                # blastn reports reverse-stranded targets on the `+' strand,
                # with the start and end coords flipped. Unflip:
                curr.trg.beg, curr.trg.end = curr.trg.end, curr.trg.beg
                # blastn reports 1-based coordinates, make 0-based:
                curr.qry.beg -= 1
                curr.trg.beg -= 1
            else:
                raise MalformedFileError("Target start > end: [%s]" % line.strip())

        if curr.qry.beg > curr.qry.end:
            raise MalformedFileError("Query start > end: [%s]" % line.strip())
            
        if curr.strand.int < 0:
            # flip strand so that target coords are on negative strand. Standard PAF always reports
            # negatively-stranded target coords on the original (positive) strand by default:
            curr.trg.beg, curr.trg.end = curr.trg.length - curr.trg.end, curr.trg.length - curr.trg.beg

        matches = field[9]
        alength = field[10]
        indels = 2 * alength - curr.qry.aligned_length - curr.trg.aligned_length
        mismatches = alength - matches - indels
        cigarstring = None
        for i in range(12, num(field)):
            if field[i].startswith('NM:i:'):
                mismatches = int(field[i][5:]) - indels
            if field[i].startswith('cg:Z:'):
                cigarstring = field[i][5:]

        if cigarstring:
            indels_ = 0
            matches_ = 0
            alength_ = 0
            mismatches_ = 0
            cigarstring_eqx = False
            for cigop in CIGAR_OPERATORS.finditer(cigarstring):
                op_len = int(cigop.group(1))
                op_tag = cigop.group(2)
                if  op_tag in MATCH_OPERATORS:
                    cigarstring_eqx = op_tag == '='
                    matches_ += op_len
                    alength_ += op_len
                elif op_tag in INDEL_OPERATORS:
                    indels_  += op_len
                    alength_ += op_len
                elif op_tag in SUBST_OPERATORS:
                    mismatches_ += op_len
                    alength_ += op_len
                    mismatches = 0

            if cigarstring_eqx:
                mismatches = mismatches_
                matches = matches_
            else:
                matches = matches_ - mismatches
                
            alength = alength_
            indels  = indels_
                
        curr.matches = matches
        curr.mismatches = mismatches
        curr.length = alength
        curr.indels = indels

        if prev_qry_name is not None and \
           prev_qry_name in prev_qry_processed:
            raise MalformedFileError(
                "Input PAF not properly sorted (i.e., `sort -k2,2n -k1,1`): %s" % arguments[0])
        if prev_qry_length and \
           curr.qry.length < prev_qry_length:
            raise MalformedFileError(
                "Input PAF not properly sorted (i.e., `sort -k2,2n -k1,1`): %s" % arguments[0])
            
        if prev_qry_name is not None and \
           curr.qry.name != prev_qry_name:
            redund_contigs(pairs, redund, qry_set, excluded, contained_only)
            prev_qry_processed.add(prev_qry_name)
            pairs = []
        
        prev_qry_length = curr.qry.length
        prev_qry_name = curr.qry.name
        pairs.append(curr)
        
        
    redund_contigs(pairs, redund, qry_set, excluded, contained_only)

    logging.info("Finished")
    

if __name__ == '__main__':
    main(sys.argv[1:])

