#!/usr/bin/env python

import os
import sys
import pysam
import getopt


__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Convert BAM to PAF format'


STRAND = ('+','-')
CIGAR_PAD_OPERATORS = {
    pysam.CHARD_CLIP,
    pysam.CSOFT_CLIP
}
CIGAR_ALN_OPERATORS = {
    pysam.CMATCH,
    pysam.CINS,
    pysam.CDEL,
    pysam.CREF_SKIP,
    pysam.CPAD,
    pysam.CEQUAL,
    pysam.CDIFF
}
CIGAR_QRY_OPERATORS = {
    pysam.CMATCH,
    pysam.CINS,
    pysam.CSOFT_CLIP,
    pysam.CHARD_CLIP,
    pysam.CEQUAL,
    pysam.CDIFF
}
CIGAR_REF_OPERATORS = {
    pysam.CMATCH,
    pysam.CDEL,
    pysam.CREF_SKIP,
    pysam.CEQUAL,
    pysam.CDIFF
}

CIGAR_MAP = {
    pysam.CMATCH    : 'M',
    pysam.CINS      : 'I',
    pysam.CDEL      : 'D',
    pysam.CREF_SKIP : 'N',
    pysam.CSOFT_CLIP: 'S',
    pysam.CHARD_CLIP: 'H',
    pysam.CPAD      : 'P',
    pysam.CEQUAL    : '=',
    pysam.CDIFF     : 'X',
    pysam.CBACK     : 'B',
}

num = len

def trim_cigar(aln):
    cigar = []
    for cig in aln.cigartuples:
        if cig[0] in CIGAR_PAD_OPERATORS:
            continue
        else:
            cigar.append("%d%s" % (cig[1],CIGAR_MAP[cig[0]]))
    return ''.join(cigar)


def _infer_align_length(aln):
    length = 0
    for cig in aln.cigartuples:
        if cig[0] in CIGAR_ALN_OPERATORS:
            length += cig[1]

    return length

            
def infer_align_length(cstats):
    length = 0
    for i in tuple(CIGAR_ALN_OPERATORS):
        length += cstats[i]
    return length


def _infer_query_length(aln):
    length = 0
    for cig in aln.cigartuples:
        if cig[0] in CIGAR_QRY_OPERATORS:
            length += cig[1]
    
    return length


def infer_query_length(cstats):
    length = 0
    for i in tuple(CIGAR_QRY_OPERATORS):
        length += cstats[i]
    return length


def infer_query_5p_clip_length(aln):
    clip_length = 0
    for i in range(len(aln.cigartuples)):
        if aln.cigartuples[i][0] in CIGAR_PAD_OPERATORS:
            clip_length += aln.cigartuples[i][1]
        else:
            break
    return clip_length


def infer_query_3p_clip_length(aln):
    clip_length = 0
    for i in range(len(aln.cigartuples)):
        if aln.cigartuples[~i][0] in CIGAR_PAD_OPERATORS:
            clip_length += aln.cigartuples[~i][1]
        else:
            break
    return clip_length


def bamtopaf(bam, bed):

    reference_lengths = dict(zip(bam.references, bam.lengths))
    
    for record in bam:
        if record.is_unmapped or \
           record.reference_id < 0:
            continue

        cstats = record.get_cigar_stats()[0]
        
        record_query_length = infer_query_length(cstats)
        record_query_start  = infer_query_5p_clip_length(record)
        record_query_end    = infer_query_3p_clip_length(record)
        record_query_end    = record_query_length - record_query_end
        record_reference_length = reference_lengths[record.reference_name]
        if record.is_reverse:
            record_query_start, record_query_end = \
                record_query_length - record_query_end, record_query_length - record_query_start

        matches = cstats[0] + cstats[7]
        alength = infer_align_length(cstats)
        
        auxtags = ''
        if record.has_tag('AS'):
            auxtags += "\tAS:i:%d" % record.get_tag('AS')
        if record.has_tag('NM'):
            auxtags += "\tNM:i:%d" % record.get_tag('NM')
            auxtags += "\tdv:f:%.3f" % (record.get_tag('NM') / float(alength))
            
        #TODO: trim hard clips from cigar
        auxtags += "\tcg:Z:%s" % trim_cigar(record)
        
        bed.write(
            "%s\t%d\t%d\t%d\t%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d%s\n" % (
                record.query_name,
                record_query_length,
                record_query_start,
                record_query_end,
                STRAND[int(record.is_reverse)],
                record.reference_name,
                record_reference_length,
                record.reference_start,
                record.reference_end,
                matches,
                alength,
                record.mapping_quality,
                auxtags
            )
        )       


def readBedFile(bedfile, rod):
    bed = open(bedfile, 'r')
    records = []
    previous_tid = -1
    previous_pos = -1
    for record in bed:
        record = record.rstrip().split('\t')
        
        if len(record) < 3:
            raise Exception("BED record has too few columns: "+str(record))
        
        record[1] = int(record[1])
        record[2] = int(record[2])
        
        current_tid = rod.get_tid(record[0])
        if current_tid < previous_tid:
            raise Exception("BED file not sorted")
        elif current_tid > previous_tid:
            previous_pos = -1
        elif record[2] < record[1]:
            raise Exception("BED record start > stop: " + str(record))
        elif record[1] < previous_pos:
            raise Exception("BED file not sorted")

        records.append(tuple(record))
        previous_tid = current_tid
        previous_pos = record[1]

    bed.close()

    return records



def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else "ERROR: %s\n\n\n" % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage: %s [options] <in.bam>\n" % (
        os.path.basename(sys.argv[0])))
    stream.write("\n")
    stream.write("Options:\n")
    stream.write("    -i <ufloat>  Min. sequence identity [0.0]\n")
    stream.write("    -h,--help    This help message\n")
    stream.write("\n")
    stream.write("Note:\n")
    stream.write("    Assumes the BAM file was produced by minimap2\n")
    stream.write("    with --eqx option\n")
    stream.write("\n")
    stream.write("\n%s" % message)
    sys.exit(exitcode)



def main(argv):
    try:
        options, arguments = getopt.getopt(argv, 'hi:', ('help',))
    except getopt.GetoptError as err:
        usage(err)

    min_idnt = 0.0
    symmetric = False
    for flag, value in options:
        if   flag in ('-h','--help'): usage(exitcode=0)
        elif flag == '-i': min_idnt = float(value)

    if num(arguments) < 1:
        usage("Too few arguments")
    elif num(arguments) > 1:
        usage("Too many arguments")

    bed = sys.stdout
    bam = pysam.AlignmentFile(arguments[0])

    bamtopaf(bam, bed)



main(sys.argv[1:])
