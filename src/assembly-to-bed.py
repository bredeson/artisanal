
import os
import sys
import getopt

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Convert JBAT assembly file to BED format'


class MalformedAssemblyFileError(BaseException):
    pass


def usage(message=None, exitcode=1, stream=sys.stderr):
    message = "" if message is None else "ERROR: %s\n\n\n" % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [-r] <in.assembly> <gap.len>\n\n\n%s" % (
        __program__, message))
    sys.exit(exitcode)

    
def main(argv):
    try:
        options, arguments = getopt.getopt(argv,'rQh', ('help',))
    except getopt.GetoptError as error:
        usage(error)

    on_query = False
    region_strings = False
    for flag, value in options:
        if flag in ('-h','--help'): usage(exitcode=0)
        if flag == '-r': region_strings = True
        if flag == '-Q': on_query = True
        
    if len(arguments) != 2:
        usage("Unexpected number of arguments")

    assembly_file = open(arguments[0], 'r')
    join_file = sys.stdout
    gap_len = int(arguments[1])
    
    prev_name = None
    prev_start = 0
    seen_head = False
    seen_body = False
    scaffold_index = 1    
    contig_name = dict()
    for line in assembly_file:
        if line.isspace() or \
           line.startswith('#'):
            continue
        
        cols = line.strip().split()
        
        if cols[0].startswith('>'):
            curr_name, curr_index, curr_length = cols
            curr_name = curr_name.lstrip('>')
            curr_index = int(curr_index)
            curr_length = int(curr_length)

            orig_name = curr_name.split(':::')

            if orig_name[0] != prev_name:
                prev_start = 0

            if on_query:
                contig_name[int(cols[1])] = (orig_name[0], curr_index, prev_start, prev_start+curr_length, curr_name)
            elif region_strings:
                contig_name[int(cols[1])] = (orig_name[0], curr_index, prev_start, prev_start+curr_length)
            else:
                contig_name[int(cols[1])] = (curr_name, curr_index, 0, curr_length)
                
            prev_name = orig_name[0]
            prev_start += curr_length

            seen_head = True

        elif not seen_head:
            raise MalformedAssemblyFileError("No cprops-style assembly file-header detected")
            
        else:
            scaffold_position = 0
            for index in cols:
                strand = '-' if index.startswith('-') else '+'
                index  = abs(int(index))

                length = contig_name[index][3] - contig_name[index][2]

                if on_query:
                    join_file.write("%s\t%d\t%d\t%s\t%d\t+\n" % (
                        contig_name[index][0],
                        contig_name[index][2],
                        contig_name[index][3],
                        contig_name[index][4],
                        contig_name[index][1]
                    ))
                        
                else:
                    if region_strings:
                        contig_id = '%s:%d-%d' % (
                            contig_name[index][0],
                            contig_name[index][2],
                            contig_name[index][3]
                        )
                    else:
                        contig_id = contig_name[index][0]

                    join_file.write("Scaffold%d\t%d\t%d\t%s\t%d\t%s\n" % (
                        scaffold_index,
                        scaffold_position,
                        scaffold_position + length,
                        contig_id,
                        contig_name[index][1],
                        strand
                    ))
                    scaffold_position += length + gap_len
                    
                seen_body = True
                
            scaffold_index += 1

    if not seen_body:
        raise MalformedAssemblyFileError("No asm-style assembly file-body detected")
            
    assembly_file.close()
    join_file.close()


if __name__ == '__main__':
    main(sys.argv[1:])

                

                
                
