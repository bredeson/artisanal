#!/usr/bin/env python3

import os
import sys
import math
import shutil
import getopt

from fileutil import zopen

__authors__ = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Calculate and print a pretty histogram'


COLOREND = '\u001b[0m'
COLORIZE = {
    'black'   : '\u001b[30m',
    'grey'    : '\u001b[30;1m',
    'gray'    : '\u001b[30;1m',
    'red'     : '\u001b[31;1m',
    'green'   : '\u001b[32;1m',
    'yellow'  : '\u001b[33;1m',
    'brown'   : '\u001b[33m',
    'blue'    : '\u001b[34;1m',
    'magenta' : '\u001b[35;1m',
    'purple'  : '\u001b[35m',
    'cyan'    : '\u001b[36;1m',
    'white'   : '\u001b[37;1m',
}


def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else 'ERROR: {}\n\n'.format(message)
    colors  = ['      '+x for x in sorted(COLORIZE.keys())]
    stream.write("""

Program: {program} ({purpose})
Version: {package} {version}
Contact: {contact}

Usage:   {program} [options] <input-file>

Options: -a,--force-ascii
         -b,--bin-width <ufloat>
         -C,--plot-color <enum>
         -c,--data-column <uint>
         -D,--plot-cdf
         -d,--delimiter <str>
         -L,--log-base <ufloat>
         -o,--outfile <file>
         -t,--table
         -W,--term-width <uint>
         -w,--weight-column <uint>
         -x,--min-x <ufloat>
         -X,--max-x <ufloat>
         -Y,--max-y <uint>
         -0,--plot-0-freq

Notes:
  - Possible color values are:
{colors}

{message}""".format(
    program=__program__,
    purpose=__purpose__,
    package=__pkgname__,
    version=__version__,
    contact=__contact__,
    colors='\n'.join(colors),
    message=message))
    sys.exit(exitcode)
    

def read_datafile(filename, value_column=1, weight_column=None, bin_width=1, log_base=-1, sep="\t"):
    datafile = zopen(filename, 'r')

    weight = 1
    num_lines = 0
    frequencies = {}
    for line in datafile:
        num_lines += 1
        
        line = line.strip()
        if line == '' or \
           line.startswith('#'):
            continue

        fields = line.split(sep)

        try:
            value = float(fields[value_column-1])
        except IndexError:
            sys.stderr.write("Invalid column: Too few columns in file: skipping %s, line %d\n" % (
                filename, num_lines))
            continue
        except ValueError:
            sys.stderr.write("Invalid type: Non-numeric value: skipping %s, line %d\n" % (
                filename, num_lines))
            continue
            
        if weight_column is None:
            weight = 1
        else:
            try:
                weight = float(fields[weight_column-1])
            except IndexError:
                raise Exception("Invalid column number, too few fields in file")
            except ValueError:
                raise Warning("Non-numeric type encountered; skipping %s, line %d" % (
                    filename, num_lines))

        if log_base > 0:
            value = math.log(value, log_base)
                              
        value_bin = int(value / bin_width)
        if value_bin not in frequencies:
            frequencies[value_bin]  = weight
        else:
            frequencies[value_bin] += weight

    return frequencies


        
def main(argv):
    short_options = "0ab:C:c:Dd:L:o:tW:w:x:X:Y:h:"
    long_options = [
        'help',
        'table',
        'force-ascii',
        'plot-color=',
        'plot-cdf',
        'min-x=',
        'max-x=',
        'max-y=',
        'log-base=',
        'outfile=',
        'delimiter=',
        'bin-width=',
        'term-width=',
        'data-column=',
        'weight-column=',
        'plot-0-freq'
    ]
    
    try:
        options, arguments = getopt.getopt(argv, short_options, long_options)
    except getopt.GetoptError as error:
        usage(error)


    min_x = None
    max_x = None
    max_y = None
    log_base = -1
    plot_cdf = False
    force_ascii = False
    color_beg = ''
    color_end = ''
    delimiter = "\t"
    bin_width = 1.0
    data_column = 1
    zero_values = False
    out_filename = None
    weight_column = None
    table_format = False
    try:
        term_width = shutil.get_terminal_size()[0]
    except:
        term_width = 80
        
    try:
        for flag, value in options:
            if   flag in ('-C','--plot-color'):
                color_beg = COLORIZE[value]
                color_end = COLOREND
            elif flag in ('-t','--table'):
                table_format = True
            elif flag in ('-c','--data-column'):
                data_column = int(value)
            elif flag in ('-D','--plot-cdf'):
                plot_cdf = True
            elif flag in ('-w','--weight-column'):
                weight_column = int(value)
            elif flag in ('-W','--term-width'):
                term_width = int(value)
            elif flag in ('-d','--delimiter'):
                delimiter = value
            elif flag in ('-a','--force-ascii'):
                force_ascii = True
            elif flag in ('-b','--bin-width'):
                bin_width = float(value)
            elif flag in ('-L','--log-base'):
                log_base = float(math.e if value == 'e' else value)
            elif flag in ('-o','--outfile'):
                out_filename = value
            elif flag in ('-x','--min-x'):
                min_x = float(value)
            elif flag in ('-X','--max-x'):
                max_x = float(value)
            elif flag in ('-Y','--max-y'):
                max_y = int(value)
            elif flag in ('-0','--plot-0-freq'):
                zero_values = True
                
    except KeyError:
        usage("Invalid enumerative color value: '%s'" % value)
    except TypeError:
        usage("Invalid numeric value: '%s'" % value)
        
    if len(arguments) == 0:
        usage()
    elif len(arguments) != 1:
        usage("Unexpected number of arguments")

    if term_width < 1:
        usage("Invalid terminal width, positive integer expected")
    if bin_width <= 0:
        usage("Invalid bin width, positive numeric value expected")

    if out_filename is None:
        out = sys.stdout
    else:
        out = open(out_filename, 'w')
        
            
    system_name = os.uname().sysname
    if force_ascii or not out.isatty():
        plot_char = ']'
        trunc_char = '>'
    elif system_name.startswith('Darwin'):
        plot_char = u'\u2588'
        trunc_char = '>'  #u'\u25B7'
    else:
        plot_char = u'\u2588'
        trunc_char = '>'  #u'\u25B7'

            
    #means = []
    #histograms = []
    #for filename in arguments:
    frequencies = read_datafile(
        arguments[0],
        data_column,
        weight_column,
        bin_width,
        log_base,
        delimiter
    )
    #histograms.append(histogram)
    #counts.append(sum(histogram.values()))


    bin_freq = 0.0
    bin_cumm = 0.0
    pdensity = 0.0
    cdensity = 0.0
    min_bin  = min(frequencies.keys())
    max_bin  = max(frequencies.keys())
    sum_freq = sum(frequencies.values())
    max_freq = max(frequencies.values())
    if plot_cdf:
        max_freq = sum_freq
    if min_x is not None:
        min_bin = float(min_x) / bin_width
    if max_x is not None:
        max_bin = float(max_x) / bin_width
    if max_y is not None:
        max_y = min(max_y, max_freq)
    else:
        max_y = max_freq
    
    if bin_width < 1.0:
        max_value_width = len("%.3f" % (max_bin * bin_width))
    else:
        max_value_width = len("%.1f" % (max_bin * bin_width))

    max_freq_width = len(str(int(max_freq)))
        
    header_string = "{1:>{0}}|{3:>{2}}|{4:>7}|{5:>7}|\n".format(
        max_value_width, 'x',
        max_freq_width, 'f',
        'PDF','CDF'
    )
    header_width = len(header_string)

    if not table_format:
        out.write(header_string)

    written_break = False
    for value in range(int(min_bin), int(max_bin) + 1):
        try:
            bin_freq  = int(frequencies[value])
            bin_cumm += bin_freq
        except KeyError:
            bin_freq = 0

        pdensity = float(bin_freq) / sum_freq
        cdensity = float(bin_cumm) / sum_freq

        plot_freq = bin_cumm if plot_cdf else bin_freq
        
        if bin_freq < 1 and not zero_values:
            if ((not table_format) and (not written_break)):
                out.write("{1:>{0}}...\n".format(header_width-1, '#'))
                written_break = True
            continue
        
        if bin_width < 1.0:
            value = "%.3f" % (bin_width * value)
        else:
            value = "%.1f" % (bin_width * value)
            
        hist_bar_width \
            = int((term_width - header_width) / max_y * min(max_y, plot_freq))

        if bin_freq > max_y:
            hist_bar = plot_char * (hist_bar_width - 1) + trunc_char
        else:
            hist_bar = plot_char * hist_bar_width

        if table_format:
            out.write(delimiter.join(
                map(str, (value, bin_freq, pdensity, cdensity))) + "\n"
            )
        else:
            out.write("{1:>{0}}|{3:>{2}}|{4:.5f}|{5:.5f}|{6:}\n".format(
                max_value_width,
                value,
                max_freq_width,
                bin_freq,
                pdensity,
                cdensity,
                color_beg + hist_bar + color_end
            ))
        written_break = False


if __name__ == '__main__':
    main(sys.argv[1:])
