
import os
import sys
import pysam
import getopt

__authors__  = 'Jessen V. Bredeson'
__program__ = os.path.basename(__file__)
__pkgname__ = '__PACKAGE_NAME__'
__version__ = '__PACKAGE_VERSION__'
__contact__ = '__PACKAGE_CONTACT__'
__purpose__ = 'Filter read pairs for proper pairing'

# PURPOSE: Read a BAM file of reads aligned to contigs and a BED file mapping
#  those contigs onto a scaffold or chromosome. Apply various user-specified
#  filtering criteria.

# BED file (bgzip'd and tabix-indexed):
#  ContigName  ContigStart  ContigEnd  ScaffoldName  IgnoredField  ScaffoldStrand


def reverse_read(contig, read_start, read_end):
    """
    Return the coordinates of the read after the contig is reverse complemented

           (upstream contigs)  R_s ----> R_e (read)
    --------------- ---------- ======================> (contig to rev comp)
    becomes:                   C_s                   C_e
    --------------- ---------- <====================== (contig rev comp'd)
                                             <----
    Equation is R_e = C_e - R_s + C_s and R_s = C_e - R_e + C_s
    """
    return ((contig.end - read_end + contig.start), (contig.end - read_start + contig.start))
    


def pair_distance(contig1, read1_start, contig2, read2_end):
    """
    This function estimates the hypothetical paired-end/mate-pair distances:

                      d1          d2    
               |-------------|-----------|
            R1 ---->.................<---- R2
    =========================+===================
                        C1_e ^ C2_s
    pair_distance = d1 + d2; where
    d1 = C1_e - R1_s
    d2 = R2_e - C2_s
    """    
    return ((contig1.end - read1_start) + (read2_end - contig2.start))



def is_FR_linking_pair(contig1, read1, contig2, read2, max_pair_distance):
    if read1.reference_start > read2.reference_start:
        read1, read2 = read2, read1
        contig1, contig2 = contig2, contig1

    read1_start = read1.reference_start
    read1_end = read1.reference_end
    read2_start = read2.reference_start
    read2_end = read2.reference_end
    if read1.is_reverse and read2.is_reverse:
        # RR, reverse read1
        #   <===================  ===============>
        #    ..<--- R1              ..<--- R2
        #    :......................:
        read1_start, read1_end = reverse_read(contig1, read1_start, read1_end)
        
    elif not read1.is_reverse and not read2.is_reverse:
        # FF, reverse read2
        #                     .....................
        #             R1 --->.:           R2 --->.:
        #   ===================>  ===============>        
        read2_start, read2_end = reverse_read(contig2, read2_start, read2_end)

    elif read1.is_reverse and not read2.is_reverse:
        # RF, reads could be in wrong order or both need to be rev comp'd
        revcomp_contig1, revcomp_contig2 = contig1, contig2
        revcomp_read1_start, revcomp_read1_end = reverse_read(contig1, read1_start, read1_end)
        revcomp_read2_start, revcomp_read2_end = reverse_read(contig2, read2_start, read2_end)

        swapped_contig1, swapped_contig2 = contig2, contig1
        swapped_read1_start, swapped_read1_end = read2_start, read2_end
        swapped_read2_start, swapped_read2_end = read1_start, read1_end
        
        if pair_distance(swapped_contig1, swapped_read1_start, swapped_contig2, swapped_read2_end) <= max_pair_distance:
            # The contigs are in correct orientations, but wrong relative order:
            # .......................................
            # :                             R1 --->.:
            # : ===================>  ===============>
            # :...<--- R2
            return True
        
        if pair_distance(revcomp_contig1, revcomp_read1_start, revcomp_contig2, revcomp_read2_end) <= max_pair_distance:
            # Both contigs are in correct relative order, but in wrong orientations:
            # .......................................
            # :                             R2 --->.:
            # : <===================  <===============
            # :.<--- R1
            return True

        return False
    
    else:
        # FR, do nothing. Correct order and orientation.
        #
        #           R1 --->......
        #   =================>  :  ==================>
        #                       :....<--- R2
        pass
        
    return pair_distance(contig1, read1_start, contig2, read2_end) <= max_pair_distance



def is_RF_linking_pair(contig1, read1, contig2, read2, max_pair_distance):
    if read1.reference_start > read2.reference_start:
        read1, read2 = read2, read1
        contig1, contig2 = contig2, contig1

    read1_start = read1.reference_start
    read1_end = read1.reference_end
    read2_start = read2.reference_start
    read2_end = read2.reference_end
    if read1.is_reverse and read2.is_reverse:
        # RR, reverse read1
        #   <===================  ===============>
        #    ..<--- R1              ..<--- R2
        #    :......................:
        read1_start, read1_end = reverse_read(contig1, read1_start, read1_end)
        
    elif not read1.is_reverse and not read2.is_reverse:
        # FF, reverse read2
        #                     .....................
        #             R1 --->.:           R2 --->.:
        #   ===================>  ===============>        
        read2_start, read2_end = reverse_read(contig2, read2_start, read2_end)

    elif not read1.is_reverse and read2.is_reverse:
        # FR, reads could be in wrong order or both need to be rev comp'd
        revcomp_contig1, revcomp_contig2 = contig1, contig2
        revcomp_read1_start, revcomp_read1_end = reverse_read(contig1, read1_start, read1_end)
        revcomp_read2_start, revcomp_read2_end = reverse_read(contig2, read2_start, read2_end)

        swapped_contig1, swapped_contig2 = contig2, contig1
        swapped_read1_start, swapped_read1_end = read2_start, read2_end
        swapped_read2_start, swapped_read2_end = read1_start, read1_end
        
        if pair_distance(swapped_contig1, swapped_read1_start, swapped_contig2, swapped_read2_end) <= max_pair_distance:
            # The contigs are in correct orienations, but wrong relative order:
            # .......................................
            # :                             R1 <---.:
            # : ===================>  ===============>
            # :...---> R2
            return True
        
        if pair_distance(revcomp_contig1, revcomp_read1_start, revcomp_contig2, revcomp_read2_end) <= max_pair_distance:
            # Both contigs are in correct relative order, but in wrong orientations:
            # .......................................
            # :                             R2 <---.:
            # : <===================  <===============
            # :.---> R1
            return True

        return False
    
    else:
        # RF, do nothing. Correct order and orientation.
        #
        #           R1 <---......
        #   =================>  :  ==================>
        #                       :....---> R2
        pass
        
    return pair_distance(contig1, read1_start, contig2, read2_end) <= max_pair_distance
    


def is_linking_pair(contig1, read1, contig2, read2, max_pair_distance, orientation='FR'):
    """
    Estimate the distance between paired-ends/mate-pairs if associatd contigs were
    directly abutted/joined end-to-end. If the distance between the outer-most
    coordinates of the reads exceeds the mean + Z * stddev, then discard the pairs.
    Doing this requires that we perform the test in the intended read orientation 

                      d1          d2    
               |-------------|-----------|
               ---->.................<---- 
    =========================+===================

    pair_distance = d1 + d2    
    """    

    if orientation == 'FR':
        return is_FR_linking_pair(contig1, read1, contig2, read2, max_pair_distance)
    else:
        return is_RF_linking_pair(contig1, read1, contig2, read2, max_pair_distance)


def calculate_repeat_fraction(repeat_bed, read):
    try:
        repeats = repeat_bed.fetch(read.reference_name, read.reference_start, read.reference_end)
    except StopIteration:
        return 0.0

    intersected_bases = 0
    for repeat in repeats:
        if repeat.start <= read.reference_start and read.reference_end <= repeat.end:
            # contained
            intersected_bases += read.reference_end - read.reference_start

        elif read.reference_start <= repeat.start and repeat.end <= read.reference_end:
            # contains
            intersected_bases += repeat.end - repeat.start
            
        elif repeat.start < read.reference_start < repeat.end:
            # 5' read overlap
            intersected_bases += repeat.end - read.reference_start

        elif repeat.start < read.reference_end < repeat.end:
            # 3' read overlap
            intersected_bases += read.reference_end - repeat.start

    return float(intersected_bases) / read.reference_length

    
def usage(message=None, exitcode=1, stream=sys.stderr):
    message = '' if message is None else '\nERROR: %s\n\n' % message
    stream.write("\n")
    stream.write("Program: %s (%s)\n" % (__program__, __purpose__))
    stream.write("Version: %s %s\n" % (__pkgname__, __version__))
    stream.write("Contact: %s\n" % __contact__)
    stream.write("\n")
    stream.write("Usage:   %s [options] <in.bam> <refmap.bed.gz> <out.bam>\n" % (
        __program__))
    stream.write("\n")
    stream.write("Options: -m <uint>  Library insert-size mean\n")
    stream.write("         -s <uint>  Library insert-size stddev\n")
    stream.write("         -p <str>   Library pair orientation expected; enum: {FR,RF} [FR]\n")
    stream.write("         -Z <uint>  Library max pair-distance Z-score to allow linking [6]\n")
    stream.write("         -L <file>  White-list of scaffold names to allow linking to\n")
    stream.write("         -X <file>  bgzip'd and tabix indexed BED file of repeat annotations\n")
    stream.write("                    to exclude intersecting repetitive reads\n")
    stream.write("         -x         Disallow linking among scaffolds listed by {-L}\n")
    stream.write("         -P         Contents of refmap.bed.gz represent contigs ordered into\n")
    stream.write("                    physical scaffolds; scaffold names in the Chrom (first)\n")
    stream.write("                    field and contig names in the Name (fourth) field\n")
    stream.write("         -V         Contents of refmap.bed.gz represent contigs grouped into\n")
    stream.write("                    virtual scaffolds; contig names in the Chrom (first)\n")
    stream.write("                    field and scaffold names in the Name (fourth) field\n")
    stream.write("         -R         Report intra-scaffold links only\n")
    stream.write("         -r         Report inter-scaffold links only\n")
    stream.write("         -c         Report inter-contig pairs only\n")
    stream.write("         -h,--help  This help documentation\n")
    stream.write("\n%s" % message)
    stream.write("\n")
    sys.exit(exitcode)
#                |---------|---------|---------|---------|---------|---------|---------|---------|
#                0         10        20        30        40        50        60        70        80 
    
    
def main(argv):
    try:
        options, arguments = getopt.getopt(argv,'m:s:Z:p:L:chrRPVxX:',['help'])
    except getopt.GetoptError as error:
        usage(error)

    insert_Z = 4
    insert_ori = 'FR'
    insert_sd = 50
    insert_mean = 500
    repeat_bed_file = None
    scaffolded_virtually = False
    scaffolded_physically = False
    reference_whitelist = set()
    reference_whitelist_file = None
    inter_contig_links_only = False
    inter_reference_links_only = False
    intra_reference_links_only = False
    reference_whitelist_crossmapping_disallowed = False
    for flag, value in options:
        try:
            if   flag in {'-h','--help'}: usage(exitcode=0)
            elif flag == '-V': scaffolded_virtually = True
            elif flag == '-P': scaffolded_physically = True
            elif flag == '-c': inter_contig_links_only = True
            elif flag == '-r': inter_reference_links_only = True
            elif flag == '-R': intra_reference_links_only = True
            elif flag == '-x': reference_whitelist_crossmapping_disallowed = True
            elif flag == '-L': reference_whitelist_file = value
            elif flag == '-X': repeat_bed_file = value
            elif flag == '-p': insert_ori = value.upper()
            elif flag == '-m': insert_mean = float(value)
            elif flag == '-s': insert_sd = float(value)
            elif flag == '-Z': insert_Z = float(value)
        except ValueError:
            usage("Non-numeric value to %r: %r" % (flag, value))
            
    if insert_ori not in {'FR','RF'}:
        usage("Invalid pair orientation: %s" % insert_ori)

    if scaffolded_physically and scaffolded_virtually:
        usage("Options '-P' and '-V' are mutually exclusive")

    if scaffolded_physically  or scaffolded_virtually:
        if inter_reference_links_only and intra_reference_links_only:
            usage("Options '-r' and '-R' are mutually exclusive with '-P' or '-V' enabled")

    elif inter_contig_links_only and intra_reference_links_only:
        usage("Options -c' and '-R' are mutually exclusive without '-P' or '-V' enabled")
        
    if len(arguments) != 3:
        usage("Unexpected number of arguments")
        
    if reference_whitelist_file is not None:
        for line in open(reference_whitelist_file):
            if line.isspace() or line.startswith('#'):
                continue
            reference_whitelist.add(line.strip().split()[0])
        reference_whitelist_file = True

    if repeat_bed_file is not None:
        repeat_bed_file = pysam.TabixFile(repeat_bed_file, parser=pysam.asBed())
        
    ibam = pysam.AlignmentFile(arguments[0])
    ibed = pysam.TabixFile(arguments[1], parser=pysam.asBed())
    obam = pysam.AlignmentFile(arguments[2], mode='wb', template=ibam)
    
    reads = {}
    insert_max = insert_mean + insert_Z * insert_sd
    for read in ibam:
        if not read.is_paired:
            continue
        if read.is_unmapped or \
           read.mate_is_unmapped or \
           read.reference_id < 0 or \
           read.next_reference_id < 0:
            continue
        if read.is_duplicate or \
           read.is_secondary or \
           read.is_supplementary:
            continue

        if read.query_name in reads:
            read1 = reads[read.query_name]
            read2 = read

            try:
                region1 = ibed.fetch(
                    read1.reference_name,
                    read1.reference_start,
                    read1.reference_end
                ).next()

                region2 = ibed.fetch(
                    read2.reference_name,
                    read2.reference_start,
                    read2.reference_end
                ).next()

            except StopIteration:
                continue

            if repeat_bed_file is not None:
                read1_repeat_fraction = calculate_repeat_fraction(repeat_bed_file, read1)
                read2_repeat_fraction = calculate_repeat_fraction(repeat_bed_file, read2)
                if read1_repeat_fraction > 0.5 or read2_repeat_fraction > 0.5:
                    continue
            
            # if the BED "name" field/attribute doesnt exist
            # attempting to access it raises a SegFault, but
            # attempting to set it raises only an IndexError.
            # There is no way to cleanly test if the field is
            # present without raising the SegFault or over-
            # writing the value. We just have to hope it's there

            read1_contig_name = read1.reference_name
            read2_contig_name = read2.reference_name
            read1_reference_name = read1.reference_name
            read2_reference_name = read2.reference_name
            if scaffolded_virtually:
                # The scaffold names are expected to be present in
                # the "name" field/attribute of the BED region
                read1_reference_name = region1.name
                read2_reference_name = region2.name
                read1_contig_name = read1.reference_name
                read2_contig_name = read2.reference_name                

            elif scaffolded_physically:
                read1_reference_name = read1.reference_name
                read2_reference_name = read2.reference_name
                read1_contig_name = region1.name
                read2_contig_name = region2.name
                
            if inter_reference_links_only and \
               read1_reference_name == read2_reference_name:
                continue
            
            if intra_reference_links_only and \
               read1_reference_name != read2_reference_name:
                # allows for bagging contigs by scaffold or if the
                # contigs have been scaffolded
                continue
                
            if reference_whitelist_file:
                if reference_whitelist_crossmapping_disallowed and \
                   read1_reference_name in reference_whitelist and \
                   read2_reference_name in reference_whitelist and \
                   read1_reference_name != read2_reference_name:
                    # don't allow linking between white-listed
                    # scaffolds/chromosomes
                    continue
                
                elif read1_reference_name in reference_whitelist or \
                     read2_reference_name in reference_whitelist:
                    pass

                else:
                    continue

            if read1_contig_name == read2_contig_name:
                if not inter_contig_links_only:
                    tmp_read1 = read1
                    tmp_read2 = read2
                    if read1.reference_start > read2.reference_end:
                        tmp_read1 = read2
                        tmp_read2 = read1
                    if tmp_read2.reference_end - tmp_read1.reference_end > 0 and \
                       tmp_read2.reference_end - tmp_read1.reference_start <= insert_max:
                        obam.write(read1)
                        obam.write(read2)

            elif is_linking_pair(region1, read1, region2, read2, insert_max, orientation=insert_ori):
                obam.write(read1)
                obam.write(read2)
                    
            del(reads[read.query_name])

        else:
            reads[read.query_name] = read
            
    obam.close()
    ibam.close()

            
if __name__ == '__main__':
    main(sys.argv[1:])
    
