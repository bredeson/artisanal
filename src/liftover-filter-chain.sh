

#TODO: do filtering in both directions Q>R, Q>R; very effective

PROGRAM=$(basename $0 .sh);
PACKAGE='__PACKAGE_NAME__';
VERSION='__PACKAGE_VERSION__';
CONTACT='__PACKAGE_CONTACT__';
PURPOSE='A pipeline for filtering liftover chain files';


function usage() {
    echo "
Program: $PROGRAM ($PURPOSE)
Version: $PACKAGE $VERSION
Contact: $CONTACT

Usage: $PROGRAM [options] <input.chain> <source.fasta> <target.fasta> <output-prefix>

Options: -b,--bridge <uint>  Bridge tiled chains spanning gaps lt <uint> bp
         -h,--help           Print this help message
         -v,--version        Print version info and exit

Notes: 
  1. The user must have UCSC Kent Tools accessible via 
     their PATH env variable.

  2. Input FASTA files MUST NOT be compressed.


" >>/dev/fd/2;
    exit 1
} 


function version() {
    printf "$PROGRAM $VERSION\n" >>/dev/fd/2;
    exit 0;
}


bridge=false;
submodule=false;
while [[ -n $@ ]]; do
    case "$1" in
        -b|--bridge) shift; bridge=$1;;
	--submodule) submodule=true;;
        -h|--help) usage;;
        -v|--version) version;;
        -*) usage;;
        *) break;;
    esac;
    shift;
done

if [ $# -ne 4 ]; then
    usage;
fi


set -e


missing_dependency=;
liftover_filter_chain=$(dirname $0)/liftover-filter-chain
for exe in faToTwoBit twoBitInfo chainSort chainPreNet chainNet \
	   netSyntenic netChainSubset chainStitchId chainBridge; do
    if [[ ! $(which $exe 2>/dev/null) ]]; then
        echo "[$PROGRAM] ERROR: $exe not detected in PATH" >>/dev/fd/2;
        missing_dependency=1;
    fi
done
if [[ $missing_dependency ]]; then
    exit 1;
fi


map_chain=$1;
src_fasta=$2;
trg_fasta=$3;
out_prefix=$4;

src_fasta_dir=$(dirname $src_fasta);
src_fasta_dir=$(cd $src_fasta_dir; pwd);
src_fasta=$src_fasta_dir/$(basename $src_fasta);
src_prefix=$(echo $src_fasta | sed 's/.f\(ast\|n\)\?a$//');

trg_fasta_dir=$(dirname $trg_fasta);
trg_fasta_dir=$(cd $trg_fasta_dir; pwd);
trg_fasta=$trg_fasta_dir/$(basename $trg_fasta);
trg_prefix=$(echo $trg_fasta | sed 's/.f\(ast\|n\)\?a$//');

map_chain_dir=$(dirname $map_chain);
map_chain_dir=$(cd $map_chain_dir; pwd);
map_chain=$map_chain_dir/$(basename $map_chain);


tmp_prefix_dir=$out_prefix.filter;
if [[ ! -d "$tmp_prefix_dir" ]]; then
    mkdir -p $tmp_prefix_dir;
fi
tmp_prefix_dir=$(cd $tmp_prefix_dir; pwd);
tmp_prefix=$tmp_prefix_dir/$(basename $out_prefix);

out_prefix_dir=$(dirname $out_prefix);
out_prefix_dir=$(cd $out_prefix_dir; pwd);
out_prefix=$out_prefix_dir/$(basename $out_prefix);

# submodule='true';
# if [[ -e "$out_prefix_dir/.pipeline" ]]; then
#     submodule='false';
# fi

if [[ ! -e "$tmp_prefix.map.chain" ]]; then
    ln -sf $map_chain $tmp_prefix.map.chain;
fi
map_chain=$tmp_prefix.map.chain


# link the src genome fasta into prefix dir and index
if [[ ! -e "$tmp_prefix.src.fasta" ]]; then
    ln -sf $src_fasta $tmp_prefix.src.fasta;
fi
src_prefix=$tmp_prefix.src
src_fasta=$src_prefix.fasta

faToTwoBit \
    $src_fasta \
    $src_prefix.2bit;

twoBitInfo \
    $src_prefix.2bit \
    $src_prefix.chr.sizes;

twoBitInfo \
    $src_prefix.2bit \
    $src_prefix.ctg.sizes \
    -noNs;


# link the trg genome fasta into prefix dir and index
if [[ ! -e "$tmp_prefix.trg.fasta" ]]; then
    ln -sf $trg_fasta $tmp_prefix.trg.fasta
fi
trg_prefix=$tmp_prefix.trg
trg_fasta=$trg_prefix.fasta

faToTwoBit \
    $trg_fasta \
    $trg_prefix.2bit;

twoBitInfo \
    $trg_prefix.2bit \
    $trg_prefix.chr.sizes;

twoBitInfo \
    $trg_prefix.2bit \
    $trg_prefix.ctg.sizes \
    -noNs;


chainSort \
    $map_chain \
    $tmp_prefix.score.chain;

chainPreNet \
    $tmp_prefix.score.chain \
    $src_prefix.chr.sizes \
    $trg_prefix.chr.sizes \
    $tmp_prefix.prenet.chain;

chainNet \
    -minSpace=1000 \
    -minFill=500 \
    -minScore=100 \
    $tmp_prefix.prenet.chain \
    $src_prefix.chr.sizes \
    $trg_prefix.chr.sizes \
    $src_prefix.raw.net \
    $trg_prefix.raw.net;

# remove "net" lines without any subsequent "fill" or "gap" lines
awk 'BEGIN{p="net";l=""} {if ($1 == "net") { if (p != "net") {print l}} else {print l} l=$_;p=$1} END{if (p != "net") {print l}}' \
    $src_prefix.raw.net >$src_prefix.fix.net
awk 'BEGIN{p="net";l=""} {if ($1 == "net") { if (p != "net") {print l}} else {print l} l=$_;p=$1} END{if (p != "net") {print l}}' \
    $trg_prefix.raw.net >$trg_prefix.fix.net

netSyntenic \
    $src_prefix.fix.net \
    $src_prefix.synt.net

# This is supposed to work, but the various scoring parameters
# would need tuning for each species pair
# netFilter \
#     -syn \
#     -minSynScore=5000 -minTopScore=10000 -minSynAli=1000 -minSynSize=10000 \
#     $src_prefix.synt.net \
#    >$src_prefix.synt.flt.net

netChainSubset \
    -skipMissing \
    $src_prefix.synt.net \
    $tmp_prefix.prenet.chain \
    $tmp_prefix.subset.chain;
#     -wholeChains  # include when netFilter is tuned correctly
    
chainStitchId \
    $tmp_prefix.subset.chain \
    $tmp_prefix.stitch.chain

if [[ $bridge == false ]]; then
    ln -sf $tmp_prefix.stitch.chain $out_prefix.filter.chain
else
    chainBridge \
	$tmp_prefix.stitch.chain \
	$src_prefix.2bit \
	$trg_prefix.2bit \
	$out_prefix.filter.chain \
	-linearGap=medium \
	-maxGap=$bridge
fi

if [[ $submodule == false ]]; then
    src_fasta_length=$(cut -f2 $src_prefix.ctg.sizes | paste -s -d '+' | bc);
    trg_fasta_length=$(cut -f2 $trg_prefix.ctg.sizes | paste -s -d '+' | bc);

    awk \
	-v S=$src_fasta_length \
	-v T=$trg_fasta_length \
'BEGIN{
  s=0;
  t=0;
} 
{
  if ($1 == "chain") { 
    s+=($7-$6); 
    t+=($12-$11)
  }
} 
END { 
  printf("\
Source genome total contig bases  = %d\n\
Source genome mapped contig bases = %d (%0.2f%%)\n\
Target genome total contig bases  = %d\n\
Target genome mapped contig bases = %d (%0.2f%%)\n",\
S, s, 100*s/(S ? S : 1), \
T, t, 100*t/(T ? T : 1))
}' $out_prefix.filter.chain
fi
