
PREFIX     := /usr/local

SRC_DIR    := src
BUILD_DIR  := build
BIN_DIR    := $(BUILD_DIR)/bin
LIB_DIR    := $(BUILD_DIR)/lib

ECHO       := echo
PYTHON     := $(filter /%,$(shell /bin/sh -c 'type python'))
INSTALL    := $(filter /%,$(shell /bin/sh -c 'type install'))
MKDIR      := $(filter /%,$(shell /bin/sh -c 'type mkdir'))
AWK        := $(filter /%,$(shell /bin/sh -c 'type awk'))
CAT        := $(filter /%,$(shell /bin/sh -c 'type cat'))
SED        := $(filter /%,$(shell /bin/sh -c 'type sed'))
GIT        := $(filter /%,$(shell /bin/sh -c 'type git'))
RM         := $(filter /%,$(shell /bin/sh -c 'type rm'))

PYTHON_VER := $(shell $(PYTHON) --version 2>&1 | awk '{if (/Python/) {split($$2,v,".");print "python"v[1]"."v[2]}}')
INSTALL_DIR = $(INSTALL) -m 755 -d
INSTALL_EXE = $(INSTALL) -m 755 -p
INSTALL_LIB = $(INSTALL) -m 644 -p
MKDIR_P     = $(MKDIR) -p
RM_R        = $(RM) -r

PACKAGE    := ARTISANAL
VERSION    := $(shell $(GIT) describe --long --tags --always)
CONTACT    := https:\/\/bitbucket.org\/bredeson\/artisanal\/issues
LICENSE    := LICENSE

BIN_TARGETS = \
	$(BIN_DIR)/assembly-patch-finder \
	$(BIN_DIR)/assembly-patch-patcher \
	$(BIN_DIR)/assembly-from-fasta \
	$(BIN_DIR)/assembly-from-layout \
	$(BIN_DIR)/assembly-overlap-stitcher \
	$(BIN_DIR)/assembly-sort-debris \
	$(BIN_DIR)/assembly-lcr-patcher \
	$(BIN_DIR)/assembly-to-bed \
	$(BIN_DIR)/assembly-to-debris \
	$(BIN_DIR)/assembly-to-fasta \
	$(BIN_DIR)/assembly-to-join \
	$(BIN_DIR)/assembly-to-layout \
	$(BIN_DIR)/bam-to-bed \
	$(BIN_DIR)/bam-to-delta \
	$(BIN_DIR)/bam-to-paf \
	$(BIN_DIR)/bam-to-psl \
	$(BIN_DIR)/bedpe-to-simple \
	$(BIN_DIR)/calculate-kaks \
	$(BIN_DIR)/call-compartments \
	$(BIN_DIR)/classify-taxrank \
	$(BIN_DIR)/cluster-collinear-bedpe \
	$(BIN_DIR)/coverage-collinear-bedpe \
	$(BIN_DIR)/completeness \
	$(BIN_DIR)/cprops-to-bed \
	$(BIN_DIR)/exonerate-to-sam \
	$(BIN_DIR)/expand-gaps \
	$(BIN_DIR)/extract-aligned-codons \
	$(BIN_DIR)/fasta-to-chain \
	$(BIN_DIR)/filter-hits \
	$(BIN_DIR)/gff3-to-prot \
	$(BIN_DIR)/hist \
	$(BIN_DIR)/liftover \
	$(BIN_DIR)/liftover-filter-chain \
	$(BIN_DIR)/make-coincidence-matrix \
	$(BIN_DIR)/minimap-to-chain \
	$(BIN_DIR)/mpGapLen \
	$(BIN_DIR)/plot-collinearity-index \
	$(BIN_DIR)/redund-contigs \
	$(BIN_DIR)/repair \
	$(BIN_DIR)/scaffold-collinear-bedpe \
	$(BIN_DIR)/scaffold-contig-map \
	$(BIN_DIR)/scaffold-read-filter

LIB_TARGETS = \
	$(LIB_DIR)/intervals.py \
	$(LIB_DIR)/fastxutil.py \
	$(LIB_DIR)/fileutil.py

.SUFFIXES: .py .sh .R

.PHONY: all install activate clean

all: $(BIN_DIR) $(BIN_TARGETS) $(LIB_DIR) $(LIB_TARGETS) activate

$(BIN_DIR): 
	$(MKDIR_P) $@

$(LIB_DIR): 
	$(MKDIR_P) $@

$(BIN_DIR)/%: $(SRC_DIR)/%.py
	-$(ECHO) '#!/usr/bin/env python' | $(CAT) - $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(BIN_DIR)/%: $(SRC_DIR)/%.sh
	-$(ECHO) '#!/usr/bin/env bash' | $(CAT) - $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(BIN_DIR)/%: $(SRC_DIR)/%.R
	-$(ECHO) '#!/usr/bin/env Rscript' | $(CAT) - $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

$(LIB_DIR)/%: $(SRC_DIR)/%
	-$(CAT) $(LICENSE) $< | \
		$(SED) "s/__PACKAGE_NAME__/$(PACKAGE)/;s/__PACKAGE_VERSION__/$(VERSION)/;s/__PACKAGE_CONTACT__/$(CONTACT)/" >$@

clean: $(BUILD_DIR)
	-$(RM_R) $(BUILD_DIR)

activate:
	@$(ECHO) 'export PYTHONPATH="$(PREFIX)/lib/$(PYTHON_VER)/site-packages:$$PYTHONPATH";' >activate
	@$(ECHO) 'export PATH="$(PREFIX)/bin:$$PATH";' >>activate
	@$(ECHO) '#setenv PYTHONPATH "$(PREFIX)/lib/$(PYTHON_VER)/site-packages:$$PYTHONPATH";' >>activate
	@$(ECHO) '#setenv PATH "$(PREFIX)/bin:$$PATH";' >>activate

install: all
	$(INSTALL_DIR) $(PREFIX)/bin
	$(INSTALL_DIR) $(PREFIX)/lib/$(PYTHON_VER)/site-packages
	$(INSTALL_EXE) $(BIN_DIR)/* $(PREFIX)/bin
	$(INSTALL_LIB) $(LIB_DIR)/* $(PREFIX)/lib/$(PYTHON_VER)/site-packages

