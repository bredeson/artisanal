# ARTISANAL
artisanal - *A*ssembly *R*eview *T*ools, *I*ncluding *S*equence *A*nalysis'*N* *A*nnotation *L*iftover

## DESCRIPTION

### Purpose
Provide utilities for curating HiC-scaffolded assemblies, such as those created by Dovetail
Genomics, Phase Genomics, Arima Genomics, etc. This is done by converting assemblies into
a form that can be plugged into [JuiceBox](https://github.com/aidenlab/Juicebox/wiki/Download)
Assembly Tools. Some utilities for producing liftOver `.chain` files for transferring annotations
between assembly versions are also provided.


## INSTALL

### Dependencies

#### Unix/Linux
 - UCSC Kent Tools (for `liftover`, `minimap-to-chain`, and `liftover-filter-chain`)
 - Exonerate (for `exonerate-to-sam` input)
 - minimap2 (for `minimap-to-chain` and `completeness`)

#### Python
The following dependencies are most easily installed using Anaconda Python.

 - bisect
 - bz2file
 - collections
 - getopt
 - gzip
 - hashlib
 - itertools
 - logging
 - lzma
 - math
 - multiprocessing
 - pysam
 - re
 - string
 - subprocess
 - sys
 - tempfile

#### R
R version 3.5 recommended.

### Installation
#### System-wide install 
```sh
    git clone https://bitbucket.org/bredeson/artisanal.git
    cd artisanal
    make
    make install
```

#### User-local install
The default (system-wide) install location is `/usr/local`, which may require administrator
privileges. If you do not have these privileges or you want to install the package into a user
directory, instead do:
```sh
    cd artisanal
    make install PREFIX=/path/to/your/install/dest
    source ./activate
```
For example, to install ARTISANAL into its own directory:
```sh
    cd artisanal
    make install PREFIX=$PWD
    source ./activate
```

## BUGS AND ISSUES
### Reporting
If you encounter a bug, have a feature request, or a suggestion for a new
tool, please submit a ticket at the
[Issues](https://bitbucket.org/bredeson/artisanal/issues) page.

### Known Issues
These scripts were written primarily in Python 2.7, and some may/may not be
compatible with Python 3 (but are expected to be soon).

## CITING THIS WORK
This work is unpublished at this time. Please cite this repo using the URL
(`https://bitbucket.org/bredeson/artisanal`) if you publish any work using
these tools.

## AUTHORS
```
Jessen V. Bredeson
Associate Specialist, Bioinformatics
Rokhsar Laboratory
Molecular and Cell Biology Department
University of California, Berkeley
```

## COPYRIGHT
```
Copyright (c)2020. The Regents of the University of California (Regents). All
Rights Reserved. Permission to use, copy, modify, and distribute this software
and its documentation for educational, research, and not-for-profit purposes,
without fee and without a signed licensing agreement, is hereby granted, provided
that the above copyright notice, this paragraph and the following two paragraphs
appear in all copies, modifications, and distributions. Contact The Office of
Technology Licensing, UC Berkeley, 2150 Shattuck Avenue, Suite 510, Berkeley,
CA 94720-1620, (510) 643-7201, for commercial licensing opportunities.

IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL,
INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF
THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE

REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS
PROVIDED "AS IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
```